### Getting Started
#### Install dotnet 6.0 (latest at the time when this is updated)
- `brew install --cask dotnet-sdk` or go to dotnet official site to install the sdk

### add new proj
- `dotnet new console/classlib -n <project-name>`
- `dotnet sln add <path-to-proj>`


### Build and Run
Main runnable console app is in `src/Fundamentals`, the `src/Fundamentals.Core` and `src/LeetCode` are the classlib that are referenced in main console app. 
- `dotnet build` to build the projects
- to run: `make run/watch`, or go to the src folder by `cd src/Fundamentals` and `dotnet watch/run`


### lint
run `make format` to format the `.cs` files
