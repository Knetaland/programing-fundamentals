format:
	./scripts/format.sh

watch:
	dotnet watch --project src/Fundamentals/Fundamentals.csproj run
run:
	dotnet run --project src/Fundamentals/Fundamentals.csproj
