/*
 * @lc app=leetcode id=148 lang=csharp
 *
 * [148] Sort List
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int val=0, ListNode next=null) {
 *         this.val = val;
 *         this.next = next;
 *     }
 * }
 */
public class Solution {
    public ListNode SortList(ListNode head) {
        if (head == null || head.next == null) return head;
        var mid = findMid(head);
        var right = SortList(mid.next);
        mid.next = null;
        var left = SortList(head);
        return merge(left, right);
    }

    ListNode findMid(ListNode node) {
        var slow = node;
        var fast = node;
        while(fast.next != null && fast.next.next !=null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }

    ListNode merge(ListNode left, ListNode right) {
        var head = new ListNode();
        var pointer = head;
        while(left != null && right != null) {
            if (left.val < right.val) {
                pointer.next = left;
                left = left.next;
            } else {
                pointer.next = right;
                right = right.next;
            }
            pointer = pointer.next;
        }
        pointer.next = left ?? right;
        return head.next;
    }
}
// @lc code=end

