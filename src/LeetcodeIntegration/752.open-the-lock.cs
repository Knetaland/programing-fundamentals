/*
 * @lc app=leetcode id=752 lang=csharp
 *
 * [752] Open the Lock
 */

// @lc code=start
public class Solution {
    public int OpenLock(string[] deadends, string target) {
        var queue = new Queue<string>();
        var deads = new HashSet<string>(deadends);
        var move = 0;
        var start = "0000";
        queue.Enqueue(start);
        var visited = new HashSet<string>();
        while(queue.Count > 0) {
            var count = queue.Count;
            for(var c =0 ; c<count; c++) {
                var combo = queue.Dequeue();
                // Console.WriteLine($"checking combo {combo}");
                if (combo == target) return move;
                if (deads.Contains(combo)) continue;
                for(var i  = 0; i < 4; i++) {
                    var next = dialUp(combo, i);
                    if (!visited.Contains(next)) {
                        queue.Enqueue(next);
                        // Console.WriteLine($"putting combo {next} into queue");
                        visited.Add(next);
                    }

                    var prev = dialDown(combo, i);
                    if (!visited.Contains(prev)) {
                        queue.Enqueue(prev);
                        // Console.WriteLine($"putting combo {prev} into queue");
                        visited.Add(prev);
                    }
                }
            }
            move++;
        }
        return -1;
    }

    private string dialUp(string s, int i) {
        var sb = new StringBuilder(s);
        if (sb[i] == '9') {
            sb[i] = '0';
        } else {
            sb[i]++;
        }
        return sb.ToString();
    }

    private string dialDown(string s, int i) {
        var sb = new StringBuilder(s);
        if (sb[i] == '0') {
            sb[i] = '9';
        } else {
            sb[i]--;
        }
        return sb.ToString();
    }
}
// @lc code=end

