/*
 * @lc app=leetcode id=518 lang=csharp
 *
 * [518] Coin Change 2
 */

// @lc code=start
public class Solution {
    public int Change(int amount, int[] coins) {
        // var res = new List<IList<int>>();
        var count = 0;
        dfs(0,0);
        // foreach(var r in res) {
        //     Console.WriteLine($"{string.Join(",", r)}");
        // }
        return count;
        void dfs(int cur , int index) {
            if (cur == amount) {
                count++;
                // res.Add(new List<int>(list));
                return;
            }
            if (cur > amount) return;
            for(var i = index; i <coins.Length; i++) {
                if (cur + coins[i] <= amount) {
                    // list.Add(coins[i]);
                    dfs(cur+coins[i], i);
                    // list.RemoveAt(list.Count -1);
                }
            }
        }
    }
}
// @lc code=end

