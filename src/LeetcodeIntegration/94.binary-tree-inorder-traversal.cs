/*
 * @lc app=leetcode id=94 lang=csharp
 *
 * [94] Binary Tree Inorder Traversal
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class Solution {
    public IList<int> InorderTraversal(TreeNode root) {
        return Dfs(root);
        // return UsingStack(root);
    }

    private IList<int> Dfs(TreeNode root) {
        var res = new List<int>();
        traverse(root);
        return res;

        void traverse(TreeNode node) {
            if (node == null) return ;
            traverse(node.left);
            res.Add(node.val);
            traverse(node.right);
        }
    }

    private IList<int> UsingStack(TreeNode root) {
        var res = new List<int>();
        if (root == null) return new List<int>();
        var stack = new Stack<TreeNode>();
        var node = root;
        while(stack.Count > 0 || node != null) {
            while(node != null) {
                stack.Push(node);
                node = node.left;
            }
            node = stack.Pop();
            res.Add(node.val);
            node = node.right;
        }
        return res;
    }


}
// @lc code=end

