/*
 * @lc app=leetcode id=341 lang=csharp
 *
 * [341] Flatten Nested List Iterator
 */

// @lc code=start
/**
 * // This is the interface that allows for creating nested lists.
 * // You should not implement it, or speculate about its implementation
 * interface NestedInteger {
 *
 *     // @return true if this NestedInteger holds a single integer, rather than a nested list.
 *     bool IsInteger();
 *
 *     // @return the single integer that this NestedInteger holds, if it holds a single integer
 *     // Return null if this NestedInteger holds a nested list
 *     int GetInteger();
 *
 *     // @return the nested list that this NestedInteger holds, if it holds a nested list
 *     // Return null if this NestedInteger holds a single integer
 *     IList<NestedInteger> GetList();
 * }
 */
public class NestedIterator {
    // private IList<int> _list = new List<int>();
    private LinkedList<NestedInteger> _linkedList;
    // private int _index = 0;
    public NestedIterator(IList<NestedInteger> nestedList) {
        //option 1:
        // foreach(var item in nestedList) {
        //     Add(item);
        // }

        // void Add(NestedInteger item) {
        //     if (item.IsInteger()) {
        //         _list.Add(item.GetInteger());
        //     } else  {
        //         foreach(var num in item.GetList()) {
        //             Add(num);
        //         }
        //     }
        // }
        _linkedList = new LinkedList<NestedInteger>(nestedList);
    }

    public bool HasNext() {
        while(_linkedList.Count > 0 && !_linkedList.First().IsInteger()) {
            var nested = _linkedList.First().GetList();
            _linkedList.RemoveFirst();
            // Print(_linkedList.First());
            for(var i = nested.Count - 1; i >=0; i--) {
                Console.WriteLine($"Adding num {Print(nested[i])} to linkedlist");
                _linkedList.AddFirst(nested[i]);
            }
        }
        return _linkedList.Count > 0;
    }

    public int Next() {
        var val = _linkedList.First().GetInteger();
        _linkedList.RemoveFirst();
        return val;
    }

    private string Print(NestedInteger number) {
        if (number.IsInteger()) {
            return number.GetInteger().ToString();
        } else {
            return string.Join(",", number.GetList());
        }
    }
}

/**
 * Your NestedIterator will be called like this:
 * NestedIterator i = new NestedIterator(nestedList);
 * while (i.HasNext()) v[f()] = i.Next();
 */
// @lc code=end

