/*
 * @lc app=leetcode id=230 lang=csharp
 *
 * [230] Kth Smallest Element in a BST
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class Solution {
    public int KthSmallest(TreeNode root, int k) {
        var res = 0;
        var index = 0;
        Traverse(root);
        return res;

        void Traverse(TreeNode node) {
            if (node == null) return ;

            Traverse(node.left);
            Console.WriteLine($"at node {node.val}");
            index++;
            if (index == k) {
                res = node.val;
                return ;
            }
            Traverse(node.right);
        }

    }
}
// @lc code=end

