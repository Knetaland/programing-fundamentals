/*
 * @lc app=leetcode id=216 lang=csharp
 *
 * [216] Combination Sum III
 */

// @lc code=start
public class Solution {
    public IList<IList<int>> CombinationSum3(int k, int n) {
        var res = new List<IList<int>>();
        var visited = new bool[n];
        bt(1, 0, new List<int>());
        return res;
        void bt(int start, int curSum, IList<int> track) {
            if (curSum == n) {
                if (track.Count == k) {
                    res.Add(new List<int>(track));
                }
                return ;
            }
            if (curSum > n) return ;

            for(var i = start; i <=9; i++) {
                if (track.Count < k) {
                    track.Add(i);
                    bt(i+1,curSum + i, track);
                    track.RemoveAt(track.Count - 1);
                }
            }
        }
    }
}
// @lc code=end

