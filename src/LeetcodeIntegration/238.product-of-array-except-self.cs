/*
 * @lc app=leetcode id=238 lang=csharp
 *
 * [238] Product of Array Except Self
 */

// @lc code=start
public class Solution {
    public int[] ProductExceptSelf(int[] nums) {
        var res = new int[nums.Length];
        for(var i = 0; i < res.Length; i++) {
            res[i] = 1;
        }
        var left = 1;
        for(var i = 0; i < nums.Length; i++) {
            if (i > 0) {
                left *= nums[i-1];
            }
            res[i] *= left;
        }
        var right = 1;
        for(var i = nums.Length -1; i >=0; i--) {
            if (i < nums.Length-1) {
                right *= nums[i+1];
            }
            res[i] *= right;
        }
        return res;
    }
}
// @lc code=end

