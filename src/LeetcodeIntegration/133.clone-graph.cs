/*
 * @lc app=leetcode id=133 lang=csharp
 *
 * [133] Clone Graph
 */

// @lc code=start
/*
// Definition for a Node.
public class Node {
    public int val;
    public IList<Node> neighbors;

    public Node() {
        val = 0;
        neighbors = new List<Node>();
    }

    public Node(int _val) {
        val = _val;
        neighbors = new List<Node>();
    }

    public Node(int _val, List<Node> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
}
*/
// queue: 3
// cur: 4
// 1: copy 1, 2: copcy 2, 4: copy4, c: copy3
// neighbors:  1, 3
// copy1.neighbors [2, 4], copy2.neighbors[1,3], copy4.nb[1,3]
public class Solution
{
    public Node CloneGraph(Node node)
    {
        if (node == null)
            return null;
        return CloneDfs(node);
    }
    // O(n)
    // O(n)
    public Node CloneBfs(Node node)
    {
        var map = new Dictionary<int, Node>();
        var queue = new Queue<Node>();
        var newRoot = new Node(node.val);
        map.Add(newRoot.val, newRoot);
        queue.Enqueue(node);
        while (queue.Count > 0)
        {
            var cur = queue.Dequeue();
            foreach (var neighbor in cur.neighbors)
            {
                if (!map.ContainsKey(neighbor.val))
                {
                    map.Add(neighbor.val, new Node(neighbor.val));
                    queue.Enqueue(neighbor);
                }
                map[cur.val].neighbors.Add(map[neighbor.val]);
            }
        }
        return newRoot;
    }

    public Node CloneDfs(Node node)
    {
        var map = new Dictionary<Node, Node>(); // store origin -> copy map
        var visited = new HashSet<Node>();
        dfs(node);
        foreach (var k in map.Keys)
        {
            foreach (var n in k.neighbors)
            { // key is original node
                map[k].neighbors.Add(map[n]);
            }
        }
        return map[node];

        void dfs(Node cur)
        {
            if (visited.Contains(cur))
                return;
            visited.Add(cur);
            map.Add(cur, new Node(cur.val));
            foreach (var n in cur.neighbors)
            {
                dfs(n);
            }
        }
    }
}
// @lc code=end

