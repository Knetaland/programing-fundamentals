/*
 * @lc app=leetcode id=23 lang=csharp
 *
 * [23] Merge k Sorted Lists
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int val=0, ListNode next=null) {
 *         this.val = val;
 *         this.next = next;
 *     }
 * }
 */
public class Solution {

    public ListNode MergeKLists(ListNode[] lists) {
        return DivideConquer(lists);
        // return MergeKLists_PQ(lists);
    }

    public ListNode DivideConquer(ListNode[] lists) {
        if (lists == null || lists.Length == 0) return null;
        
        var end = lists.Length-1;
        while(end > 0) {
            var start = 0;
            while(start < end) {
                lists[start] = Merge(lists[start], lists[end]);
                start++;
                end--;
            }
        }
        return lists[end];

        ListNode Merge(ListNode a, ListNode b) {
            var dummy = new ListNode();
            var pointer = dummy;
            while(a!= null && b!=null) {
                if (a.val < b.val) {
                    pointer.next = a;
                    a = a.next;
                } else {
                    pointer.next = b;
                    b = b.next;
                }
                pointer = pointer.next;
            }
            pointer.next = a ?? b;
            return dummy.next;
        }
    }
    public ListNode MergeKLists_PQ(ListNode[] lists) {
        var pq = new PriorityQueue<ListNode, int>();
        foreach(var node in lists) {
            if (node !=null) pq.Enqueue(node, node.val);
        }

        var head = new ListNode();
        var pointer = head;
        while(pq.Count > 0) {
            var node = pq.Dequeue();
            // Console.WriteLine($"node is {node.val}");
            pointer.next = node;
            if (node.next != null) {
                pq.Enqueue(node.next, node.next.val);
            }
            pointer = pointer.next;
        }
        // Console.WriteLine($"new head is {head.next.val}");
        return head.next;
     }
}
// @lc code=end

