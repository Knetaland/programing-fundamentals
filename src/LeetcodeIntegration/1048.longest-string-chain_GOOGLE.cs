/*
 * @lc app=leetcode id=1048 lang=csharp
 *
 * [1048] Longest String Chain
 */

// @lc code=start
public class Solution {
    // O(nlogn + n * s) s is the string length 
    // space O(n)
    public int LongestStrChain(string[] words) {
        Array.Sort(words, (a, b) => a.Length - b.Length);
        var map = new Dictionary<string, int>();
        var max = 0;
        foreach(var word in words) {
            if (!map.ContainsKey(word)) map.Add(word, 1);
            for(var i = 0; i < word.Length; i++) {
                var sb = new StringBuilder(word);
                var shrinkWord = sb.Remove(i, 1 ).ToString();
                if (map.ContainsKey(shrinkWord) && map[word] < map[shrinkWord] + 1) {
                    map[word] = map[shrinkWord] + 1;
                }
            }
            max = Math.Max(max, map[word]);
        }
        return max;
    }
}
// @lc code=end

