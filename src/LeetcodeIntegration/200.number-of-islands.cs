/*
 * @lc app=leetcode id=200 lang=csharp
 *
 * [200] Number of Islands
 */

// @lc code=start
public class Solution {
    const char LAND = '1';
    const char WATER = '0';
    public int NumIslands(char[][] grid) {
        var islands = 0;
        var m = grid.Length;
        var n = grid[0].Length;
        for(var i = 0; i < m; i++) {
            for(var j = 0; j < n; j++) {
                if (grid[i][j] == LAND) {
                    islands++;
                    Expand(i, j);
                }
            }
        }
        return islands;

        void Expand(int x, int y) {
            if (x <0 || y < 0 || x >= m || y >= n || grid[x][y] == WATER){
                return ;
            }
            grid[x][y] = WATER; // sink
            Expand(x-1, y);
            Expand(x+1, y);
            Expand(x, y-1);
            Expand(x, y+1);
        }
    }
}
// @lc code=end

