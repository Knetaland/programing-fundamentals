/*
 * @lc app=leetcode id=84 lang=csharp
 *
 * [84] Largest Rectangle in Histogram
 */

// @lc code=start
public class Solution {
    public int LargestRectangleArea(int[] heights) {
        return Find(heights, 0, heights.Length - 1);
    }

    public int Find(int[] heights, int left, int right) {
        if (left > right) return 0;
        var minIndex = left;
        for(var i = left; i <= right; i++) {
            if (heights[i] < heights[minIndex] ) {
                minIndex = i;
            }
        }

        var curMin = heights[minIndex] * (right-left +1);
        var leftSide = Find(heights, left, minIndex -1);
        var rightSide = Find(heights, minIndex + 1, right);
        return Math.Max(curMin, Math.Max(leftSide, rightSide));
    }
}
// @lc code=end

