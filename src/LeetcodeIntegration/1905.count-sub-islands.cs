/*
 * @lc app=leetcode id=1905 lang=csharp
 *
 * [1905] Count Sub Islands
 */

// @lc code=start
public class Solution {
    const int LAND = 1;
    const int WATER = 0;
    public int CountSubIslands(int[][] grid1, int[][] grid2) {
        var islands = 0;
        var m = grid1.Length;
        var n = grid1[0].Length;
        for(var i = 0; i < m; i++) {
            for(var j = 0; j < n; j++) {
                if (grid2[i][j] == LAND && grid1[i][j] == WATER) {
                    Expand(grid2, i, j); // sink
                }
            }
        }
        

        for(var i = 0; i < m; i++) {
            for(var j = 0; j < n; j++) {
                if (grid2[i][j] == LAND) {
                    islands++;
                    Expand(grid2, i, j); // sink
                }
            }
        }
        return islands;

        void Expand(int[][] grid, int x, int y) {
            if (x <0 || y < 0 || x >= m || y >= n || grid[x][y] == WATER){
                return ;
            }
            grid[x][y] = WATER; // sink
            Expand(grid, x-1, y);
            Expand(grid, x+1, y);
            Expand(grid, x, y-1);
            Expand(grid, x, y+1);
        }
    }
}
// @lc code=end

