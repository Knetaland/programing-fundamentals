/*
 * @lc app=leetcode id=1020 lang=csharp
 *
 * [1020] Number of Enclaves
 */

// @lc code=start
public class Solution {
    const int LAND = 1;
    const int WATER = 0;
    public int NumEnclaves(int[][] grid) {
        var walks = 0;
        var m = grid.Length;
        var n = grid[0].Length;
        // sink all edges
        for(var i = 0; i < m; i++) {
            Expand(i, 0);
            Expand(i, n-1);
        }

        for(var i = 0; i < n; i++) {
            Expand(0, i);
            Expand(m-1, i);
        }

        for(var i = 0; i < m; i++) {
            for(var j = 0; j < n; j++) {
                if (grid[i][j] == LAND) {
                    walks++;
                    // Expand(i, j);
                }
            }
        }
        return walks;

        void Expand(int x, int y) {
            if (x <0 || y < 0 || x >= m || y >= n || grid[x][y] == WATER){
                return ;
            }
            grid[x][y] = WATER; // sink
            Expand(x-1, y);
            Expand(x+1, y);
            Expand(x, y-1);
            Expand(x, y+1);
        }
    }
}
// @lc code=end

