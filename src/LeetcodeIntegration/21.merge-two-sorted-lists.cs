/*
 * @lc app=leetcode id=21 lang=csharp
 *
 * [21] Merge Two Sorted Lists
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int val=0, ListNode next=null) {
 *         this.val = val;
 *         this.next = next;
 *     }
 * }
 */
public class Solution {
    public ListNode MergeTwoLists(ListNode list1, ListNode list2) {
        var head = new ListNode();
        var index = head;
        while(list1 != null && list2 != null) {
            if (list1.val < list2.val) {
                index.next = list1;
                list1 = list1.next;
            } else  {
                index.next = list2;
                list2 = list2.next;
            }
            index = index.next;
        }
        index.next = list1 ?? list2;
        return head.next;
    }

    public ListNode MergeTwoLists_recusive(ListNode list1, ListNode list2) {
        if (list1 == null) return list2;
        if (list2 == null) return list1;
        if (list1.val < list2.val) {
            list1.next = MergeTwoLists_recusive(list1.next, list2);
            return list1;
        } else {
            list2.next = MergeTwoLists_recusive(list1, list2.next);
            return list2;
        }
    }
}
// @lc code=end

