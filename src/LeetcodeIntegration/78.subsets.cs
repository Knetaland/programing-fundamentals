/*
 * @lc app=leetcode id=78 lang=csharp
 *
 * [78] Subsets
 */

// @lc code=start
public class Solution {
    public IList<IList<int>> Subsets(int[] nums) {
        var res = new List<IList<int>>();
        backtrack(new List<int>(), 0);
        return res;
        void backtrack(IList<int> list, int start) {
            // if (list.Count == 3) { // if only need 3 items
                res.Add(new List<int>(list));
            // }
            for(var i = start; i < nums.Length; i++) {
                list.Add(nums[i]);
                backtrack(list, i + 1);
                list.RemoveAt(list.Count - 1);
            }
        }
    }
}
// @lc code=end

