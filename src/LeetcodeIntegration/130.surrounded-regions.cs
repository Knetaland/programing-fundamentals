/*
 * @lc app=leetcode id=130 lang=csharp
 *
 * [130] Surrounded Regions
 */

// @lc code=start
public class Solution {
    public void Solve(char[][] board) {
        if (board == null || board.Length == 0) return ;
        var row = board.Length;
        var col = board[0].Length;

        for(var r = 0; r < row; r++) {
            for(var c = 0; c < col; c++) {
                if ((r > 0 && r < row -1) && (c > 0 && c < col-1)) {
                    continue;
                    // Console.WriteLine($"middle found, skipping [{r},{c}]");
                    // c = col -1;
                    // Console.WriteLine($"skip to [{r},{c}]");
                }
                else {
                    if (board[r][c] == 'O') {
                        Console.WriteLine($"O found at [{r},{c}]");
                        BFS(r, c);
                    }
                }
            }
        }

        for (var r = 0; r < row; r++) {
            for (var c =0; c < col; c++) {
                if (board[r][c] == 'O') board[r][c] = 'X';
                else if (board[r][c] == '!') board[r][c] = 'O';
            }
        }

        void BFS(int r, int c) {
            var queue = new Queue<Tuple<int,int>>();
            
            queue.Enqueue(new Tuple<int,int>(r, c));
            while (queue.Count > 0) {
                var t =  queue.Dequeue();
                r = t.Item1;
                c = t.Item2;
                if (r >=0 && r <row  && c >=0 && c < col && board[r][c] == 'O') {
                    board[r][c] = '!';
                    // Console.WriteLine($"changing [{t.Item1},{t.Item2}] to !");
                    queue.Enqueue(new Tuple<int,int>(r-1,c));
                    queue.Enqueue(new Tuple<int,int>(r+1,c));
                    queue.Enqueue(new Tuple<int,int>(r,c-1));
                    queue.Enqueue(new Tuple<int,int>(r,c+1));
                }
                
                
                // if (r-1 >=0 && board[r-1][c] == 'O') {
                //     // board[r-1][c] = '!';
                //     Console.WriteLine($"spreading to [{r-1},{c}]");
                    
                // }
                // if (r+1 < row && board[r+1][c] == 'O') {
                //     //  board[r+1][c]  = '!';
                //     Console.WriteLine($"spreading to [{r+1},{c}]");
                    
                // }
                // if (c-1 >=0 && board[r][c-1] == 'O') {
                //     // board[r][c-1] = '!';
                //     Console.WriteLine($"spreading to [{r},{c-1}]");
                    
                // }
                // if (c+1 < col && board[r][c+1] == 'O') {
                //     // board[r][c+1] = '!';
                //     Console.WriteLine($"spreading to [{r},{c+1}]");
                    
                // }
            }
        }
    }
}
// @lc code=end

