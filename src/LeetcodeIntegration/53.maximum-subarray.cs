/*
 * @lc app=leetcode id=53 lang=csharp
 *
 * [53] Maximum Subarray
 */

// @lc code=start

// -2, 1, 
public class Solution {
    public int MaxSubArray(int[] nums) {
        // var dp = new int[nums.Length];
        // dp[0] = nums[0];
        // for(var i =1; i < nums.Length; i++) {
        //     dp[i] = Math.Max(dp[i-1] + nums[i], Math.Max(nums[i], dp[i-1]));
        //     Console.WriteLine($"{string.Join(",",dp)}");
        // }
        // return dp[nums.Length-1];
        var max = nums[0]; // first item is the max if only one item.
        var curSum = nums[0];
        for(var i = 1; i < nums.Length; i++) {
            curSum = Math.Max(curSum + nums[i], nums[i]);
            max = Math.Max(curSum, max);
        }
        return max;
    }
}
// @lc code=end

