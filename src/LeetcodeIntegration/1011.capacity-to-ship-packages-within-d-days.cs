/*
 * @lc app=leetcode id=1011 lang=csharp
 *
 * [1011] Capacity To Ship Packages Within D Days
 */

// @lc code=start
public class Solution {
    public int ShipWithinDays(int[] weights, int days) {
        var left = weights.Max();
        var right = weights.Sum();
        // Console.WriteLine($"left:{left}, right:{right}");

        while(left  < right) {
            var mid = left + (right-left)/2;
            if (getDays(weights, mid) <= days) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }
        return left;
    }

    private int getDays(int[] weights, int cap) {
        var days = 1;
        var remainingCap = cap;
        for(var i = 0; i < weights.Length; i++) {  
            if (remainingCap < weights[i]) {
                // Console.WriteLine($"need new day, remainingCap {remainingCap} not enough for weight {weights[i]}");
                days++;
                remainingCap = cap - weights[i]; // reset
            } else {
                remainingCap -= weights[i];
                // Console.WriteLine($"remaining capacity {remainingCap} for try cap:{cap}");
            }
        }
        // Console.WriteLine($"cap {cap} uses min {days} days");
        return days;
    }
}
// @lc code=end

// [1,2,3,4,5,6,7,8,9,10]