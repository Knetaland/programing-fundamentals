/*
 * @lc app=leetcode id=169 lang=csharp
 *
 * [169] Majority Element
 */

// @lc code=start
public class Solution {
    public int MajorityElement(int[] nums) {
        var count = 0;
        var candidate = 0;
        foreach(var num in nums) {
            if (count == 0) {
                // Console.WriteLine($"count reach 0, candidate is now {num}");
                candidate = num;
            }
            
            count += candidate == num ? 1 : -1;
            // Console.WriteLine($"candidate {candidate} same as num {num} ? {candidate == num}, count is now {count}");
        }
        return candidate;
    }
}
// @lc code=end

