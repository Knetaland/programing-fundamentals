/*
 * @lc app=leetcode id=76 lang=csharp
 *
 * [76] Minimum Window Substring
 */

// @lc code=start
public class Solution {
    public string MinWindow(string s, string t) {
        var map = new Dictionary<char, int>();
        int minLength = int.MaxValue, left = 0, charsNeeded = 0, minLeft =0;
        foreach(var c in t) {
            if (!map.ContainsKey(c)) map.Add(c, 0);
            map[c]++;
            charsNeeded++; // chars needed to use all string t
        }
        for(var i =0; i < s.Length; i++) {
            // Console.WriteLine($"processing {s[i]}");
            if (map.ContainsKey(s[i])) {
                if (map[s[i]] > 0) charsNeeded--;
                map[s[i]]--;
            }

            while(charsNeeded == 0) { // all chars are used
                // Console.WriteLine($"At index {i} all chars are used");
                var len = i - left + 1;
                if (len < minLength) {
                    minLength = len;
                    minLeft = left;
                }
                if (map.ContainsKey(s[left])) {
                    map[s[left]]++;
                    if (map[s[left]] > 0) charsNeeded++;
                }
                left++;
            }
        }

        return minLength == int.MaxValue ? "" : s.Substring(minLeft, minLength);
    }
}
// @lc code=end

