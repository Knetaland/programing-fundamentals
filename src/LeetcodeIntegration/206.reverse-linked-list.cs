/*
 * @lc app=leetcode id=206 lang=csharp
 *
 * [206] Reverse Linked List
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int val=0, ListNode next=null) {
 *         this.val = val;
 *         this.next = next;
 *     }
 * }
 */
public class Solution {
    public ListNode ReverseList(ListNode head) {
        // return Reverse_Interative(head);
        return Reverse_Recursive(head);
    }

    public ListNode Reverse_Interative(ListNode head) {
        ListNode pre = null;
        while(head != null) {
            var next = head.next;
            head.next = pre;
            pre = head;
            head = next;
        }

        return pre;
    }

    public ListNode Reverse_Recursive(ListNode head) {
        if (head == null || head.next == null) return head;
        var last = Reverse_Recursive(head.next);
        head.next.next = head;
        head.next = null;
        return last;
    }

}
// @lc code=end

