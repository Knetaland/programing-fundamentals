/*
 * @lc app=leetcode id=297 lang=csharp
 *
 * [297] Serialize and Deserialize Binary Tree
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int x) { val = x; }
 * }
 */
public class Codec {

    // Encodes a tree to a single string.
    public string serialize(TreeNode root) {
        var stringBuilder = new StringBuilder();
        serialize(root, stringBuilder);
        return stringBuilder.ToString();

        void serialize(TreeNode node, StringBuilder sb) {
            if (node == null) {
                sb.Append("#,");
                return ;
            }
            sb.Append($"{node.val},");
            serialize(node.left, sb);
            serialize(node.right, sb);
        } 
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(string data) {
        var q = new Queue<string>(data.Split(','));

        var root = deserialize(q);
        return root;
        TreeNode deserialize(Queue<string> queue) {
            if (queue.Count == 0) return null;

            var val = queue.Dequeue();
            if (val == "#") return null;
            var node = new TreeNode(int.Parse(val));
            node.left = deserialize(queue);
            node.right = deserialize(queue);
            return node;
        }

    }
}

// Your Codec object will be instantiated and called as such:
// Codec ser = new Codec();
// Codec deser = new Codec();
// TreeNode ans = deser.deserialize(ser.serialize(root));
// @lc code=end

