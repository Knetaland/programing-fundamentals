/*
 * @lc app=leetcode id=1249 lang=csharp
 *
 * [1249] Minimum Remove to Make Valid Parentheses
 */

// @lc code=start
public class Solution {
    public string MinRemoveToMakeValid(string s) {
        var stack = new Stack<int>();
        var toRemove = new bool[s.Length];
        for(var i = 0;  i< s.Length; i++) {
            var c = s[i];
            if(c == '(') {
                stack.Push(i);
            } else if (c == ')') {
                if (stack.Count == 0) {
                    toRemove[i] = true;
                } else  {
                    stack.Pop();
                }
            }
        }
        while(stack.Count > 0) {
            toRemove[stack.Pop()] = true;
        }
        var res = new StringBuilder();
        for(var i = 0; i < toRemove.Length; i++) {
            if (!toRemove[i]) {
                res.Append(s[i]);
            }
        }

        return res.ToString();
    }
}
// @lc code=end

