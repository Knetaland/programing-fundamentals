/*
 * @lc app=leetcode id=3 lang=csharp
 *
 * [3] Longest Substring Without Repeating Characters
 */

// @lc code=start
public class Solution {
    public int LengthOfLongestSubstring(string s) {
        var set = new HashSet<char>();
        var left = 0;
        var max = 0;
        for(var i = 0; i < s.Length; i++) {
            while (set.Contains(s[i])) { // dup found
                set.Remove(s[left]);
                left++;
            }
            set.Add(s[i]);
            max = Math.Max(max, i -left +1);
            // while (!set.Add(s[i])) {
            //     set.Remove(s[left]);
            //     left++;
            // }
            // max = Math.Max(max, i - left + 1);
        }
        return max;
    }
    public int LengthOfLongestSubstring_2(string s) {
        var map = new Dictionary<char, int>();
        var max = 0;
        var newNonDupStart = 0;
        var count = 0;
        for(var i = 0; i < s.Length; i++) {
            if (map.ContainsKey(s[i])) {
                newNonDupStart = Math.Max(newNonDupStart, map[s[i]]+1);
                count = i - newNonDupStart + 1; 
            } else {
                count++;
            }
            map[s[i]] = i;
            max = Math.Max(max, count);
        }
        return max;
    }
}
// @lc code=end

