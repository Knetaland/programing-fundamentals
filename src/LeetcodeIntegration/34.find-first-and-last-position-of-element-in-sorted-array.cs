/*
 * @lc app=leetcode id=34 lang=csharp
 *
 * [34] Find First and Last Position of Element in Sorted Array
 */

// @lc code=start
public class Solution {
    public int[] SearchRange(int[] nums, int target) {
        return UseLowerBound(nums, target);
    }

    private int[] SearchBothEnds(int[] nums, int target) {
        var left = 0;
        var right = nums.Length;
        var leftBound = SearchLeft(nums, left, right, target);
        var rightBound = SearchRight(nums, left, right, target);
        return new[] {leftBound, rightBound};
    }

    private int[] UseLowerBound(int[] nums, int target) {
        var left = 0;
        var right = nums.Length;
        var leftBound = SearchLeft(nums, left, right, target);
        if (leftBound == -1) return new [] {-1, -1};
        return new [] { leftBound, SearchLeft(nums, left, right, target+1, true) - 1 };
    }

    private int SearchRight(int[] nums, int left, int right, int target) {
        while(left < right) {
            var mid = left + (right - left)/2;
            if (nums[mid] == target) {
                left = mid+1;
            } else if (target > nums[mid]) {
                left = mid + 1;
            } else if (target < nums[mid]) {
                right = mid;
            }
        }
        if (left == 0) return -1;
        return nums[left - 1] == target ? left -1 : -1;
    }

    private int SearchLeft(int[] nums, int left, int right, int target, bool findLowerBound = false) {
        while(left < right) {
            var mid = left + (right - left)/2;
            if (nums[mid] == target) {
                right = mid;
            } else if (target > nums[mid]) {
                left = mid + 1;
            } else if (target < nums[mid]) {
                right = mid;
            }
        }
        if (findLowerBound) return left;
        if (left == nums.Length) return -1;
        return nums[left] == target ? left : -1;
    }
}
// @lc code=end

