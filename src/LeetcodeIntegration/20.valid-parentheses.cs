/*
 * @lc app=leetcode id=20 lang=csharp
 *
 * [20] Valid Parentheses
 */

// @lc code=start
public class Solution {
    public bool IsValid(string s) {
        var stack = new Stack<char>();
        var map = new Dictionary<char,char> {
            ['}'] = '{',
            [']'] = '[',
            [')'] = '(',
        };
        foreach(var c in s) {
            if (c == '(' || c == '{' || c == '[') {
                stack.Push(c);
            } else if (c == '}' || c == ')' || c == ']') {
                if (stack.Count == 0 || stack.Peek() != map[c]) {
                    return false;
                } else {
                    stack.Pop();
                }
            }
        }
        return stack.Count == 0;
    }
}
// @lc code=end

