/*
 * @lc app=leetcode id=567 lang=csharp
 *
 * [567] Permutation in String
 */

// @lc code=start
public class Solution {
    public bool CheckInclusion(string s1, string s2) {
        if (s2.Length < s1.Length) return false;
        var map = new Dictionary<char,int>();
        var charNeeded = 0;
        foreach(var c in s1.ToCharArray()) {
            if(!map.ContainsKey(c)) {
                map.Add(c, 0);
            }
            map[c]++;
            charNeeded++;
        }
        var left = 0;
        for(var i = 0; i < s2.Length; i++) {
            var c = s2[i];

            if (map.ContainsKey(c)) {
                if (map[c]>0) {
                    charNeeded--;    
                }
                map[c]--;
            }

            while (charNeeded == 0) {
                if (i - left +1 == s1.Length) return true;
                var leftChar = s2[left];
                if (map.ContainsKey(leftChar)) {
                    map[leftChar]++;
                    if (map[leftChar] > 0) charNeeded++;
                }
                left++;
            }
        }
        return false;
    }
}
// @lc code=end

