/*
 * @lc app=leetcode id=509 lang=csharp
 *
 * [509] Fibonacci Number
 */

// @lc code=start
public class Solution {
    public int Fib(int n) {
        return BottomUpDp(n);
    }

    private int TopDownDp(int n) {
        var dp = new int[n+1];
        var res = calc(n);
        return res;

        int calc(int num) {
            if (num == 0 || num == 1) return num;
            if(dp[num] !=0) return dp[num];
            return calc(num-1) + calc(num-2);
        }
    }

    private int BottomUpDp(int n) {
        if (n == 0) return 0;
        var dp = new int[n+1];
        dp[1] = 1;
        for(var i = 2; i <=n; i++) {
            dp[i] = dp[i-1] + dp[i-2];
        }
        return dp[n];
    }
}
// @lc code=end

