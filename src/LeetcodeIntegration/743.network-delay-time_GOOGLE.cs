/*
 * @lc app=leetcode id=743 lang=csharp
 *
 * [743] Network Delay Time
 */

// @lc code=start
// graph + dijkstra
public class Solution {
    public int NetworkDelayTime(int[][] times, int n, int k) {
        var dis = new int[n+1];
        for(var i = 1; i < n+1; i++) {
            dis[i] = int.MaxValue;
        }
        // Console.WriteLine($"dis is {dis.Length}");
        dis[k] = 0; // dis from start to start is 0
        var graph = new Dictionary<int, Dictionary<int, int>>();
        var queue = new PriorityQueue<int, int>();
        
        foreach(var edge in times) {
            var start = edge[0];
            var end = edge[1];
            var weight = edge[2];
            if (!graph.ContainsKey(start)) {
                graph.Add(start, new Dictionary<int,int>());
            }
            graph[start].Add(end, weight);
        }
        // foreach(var item in graph) {
        //     foreach(var tem in item.Value) {
        //         Console.WriteLine($"{item.Key} -> {tem.Key}: {tem.Value}");
        //     }
        // }
        

        queue.Enqueue(k, 0);
        while(queue.Count > 0) {
            queue.TryDequeue(out var node, out var distFromStart);
            // Console.WriteLine($"dequeued node:{node}:{distFromStart}");
            if (dis[node] < distFromStart) continue;
            if (graph.ContainsKey(node)) {
                foreach(var neighbor in graph[node]) {
                    var delay = distFromStart +  neighbor.Value;
                    if (delay < dis[neighbor.Key]) {
                        dis[neighbor.Key] = delay;
                        queue.Enqueue(neighbor.Key, delay);
                    }
                }
            }
        }
        if (dis.Any(n => n == int.MaxValue)) return -1;
        return dis.Max();
    }
}
// @lc code=end

