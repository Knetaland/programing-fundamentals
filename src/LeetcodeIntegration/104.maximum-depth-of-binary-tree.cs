/*
 * @lc app=leetcode id=104 lang=csharp
 *
 * [104] Maximum Depth of Binary Tree
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class Solution {
    public int MaxDepth(TreeNode root) {
        if (root == null) return 0;
        
        var left = MaxDepth(root.left);
        var right = MaxDepth(root.right);
        return Math.Max(left, right) + 1;
    }

    private int solution_1(TreeNode root) {
        int max = 0;
        int depth = 0;

        Traverse(root);

        return max;

        void Traverse(TreeNode node) {
            if (node == null) return ;

            depth++;
            max = Math.Max(depth, max);
            Traverse(node.left);
            Traverse(node.right);
            depth--;
        }
    }
}
// @lc code=end

