/*
 * @lc app=leetcode id=209 lang=csharp
 *
 * [209] Minimum Size Subarray Sum
 */

// @lc code=start
public class Solution {
    public int MinSubArrayLen(int target, int[] nums) {
        var min = int.MaxValue;
        var sum = 0;
        var left = 0;
        for(var i = 0; i < nums.Length; i++) {
            sum += nums[i];
            while(sum >= target) {
                min = Math.Min(min, i - left + 1);
                sum -= nums[left];
                left++;
            }
        }
        return min == int.MaxValue ? 0 : min;
    }
}
// @lc code=end

