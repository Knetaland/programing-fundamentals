/*
 * @lc app=leetcode id=875 lang=csharp
 *
 * [875] Koko Eating Bananas
 */

// @lc code=start
public class Solution {
    public int MinEatingSpeed(int[] piles, int h) {
        var left = 1;
        var right = piles.Max();
        while(left < right) {
            var mid = left + (right -left)/2;
            if (getHours(piles, mid) <= h) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }
        return left; // left will be == right when exiting the while loop
    }

    int getHours(int [] piles, int speed) {
        var hours = 0;
        foreach(var pile in piles) {
            hours += (int)Math.Ceiling((decimal)pile/speed);
        }
        return hours;
    }
}
// @lc code=end

