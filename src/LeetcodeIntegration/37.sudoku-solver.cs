/*
 * @lc app=leetcode id=37 lang=csharp
 *
 * [37] Sudoku Solver
 */

// @lc code=start
public class Solution {
    public void SolveSudoku(char[][] board) {
        int m = board.Length, n = board[0].Length;
        solve(0,0);

        bool solve(int x, int y) {
            for(var i = 0; i < m; i++) {
                for(var j =0; j < n; j++) {
                    if(board[i][j] == '.') {
                        // try 1- 9
                        for(var c = '1'; c <= '9'; c++) { // try 1-9 
                            if(isValid(i, j, c)) {
                                board[i][j] = c;  // put c in cell
                                if (solve(i, j)) return true; // dfs, see if true
                                board[i][j] = '.'; // otherwise set it back
                            }
                        }
                        return false;
                    }
                }
            }
            return true;
        }

        bool isValid(int x, int y, char c) {
            // Console.WriteLine($"done checking . at {x}{y}");
            for(int i = 0; i < m; i++) { // check column
                if (board[i][y] == c) return false;
            }
            // Console.WriteLine($"done checking column at {x}{y}");
            for(int j=0;j<n;j++) { // check row
                if (board[x][j] == c) return false;
            }
            // Console.WriteLine($"done checking row at {x}{y}");
            var squareX = x/3; 
            var squareY = y/3;
            // Console.WriteLine($"checking square at {squareX}{squareY}");
            // 0,0 -> i= 0 - 2, j -> 0 -> 2
            // 1,1 -> i = 3 - 5, j -> 3-5
            for(var i = 3*squareX; i < 3*squareX+3;i++) {
                for(var j = 3*squareY; j<3*squareY+3;j++) {
                    // Console.WriteLine($"checking square at index {i},{j}");
                    if (board[i][j] == c) return false;
                }
            }
            return true;
        }
    }
}
// @lc code=end

