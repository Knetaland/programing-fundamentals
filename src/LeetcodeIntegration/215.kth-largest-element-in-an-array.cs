/*
 * @lc app=leetcode id=215 lang=csharp
 *
 * [215] Kth Largest Element in an Array
 */

// @lc code=start

// O(N)
public class Solution {
    public int FindKthLargest(int[] nums, int k) {
        var pos = nums.Length - k;
        quickSelect(0, nums.Length -1);
        return nums[pos];

        void quickSelect(int left, int right) {
            if (left >= right) return ;
            var pivot = partition(left, right);
            if (pivot == pos) return ;
            
            if (pivot < pos) quickSelect(pivot+1, right);
            else quickSelect(left, pivot-1);
        }

        int partition(int left, int right) {
            int pivot = nums[right], wall = left;
            for(var i = left; i < right; i++) {
                if (nums[i] < pivot) {
                    swap(wall, i);
                    wall++;
                }
            }
            swap(wall, right);
            return wall;
        }

        void swap(int x, int y) {
            var temp = nums[x];
            nums[x] = nums[y];
            nums[y] = temp;
        }
    }
}
// @lc code=end

