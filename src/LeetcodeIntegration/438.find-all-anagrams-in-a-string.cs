/*
 * @lc app=leetcode id=438 lang=csharp
 *
 * [438] Find All Anagrams in a String
 */

// @lc code=start
public class Solution {
    public IList<int> FindAnagrams(string s, string p) {
        var res = new List<int>();
        var map = new Dictionary<char, int>();
        var charNeeded = 0;
        foreach(var c in p.ToCharArray()) {
            if(!map.ContainsKey(c)) {
                map.Add(c, 0);
            }
            map[c]++;
            charNeeded++;
        }

        var left = 0;
        for(var i = 0; i < s.Length; i++) {
            var c = s[i];
            if (map.ContainsKey(c)) {
                if (map[c] > 0) {
                    charNeeded--;
                }
                map[c]--;
            }

            while(i - left + 1 >= p.Length) {
                // Console.WriteLine($"left is {left}, i is {i}");
                if (charNeeded == 0) res.Add(left);
                var leftChar = s[left];
                if (map.ContainsKey(leftChar)) {
                    map[leftChar]++;
                    if (map[leftChar] > 0) charNeeded++;
                }
                left++;
            }
        }
        return res;
    }
}
// @lc code=end

