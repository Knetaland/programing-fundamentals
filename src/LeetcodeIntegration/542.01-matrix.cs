/*
 * @lc app=leetcode id=542 lang=csharp
 *
 * [542] 01 Matrix
 */

// @lc code=start
public class Solution {

    public int[][] UpdateMatrix(int[][] mat) {
         var dirs = new int [][] {
            new[] {1, 0},
            new[] {-1, 0},
            new[] {0, 1},
            new[] {0, -1},
        };
        var m = mat.Length;
        var n = mat[0].Length;
        var queue = new Queue<int[]>();
        var visited = new bool[m,n];
        for(var i = 0; i < m; i++) {
            for(var j = 0; j< n; j++) {
                if (mat[i][j] == 0) {
                    queue.Enqueue(new[] {i, j});
                    visited[i,j] = true;
                }
            }
        }

        while(queue.Count >  0) {
            var pos = queue.Dequeue();
            var x = pos[0];
            var y = pos[1];

            foreach(var dir in dirs) {
                var nextX = x + dir[0];
                var nextY = y + dir[1];
                if (nextX >=0 && nextY >=0 && nextX < m && nextY < n && !visited[nextX,nextY]) {
                    mat[nextX][nextY] = mat[x][y]+1;
                    queue.Enqueue(new[] {nextX, nextY});
                    visited[nextX, nextY] = true;
                }
            }
        }
        return mat;
         
    }
    public int[][] UpdateMatrix_Old(int[][] mat) {
        var dirs = new int [][] {
            new[] {1, 0},
            new[] {-1, 0},
            new[] {0, 1},
            new[] {0, -1},
        };
        var m = mat.Length;
        var n = mat[0].Length;
        
        var res = new int [m][];
        for (var i = 0; i < m; i++) {
            res[i] = new int[n];
        }

        for(var i = 0; i < m; i++) {
            for(var j = 0; j< n; j++) {
                if (mat[i][j] == 1) {
                    var distance = getDistance(i, j);
                    // Console.WriteLine($"setting index [{i},{j}] to distance {distance}");
                    res[i][j] = distance;
                }
            }
        }
        return res;

        int getDistance(int x, int y) {
            var visited = new bool[m,n];
            var queue = new Queue<int[]>();
            
            queue.Enqueue(new[] {x, y});
            var distance = 0;
            while(queue.Count > 0) {
                var size = queue.Count;
                for(var s =0; s < size; s++) {
                    var position = queue.Dequeue();
                    var i = position[0];
                    var j = position[1];

                    visited[i,j] = true;

                    if (mat[i][j] == 0) {
                        // Console.WriteLine($"vavlue {mat[i][j]} is 0, return distance {distance}");
                        return distance;
                    }
                    foreach(var dir in dirs) {
                        var nextX = i + dir[0];
                        var nextY = j + dir[1];

                        if ( nextX >= 0 && nextY >= 0 && nextX < m && nextY <n && !visited[nextX,nextY] ) {
                            queue.Enqueue(new[] {nextX, nextY});
                            // visited[nextX,nextY] = true;
                        }
                    }
                }
                distance++;
            }
            return distance;
        }
    }
}
// @lc code=end

