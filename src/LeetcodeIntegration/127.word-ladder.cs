/*
 * @lc app=leetcode id=127 lang=csharp
 *
 * [127] Word Ladder
 */

// @lc code=start
public class Solution {
    public int LadderLength(string beginWord, string endWord, IList<string> wordList) {
        // return LadderLength_BFS(beginWord, endWord, wordList);
        // return LadderLength_Better(beginWord, endWord, wordList);
        return LadderLength_Double_BFS(beginWord, endWord, wordList);
    }
    public int LadderLength_Double_BFS(string beginWord, string endWord, IList<string> wordList) {
        var set = new HashSet<string>(wordList);
        if (!set.Contains(endWord)) return 0;
        var beginSet = new HashSet<string>(new[] {beginWord});
        var endSet = new HashSet<string>(new[] {endWord});
        var visited = new HashSet<string>();
        var n = beginWord.Length;
        var count = 1;

        while(beginSet.Count > 0 && endSet.Count>0) {
            var nextSet = new HashSet<string>();
            Console.WriteLine($"Begin set: [{string.Join(",", beginSet)}]");
            foreach(var word in beginSet) {
                for(var i =0; i < n; i++) {
                    var sb = new StringBuilder(word);
                    for(var c = 'a'; c <= 'z'; c++) {
                        if (sb[i] != c) {
                            sb[i] = c;
                            var newWord = sb.ToString();
                            if (endSet.Contains(newWord)) return count + 1;
                            if (set.Contains(newWord) && !visited.Contains(newWord)) {
                                Console.WriteLine($"Adding {newWord} to next set: [{string.Join(",", nextSet)}]");
                                nextSet.Add(newWord);
                                visited.Add(newWord);
                            }
                        }
                    }
                }
            }

            if (endSet.Count < nextSet.Count) { // optimization, pick the shorter set to do the BFS
                beginSet = endSet;
                endSet = nextSet;
            } else {
                beginSet = nextSet;
            }
            count++;
        }
        return 0;
    }
    public int LadderLength_Better(string beginWord, string endWord, IList<string> wordList) {
        var map = new Dictionary<string, HashSet<string>>();
        var set = new HashSet<string>(wordList);
        set.Add(beginWord);
        var n = beginWord.Length;
        // precompile the possible path using the wordList, this set will be much smaller than building it on the fly
        foreach(var word in set) {
            for(var i = 0; i < n; i++) {
                var sb = new StringBuilder(word);
                for(var c = 'a'; c <='z'; c++) {
                    if (sb[i] != c) {
                        sb[i] = c;
                        var newWord = sb.ToString();
                        if (set.Contains(newWord)) {
                            if (!map.ContainsKey(word)){
                                map.Add(word, new HashSet<string>());
                            }
                            map[word].Add(newWord);
                        }
                    } 
                }
            }
        }

        var queue = new Queue<string>();
        var visited = new HashSet<string>();
        var count = 1;
        queue.Enqueue(beginWord);
        while(queue.Count > 0) {
            var size = queue.Count;
            count++;
            while(size > 0) {
                var word = queue.Dequeue();
                foreach(var nextWord in map[word]) { // word will always be in map, because they are valid words from wordlist
                    if (nextWord == endWord) return count;
                    if (!visited.Contains(nextWord)) {
                        visited.Add(nextWord);
                        queue.Enqueue(nextWord);
                    }
                }
                size--;
            }
        }

        return 0;
    }

    public int LadderLength_BFS(string beginWord, string endWord, IList<string> wordList) {
        var dictionary = new HashSet<string>(wordList);
        if(beginWord.Length != endWord.Length) return 0;
        var queue = new Queue<string>();
        var visited = new HashSet<string>();

        queue.Enqueue(beginWord);
        var count = 1;
        var n = beginWord.Length;
        while(queue.Count > 0) {
            var size = queue.Count;
            while(size > 0) {
                var word = queue.Dequeue();
                if (word == endWord) return count;
                visited.Add(word);
                for(var i = 0; i < n; i++) {
                    var sb = new StringBuilder(word);
                    for(var c = 'a'; c <='z'; c++) {
                        if (sb[i] != c) {
                            sb[i] = c;
                            var newWord = sb.ToString();
                            if (dictionary.Contains(newWord) && !visited.Contains(newWord)) {
                                queue.Enqueue(newWord);
                            }
                        } 
                    }
                }
                size--;
            }
            count++;
        }
        return 0;
    }


}
// @lc code=end

