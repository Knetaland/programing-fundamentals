/*
 * @lc app=leetcode id=125 lang=csharp
 *
 * [125] Valid Palindrome
 */

// @lc code=start
public class Solution {
    // O(n)
    // O(1): no extra space
    public bool IsPalindrome(string s) {
        var left = 0;
        var right = s.Length-1;
        while(left < right) {
            while(left < right &&  !char.IsLetterOrDigit(s[left])) left++;
            while(left < right && !char.IsLetterOrDigit(s[right])) right--;
            if (char.ToLower(s[left++]) != char.ToLower(s[right--])) return false;
        }
        return true;
    }
}
// @lc code=end

