/*
 * @lc app=leetcode id=1094 lang=csharp
 *
 * [1094] Car Pooling
 */

// @lc code=start
public class Solution {
    public bool CarPooling(int[][] trips, int capacity) {
        var n = 1000+1;
        var passengers = new int[n];
        foreach(var trip in trips) {
            var count = trip[0];
            var start = trip[1];
            var end = trip[2];

            passengers[start] += count;
            if (end <= n) {
                passengers[end] -= count;
            }
        }
        // Console.WriteLine($"[{string.Join(",", passengers)}]");
        for(var i = 0; i < n; i++) {
            var pre = i == 0 ? 0 : passengers[i-1];
            passengers[i] = pre + passengers[i];
            if (passengers[i] > capacity) return false;
        }
        return true;
    }
}
// @lc code=end

