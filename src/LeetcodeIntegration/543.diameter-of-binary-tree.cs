/*
 * @lc app=leetcode id=543 lang=csharp
 *
 * [543] Diameter of Binary Tree
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class Solution {
    public int DiameterOfBinaryTree(TreeNode root) {
        var max = 0;
        MaxDepth(root);
        return max;
        
        int MaxDepth(TreeNode node) {
            if (node == null) return 0;
            var left = MaxDepth(node.left);
            var right = MaxDepth(node.right);
            max = Math.Max(max, left + right);
            return 1 + Math.Max(left, right);
        }
    }
}
// @lc code=end

