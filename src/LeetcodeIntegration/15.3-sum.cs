/*
 * @lc app=leetcode id=15 lang=csharp
 *
 * [15] 3Sum
 */

// @lc code=start
public class Solution {
    public IList<IList<int>> ThreeSum(int[] nums) {
        var res = new List<IList<int>>();
        Array.Sort(nums);
        for(var i = 0; i < nums.Length; i++) {
            var target = 0 - nums[i];
            // Console.WriteLine($"value {nums[i]} at index {i}, target: {target}");
            var sub = TwoSum(nums, target, i+1);
            foreach(var s in sub) {
                // Console.WriteLine($"found list {string.Join(",", s)}");
                s.Add(nums[i]);
                res.Add(s);
            }
            while(i + 1 < nums.Length && nums[i+1] == nums[i]) i++;
        }
        return res;
    }

    private IList<IList<int>> TwoSum(int[] nums, int target, int start) {
        var res = new List<IList<int>>();
        var left = start;
        var right = nums.Length-1;
        while(left < right) {
            var leftVal =  nums[left];
            var rightVal = nums[right];
            var sum = leftVal + rightVal;
            if (sum == target) {
                res.Add(new List<int>{ leftVal, rightVal });
                while(left < right && nums[left] == leftVal) left++;
                while(right > left && nums[right] == rightVal) right--;
            } else if (sum < target) {
                left++;
            } else if (sum > target) {
                right--;
            }
        }
        return res;
    }
}
// @lc code=end

