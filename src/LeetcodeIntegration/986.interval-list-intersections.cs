/*
 * @lc app=leetcode id=986 lang=csharp
 *
 * [986] Interval List Intersections
 */

// @lc code=start
public class Solution {
    public int[][] IntervalIntersection(int[][] firstList, int[][] secondList) {
        var res = new List<int[]>();
        int i = 0, j = 0, l1 = firstList.Length, l2 = secondList.Length;
        while(i < l1 && j < l2) {
            var overlapStart = Math.Max(firstList[i][0], secondList[j][0]);
            var overlapEnd = Math.Min(firstList[i][1], secondList[j][1]);
            if (overlapEnd >= overlapStart) { // if not >=, no overlap
                res.Add(new[] {overlapStart, overlapEnd});
            }
            if (firstList[i][1] < secondList[j][1]) { //alwasy move pointer, move the one that ends first
                i++;
            } else {
                j++;
            }
        }
        return res.ToArray();
    }
}
// @lc code=end

