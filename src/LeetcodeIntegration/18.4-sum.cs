/*
 * @lc app=leetcode id=18 lang=csharp
 *
 * [18] 4Sum
 */

// @lc code=start
public class Solution {
    public IList<IList<int>> FourSum(int[] nums, int target) {
        Array.Sort(nums);
        return nSum(nums, target, 0, 4);
    }

    private IList<IList<int>> nSum(int[] nums, int target, int start, int n) {
        var res = new List<IList<int>>();

        if (n == 2) {
            return TwoSum(nums, target, start);
        } else {
            for(var i = start; i < nums.Length; i++) {
                var t = target - nums[i];
                var sub = nSum(nums, t, i + 1, n - 1);
                foreach(var s in sub) {
                    // Console.WriteLine($"found list {string.Join(",", s)}");
                    s.Add(nums[i]);
                    res.Add(s);
                }
                while(i + 1 < nums.Length && nums[i+1] == nums[i]) i++;
            }
        }
        
        return res;
    }


    private IList<IList<int>> TwoSum(int[] nums, int target, int start) {
        var res = new List<IList<int>>();
        var left = start;
        var right = nums.Length-1;
        while(left < right) {
            var leftVal =  nums[left];
            var rightVal = nums[right];
            var sum = leftVal + rightVal;
            if (sum == target) {
                res.Add(new List<int>{ leftVal, rightVal });
                while(left < right && nums[left] == leftVal) left++;
                while(right > left && nums[right] == rightVal) right--;
            } else if (sum < target) {
                left++;
            } else if (sum > target) {
                right--;
            }
        }
        return res;
    }
}
// @lc code=end

