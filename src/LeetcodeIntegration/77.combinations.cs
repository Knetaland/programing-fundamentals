/*
 * @lc app=leetcode id=77 lang=csharp
 *
 * [77] Combinations
 */

// @lc code=start
public class Solution {
    public IList<IList<int>> Combine(int n, int k) {
        var res = new List<IList<int>>();
        dfs(new List<int>(), 1);
        return res;

        void dfs(IList<int> level, int start) {
            if (level.Count == k){
                res.Add(new List<int>(level));
                return;
            } 
            
            for(var i = start; i <= n; i++) {
                level.Add(i);
                dfs(level, i + 1);
                level.RemoveAt(level.Count - 1);
            }   
        }
    }
}
// @lc code=end

