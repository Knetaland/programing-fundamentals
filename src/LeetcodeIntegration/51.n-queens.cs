/*
 * @lc app=leetcode id=51 lang=csharp
 *
 * [51] N-Queens
 */

// @lc code=start
public class Solution {
    public IList<IList<string>> SolveNQueens(int n) {
        var pos = new int[n];
        for(var i  = 0; i < n; i++) pos[i] = -1;
        var res = new List<IList<string>>();
        solve(0, pos);

        return res;

        void solve(int row, int[] positions) {
            if (row >= n) {
                // Console.WriteLine($"{string.Join(",", positions)}");
                res.Add(drawBoard(positions));
                return ;
            }
            for(var col = 0; col < n; col++) { // for a row, going through columns
                if(isValid(row, col, positions)) {
                    // Console.WriteLine($"put on valid postion {row}, {col}");
                    positions[row] = col;
                    solve(row + 1, positions);
                    positions[row] = -1;
                }
            }
        }

        bool isValid(int row, int col, int[] posistions) {
            for(var i = 0; i < row; i++) { // check for all queens that have been put down up to current row
                // Console.WriteLine($"posistions[row] == col {posistions[row] == col}");
                // Console.WriteLine($"row - i == Math.Abs(posistions[i] - col {row - i == Math.Abs(posistions[i] - col)}");
                if (
                    posistions[i] == col // not queen on the same col
                    || row - i == Math.Abs(posistions[i] - col) // diagonal e.g. 01: [0,1] [1,2] [2,3] => diff(row) == diff(col) 
                ) return false;
            }
            return true;
        }

        IList<string> drawBoard(int[] positions) {
            var board = new List<string>();
            for(var i = 0; i < n; i++) {
                var row = new StringBuilder(new String('.', n));
                row[positions[i]] = 'Q';
                board.Add(row.ToString()); 
            }
            return board;
        } 
    }
}
// @lc code=end

