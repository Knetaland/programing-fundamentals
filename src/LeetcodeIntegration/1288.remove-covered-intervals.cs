/*
 * @lc app=leetcode id=1288 lang=csharp
 *
 * [1288] Remove Covered Intervals

    Given an array intervals where intervals[i] = [li, ri] represent the interval [li, ri), remove all intervals that are covered by another interval in the list.

    The interval [a, b) is covered by the interval [c, d) if and only if c <= a and b <= d.

    Return the number of remaining intervals.

    

    Example 1:

    Input: intervals = [[1,4],[3,6],[2,8]]
    Output: 2
    Explanation: Interval [3,6] is covered by [2,8], therefore it is removed.
    Example 2:

    Input: intervals = [[1,4],[2,3]]
    Output: 1
    

    Constraints:

    1 <= intervals.length <= 1000
    intervals[i].length == 2
    0 <= li < ri <= 105
    All the given intervals are unique.
 */


// @lc code=start
public class Solution {
    public int RemoveCoveredIntervals(int[][] intervals) {
        // sort by start time, if same start time, the one ends later will be first to cover the bigger interval
        Array.Sort(intervals, (a, b) => a[0] == b[0] ? b[1] - a[1] : a[0] - b[0]);

        var remaining = intervals.Length;  
        // first end time
        var prevEnd = intervals[0][1];
        for(var i = 1; i < intervals.Length; i++) {
            var cur = intervals[i];
            if (cur[0] < prevEnd) { // overlap
                if (cur[1] <= prevEnd) { // cur end already included in the prev interval
                    remaining--; // ignore the current interval, so decrement the remaining
                } else {
                    prevEnd = cur[1];
                }
            }
        }
        return remaining;
    }
}
// @lc code=end

