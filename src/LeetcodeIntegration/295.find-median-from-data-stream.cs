/*
 * @lc app=leetcode id=295 lang=csharp
 *
 * [295] Find Median from Data Stream
 */

// @lc code=start
public class MedianFinder {

    private PriorityQueue<int, int> _maxHeap;
    private PriorityQueue<int, int> _minHeap;
    public MedianFinder() {
        _maxHeap = new PriorityQueue<int, int>(new MinComparer());
        _minHeap = new PriorityQueue<int, int>();
    }
    
    // use two heaps to divide numbers to two halves.
    // max_heap holds the smaller half, min_heap holds the larger half
    // pick smaller half (max_heap) as the default heap to insert to
    // => get next largest to put in max_heap => maxHeap.Enqueue(minHeap.EnqueueDequeue)
    // when not even, larger one should get enqueue => pick max from maxHeap to put in maxHeap
    // => minHeap.Enqueue(maxHeap.EnqueueDequeue)
    public void AddNum(int num) {
        //
        // Console.WriteLine($"Count: maxHeap: {_maxHeap.Count}, minHeap: {_minHeap.Count}");
        if (_maxHeap.Count == _minHeap.Count) {
            // _minHeap.Enqueue(num, num);
            var next = _minHeap.EnqueueDequeue(num, num);
            _maxHeap.Enqueue(next, next);
            // var next = _maxHeap.Dequeue();
            // Console.WriteLine($"popping {next} from maxheap");

            // _minHeap.Enqueue(next, next);
            // Console.WriteLine($"now minHeap top is {_minHeap.Peek()}");

        } else {
            // _maxHeap.Enqueue(num, num);
            var next = _maxHeap.EnqueueDequeue(num, num);
            // Console.WriteLine($"not even, popping from max heap: {next}");
            // if (_minHeap.Count > 0) {
            //     Console.WriteLine($"before enqueue, min heap top is {_minHeap.Peek()}");
            // }
            _minHeap.Enqueue(next, next);
            // Console.WriteLine($"after enqueue, min heap top is {_minHeap.Peek()}");
        }
        // Console.WriteLine($"after balancing, Count: maxHeap: {_maxHeap.Count}, minHeap: {_minHeap.Count}");
        // var maxHeapTop = _maxHeap.Count > 0 ? _maxHeap.Peek() : -1;
        // var minHeapTop = _minHeap.Count > 0 ? _minHeap.Peek() : -1;
        // Console.WriteLine($"after balancing, Tops: maxHeap: {maxHeapTop}, minHeap: {minHeapTop}");
    }
    
    public double FindMedian() {
        // _data.Sort();
        // if (_data.Count % 2 == 0) {
        //     Console.WriteLine($"count is {_data.Count}");
        //     var mid = _data.Count / 2;
        //     Console.WriteLine($"mid is {mid}");
        //     return (_data[mid-1] + _data[mid]) / 2.0;
        // }
        // return _data[_data.Count / 2];
        if (_maxHeap.Count == _minHeap.Count) {
            return (_maxHeap.Peek() + _minHeap.Peek())/2.0;
        }
        return _maxHeap.Peek();
    }

    public class MinComparer : IComparer<int>
    {
        public int Compare(int x, int y) => y - x;
    }
}

/**
 * Your MedianFinder object will be instantiated and called as such:
 * MedianFinder obj = new MedianFinder();
 * obj.AddNum(num);
 * double param_2 = obj.FindMedian();
 */
// @lc code=end

