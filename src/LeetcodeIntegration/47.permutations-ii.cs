/*
 * @lc app=leetcode id=47 lang=csharp
 *
 * [47] Permutations II
 */

// @lc code=start
public class Solution {
    public IList<IList<int>> PermuteUnique(int[] nums) {
        var res = new List<IList<int>>();
        Array.Sort(nums);
        var visited = new bool[nums.Length];
        dfs(new List<int>());
        
        return res;

        void dfs(IList<int> list) {
            if (list.Count == nums.Length) {
                res.Add(new List<int>(list));
            }

            for(var i=0; i< nums.Length; i++) {
                if (
                    visited[i] ||
                    (i > 0 && nums[i-1] == nums[i])
                        && visited[i-1]

                ) continue;
                visited[i] = true;
                list.Add(nums[i]);
                dfs(list);
                visited[i] = false;
                list.RemoveAt(list.Count -1);
            }
        }
    }
}
// @lc code=end

