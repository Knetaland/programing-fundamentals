/*
 * @lc app=leetcode id=1109 lang=csharp
 *
 * [1109] Corporate Flight Bookings
 */

// @lc code=start
public class Solution {
    public int[] CorpFlightBookings(int[][] bookings, int n) {
        var flights = new int[n];

        foreach(var booking in bookings) {
            var start = booking[0];
            var end = booking[1];
            var count = booking[2];

            flights[start-1] += count;
            if (end < n) {
                flights[end] -= count;
            }
        }


        for(var i = 1; i < n; i++) {
            flights[i] = flights[i-1] + flights[i];
        }

        Console.WriteLine($"[{string.Join(",", flights)}]");

        return flights;
    }
}
// @lc code=end

