/*
 * @lc app=leetcode id=1 lang=csharp
 *
 * [1] Two Sum
 */

// @lc code=start
public class Solution {
    public int[] TwoSum(int[] nums, int target) {
        var map = new Dictionary<int,int>();
        for(var i =0; i < nums.Length; i++) {
            if (map.ContainsKey(nums[i])) {
                return new[] {map[nums[i]], i };
            } 
            if (!map.ContainsKey(target - nums[i])) {
                map.Add(target - nums[i],  i);
            }
        }
        return new int[] {};
    }
}
// @lc code=end

