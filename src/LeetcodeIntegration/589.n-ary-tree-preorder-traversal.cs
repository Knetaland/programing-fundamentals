/*
 * @lc app=leetcode id=589 lang=csharp
 *
 * [589] N-ary Tree Preorder Traversal
 */

// @lc code=start
/*
// Definition for a Node.
public class Node {
    public int val;
    public IList<Node> children;

    public Node() {}

    public Node(int _val) {
        val = _val;
    }

    public Node(int _val,IList<Node> _children) {
        val = _val;
        children = _children;
    }
}
*/

public class Solution {
    public IList<int> Preorder(Node root) {
        var res = new List<int>();
        Traverse(root);
        return res;
        void Traverse(Node node) {
            if (node == null) return ;
            res.Add(node.val);
            foreach(var n in node.children) {
                Traverse(n);
            }
        }
    }
}
// @lc code=end

