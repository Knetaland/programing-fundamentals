/*
 * @lc app=leetcode id=695 lang=csharp
 *
 * [695] Max Area of Island
 */

// @lc code=start
public class Solution {
    const int LAND = 1;
    const int WATER = 0;
    public int MaxAreaOfIsland(int[][] grid) {
        var max = 0;
        var m = grid.Length;
        var n = grid[0].Length;
        for(var i = 0; i < m; i++) {
            for(var j = 0; j < n; j++) {
                if (grid[i][j] == LAND) {
                    var area = Expand(i, j);
                    max = Math.Max(max, area);
                }
            }
        }
        return max;

        int Expand(int x, int y) {
            if (x <0 || y < 0 || x >= m || y >= n || grid[x][y] == WATER){
                return 0;
            }
            grid[x][y] = WATER; // sink
            return 1 + Expand(x-1, y)
                + Expand(x+1, y)
                + Expand(x, y-1)
                + Expand(x, y+1);
        }
    }
}
// @lc code=end

