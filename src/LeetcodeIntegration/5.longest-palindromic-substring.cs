/*
 * @lc app=leetcode id=5 lang=csharp
 *
 * [5] Longest Palindromic Substring
 */

// @lc code=start
public class Solution {
    public string LongestPalindrome(string s) {
        int i = 0, n = s.Length, leftMost = 0, rightMost = 0;
        while(i < n) {
            var start = i;
            while(i + 1 < n && s[start] == s[i+1]) i++;

            var end = i;
            // expand both sides
            while(start >=0 && end < n && s[start] == s[end]) {
                if (end - start > rightMost - leftMost) {
                    rightMost = end;
                    leftMost = start;
                }
                start--;
                end++;
            }
            i++;
        }
        return s.Substring(leftMost, rightMost-leftMost+1);
    }
}
// @lc code=end

