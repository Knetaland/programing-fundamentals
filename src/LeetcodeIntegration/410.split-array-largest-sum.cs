/*
 * @lc app=leetcode id=410 lang=csharp
 *
 * [410] Split Array Largest Sum
 */

// @lc code=start
public class Solution {
    public int SplitArray(int[] nums, int m) {
        var left = nums.Max();
        var right = nums.Sum();

        while(left  < right) {
            var mid = left + (right-left)/2;
            if (getCount(nums, mid) <= m) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }
        return left;
    }

    int getCount(int[] nums, int cap) {
        var count = 1;
        var remaining = cap;
        foreach(var num in nums) {
            if (num > remaining) {
                count++;
                remaining = cap - num;
            } else  {
                remaining -= num;
            }
        }
        return count;
    }
}
// @lc code=end

