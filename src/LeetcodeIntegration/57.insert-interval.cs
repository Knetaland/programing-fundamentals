/*
 * @lc app=leetcode id=57 lang=csharp
 *
 * [57] Insert Interval
 */

// @lc code=start
public class Solution {
    public int[][] Insert(int[][] intervals, int[] newInterval) {
        var res = new List<int[]>();
        foreach(var cur in intervals) {
            if (newInterval[1]< cur[0]) { // before cur, prepend
                res.Add(newInterval);
                newInterval = cur;
            } else if (newInterval[0] > cur[1]) { // after cur, append cur
                res.Add(cur);
            } else { // merge
                newInterval[0] = Math.Min(newInterval[0], cur[0]);
                newInterval[1] = Math.Max(newInterval[1], cur[1]);
            }
        }
        res.Add(newInterval);
        return res.ToArray();
    }
}
// @lc code=end

