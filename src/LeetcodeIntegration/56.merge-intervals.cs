/*
 * @lc app=leetcode id=56 lang=csharp
 *
 * [56] Merge Intervals
 */

// @lc code=start
public class Solution {
    public int[][] Merge(int[][] intervals) {
        var res = new List<int[]>();
        var intervalList  = intervals.ToList();
        intervalList.Sort((a,b) => a[0] - b[0]);
        var start = intervalList[0][0];
        var end = intervalList[0][1];

        for(var i = 1; i < intervalList.Count; i++) {
            if (intervalList[i][0] <=end) {
                end = Math.Max(end,intervalList[i][1]);
            } else {
                res.Add(new[] {start, end});
                start = intervalList[i][0];
                end = intervalList[i][1];
            }
        }

        res.Add(new[] {start, end});

        return res.ToArray();
    }
}
// @lc code=end

