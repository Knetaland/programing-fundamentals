/*
 * @lc app=leetcode id=322 lang=csharp
 *
 * [322] Coin Change
 */

// @lc code=start

/*
                33
            1/  |5  \10
            32    28    23
    /1  |5 \10   
    31  27  22   (27,23,18) (22, 18, 13)
(30,26,21),              (17, 13, 8)           (12, 8, 3) 
2
1
0

*/
public class Solution {
    public int CoinChange(int[] coins, int amount) {
        if (amount <=0) return 0;
        // return CoinChangeBFS(coins, amount);
        var dp = new int [amount+1];
        for(var i = 1; i < dp.Length; i++) {
            dp[i] = amount + 1; // max
        }
        for(var i  = 1; i < dp.Length; i++) {
            foreach(var coin in coins) {
                if (i - coin >=0) { // if i is coin amount, i - coin = 0, 
                    dp[i] = Math.Min(dp[i-coin]+1, dp[i]);
                }
            }
        }

        return dp[amount] != amount+1 ? dp[amount] : -1;
    }

    public int CoinChangeBFS(int[] coins, int amount) { // out of memory
        
        var queue = new Queue<int>();
        var count = 0;
        var visited = new HashSet<int>();
        queue.Enqueue(amount);
        while(queue.Count > 0) {
            var n = queue.Count;
            for(var i = 0; i < n; i++){
                var curAmount = queue.Dequeue(); 
                visited.Add(curAmount);
                if (curAmount == 0) return count;
                foreach(var coin in coins) {
                    var temp = curAmount - coin;
                    if (temp >=0 && !visited.Contains(temp)) {
                        queue.Enqueue(temp);
                    }
                }
            }
            count++;
        }
        return -1;
    }
}
// @lc code=end

