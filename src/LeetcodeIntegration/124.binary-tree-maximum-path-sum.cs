/*
 * @lc app=leetcode id=124 lang=csharp
 *
 * [124] Binary Tree Maximum Path Sum
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class Solution {
    public int MaxPathSum(TreeNode root) {
        if (root == null) return 0;
        var max = int.MinValue;
        Dfs(root, 0);
        return max;

        void Dfs(TreeNode node, int curSum) {
            if (node == null) return;
            var newSum = curSum + node.val;
            if (node.left == null && node.right== null) {
                max = Math.Max(newSum, max);
                return;
            }

            Dfs(node.left, newSum);
            Dfs(node.right, newSum);
        }
    }
}
// @lc code=end

