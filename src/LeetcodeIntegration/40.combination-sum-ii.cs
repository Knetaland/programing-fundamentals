/*
 * @lc app=leetcode id=40 lang=csharp
 *
 * [40] Combination Sum II
 */

// @lc code=start
public class Solution {
    public IList<IList<int>> CombinationSum2(int[] candidates, int target) {
        var res = new List<IList<int>>();
        Array.Sort(candidates);
        bt(0, 0, new List<int>());
        return res;
        void bt(int start, int currentSum, IList<int> track) {
            if (currentSum == target) {
                res.Add(new List<int>(track));
                return ;
            }
            if (currentSum > target) {
                return ;
            }
            for(var i = start; i < candidates.Length; i++) {
                if (i > start && candidates[i] == candidates[i-1]) continue;
                track.Add(candidates[i]);
                bt(i+1, currentSum + candidates[i], track);
                track.RemoveAt(track.Count -1);
            }
        }
    }
}
// @lc code=end

