/*
 * @lc app=leetcode id=590 lang=csharp
 *
 * [590] N-ary Tree Postorder Traversal
 */

// @lc code=start
/*
// Definition for a Node.
public class Node {
    public int val;
    public IList<Node> children;

    public Node() {}

    public Node(int _val) {
        val = _val;
    }

    public Node(int _val, IList<Node> _children) {
        val = _val;
        children = _children;
    }
}
*/

public class Solution {
    public IList<int> Postorder(Node root) {
        var res = new List<int>();
        Traverse(root);
        return res;
        void Traverse(Node node) {
            if (node == null) return ;
            foreach(var n in node.children) {
                Traverse(n);
            }
            res.Add(node.val);
        }
    }
}
// @lc code=end

