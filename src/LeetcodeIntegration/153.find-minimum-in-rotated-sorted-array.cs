/*
 * @lc app=leetcode id=153 lang=csharp
 *
 * [153] Find Minimum in Rotated Sorted Array
 */

// @lc code=start
public class Solution {
    public int FindMin(int[] nums) {
        // 1st element  < last, means rotation
    //    if (nums[0] > nums[nums.Length-1]) {
    //        var target = nums[0];
    //        int left = 0, right = nums.Length-1;
    //        while(left <= right) {
    //            var mid = left + (right-left)/2;
    //         //    Console.WriteLine($"mid: {mid}");
    //            if (nums[mid] > nums[mid+1]) return nums[mid+1];
    //            if (nums[mid] < nums[mid-1]) return nums[mid];
    //            if (nums[mid] > target) { // ascending order, keep looking right
    //                 left = mid + 1;
    //            } else if (nums[mid] < target) { // keep looking left
    //                 right = mid -1;
    //            }
    //        }
    //    }
    //    return nums[0];
        int lo = 0, hi = nums.Length -1;
        while(lo < hi) {
            var mid = lo + (hi-lo)/2;
            Console.WriteLine($"mid is index {mid}: {nums[mid]}");
            if(nums[mid]>nums[hi]) lo = mid +1;
            else hi = mid;
            Console.WriteLine($"lo: {lo} with value {nums[lo]}, hi: {hi} with value {nums[hi]}");
        }
        return nums[lo];
    }

    private int Simple(int[] nums) {
        var cur = int.MinValue;
        foreach(var n in nums) {
            if (n < cur) {
                return n;
            }
            cur = n;
        }
        return nums[0];
    }
}
// @lc code=end

