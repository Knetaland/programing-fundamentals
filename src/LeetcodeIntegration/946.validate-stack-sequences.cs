/*
 * @lc app=leetcode id=946 lang=csharp
 *
 * [946] Validate Stack Sequences
 */

// @lc code=start
public class Solution {

    public bool ValidateStackSequences(int[] pushed, int[] popped) {
        var stack = new Stack<int>();
        var i = 0;
        foreach(var p in pushed) {
            stack.Push(p);
            while(stack.Count > 0 && stack.Peek() == popped[i]) {
                stack.Pop();
                i++;
            }
        }
        return stack.Count == 0;
    }
    public bool ValidateStackSequences_2(int[] pushed, int[] popped) {
        var stack = new Stack<int>();
        var pushedIndex = 0;
        for(var i = 0; i < popped.Length; i++) {
            if (stack.Count == 0 || stack.Peek() != popped[i]) {
                while (pushedIndex < pushed.Length && pushed[pushedIndex]!= popped[i]) {
                    stack.Push(pushed[pushedIndex]);
                    pushedIndex++;
                }
                if (pushedIndex < pushed.Length) stack.Push(pushed[pushedIndex++]);
            }
        
            var top = stack.Pop();
            if (top != popped[i]) return false;
        }
        return true;
    }
}
// @lc code=end

