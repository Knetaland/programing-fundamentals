/*
 * @lc app=leetcode id=122 lang=csharp
 *
 * [122] Best Time to Buy and Sell Stock II
 */

// @lc code=start
public class Solution {

    public int MaxProfit(int[] prices) {
        var profit = 0;
        var preLow = prices[0];
        for(var i = 1; i < prices.Length; i++) {
            if (prices[i] < preLow) {
                preLow = prices[i];
            }
            else {
                while(i+1 < prices.Length && prices[i+1] > prices[i]) {
                    i++;
                }
                profit += (prices[i]-preLow);
                preLow = prices[i];
            }
        }

        return profit;
    }
    public int MaxProfit2(int[] prices) {
        var profit = 0;
        var preLow = prices[0];
        var prePrice = prices[0];
        var i = 0;
        foreach(var price in prices) {
            if (price < prePrice) {
                profit += (prePrice - preLow);
                preLow = price;
            }
            if (i == prices.Length-1 && price >= prePrice) {
                profit += (price - preLow);
            }
            prePrice = price;
            i++;
        }
        return profit;
    }
}
// @lc code=end

