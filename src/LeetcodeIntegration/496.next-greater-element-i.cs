/*
 * @lc app=leetcode id=496 lang=csharp
 *
 * [496] Next Greater Element I
 */

// @lc code=start
public class Solution {
    public int[] NextGreaterElement(int[] nums1, int[] nums2) {
        var stack = new Stack<int>();
        var map = new Dictionary<int, int>();

        for(var i = nums2.Length -1; i >=0 ; i--) {
            while(stack.Count > 0 && nums2[i] >= stack.Peek()) stack.Pop();
            map[nums2[i]] = stack.Count == 0 ? -1 : stack.Peek();
            stack.Push(nums2[i]);
        }
        // [3,4,-1,-1]
        // 4-3-1
        /* map
            {
                2: -1
                4: -1
                3: 4
                1: 3
            }
        */
        var res = new int[nums1.Length];
        for(var i = 0; i < nums1.Length; i++) {
            res[i] = map[nums1[i]];
        }
        return res;
    }

    public int[] NextGreaterElement_Naive(int[] nums1, int[] nums2) {
        var map  = new Dictionary<int,int>();
        var res = new int[nums1.Length];
        for(var i =0; i< nums2.Length; i++) {
            map[nums2[i]] = i;
        }
        for(var i = 0; i < nums1.Length; i++) {
            var index = map[nums1[i]];
            var j = index + 1;
            for(; j< nums2.Length; j++) {
                if (nums2[j] > nums1[i]) break;
            }
            res[i] = j == nums2.Length ? -1 : nums2[j];
        }
        return res;
    }
}
// @lc code=end

