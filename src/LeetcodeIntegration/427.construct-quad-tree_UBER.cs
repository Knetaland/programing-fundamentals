/*
 * @lc app=leetcode id=427 lang=csharp
 *
 * [427] Construct Quad Tree
 */

// @lc code=start
/*
// Definition for a QuadTree node.
public class Node {
    public bool val;
    public bool isLeaf;
    public Node topLeft;
    public Node topRight;
    public Node bottomLeft;
    public Node bottomRight;

    public Node() {
        val = false;
        isLeaf = false;
        topLeft = null;
        topRight = null;
        bottomLeft = null;
        bottomRight = null;
    }
    
    public Node(bool _val, bool _isLeaf) {
        val = _val;
        isLeaf = _isLeaf;
        topLeft = null;
        topRight = null;
        bottomLeft = null;
        bottomRight = null;
    }
    
    public Node(bool _val,bool _isLeaf,Node _topLeft,Node _topRight,Node _bottomLeft,Node _bottomRight) {
        val = _val;
        isLeaf = _isLeaf;
        topLeft = _topLeft;
        topRight = _topRight;
        bottomLeft = _bottomLeft;
        bottomRight = _bottomRight;
    }
}
*/

public class Solution {
    public Node Construct(int[][] grid) {
        return construct2(0, grid[0].Length-1, 0, grid.Length-1);

        Node construct2(int left, int right, int top, int bottom) {
            if (bottom == top && left == right) {
                return new Node(grid[top][left]==1, true);
            }

            var midX = (left + right)/2;
            var midY = (top+bottom)/2;

            var topLeft = construct2(left, midX, top, midY);
            var topRight = construct2(midX+1, right, top, midY);
            var bottomLeft = construct2(left, midX, midY+1, bottom);
            var bottomRight = construct2(midX+1, right, midY+1, bottom);

            if (topLeft.isLeaf && topRight.isLeaf && bottomLeft.isLeaf && bottomRight.isLeaf
            && topLeft.val == topRight.val && topRight.val == bottomRight.val && bottomRight.val == bottomLeft.val) {
                return new Node(topLeft.val, true);
            } else {
                return new Node(false, false, topLeft, topRight, bottomLeft, bottomRight);
            }
        }

        Node construct(int left, int right, int top, int bottom) {
            var node = new Node();
            if(isSame(left, right, top, bottom)) {
                node.isLeaf = true;
                node.val = grid[top][left] == 1;
                return node;
            }

            node.isLeaf = false;
            
            var midX = (left + right)/2;
            var midY = (top+bottom)/2;
            node.topLeft = construct(left, midX, top, midY);
            node.topRight = construct(midX+1, right, top, midY);
            node.bottomLeft = construct(left, midX, midY+1, bottom);
            node.bottomRight = construct(midX+1, right, midY+1, bottom);
            return node;
        }

        bool isSame(int left, int right, int top, int bottom) {
            for(var i = top; i <= bottom; i++) {
                for(var j = left; j <=right; j++) {
                    if (grid[top][left] != grid[i][j]) return false;
                }
            }
            return true;
        }
    }
}
// @lc code=end

