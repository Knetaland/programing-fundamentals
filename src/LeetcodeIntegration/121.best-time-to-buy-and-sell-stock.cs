/*
 * @lc app=leetcode id=121 lang=csharp
 *
 * [121] Best Time to Buy and Sell Stock
 */

// @lc code=start
public class Solution {
    public int MaxProfit(int[] prices) {
        if (prices == null || prices.Length == 0) return 0;
        int lowest = prices[0], max = 0;
        foreach(var price in prices) {
            if (price < lowest) {
                lowest = price;
            } else {
                var profit = price - lowest;
                if (profit > max) {
                    max = profit;
                }
            }
        }
        return max;
    }
}
// @lc code=end

