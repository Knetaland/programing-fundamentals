/*
 * @lc app=leetcode id=39 lang=csharp
 *
 * [39] Combination Sum
 */

// @lc code=start
public class Solution {
    public IList<IList<int>> CombinationSum(int[] candidates, int target) {
        var res = new List<IList<int>>();
        Array.Sort(candidates);
        var visited = new bool[candidates.Length];

        bt(0,0, new List<int>());

        return res;

        void bt(int start, int curSum, IList<int> track) {
            if (curSum == target) {
                res.Add(new List<int>(track));
                return ;
            }
            if (curSum > target) return ;
            for(var i = start; i < candidates.Length; i++) {
                track.Add(candidates[i]);
                bt(i, curSum + candidates[i], track); // next bt can use same start index, so item can be use repeatedly.
                track.RemoveAt(track.Count - 1);
            }
        }
    }
}
// @lc code=end

