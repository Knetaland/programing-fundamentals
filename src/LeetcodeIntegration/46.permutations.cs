/*
 * @lc app=leetcode id=46 lang=csharp
 *
 * [46] Permutations
 */

// @lc code=start

// O(N!) [3, 2, 1]: 3 * 2 * 1 => 3 selections on 1st item, 2 selections on 2nd item, 1 selection on last item
public class Solution {
    public IList<IList<int>> Permute(int[] nums) {
        var res = new List<IList<int>>();
        dfs(new List<int>());
        return res;
        void dfs(IList<int> list) {
            if (list.Count == nums.Length) {
                res.Add(new List<int>(list));
            }

            for(var i=0; i< nums.Length; i++) {
                if (list.Contains(nums[i])) continue;
                list.Add(nums[i]);
                dfs(list);
                list.RemoveAt(list.Count -1);
            }
        }
    }
}
// @lc code=end

