/*
 * @lc app=leetcode id=167 lang=csharp
 *
 * [167] Two Sum II - Input Array Is Sorted
 */

// @lc code=start
public class Solution {
    public int[] TwoSum(int[] numbers, int target) {
        var left = 0;
        // var lower = lowerBound(numbers, target);
        var right = numbers.Length -1;
        Console.WriteLine($"right is {right}");

        while(left < right) {
            var sum = numbers[left] + numbers[right];
            if (sum == target) return new[] {left +1 , right + 1}; // 1 based
            else if (sum < target) {
                left++;
            } else if (sum > target) {
                right--;
            }
        }
        return new [] {-1, -1};
    }

    private int lowerBound(int[] nums, int target) {
        var left = 0;
        var right = nums.Length;
        while(left < right) {
            var mid = left + (right-left)/2;
            if (nums[mid] == target) {
                right = mid;
            } else if (nums[mid] < target) {
                left = mid + 1;
            } else if (nums[mid] > target) {
                right = mid;
            }
        }
        return left;
    }
}
// @lc code=end

