/*
 * @lc app=leetcode id=239 lang=csharp
 *
 * [239] Sliding Window Maximum
 */

// @lc code=start
public class Solution {
    public int[] MaxSlidingWindow(int[] nums, int k) {
        var res = new int[nums.Length - k +1];
        var queue = new LinkedList<int>();
        for(var i =0; i < nums.Length; i++) {
            var startWindowIndex = i - k + 1;
            // Console.WriteLine($"start window index is {startWindowIndex}");
            while(queue.Count > 0 && i - queue.First.Value >=k) {
                // Console.WriteLine($"current index {i}, largest value {nums[queue.First.Value]} at index {queue.First.Value} is outside range of {k}: {i}-{queue.First.Value}");
                // Console.WriteLine($"removing {nums[queue.First.Value]} at index {queue.First.Value}");
                queue.RemoveFirst();
            }
            while(queue.Count >0 && nums[queue.Last.Value] <= nums[i]) {
                // Console.WriteLine($"last item {nums[queue.Last.Value]} <= current number {nums[i]}, popping");
                queue.RemoveLast();
            }
            
            queue.AddLast(i);
            // Console.WriteLine($"adding {nums[i]} with index {i} to the end of the queue");
            if (startWindowIndex >=0) {
                // Console.WriteLine($"setting result[{startWindowIndex}] to be { nums[queue.First.Value]}");
                res[startWindowIndex] = nums[queue.First.Value];
            }
        }
        return res;
    }
}
// @lc code=end

