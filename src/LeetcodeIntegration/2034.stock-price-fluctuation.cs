/*
 * @lc app=leetcode id=2034 lang=csharp
 *
 * [2034] Stock Price Fluctuation 
 */

// @lc code=start
public class StockPrice {
    Dictionary<int, int> _map;
    // SortedDictionary<int, HashSet<int>> _priceToTimestampMap; // use sorted set to get past TLE
    SortedList<int, int> _priceToTimestampMap;
    int _current = int.MinValue;


    public StockPrice() {
        _map = new Dictionary<int, int>();
        // _priceToTimestampMap =  new SortedDictionary<int, HashSet<int>>();
        _priceToTimestampMap = new SortedList<int, int>();
    }
    
    public void Update(int timestamp, int price) {
        _current = Math.Max(timestamp, _current);
        if (_map.ContainsKey(timestamp)) {
            var originalPrice = _map[timestamp];
            // _priceToTimestampMap[originalPrice].Remove(timestamp);
            // if ( _priceToTimestampMap[originalPrice].Count ==0) {
            //     _priceToTimestampMap.Remove(originalPrice);
            // }
            _priceToTimestampMap[originalPrice]--;
            if ( _priceToTimestampMap[originalPrice] ==0) {
                _priceToTimestampMap.Remove(originalPrice);
            }
        }
        _map[timestamp] = price;
        if (!_priceToTimestampMap.ContainsKey(price)) {
            // _priceToTimestampMap.Add(price, new HashSet<int>());
            _priceToTimestampMap.Add(price, 0);
        }
        // _priceToTimestampMap[price].Add(timestamp);
        _priceToTimestampMap[price]++;
    }
    
    public int Current() {
       return _map[_current];
    }
    
    public int Maximum() {
        // return _priceToTimestampMap.Keys.Last();
        return _priceToTimestampMap.Keys[_priceToTimestampMap.Count -1];
    }
    
    public int Minimum() {
        // return _priceToTimestampMap.Keys.First();
        return _priceToTimestampMap.Keys[0];
    }
}

/**
 * Your StockPrice object will be instantiated and called as such:
 * StockPrice obj = new StockPrice();
 * obj.Update(timestamp,price);
 * int param_2 = obj.Current();
 * int param_3 = obj.Maximum();
 * int param_4 = obj.Minimum();
 */
// @lc code=end

