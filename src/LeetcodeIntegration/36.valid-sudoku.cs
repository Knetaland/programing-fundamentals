/*
 * @lc app=leetcode id=36 lang=csharp
 *
 * [36] Valid Sudoku
 */

// @lc code=start
public class Solution {
    public bool IsValidSudoku(char[][] board) {
        // m : 0 - 9
        // n : 0 - 9
        // square 3 x 3: (i/3 + i %3), (j/3 + j%3)
        int m = board.Length, n = board[0].Length;
        for(var i = 0; i < m; i++) {
            for(var j =0; j < n; j++) {
                if (!isValid(i,j)) {
                    Console.WriteLine($"invalid at {i}{j}");
                    return false;
                }
            }
        }
        return true;

        bool isValid(int x, int y) {
            Console.WriteLine($"checking at {x}{y}");
            if (board[x][y] == '.') return true;
            Console.WriteLine($"done checking . at {x}{y}");
            for(int i = 0; i < m; i++) { // check column
                if (i != x  && board[i][y] ==board[x][y]) return false;
            }
            Console.WriteLine($"done checking column at {x}{y}");
            for(int j=0;j<n;j++) { // check row
                if (j!=y && board[x][j] == board[x][y]) return false;
            }
            Console.WriteLine($"done checking row at {x}{y}");
            var squareX = x/3; 
            var squareY = y/3;
            Console.WriteLine($"checking square at {squareX}{squareY}");
            // 0,0 -> i= 0 - 2, j -> 0 -> 2
            // 1,1 -> i = 3 - 5, j -> 3-5
            for(var i = 3*squareX; i < 3*squareX+3;i++) {
                for(var j = 3*squareY; j<3*squareY+3;j++) {
                    // Console.WriteLine($"checking square at index {i},{j}");
                    if (i != x && j != y && board[x][y] == board[i][j]) return false;
                }
            }
            return true;
        }
    }
}
// @lc code=end

