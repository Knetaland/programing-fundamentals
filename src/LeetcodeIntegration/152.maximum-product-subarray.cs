/*
 * @lc app=leetcode id=152 lang=csharp
 *
 * [152] Maximum Product Subarray
 */

// @lc code=start
public class Solution {
    public int MaxProduct(int[] nums) {
        int max = nums[0], maxProd = nums[0], minProd = nums[0];
        for(var i = 1; i < nums.Length; i++) {
            var num = nums[i];
            var curMaxProd = maxProd * num;
            var curMinProd = minProd * num;
            maxProd = Math.Max(curMaxProd, Math.Max(curMinProd, num));
            minProd = Math.Min(curMaxProd, Math.Min(curMinProd, num));
            max = Math.Max(maxProd, max);
        }
        return max;
    }
}
// @lc code=end

