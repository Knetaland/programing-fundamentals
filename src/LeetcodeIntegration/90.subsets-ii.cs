/*
 * @lc app=leetcode id=90 lang=csharp
 *
 * [90] Subsets II
 */

// @lc code=start
public class Solution {
    public IList<IList<int>> SubsetsWithDup(int[] nums) {
        Array.Sort(nums);
        var res = new List<IList<int>>();
        backtrack(new List<int>(), 0);
        return res;
        void backtrack(IList<int> list, int start) {
            res.Add(new List<int>(list));

            for(var i =start; i < nums.Length; i++) {
                if (i > start && nums[i] == nums[i-1]) continue; // means had been added before.
                list.Add(nums[i]);
                backtrack(list, i + 1);
                list.RemoveAt(list.Count - 1);
            }
        }
    }
}
// @lc code=end

