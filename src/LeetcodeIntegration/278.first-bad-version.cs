/*
 * @lc app=leetcode id=278 lang=csharp
 *
 * [278] First Bad Version
 */

// @lc code=start
/* The isBadVersion API is defined in the parent class VersionControl.
      bool IsBadVersion(int version); */

public class Solution : VersionControl {
    public int FirstBadVersion(int n) {
        int start = 1, end = n;
        while(start < end) {
            var mid = start + (end-start) /2;
            if (!IsBadVersion(mid)) {
                Console.WriteLine($"{mid} is not bad version");
                start = mid + 1;
            } else {
                Console.WriteLine($"{mid} is BAD version");
                end = mid;
            }

            Console.WriteLine($"left:{start} right:{end}");
        }
        return start; // start or end dont matter, start == end when loop ends
    }
}
// @lc code=end