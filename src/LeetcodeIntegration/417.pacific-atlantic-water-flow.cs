/*
 * @lc app=leetcode id=417 lang=csharp
 *
 * [417] Pacific Atlantic Water Flow
 */

// @lc code=start
public class Solution {

    class Index {
        public int X { get; set; }
        public int Y { get; set; }
        public Index(int x, int y)
        {
            X = x;
            Y = y;
        }
    }

    int [][] DIRECTIONS = new [] {
        new [] {0, 1},
        new [] {0, -1},
        new [] {1, 0},
        new [] {-1, 0},
    };

    public IList<IList<int>> PacificAtlantic(int[][] heights) {
        return DepthFirstSearch(heights);
    }

    public IList<IList<int>> BreadFirstSearch(int[][] heights) {
        var res = new List<IList<int>>();
        var m = heights.Length;
        var n = heights[0].Length;

        var pacificValidIndex = new Queue<Index>();
        var alanticValidIndex = new Queue<Index>();
        var pVisited = new bool[m,n];
        var aVisited = new bool[m,n];

        // all edge indexes are valid
        for(var i = 0; i < m; i++) {
            var indexP = new Index(i, 0);
            pacificValidIndex.Enqueue(indexP);
            pVisited[indexP.X, indexP.Y] = true;

            var indexA = new Index(i, n-1);
            alanticValidIndex.Enqueue(indexA);
            aVisited[indexA.X, indexA.Y] = true;
        }
        for(var i = 0; i < n; i++) {
            var indexP = new Index(0, i);
            pacificValidIndex.Enqueue(indexP);
            pVisited[indexP.X, indexP.Y] = true;

            var indexA = new Index(m-1, i);
            alanticValidIndex.Enqueue(indexA);
            aVisited[indexA.X, indexA.Y] = true;
        }

        bfs(pacificValidIndex, pVisited);
        bfs(alanticValidIndex, aVisited);

        for(var i=0; i< m; i++) {
            for(var j = 0; j < n; j++) {
                if (pVisited[i,j] && aVisited[i,j]) res.Add(new[] { i,j });
            }
        }
        return res;

        void bfs(Queue<Index> queue, bool[,] visited) {
            while(queue.Count > 0) {
                var index = queue.Dequeue();
                foreach(var d in DIRECTIONS) {
                    var x = index.X + d[0];
                    var y = index.Y + d[1];
                    if (
                        (x < 0 || x >=m || y <0 || y >= n) 
                        || (visited[x,y])
                        || heights[x][y] < heights[index.X][index.Y] // going from ocean, so [x,y] needs to be higher than cur index
                    ) {
                        continue;
                    }
                    visited[x,y] = true;
                    Console.WriteLine($"visited {x}, {y}");
                    queue.Enqueue(new Index(x,y));
                }
            }
        }
    }

    public IList<IList<int>> DepthFirstSearch(int[][] heights) {
        var res = new List<IList<int>>();
        var m = heights.Length;
        var n = heights[0].Length;
        var pVisited = new bool[m,n];
        var aVisited = new bool[m,n];
        
        var preHeight = 0; // ocean has no height
        for(var i = 0; i < m; i++) {
            dfs(i, 0, preHeight, pVisited);
            dfs(i, n-1, preHeight, aVisited); 
        }
        for(var i = 0; i < n; i++) {
            dfs(0, i, preHeight, pVisited);
            dfs(m-1, i, preHeight, aVisited); 
        }
         for(var i=0; i< m; i++) {
            for(var j = 0; j < n; j++) {
                if (pVisited[i,j] && aVisited[i,j]) res.Add(new[] { i,j });
            }
        }
        return res;

        

        void dfs(int x, int y, int h, bool[,] visited) {
            // Console.WriteLine($"visited {x}, {y}: {visited[x,y]}, {(x <0 || y<0 ||x>=m || y>=n)}, {heights[x][y] > h }");
            if (
                (x <0 || y<0 ||x>=m || y>=n)
                || visited[x,y]
                || heights[x][y] < h 
            ) return ;
            visited[x,y] = true;
            Console.WriteLine($"visited {x}, {y}");
            var curHeight = heights[x][y];
            dfs(x-1,y,curHeight, visited);
            dfs(x+1,y,curHeight, visited);
            dfs(x,y-1,curHeight, visited);
            dfs(x,y+1,curHeight, visited);
        }
    }
}
// @lc code=end

