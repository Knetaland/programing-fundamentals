/*
 * @lc app=leetcode id=33 lang=csharp
 *
 * [33] Search in Rotated Sorted Array
 */

// @lc code=start
public class Solution {
    public int Search(int[] nums, int target) {
        int left = 0, right = nums.Length-1;

        while(left <= right) {
            var mid = left + (right - left)/2;
            if (nums[mid] == target) return mid;
            /*
            If the entire left part is monotonically increasing, which means the pivot point is on the right part
                If left <= target < mid ------> drop the right half
                Else ------> drop the left half
            If the entire right part is monotonically increasing, which means the pivot point is on the left part
                If mid < target <= right ------> drop the left half
                Else ------> drop the right half
            */
            if (nums[left] <= nums[mid]) { // left is sorted, pivot point on the right
                if (target >= nums[left] && target < nums[mid]) right = mid -1; // target in between left and mid, drop right half
                else left = mid + 1;
            } else {
                if (target > nums[mid] && target <= nums[right]) left = mid + 1; // target in between mid and right, drop the left half
                else right = mid -1;
            }
        }

        return -1;
    }
}
// @lc code=end

