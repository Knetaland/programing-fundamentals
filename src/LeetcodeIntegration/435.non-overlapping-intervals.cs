/*
 * @lc app=leetcode id=435 lang=csharp
 *
 * [435] Non-overlapping Intervals
 */

// @lc code=start

// https://www.youtube.com/watch?v=nONCGxWoUfM

// Tag: Scan and sort
public class Solution {
    public int EraseOverlapIntervals(int[][] intervals) {
        if (intervals == null  || intervals.Length == 0) return 0;
        Array.Sort(intervals, (a, b) => a[0] - b[0]); // sort by start

        var preEnd = intervals[0][1];
        var removeCount = 0;
        for ( var i = 1; i < intervals.Length; i++) {
            var cur = intervals[i];
            // check overlapping
            if (cur[0] < preEnd) {
                removeCount++;
                preEnd = Math.Min(cur[1], preEnd);
            } else { // no overlap
                preEnd = cur[1];
            }
        }
        return removeCount;
    }
}
// @lc code=end

