/*
 * @lc app=leetcode id=19 lang=csharp
 *
 * [19] Remove Nth Node From End of List
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int val=0, ListNode next=null) {
 *         this.val = val;
 *         this.next = next;
 *     }
 * }
 */
public class Solution {
    public ListNode RemoveNthFromEnd(ListNode head, int n) {
        var prevElement = FindNthFromEnd(head, n+1);
        if (prevElement == null) return head.next; // remove first item
        prevElement.next = prevElement.next.next;
        return head;
    }
    private ListNode FindNthFromEnd(ListNode head, int n) {
        var p1 = head;
        for(var i = 0; i < n; i++) {
            if (p1 == null) return null;
            p1 = p1.next;
        }
        var p2 = head;
        while(p1 != null) {
            p1 = p1.next;
            p2 = p2.next;
        }
        return p2;
    }
}
// @lc code=end

