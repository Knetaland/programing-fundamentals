/*
 * @lc app=leetcode id=207 lang=csharp
 *
 * [207] Course Schedule
 */

// @lc code=start
public class Solution
{
    public bool CanFinish(int numCourses, int[][] prerequisites) {
        var visited = new HashSet<int>();
        var adjList = new Dictionary<int, HashSet<int>>();
        for(var i = 0; i < numCourses; i++) {
            adjList.Add(i, new HashSet<int>());
        }
        foreach(var prereq in prerequisites) {
            adjList[prereq[1]](prereq[0]);
        }

        for (var i = 0; i < numCourses; i++) {
            if (!CanFinish(i)) return false;
        }

        return true;

        bool CanFinish(int course) {
            if (visited.Contains(course)) return false; // detecting a cycle
            if (adjList[course].Count == 0) return true;
            visited.Add(course);
            foreach(var v in adjList[course]) {
                if (!CanFinish(v)) return false;
                adjList[course].Remove(v);
            }
            visited.Remove(course);
            return true;
        }
    }

    public bool CanFinish_BFS(int numCourses, int[][] prerequisites)
    {
        var adjList = new Dictionary<int, List<int>>();
        var indegree = new int[numCourses];
        foreach (var prereq in prerequisites)
        {
            if (!adjList.ContainsKey(prereq[1]))
            {
                adjList.Add(prereq[1], new List<int>());
            }
            indegree[prereq[0]]++;
            adjList[prereq[1]].Add(prereq[0]);
        }

        var queue = new Queue<int>();
        for (var i = 0; i < numCourses; i++)
        {
            if (indegree[i] == 0)
            {
                queue.Enqueue(i);
            }
        }

        var finishedCourses = 0;
        while(queue.Count > 0 ) {
            var pre = queue.Dequeue();
            finishedCourses++;
            // Console.WriteLine($"{finishedCourses} out of {numCourses}: {pre}");
            if (adjList.ContainsKey(pre)) {
                foreach(var course in adjList[pre]) {
                    indegree[course]--;
                    if (indegree[course] == 0) {
                        queue.Enqueue(course);
                    }
                }
            }
        }

        return finishedCourses == numCourses;
    }


}
// @lc code=end

