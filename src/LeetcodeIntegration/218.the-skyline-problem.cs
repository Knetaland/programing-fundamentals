/*
 * @lc app=leetcode id=218 lang=csharp
 *
 * [218] The Skyline Problem
 */

// @lc code=start
public class Solution {
    public IList<IList<int>> GetSkyline(int[][] buildings) {
        var res = new List<IList<int>>();

        var points = new List<int[]>();
        foreach(var building in buildings) {
            // 0: start 1: end 2: height
            points.Add(new[] {building[0], -building[2]}); // treat it as x plane in the sky, x = height
            points.Add(new[] {building[1], building[2]}); // treat it as x plane landed, x = height
        }

        var preMax = 0;
        points.Sort((a, b) => a[0] == b[0] ? a[1] - b[1] : a[0] - b[0]);
        var set = new SortedList<int, int>();
        // set.Add(0);

        foreach(var p in points) {
            if (p[1] < 0) {
                set.Add(-p[1], p[0]); // new height, add to sorted set
            } else {
                set.Remove(p[1]);  // end of house, remove height
            }

            var curMax = set.Max;
            if (curMax != preMax) {
                res.Add(new List<int> { p[0], curMax });
                preMax = curMax;
            }
        }
        return res;
    }
}
// @lc code=end

