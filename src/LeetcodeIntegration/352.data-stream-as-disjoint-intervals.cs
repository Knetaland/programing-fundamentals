/*
 * @lc app=leetcode id=352 lang=csharp
 *
 * [352] Data Stream as Disjoint Intervals
 */

// @lc code=start
public class SummaryRanges {
    private List<int[]> _intervals;
    public SummaryRanges() {
        _intervals = new List<int[]>();
    }
    
    public void AddNum(int val) {
        var cur = new [] { val, val };
        var temp = new List<int[]>();
        foreach(var interval in _intervals) {
            if (cur[1] + 1 < interval[0]) { // fall on left
                temp.Add(cur);
                cur = interval;
            } else if (cur[0] > interval[1] + 1) { // fall on right
                temp.Add(interval);
            } else { // overlap, merge
                cur[0] = Math.Min(interval[0], cur[0]);
                cur[1] = Math.Max(interval[1], cur[1]);
            }
        }
        temp.Add(cur);
        _intervals = temp;
    }
    
    public int[][] GetIntervals() {
        return _intervals.ToArray();
    }
}

/**
 * Your SummaryRanges object will be instantiated and called as such:
 * SummaryRanges obj = new SummaryRanges();
 * obj.AddNum(val);
 * int[][] param_2 = obj.GetIntervals();
 */
// @lc code=end

