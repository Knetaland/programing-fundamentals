using System;
using System.Collections.Generic;
using Fundamental.Core.Basic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamentals.Companies.Door;
using Fundamentals.Companies.Facebook;
using Fundamentals.Companies.Misc;
using Fundamentals.Companies.Namely;
using Fundamentals.Companies.Plaid;
using Fundamentals.Companies;
using LeetCode;
using LeetCode.BackTracking;
using LeetCode.BFS;
using LeetCode.BinarySearch;
using LeetCode.DP;
using LeetCode.Graph;
using LeetCode.Sorting;
using LeetCode.Tree;
using Fundamentals.Companies.Datadog;
// using Fundamentals.Companies.CloudK;
using LeetCode.SlidingWindow;
using Fundamentals.Companies.Hopper;
using Fundamentals.Companies.Google;
using Fundamentals.Companies.SQ;
using Fundamentals.Companies.ClearMe;
using Fundamentals.Companies.Amazon;

namespace Fundamentals
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Starting app...");
            var problems = new List<IProblem>
            {
// FACEBook
            //    new IntToEnglish(),
                //new IsPalindrome(),
                //  new PrintAllWords(),
                //  new IsFamous(),
                // new Q68_TextJustification(),
                // new Q415_AddStrings(),
                // new QuickSort(),
                // new Q215_KthLargestElementInArray(),
                // new Q350_IntersectionOfTwoArrays_II(),
                // new Q414_ThirdMaximumNumber(),
                // new Q560_SubarraySumEqualsToK(),
                // new Q325_MaximumSizeSubarraySumEqualsToK(),
                // new Q273_IntegerToEnglishWords(),
                // new Q498_DiagonalTraverse(),
                // new Q426_BSTToSortedDoublyLinkedList(),
                // new Q269_AlienDictionary(),
//----------------------------------------
// Flatiron Health
                // new AirPlaneRow(),

//--------------------Namely-----------------
// new Employee_Coverage(),

// --------------------------
// new ProductsOfAllNumbersInArray(),
                //new Q1_TwoSum()
                // new Q2_AddTwoNumbers()
                // new Q3_LongestSubstringWithoutRepeatedChar()
                //new Q4_Median(),
                // new Q5_LongestPalindrom()    
                // new Q6_ZigzagConversion()
                 //new Q7_ReverseInteger(),
                // new Q8_Atoi()
                // new Q9_IsIntegerPalindrome()
                // new Q11_ContainerWithMostWater()
                //new Q13_RomanToInteger(),
                // new Q15_ThreeSum()
            //    new Q17_LetterToStrings(),
                // new Q18_FourSum()
                //new Q19_RemoveNthNodeFromEnd(),
                // new Q22_GenerateParentheses()
            //    new Q23_MergeKSortedLists()
               //new Q28_FindNeedleInHaystack(),
                // new Q29_DivideTwoIntegers(),
                // new Q31_NextPermutation(),

                // new Q38_CountAndSay()
                //  new Q40_CombinationSum2()
               // new Q41_FirstMissingPositive()
               // new Q38_CountAndSay()
               // new Q40_CombinationSum2()
            //    new Q43_MultiplyStrings(),
               //new Q76_MinWindowSubstring()
                //  new Q50_Pow()
                // new Q69_SquareRoot()
                
               // new  Q44_WildcardMatching()
               // new Q45_JumpGameII()
            //    new Q46_Permutation(),
            // new Q47_Permutation_II(),
               // new Q58_MergeIntervals()
            //    new Q66_PlusOne(),
              // new Q67_AddBinary()
              //  new Q283_MoveZero()
            //    new Q314_BinaryTreeVerticalOrder()
            //    new Q78_SubSets(),
            //   new Q200_NumberOfIslands()
            //   new Q206_ReverseLinkedList()
               //new Q252_MeetingRooms()
            //    new Q253_MeetingRoomsII()
               // new Q211_AddAndSearchWord()
            //    new Q239_SlidingWindowMaximum(),
               
            //    new Q297_SerializeDeSerializeBinaryTree(),
               //new Q300_LongestIncreasingSubsequence()
            //    new Q322_CoinChange(),
               //new QD_CuttingRods()
               // new Q377_CombinationSum_IV()
            //    new Q352_DataStreamAsJoinIntervals()
              // new Q1003_TaskScheduler_II()    
              //new Q190_ReverseBits(),
              //new Q387_FirstUniqueCharInString(),
              //new Q242_ValidAnagram(),
              //new Q8_Atoi(),
              //new Q104_MaxDepthOfBinaryTree(),
              //new Q102_TreeLevelTraversal(),
              //new Q412_FizzBuzz(),
              //new Q454_FourSum_II(),
            //   new Q77_Combinations(),
            // new Q216_CombinationSum_III(),
            // new Q784_LetterCasePermutation(),
            // new Q811_SubdomainVisitedCount(),
            // new Q819_MostCommonWord(),
            // new Q53_MaxSubArray(),
            // new Q152_MaxProductSubarray(),
            // new Q727_MinimumWindowSubsequence(),
            // new Q923_3SumWithMultiplicity(),
            // new Q127_WordLadder(),
            // new Q126_WordLadder_II(),
            // new Q417_PacificAtlanticWaterFlow(),
            // new Q211_MaximalSquare(),
            //new FindRectangle(),
            // new Q718_MaximumLengthOfRepeatedSubArray()
            // new Q1305_AllElementsInTwoBinarySearchTrees(),
            // new Q261_GraphValidTree(),
            // new Q207_CourseSchedule(),
            // new Q71_SimplifyPath()
            // new Q689_MaximumSumOf3NonOverlappingSubarrays(),
            // new Q282_ExpressionAddOperations(),
            // new Q285_InOrderSuccessorInBST()
            // new Q311_SparseMatrixMultiplication()
            // new Q490_Maze(),
            // new Q505_Maze_II()
            // new Q863_AllNodesDistanceKInBinaryTree()
            // new Q246_StrobogrammaticNumber(),
            // new Q247_StrobogrammicNumber_II()
            // new Q241_DifferentWaysToAddParentheses(),
            // new Q378_KthSmallestElementInASortedMatrix()
            // new Q658_FindKClosestElements(),
            // new Q621_TaskScheduler()
            // new Q358_ReArrangeStringsKDistanceApart(),
            // new Q210_Q207_CourseSchedule()
            // new FindParentNodeInBinaryTree(),
            // new Q137_SingleNumber_II()
            // new Q56_MergeIntervals(),
            // new Q494_TargetSum(),
            // new Q236_LowestCommonAncestorOfBinaryTree()
            // new Q113_PathSum_II(),
            // new Q536_Construct_Binary_Tree_From_String(),
            // new CentralBank(),
            // ---- Plaid ---
            // new FindSameTransaction(),
            // new Q875_Koko_Eating_Bananas()
            // new ListBusinessHours(),
            // new Same_N_Ary_Tree()
            // new Q859_Buddy_Strings()
            // new Q1272_RemoveInterval(),
            // new Q1229_MeetingScheduler(),
            // new Q759_EmployeeFreeTime(),
            // new GEMINI(),
            // new DATADOG(),
            // new CloudK_T(),
            // new Find_Menu_diff()
            // new Q295_FindMedianFromDataStream(),
            // new Q159_Longest_Substring_With_Two_Disctinc_Characters(),
            // new Find_Disjoint_Tags(),
            // new Q366_FindBinaryTreeLeavesGoogle(),
            // new Q230_FindKthSmallest(),
            // new Q567_PermutationInString(),
            // new Q1109_FlightBookings(),
            // new Q2034_StockPriceFluctuation_Google(),
            // new Hopper(),
            // new GoogleQs(),
            // new Q946_ValidateStackSequence()
            // new Q1152_TrackUser(),
            // new Q2768_NumOfBlocks(),
            // new Q49_GroupAnagams(),
            // new Q347_TopKFrequentElement()
            // new Q503_NextGreaterElement_II()
            // new Q739_DailyTemperature()
            // new Q316_Remove_Duplicate_Letters()
            // new Q42_TrappingRainWater()
            // new Q224_BasicCalculator()
            // new Q463_IslandPerimeter()
            // new Q305_Num_Of_Island_II()
            // new Q547_Num_Of_Provinces()
            // new Q128_Longest_Consecutive_Sequence()
            // new Q694_Num_Distinct_Islands()
            // new Q1254_Num_Closed_Islands()
            // new Q428_Serialize_And_Deserialize_N_ary_Tree()
            // new Q449_Serialize_Deserialize_BST()
            // new Q270_ClosestBinaryTreeValue()
            // new Q408_Valid_Word_Abbr()
            // new Q227_BasicCalculatorII(),
            // new Q1762_Buildings_With_Ocean_View()
            // new Q384_Shuffle_An_Array()
            // new Q1676_LCA_Of_Binary_Tree_IV()
            // new Q1091_Shortest_Path_In_Binary_Matrix()
            // new Q1570_Dot_Product_Of_2_Sparse_Vectors()
            // new Q708_Insert_To_Circular_Linkedlist()
            // new Q1522_Diameter_Of_Nary_Tree()
            // new Q163_Missing_Ranges(),
            // new Q162_Find_Peak_Element()
            // new Q1448_Count_Good_Nodes_In_Binary_Tree()
            // new Q2265_Count_Nodes_Equal_To_Average_of_Subtree()
            // new Find_Nearest_Smaller_Num_On_The_Left()
            // new Q1358_Num_Of_Substrings_Containing_All_Three_Chars()
            // new Q310_Min_Height_Trees()
            // new Q339_Nested_List_Weight_Sum()
            // new Q827_Making_A_Large_Island()
            // new Q1644_LCA_of_Binary_Tree_II()
            // new Q129_Sum_Root_To_Leaf_Numbers()
            // new Q670_Maximum_Swap()
            // new GameScoring()
            // new TimeInOffice()
            // new TextEditor()
            // new ItemPrice()
            // new Connect4()
            // new ChickenFoxProblem()
            // new Find_Max_Meeting_Rooms()
            // new Q121_Stock_I()
            // new Q266_Palindrome_Permutation()
            // new Q958_Check_Completeness_Of_A_BST()
            // new Min_Cost_Flight_Round_Trip()
            // new Q249_Group_Shifted_Strings()
            // new Q17_LetterCombinationsOfAPhoneNumber()
            // new FamilyTreeProblem()
            // new EventEmmiterProblem()
            // new PairCoding()
            // new Q140_WordBreak_II(),
            // new Q523_ContinuousSubarraySum(),
            // new Q301_Remove_Invalid_Parentheses()
            // new Q1004_Consecutive_Zeros_III()
            // new Q332_Recontruct_Itinerary()
            // new Q480_Sliding_Window_Median()
            // new ShortestPathForDashMart()
            // new Q286_WallsAndGates()
            // new Q994_Rotting_Oranges()
            // new Q543_DiameterOfBinaryTree()
            // new Q84_LargestRectangleArea()
            // new TechScreen()
            // new Q828_Count_Unique_Chars_Of_Substrings()
            new OAs()
            //---------------


            // new Playground()
            
            };
            using (Lib.UseTimer())
            {
                foreach (var p in problems)
                {
                    Console.WriteLine("------------------------------------------------------------------");
                    p.Run();
                }
                Console.WriteLine("------------------------------------------------------------------");
            }
            //Console.WriteLine("Finished Running");
            // Console.ReadLine();
        }
    }
}
