using System.Collections.Generic;
using System.Text;

namespace Fundamentals.Companies.Google {

    public class Q833_FindAndReplaceString {
        public string FindReplaceString(string s, int[] indices, string[] sources, string[] targets) {
            var res = new StringBuilder();
            var sourceMap = new Dictionary<int, string>();
            var targetMap = new Dictionary<int, string>();
            for(var k=0; k<  indices.Length; k++) {
                var index = indices[k];
                sourceMap.Add(index, sources[k]);
                targetMap.Add(index, targets[k]);
            }

            int i = 0;
            while(i < s.Length) {
                if (sourceMap.ContainsKey(i) && s.Substring(i).StartsWith(sourceMap[i])) {
                    res.Append(targetMap[i]);
                    i += sourceMap[i].Length;
                } else {
                    res.Append(s[i]);
                    i++;
                }
            }
            return res.ToString();
        }
    }
}
