using System;
using System.Collections.Generic;
using System.Linq;
namespace Fundamentals.Companies.Google {
    public class Q679_TwentyFour {
        public bool JudgePoint24(int[] cards) {
            var list = cards.Select(c => (double) c).ToList();
            var res = dfs(list);
            return res;
        }

        private bool dfs(List<double> list) {
            if (list.Count == 1) {
                if (Math.Abs(list[0] - 24) < 0.001) return true;
                else return false;
            }

            for(var i = 0; i < list.Count; i++) {
                for(var j = i+1; j< list.Count;j++) {
                    var nextRound = new List<double>();
                    for(var k = 0; k < list.Count; k++) {
                        if (k == i || k == j) continue;
                        nextRound.Add(list[k]); //  add the rest of cards to next round, always start from 0 for permutation because of parenthesis
                    }
                    var possibleValues = Compute(list[i], list[j]); // compute all possibilities of current 2 cards
                    foreach(var value in possibleValues) {
                        nextRound.Add(value);
                        if (dfs(nextRound)) return true;
                        else nextRound.RemoveAt(nextRound.Count -1); // possible value doesn't work out, pop and try next possible value
                    }
                }
            }
            return false;
        }

        private IList<double> Compute(double a, double b) {
            return new List<double> {a+b, a-b, b-a, a*b, a/b, b/a}; 
        }


        public bool judgePoint24_DivideConq(int[] nums) {
            int n = nums.Length;
            if (n != 4) return false;
            Array.Sort(nums);
            List<List<int>> allPermutations = new List<List<int>>();  //permuteUnique(nums);
            foreach (var list in allPermutations) {
                List<Double> result = generateNumbers(list.ToArray(), 0, 3);
                foreach (var d in result)
                    if (Math.Abs((d - (double) 24)) < 0.000001)
                        return true;
            }

            return false;
        }

        private List<Double> generateNumbers(int[] nums, int start, int end) {
            List<Double> res = new List<double>();
            if (start == end) {
                res.Add((double) nums[start]);
            } else if (start < end) {
                for (int i = start; i < end; ++i) {
                    List<Double> leftRes = generateNumbers(nums, start, i);
                    List<Double> rightRes = generateNumbers(nums, i + 1, end);
                    foreach(var left in leftRes) {
                        foreach (double right in rightRes) {
                            res.Add(left + right);
                            res.Add(left - right);
                            res.Add(left * right);
                            if (right != 0) res.Add(left / right);
                        }
                    }
                }
            }
            return res;
        }

    }
}