using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace Fundamentals.Companies.Google
{
    public class GoogleQs : IProblem
    {
         string[] GetWords(string input)
        {
            var matches = Regex.Matches(input, @"\b[\w']*\b");

            var words = from m in matches.Cast<Match>()
                        where !string.IsNullOrEmpty(m.Value)
                        select m.Value.Trim();

            return words.ToArray();
        }
        // Time: O(m*n), Space: O(1)
        int[,] FindDistanceFromBoundries_DP(char[][] grid) {
            var m = grid.Length;
            var n = grid[0].Length;
            var INF = m + n;
            var res = new int[m,n];

            for(var i = 0; i < m; i++) {
                for(var j = 0; j< n; j++) {
                    if (grid[i][j] != BOUNDARY) {
                        res[i,j] = INF;
                    }
                }
            }

            // from top left
            for(var i = 0; i < m; i++) {
                for(var j = 0; j< n; j++) {
                    if (grid[i][j] != BOUNDARY) {
                        var left = j-1 < 0 ? INF : res[i, j-1];
                        var top = i -1 < 0 ? INF : res[i-1, j];
                        res[i, j] = Math.Min(left, top) + 1;
                    }
                }
            }

            // from bottom right
            for(var i = m-1; i >=0; i--) {
                for(var j = n-1; j>=0; j--) {
                    if (grid[i][j] != BOUNDARY) {
                        var bottom = i + 1 == m ? INF : res[i+1, j];
                        var right = j + 1 == n ? INF : res[i, j + 1];
                        res[i, j] = Math.Min(res[i,j], Math.Min(bottom, right) + 1);
                    }
                }
            }

            return res;
        }

        // Time: O(m*n), Space: O(m*n) : queue size
        int[,] FindDistanceFromBoundries(char[][] grid) {
            var m = grid.Length;
            var n = grid[0].Length;
            var queue = new Queue<int[]>();
            var visited = new bool[m,n];
            var res = new int[m,n];
            for(var i = 0; i < m; i++) {
                for(var j = 0; j< n; j++) {
                    if (grid[i][j] == BOUNDARY) {
                        queue.Enqueue(new[] {i, j});
                        visited[i,j] = true;
                    }
                }
            }

            while(queue.Count > 0) {
                var pos = queue.Dequeue();
                var x = pos[0];
                var y = pos[1];
                foreach(var dir in DIRS) {
                    var nextX = x + dir[0];
                    var nextY = y + dir[1];
                    if (nextX >=0 && nextY >=0 && nextX < m && nextY < n && !visited[nextX,nextY]) {
                        res[nextX,nextY] = res[x,y]+1;
                        queue.Enqueue(new[] {nextX, nextY});
                        visited[nextX, nextY] = true;
                    }
                }
            }
            return res;
        }

        char[][] FindBoundaries(char[][] grid) {
            var m = grid.Length;
            var n = grid[0].Length;
            for(var i = 0; i < m; i++) {
                for(var j = 0; j< n; j++) {
                    if (grid[i][j] == FOREGROUND) {
                        if (isBoundary(i, j)) {
                            grid[i][j] = BOUNDARY;
                        }
                    }
                    }
            }
            return grid;

            bool isBoundary(int x, int y) {
                return isBackground(x-1, y) || isBackground(x+1, y) || isBackground(x, y-1) || isBackground(x, y+1);
            }

            bool isBackground(int x, int y) {
                if (x <0 || y< 0 || x >= m || y >= n) return false;
                return grid[x][y] == BACKGROUND;
            }
        }

        // O(N), space: O(N)
        IList<IList<int>> FindLeaves(TreeNode root) {
            var res = new List<IList<int>>();
            // var count = 0;
            dfs(root);
            // Console.WriteLine($"count called {count} times");
            return res;
            

            int dfs(TreeNode node) {
                if (node == null) return -1;
                // count++;
                var left = dfs(node.left);
                var right = dfs(node.right);
                var height = Math.Max(left, right) + 1;
                if (res.Count == height) {
                    res.Add(new List<int>());
                }
                res[height].Add(node.val);
                return height;
            }
        }

        IList<IList<int>> RemoveLeaves(TreeNode root) {
            var res = new List<IList<int>>();
            while(root != null) {
                var list = new List<int>();
                root = dfs(root, list);
                res.Add(list);
            }
            return res;

            TreeNode dfs(TreeNode node, IList<int> list) {
                if (node == null) return null;
                if (node.left == null && node.right == null) {
                    list.Add(node.val);
                    return null;
                }
                node.left = dfs(node.left, list);
                node.right = dfs(node.right, list);
                return node;
            }
        }

        IList<IList<int>> RemoveLeavesParentFirst(TreeNode root) {
            var res = new List<IList<int>>();
            var list = new List<int>();
            root = RemoveLeaves(root, list);
            Console.WriteLine($"is root empty now? {root == null}");
            return res;

            TreeNode RemoveLeaves(TreeNode node, IList<int> list) {
                if (node == null) return null;
                if (node.left == null && node.right == null) {
                    Console.WriteLine($"leaf node detected: {node.val}");
                    // list.Add(node.val);
                    return null;
                }
                node.left = RemoveLeaves(node.left, list);
                node.right = RemoveLeaves(node.right, list);
                res.Add(new List<int>(list));
                res.Add(new List<int> {node.val});
                list.Clear();
                Console.WriteLine($"parent node {node.val} now has no leaf.");
                return null;
            }
        }


        string GuessRightOrWrong(string answer, string guess) {
            // no-repeat
            // var set = new HashSet<char>(answer);
            var map = new Dictionary<char, int>();
            for(var i = 0; i < answer.Length; i++) {
                map.Add(answer[i], i);
            }
            var res = new StringBuilder();
            for(var i=0; i < guess.Length; i++) {
                var c = guess[i];
                if (!map.ContainsKey(c)) {
                    res.Append("B");
                } else {
                    res.Append(map[c] == i ? "R" : "Z");
                }
            }
            return res.ToString();
        }

        /*
        followup：目标字符串有重复字母，且规则发生变动,（猜测的字和目标的字从左往右最近的字匹配），
        每次当 猜测的字和目标的字发生匹配，无论猜没猜中位置，都会把目标字给蒙住，之后不再能使用。
        */
        string GuessRightOrWrong_FollowUp(string answer, string guess) { 
            var map = new Dictionary<char, SortedSet<int>>();
            for(var i = 0; i < answer.Length; i++) {
                if(!map.ContainsKey(answer[i])) {
                    map.Add(answer[i], new SortedSet<int>());
                }
                map[answer[i]].Add(i);
            }

            var res = new StringBuilder();
            for(var i=0; i < guess.Length; i++) {
                var c = guess[i];
                if (!map.ContainsKey(c)) {
                    res.Append("B");
                } else {
                    var atRightPos = map[c].Contains(i);
                    if (atRightPos) {
                        res.Append('R');
                        map[c].Remove(i);
                    }
                    else {
                        res.Append('Z');
                        map[c].Remove(map[c].Min);
                    }
                    if (map[c].Count == 0) {
                        map.Remove(c);
                    }
                }
            }
            return res.ToString();
        }

        const char BACKGROUND = '0';
        const char FOREGROUND = '1';
        const char BOUNDARY = 'b';

        private static int[][] DIRS = new int[][] {
            new [] {1, 0},
            new [] {-1, 0},
            new [] {0, 1},
            new [] {0, -1},
        };

         public void Run() {
            // FindBoundariesProblem();
            // FindLeavesProblem();
            // GuessStringProblem();
            // GetFileSizeProblem();
            FindFailedPairsProblem();
        }


        private void FindFailedPairsProblem() {
            var tests = "12345678";
            var testEngine = new TestRunner(new[] {"81"});
            var res = testEngine.FindFailedTestsPairs(tests);
            foreach(var r in res) {
                Console.WriteLine($"pair {r}");
            }
        }

        private void GetFileSizeProblem() {
            var root = new GFileSystem();
            root.AddFile(new GFileSystem(500));

            var folder = new GFileSystem();
            folder.AddFile(new GFileSystem(100));
            folder.AddFile(new GFileSystem(300));

            var subfolder = new GFileSystem();
            subfolder.AddFile(new GFileSystem(50));
            folder.AddFile(subfolder);

            root.AddFile(folder);

            var res = root.GetTotalSize();
            Console.WriteLine($"total file size is {res}");
        }

        private void GuessStringProblem() {
            var answer = "ABBABA";
            var guess  = "AAAABA";
            // var res = GuessRightOrWrong(answer, guess);
            var res = GuessRightOrWrong_FollowUp(answer, guess);
            Console.WriteLine($"Guess game res: {res}");
        }
        private void FindBoundariesProblem() {
            var map = new char[][] {
                new [] { '0', '0', '0', '0', '0', '0' },
                new [] { '0', '1', '1', '1', '0', '0' },
                new [] { '0', '1', '1', '1', '1', '0' },
                new [] { '0', '1', '1', '1', '0', '0' },
                new [] { '0', '0', '0', '1', '0', '0' },
            };
            var grid = FindBoundaries(map);
            foreach(var row in grid) {
                Console.WriteLine($"[{string.Join(" ", row)}]");
            }

            // var distances = FindDistanceFromBoundries(grid);
            var distances = FindDistanceFromBoundries_DP(grid);

            for(var i =0; i < grid.Length; i++) {
                Console.Write($"[");
                for(var j = 0; j < grid[0].Length; j++) {
                    Console.Write($"{distances[i,j]},");
                }
                Console.Write($"]");
                Console.WriteLine($"");
            }
        }

        private void FindLeavesProblem() {
            var root = new TreeNode(1) {
                left = new TreeNode(2) {
                    left = new TreeNode(4){
                        left = new TreeNode(7),
                        right = new TreeNode(9),
                    },
                    right = new TreeNode(5) {
                        left = new TreeNode(6),
                        right = new TreeNode(8) {
                        }
                    }
                },
                right = new TreeNode(3)
            };
            var res = FindLeaves(root);
            var linkedList = new LinkedList<int>();
            var ss = linkedList.AddFirst(1);
            var s = linkedList.First;
            // var res = RemoveLeavesParentFirst(root);
            foreach(var r in res) {
                Console.WriteLine($"{string.Join(",", r)}");
            }
        }
    }
}