using System;

namespace Fundamentals.Companies.Google {

    /*
    https://leetcode.com/problems/random-pick-with-weight/description/

    You are given a 0-indexed array of positive integers w where w[i] describes the weight of the ith index.

You need to implement the function pickIndex(), which randomly picks an index in the range [0, w.length - 1] (inclusive) and returns it. The probability of picking an index i is w[i] / sum(w).

For example, if w = [1, 3], the probability of picking index 0 is 1 / (1 + 3) = 0.25 (i.e., 25%), and the probability of picking index 1 is 3 / (1 + 3) = 0.75 (i.e., 75%).
 

Example 1:

Input
["Solution","pickIndex"]
[[[1]],[]]
Output
[null,0]

Explanation
Solution solution = new Solution([1]);
solution.pickIndex(); // return 0. The only option is to return 0 since there is only one element in w.
Example 2:

Input
["Solution","pickIndex","pickIndex","pickIndex","pickIndex","pickIndex"]
[[[1,3]],[],[],[],[],[]]
Output
[null,1,1,1,1,0]

Explanation
Solution solution = new Solution([1, 3]);
solution.pickIndex(); // return 1. It is returning the second element (index = 1) that has a probability of 3/4.
solution.pickIndex(); // return 1
solution.pickIndex(); // return 1
solution.pickIndex(); // return 1
solution.pickIndex(); // return 0. It is returning the first element (index = 0) that has a probability of 1/4.

Since this is a randomization problem, multiple answers are allowed.
All of the following outputs can be considered correct:
[null,1,1,1,1,0]
[null,1,1,1,1,1]
[null,1,1,1,0,0]
[null,1,1,1,0,1]
[null,1,0,1,0,0]
......
and so on.
 

Constraints:

1 <= w.length <= 104
1 <= w[i] <= 105
pickIndex will be called at most 104 times.
    */

    public class Q528_RandomNumberGenerator {

    }

    public class RandomWeightPicker {
        private int[] prefixSum;
        private Random random = new Random();

        // O(N)
        public RandomWeightPicker(int[] w) {
            prefixSum = new int[w.Length];
            for(var i = 0; i < w.Length; i++) {
                if (i == 0) prefixSum[i] = w[i];
                else {
                    prefixSum[i] = prefixSum[i-1] + w[i];
                }
            }
        }
        
        // O(logN)
        public int PickIndex() {
            // random between [1, prefixSum[len]]
            var target = random.Next(prefixSum[prefixSum.Length-1]) + 1; // because the weight >=1, random.next is exclusive so +1 makes sure it will not be 0

            int left = 0, right = prefixSum.Length; // legth or legth-1 dont matter here
            while(left < right) {
                var mid = left + (right - left) /2;
                if (prefixSum[mid] == target) {
                   return mid;
                } else if (prefixSum[mid] < target) {
                    left = mid +1;
                } else {
                    // 向左找，因为当前mid的值大于target，可能是"第一个大于target"的值，所以不能丢弃mid
                    // 如果mid的值不再需要了(最终不会取到现在的mid)，那么就可以right=mid-1；
                    right = mid;
                }
            }
            return left; //either left or right works, when exiting the while loop, left == right
        }
    }
}