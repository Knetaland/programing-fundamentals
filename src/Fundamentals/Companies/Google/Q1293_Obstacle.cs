using System;
using System.Collections.Generic;

public class Q1293_Obstacle {
    public int shortestPath(int[][] grid, int k) {
        int step = 0;
        int m = grid.Length;
        int n = grid[0].Length;
        int[][] DIRS = new int [][] {
            new[] {1, 0},
            new[] {-1, 0},
            new[] {0, 1},
            new[] {0, -1},
        };
        int[,] seen = new int[m, n]; // min obstacles elimination from (0,0) to (x, y)
        for (int i = 0; i < m; i++) {
             for (int j = 0; j < n; j++) {
                 seen[i,j] = int.MaxValue;
            }
        }
        Queue<int[]> q = new Queue<int[]>();
        q.Enqueue(new int[]{0, 0, 0}); // position , obstacle detroyed
        seen[0,0] = 0;
        while (q.Count > 0) {
            int size = q.Count;
            while (size > 0) {
                int[] cur = q.Dequeue();
                size--;
                if (cur[0] == m - 1 && cur[1] == n - 1) {
                    return step;
                }
                foreach (var dir in DIRS) {
                    int x = dir[0] + cur[0];
                    int y = dir[1] + cur[1];
                    if (x < 0 || x >= m || y < 0 || y >= n) {
                        continue;
                    }
                    int obstacles = grid[x][y] + cur[2];  // 1 is obstacle, unchanged if not 
                    if (obstacles >= seen[x,y] || obstacles > k) {
                        continue;
                    }
                    seen[x,y] = obstacles;
                    q.Enqueue(new int[]{x, y, obstacles});
                }
            }
            ++step;
        }
        return -1;  
    }
}