using System;
using System.Collections.Generic;
using System.Text;

namespace Fundamentals.Companies.Google
{
    public class GFileSystem
    {
        private int _size;
        private List<GFileSystem> _files;
        public GFileSystem(int size = 0) {
            this._size = size;
            this._files = new List<GFileSystem>();
        }

        public void AddFile(GFileSystem file) {
            _files.Add(file);
        }

        public int GetFileSize() {
            return _size;
        }

        public bool IsFile() {
            return _size > 0;
        }

        public IList<GFileSystem> GetFiles() {
            return _files;
        }

        public int GetTotalSize() {
            var cur = this;
            var size = 0;
            dfs(cur);
            return size;
            void dfs(GFileSystem cur) {
                if (cur.IsFile()) {
                    size += cur.GetFileSize();
                    return ;
                }
                foreach(var file in cur.GetFiles()) {
                    dfs(file);
                }
            }
        }
    }

    public class TestRunner {
        private readonly HashSet<string> _badPairs;

        public TestRunner(string[] badPairs)
        {
            _badPairs = new HashSet<string>(badPairs);
        }

        bool runTests(string tests) {
            foreach(var pair in _badPairs) {
                var a = pair[0];
                var b = pair[1];
                if (tests.Contains(a) && tests.Contains(b)) {
                    return false;
                }
            }
            return true;
        }

        public IList<string> Divide(string tests) {
            var n = tests.Length;
            var res = new List<string>();
            var trunkLength = n-2;
            
            for(var i = 0; i< n- 2; i=i+2) {
                if ( i + trunkLength > n) {
                    var temp = tests.Substring(i);
                    temp += tests.Substring(0, trunkLength - i);
                    res.Add(temp);
                } else {
                    res.Add(tests.Substring(i, trunkLength));
                }   
            }
            return res;
        }


        public IList<string> FindFailedTestsPairs(string tests) {
            List<string> res = new List<string>();
            Recurse(tests);

            return res;
            
            void Recurse(string tests) {
                if (tests.Length == 2) {
                    var s= runTests(tests);
                    if(!s) {
                    res.Add(tests);
                    }
                    return ;
                }
                var sets = Divide(tests);
                foreach(var set in sets) {
                    var success= runTests(set);
                    if (!success) {
                    Recurse(set);
                    }
                }
            }
        }

    }

    public class Scrabble {

        static int[][] DIRS = new int[][] {
            new[] {1,0},
            new[] {0,1},
        };
        public void Present(int[][] mat, HashSet<string> words) {
            var m = mat.Length;
            var n = mat[0].Length;
            var res = new List<string>();

            for(var i = 0; i < m; i++) {
                for(var j = 0; j< n; j++) {
                    if (mat[i][j] != '.') {
                        // dfs
                    }
                }
            }

            


            void dfs(int x, int y, StringBuilder sb) {
                if (x <0 || y < 0 || x >= m || y >=n || mat[x][y] == '.') {
                    if (sb.Length > 0) {
                        res.Add(sb.ToString());
                    }
                    return ;
                }

                // if 





            }
        }
    }
}