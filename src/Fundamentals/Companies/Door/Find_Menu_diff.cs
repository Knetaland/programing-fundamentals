using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;




//                 a(1)                                                
//             /       \                                                          
//          b(2)       c(3)                                               
//         /     \         \                                                         
//       d(4)    e(5)      f(6)
//                          \g(1)
      
      
//             a(1)
//                 \
//                c(3)
//                    \
//                    f(66) 
//                      \ g(1)
                   
// 4

//             a(1)                                                 
//           /       \                                                 
//         b(2)      c(3)                                   
//       /       \       \                                          
//   d(4)      e(5)      g(7)  
  
//                   a(1)
//             /          \                                                                 
//          b(2)         h(8)  
//       /     \         /  \    
//  e(5)      f(6)      d(4) g(7) 
 
//  5
//  // q1 

 
//  //q2 
 
 
//  count = 5
namespace Fundamentals.Companies.Door
{
    public class Node
    {
        public string key;
        public int value;
        public List<Node> children = new List<Node>();
    }
    public class Find_Menu_diff : IProblem
    {
        public int FindChanges(Node a, Node b)
        {
            if (a == null && b == null)
                return 0;
            if (a == null || b == null)
            {
                return a == null ? GetCount(b) : GetCount(a);
            }
            var queueA = new Queue<Node>();
            var queueB = new Queue<Node>();

            queueA.Enqueue(a);
            queueB.Enqueue(b);
            var result = 0;
            while (queueA.Count > 0 && queueB.Count > 0)
            {
                var countA = queueA.Count;
                var countB = queueB.Count;
                var listA = new Dictionary<string, Node>();
                var listB = new Dictionary<string, Node>();
                while (countA > 0)
                {
                    var node = queueA.Dequeue();
                    listA.Add(node.key, node);
                    countA--;
                }
                while (countB > 0)
                {
                    var node = queueB.Dequeue();
                    listB.Add(node.key, node);
                    countB--;
                }

                var diffNodes = new List<Node>();
                var sameKeyNodes = new Dictionary<string, List<Node>>();
                foreach (var item in listA)
                {
                    if (!listB.ContainsKey(item.Key))
                    {
                        diffNodes.Add(item.Value);
                    }
                    else
                    {
                        sameKeyNodes.Add(item.Key, new List<Node> { item.Value });// f: f(6)
                    }
                }
                foreach (var item in listB)
                { // f(66)
                    if (!listA.ContainsKey(item.Key))
                    {
                        diffNodes.Add(item.Value);
                    }
                    else
                    {
                        sameKeyNodes[item.Key].Add(item.Value);// f: f(6), f(66) 
                    }
                }
                // diff
                foreach (var diffNode in diffNodes)
                {
                    Console.WriteLine("diff node " + diffNode.key + diffNode.value);
                    result += GetCount(diffNode);
                }
                // same
                foreach (var sameKeyNode in sameKeyNodes)
                {
                    var node1 = sameKeyNode.Value[0];
                    var node2 = sameKeyNode.Value[1];
                    if (node1.value != node2.value)
                    {
                        result += 1;
                    }
                    else
                    {
                        foreach (var child in node1.children)
                        {
                            queueA.Enqueue(child);
                        }
                        foreach (var child in node2.children)
                        {
                            queueB.Enqueue(child);
                        }
                    }
                }
            }
            return result;
        }

        public void Run()
        {

            var tree1 = new Node
            {
                key = "a",
                value = 1,
                children = new List<Node> {
                new Node {
                    key = "b",
                    value = 2,
                    children = new List<Node> {
                        new Node{
                            key= "d",
                            value = 4
                        },
                        new Node {
                            key = "e",
                            value= 5
                        }
                    }
                },
                new Node {
                    key = "c",
                    value = 3,
                    children = new List<Node> {
                        new Node{
                            key= "g",
                            value = 7,
                        },
                    }
                },
            }
            };
                        
//             a(1)                                                 
//           /       \                                                 
//         b(2)      c(3)                                   
//       /       \       \                                          
//   d(4)      e(5)      g(7)  
  
//                   a(1)
//             /          \                                                                 
//          b(2)         h(8)  
//       /     \         /  \    
//  e(5)      f(6)      d(4) g(7) 
 
            var tree2 = new Node
            {
                key = "a",
                value = 1,
                children = new List<Node> {
                new Node {
                    key = "b",
                    value = 2,
                    children = new List<Node> {
                        new Node{
                            key= "e",
                            value = 5
                        },
                        new Node {
                            key = "f",
                            value= 6
                        }
                    }
                },
                new Node {
                    key = "h",
                    value = 8,
                    children = new List<Node> {
                        new Node{
                            key= "d",
                            value = 4
                        },
                        new Node{
                            key= "g",
                            value = 7
                        },
                    }
                },
            }
            };
            var res = FindChanges(tree1, tree2);
            Console.WriteLine(res);
        }

        private int GetCount(Node a)
        {
            if (a == null)
                return 0;
            var res = 0;
            var queue = new Queue<Node>();
            queue.Enqueue(a);
            while (queue.Count > 0)
            {
                var node = queue.Dequeue();
                res += 1;
                if (node.children != null) {
                    foreach (var child in node.children)
                    {
                        queue.Enqueue(child);
                    }
                }
                
            }
            Console.WriteLine("Count is " + res);
            return res;
        }
    }



}
