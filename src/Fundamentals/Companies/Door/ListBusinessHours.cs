using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Door
{
    public class ListBusinessHours : IProblem
    {
        /*
         input start and end time ['mon 11:22 am', 'mon 12:14 pm'], output all time in between with interval of 5 mins
         output: ['11125, 11130, 11135, 11140, 11145, 11150, 11155, 11200, 11205, 11210]

         thought: 11122 11214

        */
        const int WEEK = 7;
        public void Run() {
            var inputs = new [] {"sun 11:00 pm", "mon 01:14 am"};
            var res =  ListTimes(inputs);
            Console.WriteLine($"between {string.Join(",", inputs)}");
            Console.WriteLine(string.Join(",", res));
        }

        public IList<string> ListTimes(string[] input) {
            var start = Parse(input[0]);
            var end = Parse(input[1]);
            if (end < start) {
                end += WEEK * 10000;
            }
            var res = new List<string>();
            var remainder = 5 - start % 5;
            start += remainder == 5 ? 0 : remainder;
            // Console.WriteLine($"remainder {remainder}");
            var co = false;
            Console.WriteLine($"start: {start}, end: {end}");
            while(start < end) {
                // Console.WriteLine($"while start: {start} < end: {end}");
                if ((start%100)%60 == 0 && co) {
                    start -=60;
                    start += 100;
                    co = false;
                }
                // Console.WriteLine($"after min parse, start: {start}, end: {end}");
                if ((start%10000)/100 >0) {
                    if (((start%10000)/100)%24 ==0) {
                        start -= 2400;
                        start += 10000;
                    }
                }
                // Console.WriteLine($"adding start: {start}");
                // res.Add((start > WEEK*10000 ? start%(WEEK*10000) : start).ToString());
                res.Add((start >= ((WEEK+1)*10000) ? start%(WEEK*10000) : start).ToString());

                start += 5;
                if ((start%100)%60 == 0) co = true;
            }
            
            return res;
        }



        private int Parse(string input) {
            var secs = input.Split(' ');
            var weekday = ParseWeekDay(secs[0]);
            var hour_min = ParseTime(secs[1], secs[2]);
            return weekday * 10000 + hour_min;
        }

        private int ParseTime(string time, string tt) {
            var hour_min = time.Split(':');
            var hour = int.Parse(hour_min[0]);
            var min = hour_min[1];
            if (tt.ToLower() == "pm") {
                // 1pm = 13
                if (hour != 12) {
                    hour += 12;
                }
            } else {
                // am
                // 12am = 0
                if (hour == 12) {
                    hour -= 12;
                }
            }
            return hour*100 + int.Parse(min);
        }

        private int ParseWeekDay(string weekday) {
            switch (weekday) {
                case "mon":
                    return 1;
                case "tue":
                    return 2;
                case "wed":
                    return 3; 
                case "thu":
                    return 4;
                case "fri":
                    return 5;
                case "sat":
                    return 6;
                case "sun":
                    return 7;
                default:
                    throw new System.Exception("Not vailld weekday");
            }
        }
    }
}
