using System;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Door;


// # A DashMart is a warehouse run by DoorDash that houses items found in
// # convenience stores, grocery stores, and restaurants. We have a city with open
// # roads, blocked-off roads, and DashMarts.
// #
// # City planners want you to identify how far a location is from its closest
// # DashMart.
// #
// # You can only travel over open roads (up, down, left, right).
// #
// # Locations are given in [row, col] format.
// #
// # Example:
// #
// # [
// # # 0 1 2 3 4 5 6 7 8 
// # ['X', ' ', ' ', 'D', ' ', ' ', 'X', ' ', 'X'], # 0
// # ['X', ' ', 'X', 'X', ' ', ' ', ' ', ' ', 'X'], # 1
// # [' ', ' ', ' ', 'D', 'X', 'X', ' ', 'X', ' '], # 2
// # [' ', ' ', ' ', 'D', ' ', 'X', ' ', ' ', ' '], # 3
// # [' ', ' ', ' ', ' ', ' ', 'X', ' ', ' ', 'X'], # 4
// # [' ', ' ', ' ', ' ', 'X', ' ', ' ', 'X', 'X'] # 5
// # ]
// #
// # ' ' represents an open road that you can travel over in any direction (up, down, left, or right).
// # 'X' represents an blocked road that you cannot travel through.
// # 'D' represents a DashMart.
// #
// # # list of pairs [row, col]
// # locations = [
// # [2, 2],
// # [4, 0],
// # [0, 4],
// # [2, 6],
// # ]
// #
// # answer = [1, 4, 1, 5]
// #
// # Imple‍‍‌‍‌‌‍‍‌‌‍‍‍‌‌‌‍‍ment Function:
// # - get_closest_dashmart(city, locations)
// #
// # Provided:
// # - city: List[str]
// # - locations: List[List[int]]
// #
// # Return:
// # - answer: List[int]

public class ShortestPathForDashMart : IProblem
{
    int[][] DIRS = [
        [0,1],
        [0,-1],
        [1,0],
        [-1,0]
    ];

    public IList<int> FindShortestDistanceForLocations(char[][] cityPlan, int[][] locations) {
        var m = cityPlan.Length;
        var n = cityPlan[0].Length;
        const char BLOCK = 'X';
        const char DASH_MART = 'D';
        const char OPEN = ' ';
        var distances = new List<int>();
        
        // #1 go through each location and BFS on each
        // foreach(var location in locations) {
        //     var step = bfs(location[0], location[1]);
        //     distances.Add(step);
        // }

        // #2 build the distance map
        var distanceMap = getDistances();
        foreach(var location in locations) {
            if (distanceMap[location[0], location[1]] != int.MaxValue) {
                distances.Add(distanceMap[location[0], location[1]]);
            } else {
                distances.Add(-1);
            }
        }
        return distances;


        int[,] getDistances() {
            var queue = new Queue<(int,int)>();
            int[,] distanceMap = new int[m,n];
            var visited = new bool[m,n];

            // add queue
            for(var i =0; i < m; i++) {
                for(var j = 0; j < n; j++) {
                    if (cityPlan[i][j] == DASH_MART) {
                        queue.Enqueue((i,j));
                        visited[i,j] = true;
                    } else {
                        distanceMap[i,j] = int.MaxValue;
                    }
                }
            }

            // build distance map
            while(queue.Count > 0) {
                var position = queue.Dequeue();
                var x = position.Item1;
                var y = position.Item2;

                foreach(var dir in DIRS) {
                    var nextX = x + dir[0];
                    var nextY = y + dir[1];
                    if (isValid(nextX, nextY) && !visited[nextX,nextY]) {
                        queue.Enqueue((nextX,nextY));
                        visited[nextX, nextY] = true;
                        if (distanceMap[x,y] + 1 < distanceMap[nextX, nextY]) {
                            distanceMap[nextX, nextY] = distanceMap[x,y] + 1;
                        }
                    }
                }
            }

            return distanceMap;
        }


        // go through each location, BFS on each
        int bfs(int x, int y) {
            var queue = new Queue<(int, int)>();
            queue.Enqueue((x,y));
            var visited = new bool [m,n];
            var step = 0;
            while(queue.Count > 0) {
                
                var count = queue.Count;
                for(var i =0; i < count; i++) {
                    var position = queue.Dequeue();
                    var positionX = position.Item1;
                    var positionY = position.Item2;
                    // found
                    if (cityPlan[positionX][positionY] == DASH_MART) return step;

                    foreach(var dir in DIRS) {
                        var nextX = positionX + dir[0];
                        var nextY = positionY + dir[1];
                        
                        if (isValid(nextX, nextY) &&!visited[nextX, nextY] ) {    
                            visited[nextX, nextY] = true;
                            queue.Enqueue((nextX, nextY));
                        }
                    }
                }
                step++;
            }
            return -1;

        }

        bool isValid(int x, int y) {
            if (x >= 0 && x < m && y >=0 && y < n && cityPlan[x][y] != BLOCK) return true;
            return false;
        }
    }

    public void Run() {
        char[][] cityPlan = [
            ['X', ' ', ' ', 'D', ' ', 'X', 'X', ' ', 'X'],
            ['X', ' ', 'X', 'X', 'X', ' ', ' ', ' ', 'X'],
            [' ', ' ', ' ', 'D', 'X', 'X', ' ', 'X', ' '],
            ['X', ' ', ' ', 'D', ' ', 'X', ' ', ' ', ' '],
            [' ', 'X', ' ', ' ', ' ', 'X', ' ', ' ', 'X'],
            [' ', 'X', ' ', ' ', 'X', ' ', ' ', 'X', 'X']
        ];
        int[][] locations = [
            [2, 2],
            [4, 0],
            [0, 4],
            [2, 6],
        ];
        var res = FindShortestDistanceForLocations(cityPlan, locations);

        res.Print();
    }
}
