using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Hopper
{
    public class Hopper : IProblem
    {
        public class Flight {
            public string City { get; set; }
            public int Loyalty { get; set; }
        }

        public class Flight2 {
            public string Name { get; set; }
            public string Origination { get; set; }
            public string Destination { get; set; }
        }

        // public class CustomComparer : IComparer<string>
        // {
        //     public int Compare(string x, string y) {
                
        //     }

        //     private IList<string> ParseString(string s) {
        //         var sb = new StringBuilder(s);
        //         for(var i = 0; i < s.Length; i++) {
        //             if (char.IsDigit(s[i])) {

        //             } else {
        //                 s[i]
        //             }
        //         }
        //     }
        // }

        public class Trie {
            public Dictionary<char, Trie> Children { get; set; } = new Dictionary<char, Trie>();
            public int Count { get; set; }

            public void Insert(string word) {
                var cur = this;
                foreach(var c in word) {
                    if (!cur.Children.ContainsKey(c)) {
                        cur.Children.Add(c, new Trie());
                    }
                    var node = cur.Children[c];
                    node.Count++;
                    cur = node;
                }
            }

            public string FindShortestPrefix(string word) {
                var cur = this;
                var res = new StringBuilder();
                foreach(var c in word) {
                    if (!cur.Children.ContainsKey(c)) {
                        return null;
                    }
                    if (cur.Count == 1) { // means unique path to identify the word
                        return res.ToString();
                    } else {
                        res.Append(c);
                    }
                    cur = cur.Children[c];
                }
                return res.ToString();
            }
        }

        public IList<string> ShortestPrefix(IEnumerable<string> inputs) {
            var trie = new Trie();
            var res = new List<string>();
            foreach(var i in inputs) {
                trie.Insert(i);
            }

            foreach(var i in inputs) {
                res.Add(trie.FindShortestPrefix(i));
            }
            return res;
        }

        public Dictionary<string, PriorityQueue<Flight, int>> Sort(IEnumerable<Flight> flights) {
            var map = new Dictionary<string, PriorityQueue<Flight, int>>();
            foreach(var f in flights) {
                if (!map.ContainsKey(f.City)) {
                    map.Add(f.City, new PriorityQueue<Flight, int>(Comparer<int>.Create((x, y) => y-x)));
                }
                map[f.City].Enqueue(f, f.Loyalty);
            }
            return map;
        }


        public string RotateWords(string words) {
            var sb = new StringBuilder(words);
            var left = 0;
            var right = sb.Length-1;
            
            while(left < right) {
                while(left < right && sb[left] == ' ') left++;
                while(right > left && sb[right] == ' ') right--;
                if (left == right) Console.WriteLine($"land on the same char at index {left}");
                var temp = sb[left];
                sb[left] = sb[right];
                sb[right] = temp;
                left++;
                right--;
            }
            return sb.ToString();
        }

        public string RotateWords2(string words) {
            var sb = new StringBuilder();
            var list = new List<string>();
            for(var i =0; i < words.Length; i++) {
                if (!char.IsLetterOrDigit(words[i])) {
                    list.Add(sb.ToString());
                    sb.Length=0;
                    list.Add(words[i].ToString());
                } else {
                    sb.Append(words[i]);
                }
            }
            if (sb.Length > 0) list.Add(sb.ToString());
            Console.WriteLine($"list: [{string.Join(",", list)}]");
            var left =0; 
            var right = list.Count-1;
            while(left < right) {
                while(left < right && isSymbol(list[left])) left++;
                while(right > left && isSymbol(list[right])) right--;
                var temp = list[left];
                list[left] = list[right];
                list[right] = temp;
                left++;
                right--;
            }
            Console.WriteLine($"list: [{string.Join(",", list)}]");
            var res = new StringBuilder();
            foreach(var l in list) {
                res.Append(l);
            }
            
            return res.ToString();

            bool isSymbol(string w) {
                var te =  w.Length == 1 && !char.IsLetterOrDigit(w[0]);
                Console.WriteLine($"checking if {w} is symbol: {te}");
                return te;
            }
        }
        

        public IList<IList<Flight2>> FindAirLines(IList<Flight2> flights, string origin, string destination) {
            var map = new Dictionary<string, HashSet<Flight2>>();
            var res = new List<IList<Flight2>>();
            foreach(var f in flights) {
                var ori = f.Origination;
                if (!map.ContainsKey(ori)) {
                    map.Add(ori, new HashSet<Flight2>());
                }
                map[ori].Add(f);
            }


            if (map.ContainsKey(origin)) {
                foreach(var flight in map[origin]) {
                    var visited = new HashSet<Flight2>(); // reset visited, so that we list all the routes
                    dfs(flight, visited, new List<Flight2>());
                }
            }
            

            return res;

            void dfs(Flight2 f, HashSet<Flight2> visited, IList<Flight2> route) {
                if (visited.Contains(f)) return ;
                visited.Add(f);
                route.Add(f);
                
                if (f.Destination == destination) {
                    res.Add(new List<Flight2>(route));
                    route.RemoveAt(route.Count - 1);  // remember to remove
                    return ;
                }

                if (map.ContainsKey(f.Destination)) {
                    foreach(var next in map[f.Destination]) {
                        dfs(next, visited, route);
                    }
                }

                route.RemoveAt(route.Count - 1);
            }
        }

        public void Run() {
            //  sortFlights();
            // rotateWordProblem();
            // findShortestPrefix();
            findFlights();
        }

        private void findFlights() {
            var flights = new List<Flight2> {
                new Flight2 {
                    Name = "H1",
                    Origination = "A",
                    Destination = "B",
                },
                new Flight2 {
                    Name = "H2",
                    Origination = "A",
                    Destination = "C",
                },
                new Flight2 {
                    Name = "H3",
                    Origination = "B",
                    Destination = "C",
                },
                new Flight2 {
                    Name = "H4",
                    Origination = "C",
                    Destination = "F",
                },
                new Flight2 {
                    Name = "H5",
                    Origination = "A",
                    Destination = "D",
                },
                new Flight2 {
                    Name = "H51",
                    Origination = "D",
                    Destination = "F",
                },
                new Flight2 {
                    Name = "H21",
                    Origination = "B",
                    Destination = "F",
                },
                new Flight2 {
                    Name = "HCC",
                    Origination = "C",
                    Destination = "A",
                },
            };
            var res = FindAirLines(flights, "A", "F");
            foreach(var r in res) {
                Console.WriteLine($"[{string.Join(",", r.Select(f => $"{f.Name}"))}]");
            }
        }

        private void findShortestPrefix() {
            var inputs = new [] {"zebra", "dog", "duck", "dove"};
            var res = ShortestPrefix(inputs);
            Console.WriteLine($"com prefixes are [{string.Join(",", res)}]");
        }

        private void rotateWordProblem() {
            var input = "this*is#a-good&day";
            var res = RotateWords2(input);
            Console.WriteLine(res);
        }

        private void sortFlights() {
            var cities = new [] {"A", "B", "C", "D"};
            var random = new Random();
            var n = 50;
            var flights = new List<Flight>();
            while(n > 0) {
                var city = cities[random.NextInt64(0, 3)];
                var loyalty = (int)random.NextInt64(100);
                flights.Add(new Flight {
                    City = city,
                    Loyalty = loyalty,
                });
                n--;
            }
            var map = Sort(flights);

            foreach(var m in map) {
                Console.WriteLine("-------------");
                while(m.Value.Count > 0) {
                    var f = m.Value.Dequeue();
                    Console.WriteLine($"[city:{f.City}, loyalty: {f.Loyalty}");
                }
                Console.WriteLine("-------------");
            }
        }
    }
}