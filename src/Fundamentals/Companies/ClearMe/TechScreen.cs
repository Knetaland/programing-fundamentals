using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.ClearMe;

public class TechScreen :IProblem
{
    // 1. substring of size 3

    public int getSubstring(string s) {
        if (s == null) return 0;
        var k = 3;
        var set = new HashSet<string>();
        for(var i = 2; i < s.Length; i++) {
            set.Add(s.Substring(i - 2, k));
        }
        foreach(var i in set) {
            i.Print();
        }
        return set.Count;
    }

    /** 2. 
    Given a string consists of a's and b's, supposed there is a space between 2 adjacent characters.
    A person can jump from a space to space next to it, after jumping, the char in between the space is flipped from a to b or vice versa.
        Starting from given space, find minumum jumps a person can make to make the string have equal number of a's and b's, 
        
        example 1: "aaabaa" starting at position 6 looks like
        0 1 2 3 4 5 6
        a a a b a a*
        after one jump to the left, it becomes
        0 1 2 3 4 5 6
        a a a b a *b (b is flipped at the end)
        another jump:
        0 1 2 3 4 5 6
        a a a b *b b ( a is flipped btw position 4 & 5)
        => minumum 2 jumps

        example 2: 
        "aaabab", starting at position 6
            "aaabab*" -> aaaba*a -> aaab*ba -> aaa*aba -> aa*baba -> a*bbaba    
        min 5 jumps

        example 3:
        "aabba", starting at position 3, 
        => impossible, return -1
    */ 


    public void Run()  {
        // 1.
        // var s = "abababaaab";
        // var res = getSubstring(s);

        // 2
        var s = "aaabab";
        var start = 3;
        var fb = new  FlipToBalance();
        var res = fb.minJump(s, start);
        res.Print();
    }
}

public class FlipToBalance {
    public int minJump(string s, int pos) {
        if (s.Length % 2 == 1) return -1;
        var min = int.MaxValue;
        var sb = new StringBuilder(s);
        pos = pos -1;
        if (pos < 0) pos = 0;
        var originalACount = s.Count(c => c == 'a');
        var originalBCount = s.Count(c => c == 'b');
        var aCount = originalACount;
        var bCount = originalBCount;
        var step = 0;
        // going left
        for(var i = pos; i >=0; i--) {
            if (aCount == bCount) {
                min = Math.Min(step, min);
                break;
            }
            step++;
            if (sb[i] == 'a') {
                sb[i] = 'b';
                aCount--;
                bCount++;
            } else if (sb[i] == 'b') {
                sb[i] = 'a';
                aCount++;
                bCount--;
            }
        }
        // going right
         sb = new StringBuilder(s);
        aCount = originalACount;
        bCount = originalBCount;
        step = 0;
        for(var i = pos; i < s.Length; i++) {
            if (aCount == bCount) {
                min = Math.Min(step, min);
                break;
            }
            step++;
            if (sb[i] == 'a') {
                sb[i] = 'b';
                aCount--;
                bCount++;
            } else if (sb[i] == 'b') {
                sb[i] = 'a';
                aCount++;
                bCount--;
            } 
        }
        return min;
    }
}
