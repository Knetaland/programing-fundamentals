/*
  Given a set of company names with homepage URLs and a large block of text like a wikipedia page, write a function that will find all the matching company names in the text and generate Markdown syntax to create a link.
  If they’re not familiar with markdown, links look like this: [Amazon](https://www.amazon.com).  I used to ask candidates to generate html <a> tags.  Markdown is easier to deal with.
*/

using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Better {
    public class LinkEntities : IProblem
    {
        public void Run() => throw new System.NotImplementedException();
    }
}