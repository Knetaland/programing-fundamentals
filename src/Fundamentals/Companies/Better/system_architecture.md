## Rationale
The primary architectural paradigm of the Engineering Team is event-driven, distributed systems, where small, highly-decoupled web services communicate with one another over HTTP and where asynchronous messages are sent using queues. We've also adopted microservices, which implies that owning many features means owning many services, which gather information not only from their own database, but from other services, queues, and Pub/Sub mechanisms.

Engineers across all levels will be expected to debug and sometimes design systems that are composed of many services. This exercise is meant to determine the amount of experience a candidate has architecting event-based, distributed HTTP systems, starting from basic interactions between a browser and an API, leading up to a multi-service environment with an event-driven architecture.

## Structuring the Question
This problem is structured such that the candidate is initially tackling fundamentals, then is asked to solve more complex problems over time. The problem starts with a basic set of interactions between the browser and an API, and progressively becomes more difficult over the duration of the exercise. The questions are meant to have multiple possible solutions, but ultimately a single best option.

The interviewer should conduct the question as if they’re designing the problem with them, but always deferential to the candidate. The goal is to make the candidate feel comfortable, that you are both on the same team solving the problem together.

## Expectations of the Candidate
The goal is to lead the candidate to the most specific answer they can provide. When they provide an answer that's vague or too high-level, ask further questions to see what detailed decisions they'd make. An interviewer should aim to be able to answer as many of the following YES/NO questions as possible by the end of the interview.

## Rubric (Yes/No/No Answer)
- API Design and HTTP Semantics

  - Can employ basic HTTP mechanisms for designing REST APIs

  - Understands the conventional usage (and semantic underpinnings) of different HTTP methods in an API

  - Makes REST design decisions consistent with industry standard practices

- System Design

  - Can recognize common components of a single microservice

  - Understands the basic responsibilities of each component of the system

  - Makes decisions about architecture based on an understanding failure modes

- System Scalability

  - Can recognize common scalability bottlenecks within the architectural stack

  - Can recommend strategies for relieving scalability bottlenecks

  - Can recognize the need for redundancy as a means to achieve high availability

- Event-driven Systems Design and Deferred Processing

  - Can identify when deferred processing using queues is appropriate

  - Understands common failure scenarios of event-driven distributed systems

  - Can recommend common resiliency strategies for event-driven failure scenarios

- Cloud Provider-specific Knowledge

  - Can identify cloud provider-specific technologies used in systems design

  - Can articulate pros/cons of using cloud provider-specific managed services

# Question
## Framing the Discussion
We're going to build a world class three step process for getting borrowers preapproved for a mortgage to buy their first home! The market differentiating feature is its simplicity. The steps a borrower has to take are:

- Fill out a form with self-reported information regarding their income, assets, and target purchase home

- Execute a soft credit pull to obtain their credit score (don't worry, a soft credit pull won't negatively impact their credit score)

- Get approved!

Note that this is much more complex in reality, but we can't design an end-to-end preapproval process in an hour.

This is a microservices architecture, where there are three key systems:

- Serve static assets such as HTML, CSS, and JavaScript. This system is also responsible for rendering the self-reported information form.

- Profile Service, for converting and storing the self-reported information as a Borrower Profile.

- Credit Report Service, for proxying requests to TransUnion, and storing results for posterity.

**Note for interviewer**: at this point it’s usually helpful to start with a drawing. Use this as a reference point. Try to leave space at the top of the dotted box, and above the dotted box to draw infrastructure we'll need for the CDN, path based routing, and load balancers. Annotate the drawing with the following information:

- The dotted line represents a virtual network resources are deployed to

- Applications are deployed to separate infrastructure (independent servers)

- Deploying one application doesn't mean deploying another

- Applications are network partitioned, calls to one another are over HTTP


***Designing the Profile Service***
Given the following requirements, design a Profile Service:

It should be a REST API

JSON is the primary data format

The API should represent a single profile as well as a collection

The collection resource should be filterable by credit score ranges

The API should support creating new and updating existing profiles

A profile contains the following data:: full name, annual income, credit score, email, and the address of the home they're purchasing

I'm interested in hearing from you the following things:

What routes are needed to support these requirements? You can specify them in the form of URL paths.

What HTTP methods are used to perform the different operations specified in the requirements? Why specifically these methods?

What are the possible expected HTTP response codes for each operation?

Note for the interviewer: The end result of this exercise is to have the candidate write a basic OAI/Swagger specification for a simple API. Interviewers should question design decisions, especially if they don't seem standard.

Let's say that the engineering team working on this has grown very large and there are fully separate teams developing the profile service and the services and frontends that use it. How can the profile service team continue to add features and evolve their API without negatively impacting the other teams?

Note for the interviewer: You can use these hints:

When should we consider making backward compatibility breaking changes?

How can we version this API so that when things change, clients won't break?

Things have gotten sufficiently complex in our ecosystem! We now have multiple clients that are creating and updating profiles. How can we design our API such that two separate clients aren't overwriting each others changes?

Note for the interviewer: You can use these hints:

Clients should get the latest version before trying to make an update.

Clients should expect 4xx level responses that they can be resilient to.

Etags are a great way to handle race conditions and edit conflicts.

Many of our borrowers are working on their mortgages in their spare time during their commutes. As a result they tend to come in and out of cell service. We have started seeing problems where we do not know if requests are actually succeeding, and we cannot discern whether the client just isn’t getting a response, or if the request actually failed. What can we do to solve this type of problem? How can we be sure that our API is signalling to developers which calls are safe to retry and which are not? Can you speak to the different use-cases for the different HTTP methods and why we might use POST over PUT in a particular situation? 

What to look for:

Candidate asks questions about current implementation of the client

Candidate knows when to use POST vs PUT vs PATCH, or at least can identify that they have different use-cases that would help solve this problem

Spec-conforming structure for endpoints that accept PUT requests vs POST requests

Microservices System Design: A Single Microservice
At this point, we now have our profile service design and we're happy with it. We can get profiles, create profiles, and update profiles. We also know how we're going to version the API, and we can handle lots of simultaneous writes!
It's now time to architect the application infrastructure. Given the following constraints, what systems infrastructure will we need?

It is responsible for serving HTTP requests

It must be able to run custom business logic

It must be able to persist data for later querying

Note for the interviewer: You can use the following clarifying questions:

How is the application written? What programming language? What framework?

Is there a web server? Which one and why? What function does it play?

Is there a database? Which one and why? What function does it play?

How does containerization fit in? Lambda/FAAS?

How does code go from their laptop to the server infra they’ve suggested?

Note for the interviewer: We're looking for a very high level response. The goal is to be set up for the next set of questions, which is about scaling.

The application is written in Node.js, Python, Golang, etc.

It might use a web server like Nginx or Apache, maybe it doesn't need one.

It might sit behind a load balancer if multiple instances are required.

It will use a relational database like MySQL or PostgreSQL for storing data

  related to Products.

It will use an in-memory cache such as Redis to cache common operations

  around parsing configuration, database operations, or getting data from other

  services.

With this basic architecture in place, we're able to serve our borrowers. But our new marketing campaign was a success! We're seeing tons and tons of traffic, and the site is slowing down and in some instances crashing.

Where do you anticipate seeing bottlenecks in this architecture?

Candidates will often talk about the web server and the database. Follow up questions can be:

What resources are constrained? Memory? CPU? Network? And why?

How do we address each of these bottlenecks?

Candidates will often talk about horizontal/vertical scaling, adding cache layers, sharding the database, etc.

Microservices System Design: Lots of Services
We are succeeding beyond our wildest dreams! That means that we are finally able to add much-needed functionality to the profile service like income verification and address verification.. Unfortunately, the service is becoming to complex for a single small team to manage, and release speed is taking a toll despite a growing engineering team. 

How would you solve this problem?

Candidate should at this point mention decomposing the profile service into multiple services with discrete functions.

We’ve managed to maintain a well-separated service-oriented architecture, so splitting our Profile Service into CoreProfile, IncomeVerification and AddressVerification wasn’t too tough.

How do these services communicate?

How do we deal with some of these services needing different resources?

We notice that AddressVerification gets called twice as often as any other service, and response times subsequently spike. How do we mitigate this?

Note for the interviewer: It’s good to discuss with the candidate at this point the tradeoffs and considerations behind:

How they would structure the various service’s APIs

Whether the CoreProfile service would sit “in front” of the others

Alternative routing mechanisms (i.e. a separate nginx instance routes to all of them) 

Independent autoscaling

Load balancing requests

Database dependencies

Deferred Processing & Event Driven Architecture
The borrower is nearing the end of the process for getting preapproved for their mortgage. The last step is for us to programmatically check their credit score. We use a third party service provider called CoreLogic. When we do the credit pull, we want to do three things: 

Make a request for the credit report to TriMerge Credit's API

Store the response to the request in our database

Email the borrower details of their credit report using third party email SaaS product, MailChimp.

Currently, all three of these things are happening synchronously, and the borrower is waiting for a response from the browser. Finally, after all three server-side steps complete, the borrower sees a response, hopefully that their credit score is high enough to qualify for the loan they want!

What we've noticed is that borrowers are abandoning us at this step, so we check our database and our logs and we notice a couple of things:

Borrowers that had a high enough credit score were leaving us

Borrowers experienced a high incidence of server timeout errors

After further inspection, we notice that of the three requirements for our credit pull service, MailChip is very slow to respond. In some incidents, it's so slow that our credit pull service is causing the borrowers to see timeouts!

We want to maintain our API contract: a single request should do all three things, credit pull from CoreLogic, write to our database, and send an email via MailChip. But we want to decouple sending email from returning a response to the client.

What infrastructure or application change do we need to make to facilitate this new requirement?

Usually a candidate will get this right away, and recommend some kind of queuing system. Often they don't talk about how data then gets from the queue to the email provider, MailChip. In this case, ask:

Once a message is placed in the queue, how is it then sent to MailChip?

The answer is that a worker is often a consumer of a queue, listening for messages then performing some business logic. In this case that business logic is sending email via MailChip and logging a response.

MailChip will respond with three different HTTP status codes: 200, 400, and 500.

What do each of these responses mean?

200 OK success status response code indicates that the request has succeeded. A 200 response is cacheable by default.

400 Bad Request response status code indicates that the server cannot or will not process the request due to something that is perceived to be a client error (e.g., malformed request syntax, invalid request message framing, or deceptive request routing).

500 Internal Server Error server error response code indicates that the server encountered an unexpected condition that prevented it from fulfilling the request.

In the event of intermittent 400 level errors, what should the worker do?

400 level errors indicate that you need to revise the request to correct the problem before trying again.

Since these errors are intermittent, we don't want to kill the worker. Some messages that were put into the queue have issues. An example of an issue is that the email address we're trying to send an email to is malformed, and MailChimp wouldn't process it. The worker should log the error response from the server along with message details. For easy replayability and later inspection, the worker can place the message into a dead letter queue. The worker can also trigger alerts so that a developer can inspect the issue.

Sometimes a candidate will recommend putting the message back onto the queue at the end. This is strictly wrong because the message in its current format will never be processed by MailChimp.

In the event of consistent 500 level errors, what should the worker do?

The worker should retry original requests that receive 500 level server errors.

In addition to simple retries, the worker should implement an exponential backoff algorithm. The idea behind exponential backoff is to use progressively longer waits between retries for consecutive error responses. The worker should implement a maximum delay interval, as well as a maximum number of retries.

Sometimes a candidate will recommend putting the message back onto the queue at the end. This is strictly wrong because the message isn't the problem, and moving quickly to the next message will result in the same error.

Note for the Interviewer: We ask this next question because we want to understand whether the candidate understands the implications of deferred processing to the end user, the borrower.

If MailChimp is returning 500s for 1 hour, what are the implications for the borrower?

They don't receive any email from our system for at least 1 hour. Email is delayed while MailChimp is returning 500s. Email sending is further delayed while the queue drains.

What are some real world queuing technologies?

The answer to this varies widely. We should gain a sense of what technologies they've worked with. It's important to understand whether or not they have real world hands on experience, but the actual technologies they've used is less important.

Other Queue Questions:

We queue a variety of operations that are processed out-of-band, from emails, to 2FA codes, to phone call assignments for employees. How would you establish the max backlog size for these queues? What would you have happen when the max backlog size is hit in a queue?

The backlog size should correlate to our tolerance for processing time. 2FA codes need to be delivered well in advance of their expiration time, while emails are less time-sensitive. When the backlog size starts approaching the max, our system should generate more workers automatically to help drain it.

Can single instance queues fail and restart safely? What is the impact of a restart if there is only one instance of a queue? How would you go about ensuring that messages are not lost once they are stored in the queue?

Broadly what we are looking for here is that queues trade per-instance durability for speed. If a single instance queue fails, its message will be lost. Some mechanisms for assuring message durability/avoiding loss are queue mirroring, in which a master queue node receives all work first and is mirrored to other instances, and writing messages to disk.

Are there drawbacks to adding more workers?

Increased (potentially needless) resource consumption. 

How does adding more workers impact jobs that need to be processed in order? How would you enforce in-order processing? What is the impact of the need for in-order processing?

There are a few ways to do this. One potential approach is somehow storing the processing status of each job, and a reference to which jobs must be done prior / next. Another is marking dependent jobs as “ready” when the jobs they depend on are completed. Each of these approaches invariably impacts queue processing speed when compared to fully-parallel jobs. What we’re looking for here is primarily discussion.

What are some strategies for signalling between the publisher, queue, and consumer that a message has been received? 

Queues can consider a message delivered either automatically, on dispatch, or after manual acknowledgement by the subscriber. This can be represented by giving the job a state, and marking it as “Processing”, or by removing it from the queue, depending on need. The publisher can look for confirmation of the client picking up the job from the queue in a variety of ways, for example by polling or even blocking until confirmation, but there will be impact on the publisher instance’s ability to process other work.
 Message acknowledgements: signalling between queue and publishers/subscribers

Delivery Guarantees

Queue workers picking up out-of-band activities solves a handful of our problems, namely consuming too many resources on our main instances and moving any unnecessary, potentially blocking operations out, but we still have to deal with delivery guarantees. We’ve decided to start doing hard credit checks during our flow, which will affect a borrower’s credit score every time they are performed (feel free to clarify that we don’t do this) but which can be worked around if they fail. We’ve also started issuing MFA tokens to applicants who want to pick up their application where they left off, which are mandatory for the user to provide, there are no workarounds. We have a simple SmsService that fires a text message every single time it receives a request from the CoreProfile service with an MFA token attached. 

Now that all these systems are communicating over a network, what are your thoughts on what sorts of delivery guarantees the services should offer each other? What are the benefits and drawbacks of each?

Note for the Interviewer: If the candidate is unfamiliar with delivery guarantees (in name, or as a concept) feel free to provide these:

Delivery guarantees:

At-most-once delivery. This means that a message will never be delivered more than once but messages might be lost.

At-least-once delivery. This means that we'll never lose a message but a message might end up being delivered to a consumer more than once.

Exactly-once delivery. The holy grail of messaging. All messages will be delivered exactly one time.

Loosely, the correct answer is that hard credit check requests should be guaranteed *at-most-once*, since there is a workaround and we would potentially be affecting a Borrower’s credit. MFA token delivery should be guaranteed *at-least-once*, since sending the same MFA token again wouldn’t negatively affect the Borrower’s ability to proceed. We want the candidate to mention that at-least-once is inherently slower than at-most-once, since it waits on a response, and the challenges associated with exactly-once delivery.

Other Point for Discussion

We now have a fairly robust microservice diagram up on the board. How would you go about testing it? What are your thoughts on testing coverage? What are your thoughts on the distribution of tests between end-to-end, integration, and unit tests?

This part is mostly freeform, we’re mostly looking for the candidate to have an opinion.

We are receiving reports from our users that emails are failing to send. Unfortunately we can’t get any more detail from the users. How would you go about figuring out what’s going wrong? What tools would you use? Once you know the issue, how would you ensure that we are aware of this sort of issue without relying on our users in the future?

The correct answer should talk about logging, and metrics collection like Datadog. We want to hear them mention an error catching service like Rollbar/Sentry when talking about how we could proactively monitor for future issues.