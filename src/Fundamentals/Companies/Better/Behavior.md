#### Focus on specifics
- **Situation**
- **Task**
- **Action**
- **Result**


### This part of the interview is oriented around your work experience. To demystify a bit, there is a basic formula to it. 

1. I'll tell you about an aspect of what daily work like is here, and ask you to walk me through an example from any of your past experiences that is similar. If you don't have an example that's exactly the same, that's okay.

2. In the context of your relevant experience, we’re looking to understand how your work lines up with the three main axes of our career ladder: technical excellence, influence, and initiative.

3. This works best if you can zoom in as much as possible into specific situations you experienced and be clear about your responsibility and contribution.

4. It is useful to highlight examples from across projects or throughout your career.

5. Hopefully you also come away with a feel for what work is like here and what our challenges are.

6. I’ll be taking notes just to make sure I’m capturing your experience accurately.

Any questions?


## Core questions
### Technical depth 

We entrust engineers at all levels with the ability to create solutions to problems with technology, rather than distributing work to be done. It’s helpful for us to get a feel for some of the more complex work that our candidates have delivered.

***Question: Can you walk me through a particularly technically challenging task that represents a highlight of your achievement as an engineer, or that really pushed your capabilities?***

**Follow-up**: (If they answered generally) Was there one part that stood out as challenging, specifically from a technical perspective?

**Follow-up**: How did this expand your outlook on technology compared to what you knew going in?

### Influence / Leadership

We like to put individual engineers in places where they can drive projects forward, and so it’s helpful to know what levels of leadership responsibility folks have held before.

***Question: Could you talk about your highest level of responsibility you’ve had within a team, whether as an official role or informally?***

Follow-up: In this responsibility, were there any situations in working with people that forced you to change your style of collaboration or leadership, or to grow? In other words, lessons learned?

### Initiative

Openness to ideas has been a key aspect of company culture, both on the tech and operations side. It’s a place where people with new ideas can really make an impact.

***Question: Can you walk me through a time when you moved an idea forward within your company? This might be tech, or it could be around process, product, or business.***

Follow-up: Did you encounter any obstacles to moving forward with this idea, and if so, how did you navigate that friction?

## Backup questions 
(You will likely not make it through the previous questions before you get to the 45 minute mark, to start Q&A, but the following can be used if you have extra time):

Technical adaptability

This company has a number of steep learning curves, both from a business and tech perspective. From a tech perspective, our loan origination system is built using a workflow programming framework we developed in-house, so every developer who joins will have at least one major technical hurdle to overcome.

***Question: Is there an experience you can speak to in taking on a task, where you knew it would require technical knowledge you didn't have? How did you approach that?***

Possible follow-up: How did this expand your outlook on technology compared to what you knew going in?

### Domain knowledge engineering

Our business domain is very deep and nuanced. Most folks don't come in with much prior knowledge of company business, but it’s important to develop that context, in order to encode it into the business logic of the software we develop.

***Question: Are there experiences you can speak to where you've had to gain a lot of domain knowledge to be productive? Can you speak to translating that knowledge into engineering product?***

### Openness to change

We're growing quite rapidly, and one thing we anticipate is a lot of change. While exciting, it’s important for people joining us to know to expect a rapidly changing environment, because it can also sometimes produce frustrating situations.

***Question: Can you speak to any work or school experiences that required you to adapt to change?***

### Legacy systems
At the heart of our infrastructure is a custom workflow programming engine. It lets us model pieces of the lending process as structured workflows, assigning tasks to the borrower and our back-office staff. At this point, this codebase contains a tremendous amount of business logic, specialized to different lending scenarios and local rules. While powerful, it can be a challenge to learn to navigate and manipulate.

***Question: Is there an experience you can speak to where you had to work within a large pre-existing system, with its own internal logic?***

**Product/UX engineering** A lot of the work on my team is implementing user features for internal or external customers in a large SPA.

**Question** Are there product engineering experiences you can speak to and technical challenges you've overcome implementing UX? This might be complex pure front-end interactions or full-stack features, for example.

**Data engineering** Along those lines, a lot of work deals with modeling a borrower’s mortgage application, and as you might imagine, borrowers have all sorts of situations, which we have to model the nuance of in the ways in which we approach data storage.

**Question** Is there an experience you can walk me through where you worked with a very nuanced, complex data model, perhaps involving migrations? What tech was used? Can you walk me through a feature you implemented?

**Architecture** We do a decent amount of building out entirely new features from scratch, which might involve an engineer spearheading everything from tech selection, to data model, to UX.

**Question** Can you walk me through a feature or system you architected, and how you went about making these decisions?

The hiring manager should make the decision of whether to adapt this to be more specific if evaluating a candidate for a particular area of expertise.

## Rubric
Our goal in evaluation is to understand how the candidate matches up to our career ladder. The three main dimensions of our career ladder are:

- **Technical excellence**

- **Initiative**

- **Influence**

The challenging part is that each of the candidate’s answers may or may not speak directly to the dimensions in predictable ways. If an answer speaks to one or more of the dimensions, rate each of applicable dimension on the scale of the career ladder:

L3: Entry-level

L4: Mid-level

L5: Upper mid-level

L6: Senior

L7: Advanced Senior

L8: Staff/Principal

For example, if the candidate’s answer to the Technical Depth question speaks to technical excellence and initiative, but not influence, you might rate it as **Technical excellence: L5, Initiative: L6**. 
