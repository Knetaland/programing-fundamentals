Question 1
> Say you have a large directory—project—with many files and directories within. We want to locate and print out the paths to all Makefile files that contain the text clean::. Please furnish a shell command for doing so.

```
$ find project -name Makefile -exec grep clean:: {} \; -print
$ ag -l clean:: project
$ ack -l clean:: project
# In ZSH shell:
% for p in $(project/**/Makefile); do grep -H clean:: $p; done
```

Question 2
> Say you have a single file—named data.txt—with “|pipe|separated|data|” within. Please furnish a command that prints out the second column for all lines where the first column contains the word “foo” and the third contains “bar”. For example, the following file:

>|foo|Hello|bar|
|bar|Greetings|foo|
|baz|Programs|bob|
|foo|World|bar|

would produce the output:

>Hello
World

sample answer:
```
$ sed -n 's/|foo|\(.*\)|bar|/\1/p' data.txt
$ awk -F'\|' '/\|foo\|.*\|bar\|/ {print $3}' data.txt
$ perl -F'\|' -a -ne 'print @F[2]."\n" if m/\|foo\|.*\|bar\|/' data.txt
$ perl -ne 's/\|.*\|(.*)\|.*\|/\1/ and print if m/\|foo\|.*\|bar\|/' data.txt
```

Question 3
> Say you have a single file—named data.txt—with “|pipe|separated|data|” within. Please furnish a command that prints out the counts for each word. For example, the following file:

>|foo|Hello|bar|
|bar|Greetings|foo|
|baz|Programs|bob|
|foo|World|bar|

would produce the output:


>   8 
   1 Greetings
   1 Hello
   1 Programs
   1 World
   3 bar
   1 baz
   1 bob
   3 foo

Example answers
```
sed 's/\|/\n/g' data.txt | sort | uniq -
```

## Technical Evaluation
### Nota Bene
For all leveling standards, a “line” is delimited by either newline “\n” or semicolon “;”. Therefore `for l in $(cat foo); do echo $l; done` counts as three lines, not one!

### Minimum Standard to Pass
The candidate must answer at least one question, no matter how many lines or pipeline steps they may require to do so.

### Level 4 and below
The candidate has answered at least a single question. Further, the candidate’s answer uses many lines or pipeline steps (>3).

### Level 5
The candidate has answered at least two questions. Further, the candidate’s answers use few lines or pipeline steps (2..3).

### Level 6 and above
The candidate has answered at least three questions. Further, the candidate’s answers use one line and no more than one pipeline step.

## Sort Digits (5 min)
Question
This is a warm-up question, a bit algo initially, but more interesting to focus on practical aspects:

Say you have 100M digits (so only 0, 1, 2, 3, 4, 5, 6, 7, 8, 9) in an array. How would you sort this array? What is the time complexity of the algorithm?

Most candidates will be familiar with standard sorting algorithms such as quicksort and mergesort. If they are having trouble starting or making any progress you can use the following questions:

Do you know any sorting algorithms that you can use off the shelf?

Great starting point! what's the complexity of those?

Do you think we can do faster than O(n log n)?

If they keep getting stuck you can pivot to the next set of hints:

What do we know about the contents of this array?

How do we take advantage of the fact that there's only 10 unique values

The final set of hints are below:

What does the result look like? [00...011...12...999]

What's the fastest way to create this array? if you just focus on creating this array?

What do we need to know to create such an array?

The correct solution would be to count all digits in a separate array, and then write them back. The optimal solution is an in-place algo, linear time. Some candidates may subsitute using a dictionary instead of an array. A followup question would be is there some other data structure they can use that's faster? Once they get to an array a good question to ask is why it is faster? The correct answer would be that arrays can take advantage of simple CPU operations and memory access patterns while avoiding the overhead of hashing in dictionaries.

Most people figure this out fairly quickly. The follow up question is:

Let's say you implemented this in a compiled language (eg. Java, C). Further, let’s say that you run this on a computer with plenty of RAM and a 1GhZ single-threaded CPU. Finally, let’s say that once the program starts, the OS won’t ever interrupt it. How long would it take to run this? Just looking for orders of magnitude here. What's the bottleneck?

The point isn't to look for a close answer here, it's more to weed out candidates who have no clue. I've had candidates estimate it to be several million seconds (i.e. multiple weeks) or a few milliseconds. A reasonable answer is around 0.10s..1.0s. There are a few ways to get to this answer:

1. Minimum possible time: 100M * 1 cycle = 100M cycles, or 0.1s.

1. Need about 100M * 10 clock cycles for this, our assumed computer runs at 1 GHz, so 1.0s.

1. Memory bandwidth is a few hundred megabytes per second, so 100MB pages from RAM in about 1.0s.

## Technical Evaluation for General Engineer Candidates
Minimum Standard to Pass
The candidate is able to articulate a correct answer that sorts the list.

Level Three and Below
One or more of the following statements is true:

The candidate’s algorithm is unable to sort the list with asymptotic complexity less than O(n lg n).

The candidate is unable to reason about the asymptotic complexity of their solution.

The candidate’s wallclock time is off by 2 or more orders of magnitude (e.g. They claim their solution runs in nanoseconds, hours or days).

The candidate needs an excessive amount of help to get to the optimal solution (more than 5 hints).

Level Four
The candidate articulates a linear time solution—such as count sort—and correctly reasons an asymptotic complexity of O(n) with a small amount of help (hints in the range of (3, 5]). The candidate determines a wallclock time within 1 order of magnitude (milliseconds, seconds, minutes) but cannot justify their claim, resorting to claiming experience or feelings.

Level Five
The candidate articulates a linear time solution—such as count sort—and correctly reasons an asymptotic complexity of O(n) with minimal help (hints in the range of [1, 3]). The candidate determines a wallclock time within 1 order of magnitude (milliseconds, seconds, minutes) and justifies their claim using logic and reason.

Level Six and Above
The candidate articulates a linear time solution—such as count sort—and correctly reasons an asymptotic complexity of O(n) with no help. The candidate determines a wallclock time in the range [0.1, 1.0] seconds and justifies their claim with logic and reason.

## Technical Evaluation for DevEx Engineering Candidates
Minimum Standard to Pass
The candidate is able to articulate a correct answer that sorts the list.

Level three
The candidate is able to articulate a correct answer that has an asymptotic runtime of O(n) or O(n lg n), perhaps with either many (>5) hints or much “hand holding”.

Level four
The candidate is able to articulate a correct answer that has an asymptotic runtime of O(n), perhaps with many hints (3..5) or some “hand holding”.

Level five
The candidate is able to articulate a correct answer that has an asymptotic runtime of O(n) with few (1..2) light hints and no “hand holding”.

Level six and above
The candidate is able to articulate a correct answer that has an asymptotic runtime of O(n) with no hinting or “hand holding”.

# Video upload (20 min)
Question
Let's say your building a web interface that lets people upload a video and it then converts it and adds a bunch of special effects etc. Ideally it's a simple page where the user can upload a video. We show a progress bar for the upload and then a progress bar for the conversion since it might take a long time to do (minutes or hours). Once it's done the web pages should show a link so the user can download it. To illustrate this on the whiteboard, draw the frontend of the webpage as the figure below.


 

Let's not worry about the conversion itself – we can treat it as a black box. Specifically the conversion is handled by an FX library where you pass it a file reference to the video and it outputs a converted video file with effects. A convenience function exists in the library which gives an onProgress hook which outputs the current percentage completed for the conversion from 0 - 100% and can run arbitrary code. If you want to be more specific, the transcoding function will return to you a file handle of the transcoded file and will take as arguments a file handle and a callback function which will fire periodically; the argument to it will be the percentage of transcoding completion and the arguments of the job itself can assume to be in scope.


 

Tell the candidate that we are going to build a server architecture to support this functionality. The resulting architecture should be robust enough to intermittent failures in server components and handle a moderate amount of traffic (between 5-20 users using the service at any given time if asked for specifics). At this point ask the candidate if they have any questions or anything is unclear.

Note that after answering/ clarifying any questions, the interview usually goes 2 ways. If the candidate has more experience with system design problems, they will usually be comfortable with tackling the problem directly without much guidance. For candidates who have not done many system design questions, you can guide them with the following questions.

- One way of easing them in is asking how the initial web page is hosted. What server would they use and how is the initial page hosted?

  - A reasonable answer should include some server statically hosting HTML/JS/CSS and delivering them to the client when they visit a certain web page.

  - Some example answers should be an nginx/apache server hosting files, a nodejs application serving files statically or S3/cloudfront sharing the files statically.

- As a followup to the first question, you can ask how does the video get to your system. Where does the video file ultimately get stored?

  - It should suffice for the candidate say they will use the browser API to post the original video file their web backend. Although not strictly required, the candidate may proceed to detail how the frontend works i.e use a react single page application with the dropzone library to upload to their server using a multi part form upload.

  - As far as where the file should be stored, most candidates will have experience with a block storage system like Amazon S3 or Azure Blob storage. Some candidates may say they would use a networked file system. Although it would make the system a bit more complex to implement and manage, it should be an acceptable solution.

  - Some non ideal solutions for where the file should be stored include storing the video files on the application server disk or in a database. Although technically these could work in limited cases, they run into issues when processing more than 1 file concurrently. You can proceed to the next part of the question and get back to this if time allows and the candidate has explored more of the problem. Usually by simply asking the candidate how they would update their architecture to support more video processing scale, they will correct this part of the problem.

- Once the video is uploaded, how do you start the conversion?

  - A nonideal solution would be to have the same application server process the video after it has been uploaded in the same post request.

  - A better solution would involve making a post request to a separate conversion microservice with the address of where the video is stored. The separate microservice could download the video locally, do the conversion and then upload the new video back to the object store.

  - An ideal solution would involve having a queue where the main application server enqueues a job with the address of the newly uploaded video. A worker microservice would dequeue the job, download the video, do the conversion and upload the new video back to the object store before marking the job as complete.

- How would you implement the progress bar?

  - A nonideal solution would involve a microservice/worker making direct updates to the frontend as the onProgress event is fired and usually this shows a lack of understanding about HTTP requests. Usually by asking how does the client initiate the request to the worker/microservice, the candidate can self correct.

  - A good solution would involve the microservice/worker updating a database table or cache entry with the progress of the video using the onProgress hook provided by the video FX library. From here either the frontend could poll an application server progress endpoint with the video id or the frontend could initiate a websocket with an application server which pushes out updates as the progress updates.

- What happens when the user clicks on the final link. How does the user get the processed video?

  - A good answer would involve making a request to the application server and the server looking up the processed video in the object store and returning it.

  - A nonideal answer would have the browser directly get the object from S3. Unless the candidate talks about using pre signed tokens or some other mechanism to ensure only the specific client can access that file, this is usually a bad answer. Usually the candidate can self correct if you ask them how they would update the system to ensure that only the specific browser that uploaded the video can download it.


### Technical Evaluation
1/5: The candidate is unable to give a system design capable of supporting video upload/conversion for any number of users

2/5: The candidate is able to give a very basic system design capable of supporting a very limited number of users

The candidate may have a suboptimal way of addressing 2 or more the requirements

The candidate needed substantial coaching to get to a system capable of meeting the basic requirements

The candidate may have HTTP requests that are not possible (i.e microservice initiating a request to the webpage without having the webpage setup a socket connection first)

3/5: The candidate is able to give a system design capable of supporting a reasonable number of users. Although the candidate has an architecture that works they may have demonstrated a suboptimal way of addressing 1 of the requirements

4/5: The candidate is able to give a robust system design capable of supporting a large number of users.

5/5: The candidate is able to give a robust system design capable of supporting a large number of users. In addition the candidate may have demonstrated 2 or more of the following conditions

The candidate brought up tradeoffs in their decision making and gave a reasonable reason why they chose a certain route over another. (i.e using a cache vs database for storing progress, using long polling vs websockets for sending messages to the frontend etc)

The candidate is able to list different example of specific technologies for each item in their system (i.e nginx/apache for web server, activemq/rabbitmq for queue, mysql/dynamoDB for database etc)

The candidate went out of their way to account for different failure scenarios (i.e accounting for failed transcoding jobs, hard disk storage on application servers, etc)

## Compare Data stores (10 min)
Question
Ask the candidate what different data stores they are familiar with. You can pull this directly from their resume or ask them directly. Most of the time, the data stores fall into the following groups: Relational (Postgres, MySQL, MariaDB), Caching (Memcached, Redis), NoSQL (MongoDB, Elasticsearch, Cassandra, DynamoDB, Neo4j), Data Warehouse (Redshift, Cloudspanner, Snowflake). Ideally they should be able to list 3 different data stores that map out one of the categories listed however 2 data stores should suffice for less experienced candidates. Write a data store from each of the categories that they list on the board and ask the candidate the following questions

1. How do you query the following data stores?
1.  What are some examples of workloads that each data store is good at?
1.  What are some examples of workloads that each data store would be a poor choice for?
1.  How do each of the data store scale the amount of requests they can serve?
1.  How is data stored within each data store?

Note if the candidate is having a rough time with questions 1-3, you can skip the questions 4-5 and cut to answering questions about Better for the candidate. If the candidate finished early or you need to make time, you can probe their experience by asking them to describe the project where they used the specific data store and their specific role in it.

**Example Answers**
**Relational (Postgres, MySQL, MariaDB)**
- Query using SQL

- What are some great use cases for this data store?

  - Is pretty good all around but really shines when data is relational and structured.

  - When the need for data integrity is high or support for transactions and ACID

- What are some examples of poor workloads?

  - At massive scales relational databases tend to be bottlenecks for transactional applications and get complicated to scale

  - When your data is represented as a hierarchy or graph it might tough to model in a relational database

- How does the system scale?

  - Can scale vertically through getting a better computer

  - Can scale reads horizontally through read replicas and balancing out reads

  - Can scale horizontally through sharding based on region, id etc

- How is data stored?

  - Data is stored as columns, rows, and tables

  - Data is persisted to block storage (usually hard disk), indexes are usually stored in tree like structure like btree's for quick lookups

**Caching (Memcached, Redis)**
- Query key value lookups

- What are some great use cases for this data store?

  - Is good for very quick lookups where data can be found with a simple key

- What are some examples of poor workloads?

  - Bad for any data that needs to be persisted and durable

- How does the system scale?

  - Usually caches are bounded by the size of their memory (RAM). Increasing memory resources is a way of scaling vertically

  - Modern caches can also be deployed in cluster configurations with read only replicas or sharding configurations

- How is data stored?

  - Data is stored in memory as a large hashmap and some caches offer the ability to persist data to the harddisk

**NoSQL (MongoDB)**
Mongo's Query JSON syntax

- What are some great use cases for this data store?

  - Good for schemaless projects, i.e. rapid prototyping where the schema may change a lot

  - Large scale applications where consistency guarantees are not as important (i.e large volume analytics storage, facebook like button etc)

- What are some examples of poor workloads?

  - Highly relational data with foreign key relationships can be difficult to model in Mongo

  - Traditionally MongoDB has traded consistency guarantees for ease of horizontal scaling. Although Mongo is improving its consistency guarantees, applications that depend on transactions and high consistency may not be the best choice (i.e. maintaining an ATM's cash state)

- How does the system scale?

  - Scales pretty well horizontally. By adding another node, reads and write capacity is expanded without sacrificing other metrics like latency.

- How is data stored?

  - Data is stored in collections, documents and fields

  - Documents are stored in BSON, a binary form of JSON optimized for space and traversal. BSON is optimized for encoding/decoding and adds additional data types to json

**Data Warehouse (Redshift, Snowflake)**
Most systems use some SQL dialect to query data

- What are some great use cases for this data store?

  - Good for storing a large amount of data, often aggregates from multiple other data stores

  - Optimized for analytical (OLAP) type queries for business reporting etc

- What are some examples of poor workloads?

  - Highly transactional type workloads with high writes are usually a poor choice (ecommerce or any consumer facing site)

- How does the system scale?

  - Scales pretty well horizontally adding more nodes allows for expanded read and storage capacity.

  - Many managed data warehouse systems take the operations away from teams and scaling is just a matter of increasing a capacity dial

- How is data stored?

  - Since analytical type workloads aggregate across lots of rows, they often store data in a columnar fashion. by storing in a columnar fashion, it greatly reduces the amount of resources (disk space, memory, cpu) to perform a query as well as optimizes for the compression of data on the disk.

### Technical Evaluation
The criteria below should be used as a rough guideline for evaluating the candidate's performance. Since the question is rather freeform, there is a lot of leeway in what the candidate can say and how to interpret the answers. The best answers should leave the interviewer with a firm belief that a candidate is thoroughly familiar with their tools and they can recognize the tradeoffs in the tools that they use.

1/5: The candidate gives incorrect/ nonsensical answers for the data stores they listed. They can only answer 1 or less of the questions above per datastore that they listed.

2/5: The candidate can only speak very generally about the data stores they listed. They can only reliably answer 2 of the questions above per datastore that they listed

3/5: The candidate can answer 3 questions per data store they listed

4/5: The candidate can answer 4 questions per data store they listed

5/5: The candidate can answer all of the questions thoroughly for the data stores they listed

Note that for this question it is common for candidates with less experience to only have experience with 2 data stores (usually relational db and cache). However for candidates with more experience it is usually expected that they have experience with 3 data stores or deeper experience in just 2 data stores.

### Overall Evaluation
The questions presented above should are not designed necessarily with 1 ideal answer, but aim to explore how the candidate analyzed the scenario, their problem-solving approach, collaboration and communication style. While not testing these things directly, if anything in these areas stands out it should be noted in the interview feedback in greenhouse. While technical evaluation guidance is given, it is not necessarily firm rules for how to evaluate a candidate. When in doubt it is much more helpful to write out details describing the solutions and conversations with the candidate rather than simply assigning a number. It will help the hiring manager have more details to make a wholistic decision on whether or not to hire the candidate.

##  Performance Metrics
**Sort 100M digits**
L4
Can (with minor hints) understand that the digits are just “values” and have no “identity” (like objects) – we’re not constrained to algorithms that simply rearranges them

L5
Realizes (with minor hints) that we just need to count the digits

Can (with minor hints) understand why a simple array is marginally faster than a hash table for counting

Can mention some basic numbers about computer speed, eg that a typical computer is 3Ghz

L6+
Finds the counting algorithm with essentially no hints

Understand the difference between instructions and cycles

Can do the math themselves and end up with a runtime estimation that’s within an order of magnitude

Can explain why a simple array is faster than a hash table without any hints

**Video conversion**
L4
Talks about the frontend to backend initial requests

Sketches out the basic API endpoints for putting the video and getting the status updates

Explains how the conversion happens on the backend (eg through a subprocess that parses standard out and stores updates)

Explains a simple polling mechanism for status updates

Understands that you can’t leave the page when you’re uploading, but understands that you can leave the page and come back during conversion

L5
Can suggest a storage mechanism for the files (like S3), not just local storage on the server

Can point out the basic bottlenecks in terms of scalability (that the conversion is CPU-bound)

Can suggest a permanent URL as a solution to the case when you close the page at the conversion step but come back to it later.

L6
When it’s pointed out that S3 supports direct upload from frontend, can figure out based on basic security arguments that there’s some token exchange through the backend

Proactively suggests having multiple workers scaling independently of the service

Understands the complexity of a push (websocket) based solution for status updates from the workers

For the polling case, can suggest a database solution to store state

When pointed out that you can track the upload frontend-side, understands how that simplifies the problem

Can talk about different REST verbs for the endpoint

If they push for a serverless architecture, understands why runtime limitations (15 min execution time per lambda) would be a problem

L7
Suggests using a queue or similar to farm out work to workers

Suggests straight upload from the frontend to S3 (or equivalent cloud storage) and the token exchange

Talks about autoscaling of the workers

Can figure out a working websocket based push mechanism for status updates

Can argue about how the database sits in the architecture, understands why having both workers and servers connect to the database would be a bit of an “integration database” antipattern, suggests having an internal endpoint

Knows that there’s frontend-side callback handlers in the browser to track upload progress browser-side with no backend involved

Doesn’t argue for a serverless architecture since the runtime limitations would make it very difficult

Can talk about the different frontend routes and when you transitions from one URL to another

(Bonus question) if you want to delete files after say 24h, they know you can do that using an expiration policy on the S3 bucket (or similar)