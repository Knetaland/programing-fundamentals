﻿using System;
using System.Text;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.SQ;

public class TextEditor: IProblem {
     public void Run() {
        var editor = new Editor();
        editor.typeIn('c');
        editor.typeIn(' ');
        editor.typeIn('o');
        editor.moveCursor(CursorDir.LEFT);
        editor.typeIn('y');
        editor.newLine();
        editor.moveCursor(CursorDir.RIGHT);
        editor.typeIn('u');
        editor.moveCursor(CursorDir.RIGHT);
        editor.moveCursor(CursorDir.RIGHT);
        editor.newLine();

        var res = editor.print();
        res.Print();
     }
     /* 
     c y*
     ou

     */
}
public class Editor
{
    private StringBuilder _sb;
    private int _cursor;
    public Editor()
    {
        _sb = new StringBuilder();
    }
    public void typeIn(char c) {
        _sb.Insert(_cursor, c);
        _cursor++;
    }

    public void backspace() {
        if (_cursor >0) {
            _sb.Remove(_cursor, 1);
            _cursor--;
        }
    }

    public void moveCursor(CursorDir dir) {
        switch(dir) {
            case CursorDir.LEFT:
                if (_cursor > 0) {
                    _cursor--;
                }
                break;
            case CursorDir.RIGHT:
                if (_cursor < _sb.Length) {
                    _cursor++;
                }
                break;
            case CursorDir.UP:
                break;
            case CursorDir.DOWN:
                break;
        }
    }

    public void newLine() {
        typeIn('\n');
        // _sb.
    }

    public string print() {
        return _sb.ToString();
    }
}

public enum CursorDir {
    LEFT,
    RIGHT,
    UP,
    DOWN,
}
