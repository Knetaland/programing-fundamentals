﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamentals;

public class FamilyTreeProblem : IProblem
{
    public void Run() {
        var familyTree = new FamilyTree();
        familyTree.AddParentChildrenRelationship("kevin", "nathan", "zac");
        familyTree.AddParentChildrenRelationship("maggie", "nathan", "zac");
        familyTree.AddParentChildrenRelationship("zhen", "kevin", "justin");
        familyTree.AddParentChildrenRelationship("saiying", "kevin", "justin");
        familyTree.AddParentChildrenRelationship("fen", "maggie", "dongfeng");
        familyTree.AddParentChildrenRelationship("keng", "maggie", "dongfeng");
        familyTree.AddParentChildrenRelationship("jinlian", "zhen", "xiaogu", "dagu");

        string[] names = ["kevin", "zac", "maggie", "nathan", "justin", "zhen"];
        foreach(var name in names) {
            Console.WriteLine($"processing data for {name}");
            Console.WriteLine("Parents");
            var parents = familyTree.GetParents(name);
            parents.Select(p => p.Name).ToList().Print();
            Console.WriteLine("GrandParents");
            var gparents = familyTree.GetGrandParents(name);
            gparents.Select(p => p.Name).ToList().Print();
            Console.WriteLine("Siblings");
            var siblings = familyTree.GetSiblings(name);
            siblings.Select(p => p.Name).ToList().Print();
        }
        
        


    }
}

public class FamilyTree
{
    private Dictionary<string, Person> tree  = new Dictionary<string, Person>();
    
    public void AddPerson(string name) {
        tree.TryAdd(name, new Person(name));
    }

    public void AddParentChildrenRelationship(string parent, params string[] children) {
        if (!tree.ContainsKey(parent)) {
            AddPerson(parent);
        }
        foreach(var childName in children) {
            if (!tree.ContainsKey(childName)) {
                AddPerson(childName);
            } 
            tree[parent].Children.Add(tree[childName]);
            tree[childName].Parents.Add(tree[parent]);
        }
    }


    public List<Person> GetParents(string name) {
        if (!tree.ContainsKey(name)) {
            return [];
        }
        return tree[name].Parents;
    }

    public List<Person> GetGrandParents(string name) {
        var res = new List<Person>();
        if (tree.ContainsKey(name)) {
            var parents = tree[name].Parents;
            foreach(var parent in parents) {
                if (tree.ContainsKey(parent.Name)) {
                    var grandParents = tree[parent.Name].Parents;
                    res.AddRange(grandParents);
                }
            }
        }
        return res;
    }

    public List<Person> GetSiblings(string name) {
        var res = new HashSet<Person>();
        if (tree.ContainsKey(name)) {
            var parents = tree[name].Parents;
            foreach(var parent in parents) {
                var children = parent.Children;
                foreach(var child in children) {
                    if (child.Name != name) {
                        res.Add(child);
                    }
                 }
            }
        }
        return res.ToList();
    }


    public class Person {
        public string Name { get; set; }
        public List<Person> Children { get; set; }
        public List<Person> Parents { get; set; }

        public Person(string name)
        {
            Name = name;
            Children = new List<Person>();
            Parents = new List<Person>();
        }
    }
}
