﻿using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.SQ;

public class Connect4 : IProblem
{
    public void Run() {
        var game = new ConnectGame(3, 6, 4);
        game.play(0, ConnectGameColor.RED);
        game.play(0, ConnectGameColor.YELLOW);
        game.play(1, ConnectGameColor.YELLOW);
        game.play(1, ConnectGameColor.YELLOW);
        game.play(0, ConnectGameColor.YELLOW);
        game.play(1, ConnectGameColor.YELLOW);
    }
}

public enum ConnectGameColor {
    YELLOW,
    RED
}

public class ConnectGame {
    // public const char YELLOW = 'Y';
    // public const char RED = 'R';
    public const char EMPTY = '_';
    public int row;
    public int col;
    public int toWin;

    private char[][] _grid;

    public ConnectGame(int m, int n, int win) {
        row = m;
        col = n;
        toWin = win;
        _grid = new char[m][];
        for(var i = 0; i < m; i++) {
            _grid[i] = new char[n];
            Array.Fill(_grid[i], EMPTY);
        }
        Print();
    }

    public bool checkWinner(int x, int y, char color) {
        var count  = 1; // current
        int[][] dirs = [
            [0,1], [0,-1], [1,0], [-1,0]
        ];

        
        foreach(var dir in dirs) {
            // Console.WriteLine($"going direction [{dir[0]}, {dir[1]}]");
            int r = x + dir[0];
            int c = y + dir[1];
            while (r >=0 && c >=0 && r < row && c < col && _grid[r][c] == color) {
                count++;
                // Console.WriteLine($"count is {count}");
                if (count == toWin) return true;
                r  += dir[0];
                c  += dir[1];
            }
        }
        return count >= toWin;

        bool bfs() {
            var connected = 0;
            var queue = new Queue<(int,int)>();
            queue.Enqueue((x,y));
            var visited = new bool[row,col];
            while(queue.Count > 0 ) {
                var pos = queue.Dequeue();
                connected++;
                visited[pos.Item1,pos.Item2] = true;
                if (connected == toWin) return true;
                foreach(var dir in dirs) {
                    var r = x + dir[0];
                    var c = y + dir[1];
                    if (r >=0 && c >=0 && r < row && c < col && !visited[r,c] && _grid[r][c] == color) {
                        queue.Enqueue((r,c));
                    }
                }
            }
            return false;
        }
    }


    public void play(int col, ConnectGameColor color) {
        Console.WriteLine($"Placing {color} on {col}");
        var index = row -1;
        while(index >=0 && _grid[index][col] != EMPTY) {
            index --;
        }
        if (index < 0) {
            Console.WriteLine("invalid move");
            return ;
        } else {
            _grid[index][col] = getColor(color);
        }
        Print();
        if (checkWinner(index, col, getColor(color))) {
            Console.WriteLine("Winnder found");
        }
    }

    private char getColor(ConnectGameColor color) {
        switch(color) {
            case ConnectGameColor.RED:
                return 'R';
            case ConnectGameColor.YELLOW:
                return 'Y';
            default:
                return '_';
        }
    }



    public void Print() {
        foreach(var row in _grid) {
            Console.WriteLine(String.Join(' ', row));
        }
    }
}
