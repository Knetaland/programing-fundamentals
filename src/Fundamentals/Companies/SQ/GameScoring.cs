﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.SQ;

public class GameScoring : IProblem
{
    public void Run() {
        var game = new Game("Cow");
        game.AddScores([
            ("K", 10),
            ("B", 10),
            ("A", 10),
        ]);
        var res = game.GetLeaderBoard();
        var game2 = new Game("Beef");
         game2.AddScores([
            ("K", 11),
            ("B", 10),
            ("A", 10),
        ]);
        var res2 = game2.GetLeaderBoard();

        foreach(var p in res) {
            Console.WriteLine($"[{p.Key}: {p.Value}]");
        }
        Console.WriteLine("GAME 2");
        foreach(var p in res2) {
            Console.WriteLine($"[{p.Key}: {p.Value}]");
        }

        Console.WriteLine("Calcualting total");
        var total = CalculateTotal([res, res2]);
        foreach(var p in total) {
            Console.WriteLine($"[{p.Key}: {p.Value}]");
        }


    }

    public Dictionary<string, double> CalculateTotal(IEnumerable<Dictionary<string,double>> games) {
        var res = new Dictionary<string, double>();
        foreach(var game in games) {
            foreach(var score in game) {
                res[score.Key] = res.GetValueOrDefault(score.Key, 0) + score.Value;
            }
        }
        return res;
    }
}

public class Game {
    public string name;
    // public Dictionary<string, double> scoreBoard;
    private List<(string, int)> _scores;

    public Game(string gameName)
    {
        name = gameName;
    }

    public void AddScores(IEnumerable<(string, int)> playerScores) {
         _scores = playerScores.ToList();
    }

    public Dictionary<string, double> GetLeaderBoard() {
        var scoreBoard = new Dictionary<string, double>();
        _scores.Sort((a,b) => b.Item2 - a.Item2); // sort by point
        var topScore = _scores.Count;
        for(var i = 0; i < _scores.Count; i++) {
            var totalScore = topScore;
            var players = new List<string>
            {
                _scores[i].Item1
            };
            while(i+1 < _scores.Count && _scores[i+1].Item2 == _scores[i].Item2) {
                players.Add(_scores[i+1].Item1);
                topScore--;
                totalScore += topScore;
                i++;
            }
            foreach(var player in players) {
                scoreBoard.Add(player, (double)totalScore/players.Count);
            }
            topScore--;
        }
        return scoreBoard;
    }
}
