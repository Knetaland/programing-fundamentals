﻿using System;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamentals;

public class PairCoding : IProblem
{
    public void Run() {
        // var test = new ApiSimulator();
        // for(var i = 0; i < 10; i++) {
        //     Console.WriteLine("Running test " + i);
        //     var res = test.RemoteCallWrapper("test");
        //     res.Print();
        // }

        //  Question 2
        // RouteDifficulty routeDifficulty = new RouteDifficulty {
        //     Difficulty = 5.0,
        //     Name ="Kevin",
        // };
        
        // RouteDifficulty routeDifficulty1 = new RouteDifficulty {
        //     Difficulty = 5.1,
        //     Name ="Maggie",
        //     Level = 'a'
        // };
        // RouteDifficulty routeDifficulty2 = new RouteDifficulty {
        //     Difficulty = 5.1,
        //     Name ="Justin",
        //     Level = 'b'
        // };
        // RouteDifficulty routeDifficulty3 = new RouteDifficulty {
        //     Difficulty = 5.1,
        //     Name ="Nathan",
        // };
        // routeDifficulty.Compare(routeDifficulty1);
        // routeDifficulty1.Compare(routeDifficulty2);
        // routeDifficulty1.Compare(routeDifficulty3);
        // routeDifficulty.Compare(routeDifficulty3);
        
        // 3
        // var question = new MapToBinaryTree();
        // var res = question.ToMap("aabbbbbcDDD");
        // foreach(var pair in res) {
        //     Console.WriteLine($"{pair.Key} : {pair.Value}");
        // }
        // var tree = question.ToTree();
        // tree.Print();

        // var qes = new Pagination();
        // var res = qes.GeneratePagination(10, 7, 3);
        // res.Print();

        var que = new TaxCalculation();
        var res = que.CalculateTax(243799);
        Console.WriteLine(res);

    }
}

public class ApiSimulator { 
    Random random;
    public ApiSimulator() {
        random = new Random();
    }
    public string RemoteCall(string input) {
        var pos = random.Next(100);
        if (pos >=0 && pos <=49) {
            return $"{input}-processed";
        } else if (pos > 49 && pos <= 75) {
            throw new CustomException(1, "Transient");
        } else {
            throw new CustomException(2, "Fatal");
        }
    }

    public string RemoteCallWrapper(string input, int retryLimit = 3) {
        // var completed = false;
        var currentRetry = 0;
        while (true) {
            try {
                return RemoteCall(input);
            } catch(CustomException ce) {
                if (ce.Code == 1) {
                    Console.WriteLine("transient error, retry...");
                    currentRetry++;
                    if (currentRetry >= retryLimit) {
                        return "retry exhausted";
                    }
                } else if (ce.Code == 2) {
                    return "fatal error";
                }
            }
        }
    }
}

public class CustomException : Exception {
    public int Code {get; set;}
    public CustomException(int code, string error) : base(error)
    {
        Code = code;
    }
}


public class RaceLeague {
    private List<List<PlayerScore>> _scoresBySeason;

    // public 
    public void Print() {
        Console.WriteLine("Rider    Points");
        Console.WriteLine("---------------");
        foreach(var scores in _scoresBySeason) {
            foreach(var score in scores) {
                Console.WriteLine($"{score.Name}    {score.Score}");
            }
        }
    }


    public class PlayerScore {
        public string Name { get; set; }
        public int Score { get; set; }
    }
}


public class RouteDifficulty : IComparable<RouteDifficulty>
{
    public string Name { get; set; }
    public double Difficulty { get; set; }
    public char Level { get; set; }
    public int CompareTo(RouteDifficulty other) {
        if (other.Difficulty != Difficulty ) {
            return Difficulty.CompareTo(other.Difficulty);
        }
        if (Level != null && other.Level != null) {
            return Level.CompareTo(other.Level);
        }
        if (Level != null) return 1;
        else return -1;
    }

    public void Compare(RouteDifficulty other) {
        if (this.CompareTo(other) > 0) {
            Console.WriteLine($"{this.ToString()} is Harder than {other.ToString()}");
        } else if (CompareTo(other) < 0) {
            Console.WriteLine($"{this.ToString()} is Easier than {other.ToString()}");
        } else {
            Console.WriteLine($"{this.ToString()} is same difficulty as {other.ToString()}");
        }
    }

    public string ToString() {
        var res = $"{Name}: {Difficulty}";
        if (Level != null) {
            res += Level.ToString();
        }
        return res;
    }
}


public class MapToBinaryTree {
    private Dictionary<char, int> _map = new Dictionary<char, int>();

    public Dictionary<char, int> ToMap(String input) {
        foreach(var c in input) {
            _map[c] = _map.GetValueOrDefault(c, 0) + 1;
        }
        return _map;
    }

    public SQTreeNode ToTree() {
        var list = new List<(char, int)>();
        foreach(var c in _map.Keys) {
            list.Add((c, _map[c]));
        }
        if (list.Count ==  1) {
            return new SQTreeNode(list[0].Item1, list[0].Item2);
        }
        list.Sort((a,b) => b.Item2 - a.Item2);
        return rec(0);

        SQTreeNode rec(int index) {
            if (index == list.Count - 2 ) {
                var left = new SQTreeNode(list[index].Item1,list[index].Item2);
                var right = new SQTreeNode(list[index+1].Item1,list[index+1].Item2);
                return new SQTreeNode('#',left.Count + right.Count) {
                    Left = left,
                    Right = right,
                };
            }
            var leftNode = new SQTreeNode(list[index].Item1, list[index].Item2);
            var rightNode = rec(index+1);
            return new SQTreeNode('#',leftNode.Count + rightNode.Count) {
                Left = leftNode,
                Right = rightNode,
            };
        }
    }
}

public class SQTreeNode {
    public char Name {get; set;}
    public int Count { get; set; }
    public SQTreeNode Left { get; set; }
    public SQTreeNode Right { get; set; }

    public SQTreeNode(char name, int count) {
        Name = name;
        Count = count;
    }
    public void Print() {
        PrintTree(this);
    }

    void PrintTree(SQTreeNode root)
    {
        if (root == null)
            return;

        Console.Write("(");
        PrintTree(root.Left);
        Console.Write(root.Name + ", " + root.Count);
        PrintTree(root.Right);
        Console.Write(")");
    }
}


public class Pagination {
    public string GeneratePagination(int totalPages, int currentPageRange, int currentPage)
    {
         // Define the range around the current page to display
        int start = Math.Max(1, currentPage - currentPageRange / 2);
        // Console.WriteLine($"start: {start}");
        int end = Math.Min(totalPages, start + currentPageRange - 1);
        // Console.WriteLine($"end: {end}");

        // Adjust the start index if necessary to keep the range size fixed
        start = Math.Max(1, end - currentPageRange + 1);

        // Create the pagination string
        string pagination = start == 1 ? "" : "<";

        // Add the pages to the pagination string
        for (int i = start; i <= end; i++)
        {
            pagination += (i == currentPage) ? " *" : $" {i}";
        }
        if (end < totalPages) {
            pagination += " >";
        }
        
        return pagination;
    }
}

public class TaxCalculation {
    private readonly Dictionary<double, double> TaxBrackets = new Dictionary<double, double>
    {
        { 11600, 0.1 },
        { 47150, 0.12 },
        { 100525, 0.22 },
        { 191950, 0.24 },
        { 243725, 0.32 },
        { double.MaxValue, 0.35 }
    };

    public double CalculateTax(double income)
    {
        double tax = 0;
        double previousBracket = 0;

        foreach (var bracket in TaxBrackets)
        {
            if (income <= bracket.Key)
            {
                tax += (income - previousBracket) * bracket.Value;
                Console.WriteLine($"income {income} < {bracket.Key} => tax owed =>  ({income} - {previousBracket}) * {bracket.Value} = {(income - previousBracket) * bracket.Value}");
                Console.WriteLine($"tax owe now: {tax}");
                break;
            }
            else
            {
                tax += (bracket.Key - previousBracket) * bracket.Value;
                Console.WriteLine($"Adding tax: {(bracket.Key - previousBracket)} * {bracket.Value} = {(bracket.Key - previousBracket) * bracket.Value}");
                Console.WriteLine($"tax owe now: {tax}");
                previousBracket = bracket.Key;
            }
        }

        return tax;
    }
}