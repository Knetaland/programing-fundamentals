﻿using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace Fundamentals;


public class EventEmmiterProblem : IProblem 
{
    public void Run() {
        var test = new EventEmmiter();
        var action1 = () => Console.WriteLine("this is action1");
        var action2 = () => Console.WriteLine("this is action2");
        var action3 = () => Console.WriteLine("this is action3");
        var action4 = () => Console.WriteLine("this is action4");
        var pressEvent = "press";
        var pushEvent = "push";
        test.AddListener(pressEvent, action1);
        test.AddListener(pressEvent, action3);
        test.AddListener(pushEvent, action2);
        test.AddListener(pushEvent, action4);

        test.Emit(pressEvent);
        test.Emit(pushEvent);

        Console.WriteLine("After removing event listener");
        test.RemoveListener(pressEvent, action3);
        test.RemoveListener(pressEvent, action2);
        test.RemoveListener(pushEvent, action2);
        test.RemoveListener(pushEvent, action1);

        test.Emit(pressEvent);
        test.Emit(pushEvent);
    }
}

public class EventEmmiter
{
    private Dictionary<string, ISet<Action>> eventListeners = new Dictionary<string, ISet<Action>>();
    private Dictionary<string, ISet<Action<int>>> eventListeners2 = new Dictionary<string, ISet<Action<int>>>();

    public void AddListener(string eventName, Action listerner) {
        if (!eventListeners.ContainsKey(eventName)) {
            eventListeners.Add(eventName, new HashSet<Action>());
        }
        eventListeners[eventName].Add(listerner);
    }

    public void Emit(string eventName) {
        if (eventListeners.ContainsKey(eventName)) {
            foreach(var listener in eventListeners[eventName]) {
                listener();
            }
        }
    }

    public void RemoveListener(string eventName, Action listener) {
        if (eventListeners.ContainsKey(eventName))  {
            eventListeners[eventName].Remove(listener);
        }
    }
}
