﻿using System;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.SQ;

public class ItemPrice : IProblem
{
    private Dictionary<string, Dictionary<DateTime, int>> _logs =  new Dictionary<string, Dictionary<DateTime, int>>();
    public void record(string input) {
        // var map = new Dictionary<string, Dictionary<DateTime, int>>();
        var data = input.Split("\n");
        foreach(var line in data) {
            var item = line.Split(',');
            if (item.Length == 2) {
                var name = item[0].Trim();
                var dateAndPrice = item[1].Trim().Split(' ');
                // Console.WriteLine($"name {name}, dataprice: {dateAndPrice}");
                if (DateTime.TryParse(dateAndPrice[0], out var date)) {  
                    var price = int.Parse(dateAndPrice[1]);
                    // Console.WriteLine($"name {name}, date {date}, price: {price}");
                    if (!_logs.ContainsKey(name)) {
                        _logs.Add(name, new Dictionary<DateTime, int>());
                    }
                    _logs[name][date.Date] = price;
                } else {
                    Console.WriteLine($"invalid date");
                }
            } else {
                Console.WriteLine($"invalid data");
            }
        }
        // foreach(var item in _logs) {
        //     Console.WriteLine($"Item {item.Key}");
        //     foreach(var itemDetails in item.Value) {
        //         Console.WriteLine($"{itemDetails.Key}: {itemDetails.Value}");
        //     }
        // }
    }

    public int getPrice(string name, string dateString) {
        if (string.IsNullOrEmpty(name)) return -1;
        if (DateTime.TryParse(dateString, out var date)) {
            if (!_logs.ContainsKey(name)) return -1;
            if (_logs[name].TryGetValue(date, out var price)) {
                return price;
            }
            return -1;
        }
        return -1;
    }

    public string priceDiff(string name, string startDateString, string endDateString) {
        var startPrice = getPrice(name, startDateString);
        var endPrice = getPrice(name, endDateString);
        if (startPrice == -1 || endPrice == -1 ) return "NA";
        var res = (double) (endPrice - startPrice) / startPrice * 100;
        // Console.WriteLine(res);
        // String.Format("{0:.##}", res)
        // Console.WriteLine($"{Math.Round(res, 2)}%");
        return $"{Math.Round(res, 2)}%";
    }



    public void Run() {
        var input = 
            @"Item 1, 2010-01-01 2200
            Item 1, 2010-01-02 2100
            Item 1, 2010-01-03 2400
            Item 1, 2010-01-03 2500
            Item 1, 2010-01-04 2600
            Item 1, 2010-01-05 2700
            Item 1, 2010-01-06 2900
            Item 1, 2010-01-07 2000
            Item 1, 2010-01-08 3200
            Item 2, 2010-01-01 2300";
        record(input);
        var price = getPrice("Item 1", "2010-01-01");
        var price2 = getPrice("Item 2", "2010-01-01");
        // Console.WriteLine($"price {price}");
        // Console.WriteLine($"price2 {price2}");
        var diff = priceDiff("Item 1", "2010-01-01", "2010-01-03");
        diff.Print();
    }
}
