﻿using System;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.SQ;

public class TimeInOffice : IProblem
{
    public ISet<string> findIntervals(List<(string, int[])> timesInAndOut, int[] query) {
        // timesInAndOut.Sort((a,b) => {
        //     if (a.Item2[0] == b.Item2[0]) {
        //         return a.Item2[1] - b.Item2[1]; //the one ends first be front
        //     } else {
        //        return  a.Item2[0] - b.Item2[0];
        //     }
        // });
        var set = new HashSet<string>();
        foreach(var time in timesInAndOut) {
            Console.WriteLine($"{time.Item1}: [{time.Item2[0]} - {time.Item2[1]}]");
            if(time.Item2[0] <= query[0] && time.Item2[1]>=query[1]) {
                Console.WriteLine($"Adding employee {time.Item1}");
                set.Add(time.Item1);
            }
        }
        return set;
    }

    public void Run() {
        var input = new List<(string, int[])> {
                ("A", [8,17]),
                ("B", [9,18]),
                ("C", [10, 19]),
        };
        int[]  query = [15,19];
        var res = findIntervals(input, query);
        res.Print();
    }
}
