﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Interfaces;

namespace Fundamentals;

public class ChickenFoxProblem : IProblem
{
    public void Run() {
        var game = new FarmerGame();
        game.start();
    }
}

public class FarmerGame {
    private const char CHICKEN = 'C';
    private const char FOX = 'F';
    private const char RICE = 'R';
    private List<char> _homeSide;
    private List<char> _otherSide;
    private bool isAtHomeSide;
    private bool _missionCompleted;
    public FarmerGame() {
        isAtHomeSide = false;
        _homeSide = new List<char>();
        _otherSide = [RICE, FOX, CHICKEN, ];
        Print();
    }

    public void crossRiver() {
        isAtHomeSide = !isAtHomeSide;
    }

    public void Print() {
        Console.WriteLine($"Farmer is at {(isAtHomeSide ? "Home" : "Store")} side");
        Console.WriteLine($"Store side has {string.Join(',', _otherSide)}");
        Console.WriteLine($"Home side has {string.Join(',', _homeSide)}");
        Console.WriteLine("---------------------------------------------------");
    }

    public void start() {
        while(!_missionCompleted) {
            var thingsOnHand = isAtHomeSide ? _homeSide : _otherSide;
            var thingsOnTheOtherSide = isAtHomeSide ? _otherSide : _homeSide;
            char? toTake = null;
            foreach(var thing in thingsOnHand) {
                if (okToTake(thing, thingsOnHand)) {
                    toTake = thing; 
                    break;
                }
            }
            if (toTake.HasValue) {
                Console.WriteLine($"Farmer is taking {toTake.Value} to {(isAtHomeSide ? "store side": "home side")}");
                thingsOnHand.Remove(toTake.Value);    
                crossRiver();
                thingsOnTheOtherSide.Add(toTake.Value);
            } else {
                Console.WriteLine($"Farmer is taking Nothing to {(isAtHomeSide ? "store side": "home side")}");
                crossRiver();
            }
            if (_homeSide.Count == 3 && isAtHomeSide) {
                _missionCompleted = true;
            }
            Print();
        }
    }

    private bool okToTake(char item, List<char> items) {
        // is it ok to take nothing? 
        if (isAtHomeSide && !hasConflict(items)) return false;
        var rest = items.Where(t => t != item).ToList();
        if (isAtHomeSide && rest.Count() == 0) return false;
        if (hasConflict(rest)) {
            return false;
        }
        return true;
    }

    private bool hasConflict(List<char> items) {
        if (items.Contains(FOX) && items.Contains(CHICKEN)) {
            return true;
        }
        if (items.Contains(CHICKEN) && items.Contains(RICE)) {
            return true;
        }
        return false;
    }
}