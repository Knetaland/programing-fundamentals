using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Datadog
{
    public class Find_Disjoint_Tags : IProblem
    {
        /*
            ['apple, facebook, google', 'banana, facebook', 'facebook, google, tesla', 'intuit, google, facebook']
            然后有一个 filter list， 根据 filter list 输出这些 Tags 的补集
            比如 filter by ['apple']那么 return ['facebook', 'google']
            比如 filter by ['facebook', 'google']那么 return ['apple', '‍‍‌‍‌‌‍‍‌‌‍‍‍‌‌‌‍‍tesla','intuit']
        */

        public IList<string> findSets2(string[] data, string[] filters) {
            var map = new Dictionary<string, HashSet<string>>();
            foreach(var d in data) {
                var visited = new HashSet<string>();
                var tags = d.Split(',', StringSplitOptions.TrimEntries);
                foreach(var tag in tags) {
                    if(!map.ContainsKey(tag)) {
                        map[tag] = new HashSet<string>();
                    }
                    foreach(var tag2 in tags) {
                        if(tag2 != tag) {
                            map[tag].Add(tag2);
                        }
                    }
                }
            }

            IEnumerable<string> intersection = null;

            foreach(var filter in filters) {
                if (map.ContainsKey(filter)) {
                    if (intersection == null) intersection = map[filter];
                    else {
                        intersection = intersection.Intersect(map[filter]);
                    }
                }
            }

            if (intersection == null) return new List<string>();
            return intersection.ToList();
        }

        public IList<string> findSets(string[] data, string[] filters) {
            var index = new Dictionary<string, IList<int>>();
            var res = new List<string>();
            for(var i=0; i< data.Length; i++) {
                var tags = data[i].Split(',', System.StringSplitOptions.TrimEntries);
                foreach(var tag in tags) {
                    if (!index.ContainsKey(tag)) {
                        index.Add(tag, new List<int>());
                    }
                    index[tag].Add(i);
                }
            }

            var found = new HashSet<string>();
            IEnumerable<int> intersection = null;
            foreach(var filter in filters) {
                if(index.ContainsKey(filter)) {
                    if (intersection == null) {
                        intersection = index[filter];
                    } else {
                        intersection = intersection.Intersect(index[filter]);
                    }
                }
            }

            //  Console.WriteLine($"intersection is {string.Join(",",intersection)}");
            if (intersection != null) {
                foreach(var i in intersection) {
                    var tags = data[i].Split(',', System.StringSplitOptions.TrimEntries);
                    var temp = tags.Except(filters);
                    foreach(var t in temp) {
                        found.Add(t);
                    }
                }
            }

            return found.ToList();
        }

        public void Run() {
            var input = new [] {
                "apple, facebook, google",
                "banana, facebook",
                "facebook, google, tesla",
                "intuit, google, facebook"
            };
            var filters = new [] {
                "facebook", "google"
            };
            var res = findSets(input, filters);
            Console.WriteLine($"res is {string.Join(",", res)}");
        }
    }
}