using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace Fundamentals.Companies.Datadog{
    public class DATADOG : IProblem
    {
        public IList<int> GetCounter(int[] nums, int bucketCount, int width) {
            /*
                1. put a list of integers into a list of buckets, with a specific bucket width, return counter per bucket
                for example, 
                a list of integers - [1,2,11,20, 100]
                num of bucket - 3
                bucket width - 10
                0-9:       2 (1,2)
                10-19:   1 (11)
                20+:      2 (20, 100)
            */
            var l = nums.Length;
            var buckets = new int[bucketCount];

            foreach(var num in nums) {
                var bucketIndex = getBucket(num);
                buckets[bucketIndex]++;
            }

            return buckets;

            int getBucket(int num) {
                var i = num/width;
                return i >= bucketCount - 1 ? bucketCount -1 : i;
            }
        }
        
        public IList<IList<int>> GetCoordinates(int[][] points, int interval = 5) {
            /*  
                 给一个list 里面有坐标，按间隙补齐缺失坐标,  点和点之间是直线连接，缺失的点也必须在直线上
                for example, interval=5, interpolate missing point at x-coordinate with incremental of 5 (e.g. (0,y1), (5,y2), (10,y3)....
                input = [(0,10), (10,10),(20, -10)]
                output = [(0,10), (5,10),(10,10)‍‍‌‍‌‌‍‍‌‌‍‍‍‌‌‌‍‍,(15,0) ,(20,-10)]
                (5,10) 在直线(0,10)-(10,10)上, (15,0)在直线(10,10)-(20,-10)上
            */
            var res = new List<IList<int>>();
            res.Add(points[0]);
            var preX = points[0][0];
            var preY = points[0][1];
            // [(10,10), (20,-10)]
            // disX = 10
            // disY = -20
            // steps = 2
            // speedX = 5
            // speedY = -10
            // curX = 10
            // curY = 10
            for(var i =1; i < points.Length; i++) {
                var distantX = points[i][0] - preX; 
                var distantY = points[i][1] - preY;
                var steps = Math.Abs(distantX) / interval;
                var speedX = distantX / steps;
                var speedY = distantY / steps;
                for(var step = 1; step <= steps; step++) {
                    var curX = preX + speedX;
                    var curY = preY + speedY;
                    res.Add(new[] {curX, curY});
                    preX = curX;
                    preY = curY;
                }
                preX = points[i][0];
                preY = points[i][1];
                res.Add(points[i]);
            }

            return res;
        }

        // max path sum from root to leaf
        public int MaxSumFromRootToLeaf(TreeNode root) {
            var max = int.MinValue;
            dfs(root, 0);
            return max;

            void dfs(TreeNode node, int sum) {
                if (node == null) return;
                var curSum = sum + node.val;
                if (node.left == null && node.right ==null) {
                    max = Math.Max(max,  curSum);
                    return ;
                }
                dfs(node.left, curSum);
                dfs(node.right, curSum);
            }
        }
        // coin change

        public void Run() {
            // buckets
            // var inputs = new [] {1,9, 10,31,12,11,15};
            // var b = 2;
            // var w = 11;
            // var res = GetCounter(inputs, b, w);
            // Console.WriteLine($"{string.Join(",", res)}");

            // coordinates
            var res = GetCoordinates(new int [][] {
                new [] {0,10},
                new [] {-10,0},
                new [] {-20,6},
                new [] {0,-18},
                new [] {10,0},
            });
            foreach(var point in res) {
                Console.WriteLine($"[{string.Join(",", point)}]");
            }
            
        }
    }
}