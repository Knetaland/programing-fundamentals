using Fundamental.Core.Interfaces;
using System.Collections.Generic;
using System;

namespace Fundamentals.Companies.Plaid
{
    /*

    Part 1
    Minimum 3 transactions are required to consider it as a recurring transaction
    Same company, same amount, same number of days apart - recurring transactions

    Input: Company, Amount, Timestamp (Day of the transaction)
    Output: An array of companies who made recurring transactions

    Part 2
    The amounts and timestamps - similar
    20% higher than minimum

    (minimum of all amount) 10$ + 20% -> 12$
    [10, 11, 12]

    [
        ("Netflix", 9.99, 0),
        ("Netflix", 9.99, 10),
        ("Netflix", 9.99, 20),
        ("Netflix", 9.99, 30),
        ("Amazon", 27.12, 32),
        ("Sprint", 50.11, 45),
        ("Sprint", 50.11, 55),
        ("Sprint", 50.11, 65),
        ("Sprint", 60.13, 77),
        ("Netflix", 9.99, 50),
    ]

    */
    struct Transaction
    {
        public string company;
        public double price;
        public int time;

        public Transaction(string company, double price, int time)
        {
            this.company = company;
            this.price = price;
            this.time = time;
        }
    }

    public class FindSameTransaction : IProblem
    {
        private IEnumerable<string> FindRecurringTransactions(IEnumerable<Transaction> transactions)
        {
            var res = new Dictionary<string, Dictionary<double, List<int>>>();
            foreach (var t in transactions)
            {
                if (!res.ContainsKey(t.company))
                {
                    res.Add(t.company, new Dictionary<double, List<int>>());
                }
                if (!res[t.company].ContainsKey(t.price))
                {
                    res[t.company].Add(t.price, new List<int>());
                }

                res[t.company][t.price].Add(t.time);
            }

            var final = new List<string>();

            foreach (var companyTranWithSameAmount in res)
            {
                foreach (var priceWithTimes in companyTranWithSameAmount.Value)
                {
                    Console.WriteLine($"checking for {companyTranWithSameAmount.Key}");
                    if (HasRecurring(priceWithTimes.Value)) {
                        final.Add(companyTranWithSameAmount.Key);
                    }
                }
            }

            return final;
        }

        private bool HasRecurring(List<int> times)
        {
            if (times.Count >= 3)
            {
                times.Sort();
                var comm = times[1] - times[0];
                var validCount = 2;
                for(var i = 2; i < times.Count; i++) {
                    var interval = times[i] - times[i-1];
                    Console.WriteLine($"comm interval is {comm}, new interval is {interval}, same? {interval == comm}");
                    if (interval == comm) {
                        validCount++;
                    } else {
                        comm = interval;
                        validCount = 2;
                    }

                    if (validCount >=3) return true;
                }
                

                // var lastTimeTransactionHappened = times[0];
                // var commonInterval = -1;
                // var count = 1;
                // for (var i = 1; i < times.Count; i++)
                // {
                //     var interval = times[i] - lastTimeTransactionHappened;
                //     if (count == 1 || commonInterval == interval)
                //     {
                //         commonInterval = interval;
                //         count++;
                //         // interval = 10
                //         // common = 10
                //         // count = 3
                //         // last = 20
                //     }
                //     else
                //     {
                //         count = 1;
                //     }
                //     lastTimeTransactionHappened = times[i];
                //     if (count >= 3)
                //     {
                //         return true;
                //     }
                // }
            }
            return false;
        }

        public void Run()
        {
            var input = new[] {
                new Transaction("Netflix", 9.99, 0),
                new Transaction("Netflix", 9.99, 10),
                new Transaction("Netflix", 9.99, 20),
                new Transaction("Netflix", 9.99, 30),
                new Transaction("Amazon", 27.12, 32),
                new Transaction("Sprint", 50.11, 45),
                new Transaction("Sprint", 50.11, 55),
                new Transaction("Sprint", 50.11, 65),
                new Transaction("Sprint", 60.13, 77),
                new Transaction("Netflix", 9.99, 50),
            };

            var res = FindRecurringTransactions(input);
            foreach (var r in res)
            {
                Console.WriteLine(r);
            }
        }
    }
}
