using System;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Plaid
{
    /*
    Netting Engine - ACH association
    银行转帐都会通过一个中央行A转账，给一个list的转账信息 最后需要output一个简化版本的list
example 2
input: ["AB1", "BA2", "BC3"] // A转$1到B，B转$2到A, B转$3到C
output: ["BA4", "AC3"] // 合起来算B转$4到A，A转$3到C
followup是 不指定中央行是A怎么找到一个最合适的中央行，然后output

A -> B: 1
B -> A: 2
B -> C: 3 => B->A 3 then A -> C 3


A -1 +2 = +1
B +1 -2 -3 = -4
C +3 = +3



    comment in post: https://leetcode.com/problems/optimal-account-balancing/
    https://leetcode.com/discuss/interview-question/1367137/Plaid-Phone-Interview

    /*
from collections import defaultdict
def simplify_transactions(orig_transactions, cent_bank):
    flow_map = defaultdict(int) # dict{bank : $ flow in, negative means expense}
    res = []
    # step 1: process and populate flow_map, clearing house
    for tran in orig_transactions:
        payer, payee, amount = tran[0], tran[1], int(tran[2:])
        if payee != cent_bank:
            flow_map[payee] += amount
        if payer != cent_bank:
            flow_map[payer] -= amount
    # step 2: output
    for bank, amount in flow_map.items():
        if amount > 0:
            res.append(cent_bank + bank + str(amount))
        elif amount < 0:
            res.append(bank + cent_bank + str(-amount))
    return res
def simplify_transactions_v2(orig_transactions):
    flow_map = defaultdict(int) # dict{bank : $ flow in, negative means expense}
    # step 1: process and populate flow_map, clearing house
    for tran in orig_transactions:
        payer, payee, amount = tran[0], tran[1], int(tra‍‍‌‍‌‌‍‍‌‌‍‍‍‌‌‌‍‍n[2:])
        flow_map[payee] += amount
        flow_map[payer] -= amount
    # step 2: choose central bank
    max_net_amount = 0
    cent_bank = None
    for bank, amount in flow_map.items():
        if amount != 0 and abs(amount) > max_net_amount:
            max_net_amount = amount
            cent_bank = bank
    if max_net_amount == 0:
        return []
    # step 3: output
    return simplify_transactions(orig_transactions, cent_bank)
/*

    */
    public class CentralBank : IProblem
    {
        public IList<string> GetTransactions(IList<string> transactions, char centralBank) {
            if (transactions == null) return new List<string> {};
            var moneyFlow = ConstructMoneyFlow(transactions);
            return ListTransactions(moneyFlow, centralBank);
            // foreach(var flow in moneyFlowMap) {
            //     if (flow.Key != centralBank) {
            //         if (flow.Value > 0) {
            //             res.Add($"{centralBank}{flow.Key}{flow.Value}");
            //         } else if (flow.Value < 0) {
            //             res.Add($"{flow.Key}{centralBank}{flow.Value}");
            //         }
            //     }
            // }
            // return res;
        }

        private Dictionary<char, int> ConstructMoneyFlow(IList<string> transactions) {
            var moneyFlow = new Dictionary<char, int>();
            foreach(var transaction in transactions) {
                var outGoingBank = transaction[0];
                var incomingBank = transaction[1];
                var amount = int.Parse(transaction.Substring(2));

                Console.WriteLine($"{outGoingBank} sent {incomingBank} amount: {amount}");

                if (!moneyFlow.ContainsKey(outGoingBank)) {
                    moneyFlow.Add(outGoingBank, 0);
                }
                moneyFlow[outGoingBank] -= amount;
                if (!moneyFlow.ContainsKey(incomingBank)) {
                    moneyFlow.Add(incomingBank, 0);
                }
                moneyFlow[incomingBank] += amount;
            }
            Console.WriteLine($"money flow:");
            foreach(var v in moneyFlow) {
                Console.WriteLine($"Bank {v.Key}: {v.Value}");
            }
            return moneyFlow;
        }

        private  IList<string> ListTransactions(Dictionary<char, int> moneyFlow, char centralBank) {
            var res = new List<string>();
            foreach(var flow in moneyFlow) {
                if (flow.Key != centralBank) {
                    if (flow.Value > 0) {
                        res.Add($"{centralBank}{flow.Key}{flow.Value}");
                    } else if (flow.Value < 0) {
                        res.Add($"{flow.Key}{centralBank}{flow.Value}");
                    }
                }
            }
            return res;
        }

        public IList<string> GetTransactions2(IList<string> transactions) {
            var res = new List<string>();
            if (transactions == null) return res;
            var moneyFlow = ConstructMoneyFlow(transactions);
            var central = FindCentralBank(moneyFlow);
            Console.WriteLine($"central bank is {central}");
            return ListTransactions(moneyFlow, central);
            

            char FindCentralBank(Dictionary<char, int> flowMap) {
                var maxTrans = int.MinValue;
                char central = '\0';
                foreach(var f in flowMap) {
                    if (f.Value != 0 && Math.Abs(f.Value) > maxTrans) {
                        central = f.Key;
                        maxTrans = Math.Abs(f.Value);
                    }
                }
                return central;
            }
        }


        public IList<string> GetTransactions3(IList<string> transactions) {
            var res = new List<string>();
            if (transactions == null) return res;
            var moneyFlow = new Dictionary<char, (int, int)>();
            foreach(var transaction in transactions) {
                var outGoingBank = transaction[0];
                var incomingBank = transaction[1];
                var amount = int.Parse(transaction.Substring(2));

                Console.WriteLine($"{outGoingBank} sent {incomingBank} amount: {amount}");

                if (!moneyFlow.ContainsKey(outGoingBank)) {
                    moneyFlow.Add(outGoingBank, (0,0));
                }
                moneyFlow[outGoingBank] = (moneyFlow[outGoingBank].Item1 +1, moneyFlow[outGoingBank].Item2 - amount);
                // moneyFlow[outGoingBank].Item2 -= amount;
                if (!moneyFlow.ContainsKey(incomingBank)) {
                    moneyFlow.Add(incomingBank, (0, 0));
                }
                moneyFlow[incomingBank] = (moneyFlow[incomingBank].Item1 +1, moneyFlow[incomingBank].Item2 + amount);
            }
            Console.WriteLine($"money flow:");
            foreach(var v in moneyFlow) {
                Console.WriteLine($"Bank {v.Key}: {v.Value.Item2}");
            }
            var central = FindCentralBank(moneyFlow);
            Console.WriteLine($"central bank is {central}");
            return ListTransactions(moneyFlow, central);
            

            char FindCentralBank(Dictionary<char, (int, int)> flowMap) {
                var maxTransCount = int.MinValue;
                char central = '\0';
                foreach(var f in flowMap) {
                    if (f.Value.Item1 > maxTransCount) {
                        Console.WriteLine($"Bank {f.Key} has count of {f.Value.Item1} > {maxTransCount}");
                        maxTransCount = f.Value.Item1;
                        central = f.Key;
                    }
                }
                return central;
            }

            IList<string> ListTransactions(Dictionary<char, (int, int)> moneyFlow, char centralBank) {
                var res = new List<string>();
                foreach(var flow in moneyFlow) {
                    if (flow.Key != centralBank) {
                        if (flow.Value.Item2 > 0) {
                            res.Add($"{centralBank}{flow.Key}{flow.Value.Item2}");
                        } else if (flow.Value.Item2 < 0) {
                            res.Add($"{flow.Key}{centralBank}{flow.Value.Item2}");
                        }
                    }
                }
                return res;
            }
        }

        public void Run() {
            // var input = new [] {"AB12", "BA24", "BC31"};
            var input = new [] {"AB12", "BA160", "CB13", "BC31", "CD1", "DC1", "CD1", "DC1", "CD1", "DC2"};
            var s = GetTransactions(input, 'A');
            Lib.Print(string.Join(",", s));

            Lib.Print("--------------");
            var s2 = GetTransactions2(input);
            Lib.Print("finding central bank:" + string.Join(",", s2));


            Lib.Print("--------------");
            var s3 = GetTransactions3(input);
            Lib.Print("finding central bank with most trans count:" + string.Join(",", s3));
        }


        // B 130, C 19, D -1, A -149
    }
}

