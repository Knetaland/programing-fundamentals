using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies
{
    // To execute C#, please define "static void Main" on a class
    // named Solution.


    // Given a deck of cards, write a function that finds every combination of cards that satisfies the criteria of a valid set.

    // All cards in the set have three categories: shape, color, and number.

    // A valid set:
    // Contains 3 cards
    // Each category from the cards must:
    // Be all the same
    // OR
    // Be distinct 

    // Categories of a card:
    // Shape - triangle, square, circle
    // Color - green, orange, red
    // Number - 1, 2, 3

    // Example: 

    // If the input was:
    // Card_A(Triangle, Green, 1)
    // Card_B(Square, Green, 2)
    // Card_C(Circle, Green, 2)
    // Card_D(Circle, Green, 3)
    // Card_E(Triangle, Green, 2)

    // res= [ABC, BCE]
    // A valid set would be (B, C, E). Find the rest! 

    public class Card
    {
        public int Number { get; set; }
        public string Color { get; set; }
        public string Shape { get; set; }
        public string Name { get; set; }
        public Card(string name, string shape, string color, int number)
        {
            Number = number;
            Color = color;
            Shape = shape;
            Name = name;
        }
    }

    public class GEMINI : IProblem
    {


        public  List<List<Card>> GetValidSets(List<Card> cards)
        {
            var res = new List<List<Card>>();
            if (cards.Count < 3)
                return res;
            var tempList = new List<List<Card>>();

            for (var i = 0; i < cards.Count; i++)
            {
                for (var j = i + 1; j < cards.Count; j++)
                {
                    for (var k = i + 2; k < cards.Count; k++)
                    {
                        if(IsValid(cards[i], cards[j], cards[k])) {
                            res.Add(new List<Card>{ cards[i], cards[j], cards[k] });
                        }
                    }
                }
            }
            return res;


            // foreach (var card in cards)
            // {
            //     foreach (var list in tempList)
            //     {
            //         list.Add(card);
            //         if (list.Count == 3)
            //         {
            //         }
            //     }

            //     tempList.Add(new List<Card> { card });
            //     foreach (var l in tempList)
            //     {
            //         var ss = new List<string>();
            //         foreach (var item in l)
            //         {
            //             ss.Add(item.Name);
            //         }
            //         Console.WriteLine(String.Join(",", ss));
            //         Console.WriteLine("---------");
            //     }
            // }

            // return res;
        }



        public void Run()
        {
            var input = new List<Card> {
                new Card("A", "Triangle", "Green", 1),
                new Card("B", "Square", "Green", 2),
                new Card("C", "Circle", "Green", 2),
                new Card("D", "Circle", "Green", 3),
                new Card("E", "Triangle", "Green", 2),
            };

            var res = GetValidSets(input);
            Print(res);

        }
        void Print(List<List<Card>> sets)
        {
                foreach (var l in sets)
                {
                    var ss = new List<string>();
                    foreach (var item in l)
                    {
                        ss.Add(item.Name);
                    }
                    Console.WriteLine(String.Join(",", ss));
                }
        }

        bool IsValid(Card a, Card b, Card c) {
            // var list = new List<Card> { a, b, c };
            // var color = list.Select(l => l.Color).Distinct().Count();
            // if (color != 1 || color != 3) return false;
            var map = new Dictionary<string, int>();
            foreach(var card in new List<Card> {a, b, c}) {
                tryIncrement(card.Shape);
                tryIncrement(card.Color);
                tryIncrement(card.Number.ToString());
            }
            return !map.Values.Contains(2);

            void tryIncrement(string key) {
                if(!map.ContainsKey(key)) {
                    map.Add(key, 0);
                }
                map[key]++;
            }
        }

    }
}
