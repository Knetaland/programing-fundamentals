using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Security.Cryptography.X509Certificates;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;
using Fundamentals.Companies.Door;

namespace Fundamentals.Companies.Amazon;

public class OAs : IProblem
{
    /*
        Get Minimum Pen Drive Size

        1. distribute the games into k group - each group cannot exceed the minSize
    */

    public int getMinSize(int[] gameSize, int k) {
        // write your code here
        Array.Sort(gameSize);
        var n = gameSize.Length;
        int low = gameSize[n-1], high = low * 2;
        while(low < high) {
            var mid = low + (high - low) /2;
            if (isValid(mid)) {
                high = mid;
            } else {
                low = mid + 1;
            }
        }
        return low;

        bool isValid(int limit) {
            int left = 0, right = n-1;
            var group = 0;
            while(left <= right) {
                if (gameSize[left] + gameSize[right] <= limit) {
                    left++;
                }
                right--;
                group++;
            }
            return group <= k;
        }
    }

      /*
        Find Requests in Queue
        Amazon Web Services (AWS) is a cloud computing platform with multiple servers. One of the servers is assigned to serve customer requests. There are n customer requests placed sequentially in a queue, where the ith request has a maximum waiting time denoted by wait[i]. That is, if the ith request is not served within wait[i] seconds, then the request expires and it is removed from the queue. The server processes the request following the First In First Out (FIFO) principle. The 1st request is processed first, and the nth request is served last. At each second, the first request in the queue is processed. At the next second, the processed request and any expired requests are removed from the queue.
        Given the maximum waiting time of each request denoted by the array wait, find the number of requests present in the queue at every second until it is empty.
        */
        public IList<int> getTasksInQueue(int[] waitTimes) {
            var queue = new List<int>(waitTimes);
            if (waitTimes == null || waitTimes.Length == 0) { return queue;}
            var queueSize = new List<int>();
            while(queue.Count > 0) {
                queueSize.Add(queue.Count);
                var next = new List<int>();
                // skipped first one, it will be removed as processed
                for(var i = 1; i < queue.Count; i++) {
                    queue[i]--;
                    if (queue[i] > 0) {
                        next.Add(queue[i]);
                    }
                }
                queue = next;
            }
            queueSize.Add(0); // last one
            return queueSize;
        }

    /*
        Channel max quality
    */
    public int calculateMedianSum(int[] packets, int n) {
        Array.Sort(packets, (a, b) => b - a);
        var sum = 0;
        for(var i =0; i < n - 1; i++) {
            sum += packets[i];
        }
        // last one, get median
        // even
        var median = getMedian(n-1, packets.Length-1);
        return sum + median;

        int getMedian(int left, int right) {
            var size = right - left + 1;
            var mid = size / 2;
            if (size % 2 != 0) {
                return packets[left + mid];
            } else {
                var median = (packets[left + mid -1] + packets[left + mid]) / 2.0;
                return (int)Math.Ceiling(median);
            }
        }
    }

    // longest non-increasing linked-list
    public ListNode getLongestNonIncreasingSegment(ListNode head) {
        if (head == null || head.next == null) return head;
        ListNode prev = null;
        ListNode cur = head;
        ListNode left = head;
        ListNode leftMost = head;
        var longest = int.MinValue;
        var curMax = 0;
        
        while(cur != null) {
            if (prev != null && cur.val <= prev.val) {
                curMax++;
                if (curMax > longest) {
                    longest = curMax;
                    leftMost = left;
                    Console.WriteLine($"longest happened at node {left.val}");
                }
            } else {
                curMax = 1;
                left = cur;
            }
            prev = cur;
            cur=cur.next;
        }
        return leftMost;
    }

    // suitable locations


    // amazon care get min score
    public int findSubarrayWithMinimumDistinctIntegers(int[] array, int series1, int series2) {
        var same = series1 == series2;
        var map = new Dictionary<int, int>();
        map.Add(series1, 1);
        if (map.ContainsKey(series2)) {
            map[series2]++;
        } else {
            map.Add(series2, 1);
        }
        int needed = 2;
        int min = int.MaxValue;
        int left = 0;
        var dictionary = new Dictionary<int, int>();
        var distinctCount = 0;
        for(var i = 0; i < array.Length; i++) {
            var cur = array[i];
            if (map.ContainsKey(cur)) {
                if (same) return 1;
                if (map[cur]>0) needed--;
                map[cur]--;
            }
            dictionary[cur] = dictionary.GetValueOrDefault(cur, 0)+1;

            while (needed == 0) {
                var leng = i - left +1;
                if (leng < min) {
                    min = leng;
                    distinctCount = dictionary.Count;
                    foreach (var kv in dictionary) {
                        Console.WriteLine($"dic item: {kv.Key}:{kv.Value}");
                    }
                    Console.WriteLine($"min found at index {left} with length {leng}");
                }

                var item = array[left];
                if (map.ContainsKey(item)) {
                    map[item]++;
                    if(map[item] > 0) needed++;
                }

                dictionary[item]--;
                if (dictionary[item] == 0) {
                    dictionary.Remove(item);
                }
                left++;
            }
        }
        return distinctCount;
    }


    // max score from sprints
    public int getMaxPoints(int[] days, int k) {
        var totalDays = days.Sum();
        var points = new int[totalDays];
        var pointer = 0;
        for(var i = 0; i < days.Length; i++) {
            for(var j = 1; j <= days[i]; j++) {
                points[pointer] = j;
                pointer++;
            }
        }

        var totalPoints = 0;
        for(var i = 0; i < k; i++) {
            totalPoints += points[i];
        }
        var max = totalPoints;
        int right = k;
        int left = 0;
        while(right < points.Length) {
            Console.WriteLine($"total point is now {totalPoints}");
            totalPoints += points[right] - points[left];
            right++;
            left++;
            if (totalPoints > max) {
                max = totalPoints;
                Console.WriteLine($"max point found at index {left}-{right}");
            }
        }

        // points.Print();

        return max;
    }
    

    // reduce gift, so any k items don't exceed threshold
    public int minRemovalToMeetThreshold(int[] prices, int k, int threshold) {
        if (prices.Length < k) {
            return 0;
        }

        Array.Sort(prices, (a,b) => b - a);
        
        int sum = 0;
        // sum up to k items
        for(var i = 0; i < k; i++) {
            sum += prices[i];
        }
        int left = 0, right = k - 1;
        var removal = 0;
        while ( sum > threshold && right < prices.Length) {
            removal++;
            // remove left(biggest), append right
            sum += prices[right] - prices[left];
            left++;
            right++;
        }
        return removal;
    }

    public int maximizeNegativePnLMonths(int[] PnL) {
        return maximizeNegativePnLMonths(PnL);
        var pq = new PriorityQueue<ListNode, int>(Comparer<int>.Create((x, y) => y-x));
        int count = 0, curSum = 0;
        foreach(var val in PnL) {
            Console.WriteLine($"Processing for {val}, curSum is {curSum}, count is {count}");
            if(curSum - val > 0) {
                Console.WriteLine($"ok to flip {val}, after negation, curSum is {curSum-val} ");
                curSum -= val;
                Console.WriteLine($"putting {val} to pq ");
                pq.Enqueue(new ListNode(val), val);
                count++;
            } else if (pq.Count > 0 && pq.Peek().val > val) {
                Console.WriteLine($"NOT ok to flip {val}, and pq top {pq.Peek().val} > {val}... try unflipping pq top");
                curSum += pq.Dequeue().val *2;
                curSum -= val;
                Console.WriteLine($"after unflip, curSum is {curSum}, putting {val} to pq ");
                pq.Enqueue(new ListNode(val), val);
            } else {
                curSum += val;
                Console.WriteLine($"can't flip val {val}, adding to curSum... new sum is {curSum}");
            }
        }

        return count;

        int maximizeNegativePnLMonths(int[] PnL) {
            return maximizeNegativePnLMonthsRecursive(PnL, 0, 0, 0);
        }

        int maximizeNegativePnLMonthsRecursive(int[] PnL, int index, int cumulativeSum, int negativeMonths) {
        // Base case: if all months are processed, return the count of negative months
            Console.WriteLine($"calling with index {index} and sum {cumulativeSum} with negativeMonths {negativeMonths}");
            if (index == PnL.Length) {
                return negativeMonths;
            }

            // Option 1: Leave the PnL as is and move to the next month
            int maxNegativeMonths = maximizeNegativePnLMonthsRecursive(PnL, index + 1, cumulativeSum + PnL[index], negativeMonths);
            Console.WriteLine($"maxNegativeMonths is {maxNegativeMonths} if not flipping");
            // Option 2: Flip the sign of the PnL for the current month if it does not make the cumulative sum non-positive
            if (cumulativeSum - PnL[index] > 0) { // Check if flipping won't result in a non-positive cumulative sum
                maxNegativeMonths = Math.Max(maxNegativeMonths, 
                    maximizeNegativePnLMonthsRecursive(PnL, index + 1, cumulativeSum - PnL[index], negativeMonths + 1));
            }

            return maxNegativeMonths;
        }
    }

    /* suitable locations*/
    public int numberOfSuitablePlaces(int[] centers, int d) {
        Array.Sort(centers);
        int low = centers[0], high = centers[centers.Length-1];
        var point = low  +(high-low)/2;
        var lower = findLower(-1_000_000_000, point+1);
        var upper = findUpper(point+1, 1_000_000_000+1);
        if (lower == int.MinValue)  return 0;
        Console.WriteLine($"found positions from {lower} to {upper}");
        return upper - lower + 1;

        int findLower(int lo, int hi) {
            var dis = 0L;
            while(lo < hi) {
                // Console.WriteLine($"checking {lo} vs {hi}");
                int mid = lo + (hi - lo)/2;
                dis = getDistance(mid);
                Console.WriteLine($"dis is {dis}");
                if (dis > d) {
                    lo = mid + 1;
                } else {
                    hi = mid;
                }
            }
            dis= getDistance(lo);
            Console.WriteLine($"exit, dis is {dis}");
            if (getDistance(lo) > d) return int.MinValue;
            Console.WriteLine($"high is {hi} and lo is {lo}");
            return lo;
        }

        int findUpper(int lo, int hi) {
            var dis = 0L;
            while(lo < hi) {
                int mid = lo + (hi - lo)/2;
                Console.WriteLine($"checking {lo} vs {hi}, mid is {mid}");
                dis = getDistance(mid);
                Console.WriteLine($"dis is {dis}");
                // if (dis == d) return mid;
                if (dis <= d) {
                    lo = mid + 1;
                } else {
                    hi = mid;
                }
            }
            dis = getDistance(hi-1);
            Console.WriteLine($"exit, dis is {dis}");
            // if (getDistance(hi) > d) return int.MaxValue;
            Console.WriteLine($"high is {hi} and lo is {lo}");
            return lo-1;
        }

        long getDistance(int location) {
            long dis = 0;
            foreach (var c in centers) {
                dis += 2* (long)Math.Abs(c - location);
            }
            return dis;
        }
        
    }

    public void Run() {
        // 1. 
        // var gameSize = new[] { 2, 4, 9, 6};
        // var k = 2;
        // var res = getMinSize(gameSize, k);
        // res.Print();

        // 2
        // var wait = new int[] {3, 1, 2, 1};
        // var res = getTasksInQueue(wait);
        // res.Print();

        // 3
        // var nums = new int [] {89, 48, 14};
        // var n = 3;
        // var res = calculateMedianSum(nums, n);

        // 4
        // var head = Lib.ToListNode([2,3,4,5,6]);
        // var res = getLongestNonIncreasingSegment(head);
        // Console.WriteLine(res.val);

        // 5 reduce gift
        // var prices = new int[] {3,2,1,4,6,5};
        // int k = 3;
        // int threshold = 14;
        // var res = minRemovalToMeetThreshold(prices, k, threshold);


        // 6
        // var res = getMaxPoints([4,5,3], 4);

        // var res = findSubarrayWithMinimumDistinctIntegers([2, 4, 9], 1, 1);

        // var res = maximizeNegativePnLMonths([5, 3, 1, 2]);

        var res = numberOfSuitablePlaces([9,8,7], 12);
        res.Print();
    }
}
