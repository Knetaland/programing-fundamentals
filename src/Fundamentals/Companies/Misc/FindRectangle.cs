using System;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Misc
{
    public class FindRectangle : IProblem
    {
        public void Find(int[,] d)
        {
            var m = d.GetLength(0);
            var n = d.GetLength(1);

            for (var i = 0; i < m; i++)
            {
                for (var j = 0; j < n; j++)
                {
                    if (d[i, j] == 0)
                    {
                        Explore(i, j);
                    }
                }
            }

            void Explore(int i, int j)
            {
                var w = 1;
                var h = 1;

                while (i + h < m && d[i + h, j] == 0)
                {
                    d[i + h, j] = 1;
                    h++;
                }
                while (j + w < n && d[i, j + w] == 0)
                {
                    d[i, j + w] = 1;
                    w++;
                }
                for (var x = i + 1; x < i + h; x++)
                {
                    for (var y = j + 1; y < j + w; y++)
                    {
                        d[x, y] = 1;
                    }
                }
                Console.WriteLine($"point is ({i},{j}), ({i + h - 1},{j + w - 1})");
            }
        }
        public void Run()
        {
            var d = new[,] {
                {1,1,1,1,1,0,0},
                {1,0,0,1,1,0,0},
                {1,0,0,1,1,0,0},
                {1,1,1,0,1,0,0},
            };
            Find(d);
        }
    }

}
