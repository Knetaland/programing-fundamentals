using System;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace Fundamentals.Companies.Misc
{
    public class FindParentNodeInBinaryTree : IProblem
    {
        public TreeNode FindParent(TreeNode root, TreeNode node)
        {
            if (root == null || root == node)
            {
                return null;
            }

            if (root.left == node || root.right == node)
            {
                return root;
            }
            return FindParent(root.left, node) ?? FindParent(root.right, node);
        }

        public void Run()
        {
            var tree = new TreeNode(4)
            {
                left = new TreeNode(2)
                {
                    left = new TreeNode(1),
                    right = new TreeNode(3),
                },
                right = new TreeNode(5)
            };

            var p = tree.right;

            var parent = FindParent(tree, p);
            Console.WriteLine("parent is " + parent.val);
        }
    }
}
