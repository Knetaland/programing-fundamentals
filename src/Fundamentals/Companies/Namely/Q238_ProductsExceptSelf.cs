using System;
using System.Linq;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Namely
{
    /*
     Given an array of integers, return a new array of integers of the same size where each number
     in the array is replaced with the product of all numbers in the array except itself.
     Do not use division and try to make the function as efficient as possible.
         */
    public class Q238_ProductsExceptSelf : IProblem
    {
        public int[] GetProduct(int[] input)
        {
            var leftArr = new int[input.Length];
            var rigthArr = new int[input.Length];

            var val = 1;
            for (var i = 0; i < leftArr.Length; i++)
            {
                if (i == 0)
                {
                    leftArr[i] = 1;
                }
                else
                {
                    val *= input[i - 1];
                    leftArr[i] = val;
                }
            }

            val = 1;
            for (var i = rigthArr.Length - 1; i >= 0; i--)
            {
                if (i == rigthArr.Length - 1)
                {
                    rigthArr[i] = 1;
                }
                else
                {
                    val *= input[i + 1];
                    rigthArr[i] = val;
                }
            }
            var ans = new int[input.Length];
            for (var i = 0; i < ans.Length; i++)
            {
                ans[i] = leftArr[i] * rigthArr[i];
            }
            return ans;
        }

        //  1, 3, 4, 6, 9
        //  left = 12
        // res [648, 216, 162, 108, 72]
        //  right = 648

        public int[] productExceptSelf(int[] nums)
        {
            var n = nums.Length;
            var res = new int[n];
            // Calculate lefts and store in res.
            var left = 1;
            for (var i = 0; i < n; i++)
            {
                if (i > 0)
                {
                    left *= nums[i - 1];
                }

                res[i] = left;
            }
            // Calculate rights and the product from the end of the array.
            var right = 1;
            for (var i = n - 1; i >= 0; i--)
            {
                if (i < n - 1)
                {
                    right *= nums[i + 1];
                }

                res[i] *= right;
            }
            return res;
        }

        public int[] OnePass(int[] nums)
        {
            if (nums == null || nums.Length == 0)
            {
                return new int[0];
            }

            var n = nums.Length;
            var res = Enumerable.Repeat(1, n).ToArray();
            var left = nums[0];
            var right = nums[n - 1];
            for (int i = 1, j = n - 2; i < n; i++, j--)
            {
                res[i] *= left;
                res[j] *= right;
                left *= nums[i];
                right *= nums[j];
            }
            return res;
        }


        public void Run()
        {
            var input = new[] { 1, 3, 4, 6, 9 };
            // var output = GetProduct(input);
            var output = productExceptSelf(input);
            Console.WriteLine($"output is {string.Join(",", output)}");
        }
    }
}
