using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Namely
{
    public class Employee_Coverage : IProblem
    {
        /*
        Given N employees and an array of vacation requests, validate that at least M employees are on staff at all times. A vacation request is represented as a tuple of integer departure and return dates -- (1, 4) means an employee leaves on day 1and returns on day 4.

        For example, given:

        N = 5
        M = 2
        requests = [(1, 4), (1, 4), (2, 5), (3, 4)]
        Output False because only 1 employee is staffed on day 3.
        */

        public bool CanCover((int, int)[] ptos, int m, int n)
        {
            if (ptos == null || ptos.Length == 0)
            {
                return true;
            }

            var list = new List<(int, bool)>(); // bool means is start
            foreach (var pto in ptos)
            {
                list.Add((pto.Item1, true));
                list.Add((pto.Item2, false));
            }
            list.Sort((a, b) =>
            {
                if (a.Item1 != b.Item1)
                {
                    return a.Item1 - b.Item1;
                }
                else
                {
                    return a.Item2 ? 1 : -1; // is it start? if it is, let it be after end
                }
            });

            foreach (var i in list)
            {
                if (i.Item2)
                {
                    n--;
                }
                else
                {
                    n++;
                }
                if (n < m)
                {
                    return false;
                }
            }
            return true;
        }
        public void Run()
        {
            var request = new[] { (1, 4), (1, 4), (2, 5), (3, 4) };
            var res = CanCover(request, 5, 2);
            Console.WriteLine("can cover " + res);
        }
    }
}
