using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Facebook
{
    /*
    Given a non-empty string s, you may delete at most one character. Judge whether you can make it a palindrome.
    */

    // o(n)
    public class Q680_ValidPalindrome_II : IProblem
    {
        // O(n)
        // O(1)
        public bool ValidPalindrome(string s)
        {
            int l = 0, r = s.Length - 1;
            while (l < r)
            {
                if (s[l] != s[r])
                {
                    return ValidPalindrome(s, l + 1, r) || ValidPalindrome(s, l, r - 1);
                }
                l++;
                r--; 
            }
            return true;
        }

        bool ValidPalindrome(string s, int l, int r)
        {
            while (l < r)
            {
                if (s[l] != s[r])
                {
                    return false;
                }
                l++;
                r--;
            }
            return true;
        }

        // generazlied for k removal, see valid palindrom III (1216)

        bool ValidPalindrome(string s, int l, int r, int k)
        {
            if (k < 0) return false;
            while (l < r)
            {
                if (s[l] != s[r])
                {
                    return ValidPalindrome(s, l+1, r, k-1) 
                    || ValidPalindrome(s, l, r-1, k-1);
                }
                l++;
                r--;
            }
            return true;
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }


    }
}
