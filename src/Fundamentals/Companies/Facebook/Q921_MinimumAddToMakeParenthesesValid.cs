using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Facebook
{
    // Q1249 min remove
    public class Q921_MinimumAddToMakeParenthesesValid : IProblem
    {
        public int MinAddToMakeValid(string s)
        {
            return NoStack(s);
        }

        public int UsingStack(string S)
        {
            var stack = new Stack<int>();
            var count = 0;
            foreach (var c in S)
            {
                if (c == ')')
                {
                    if (stack.Count == 0)
                    {
                        count++;
                    }
                    else
                    {
                        stack.Pop();
                    }
                }
                else if (c == '(')
                {
                    stack.Push(1);
                }
            }
            count += stack.Count;
            return count;
        }

        public int NoStack(string s)
        {
            int open = 0, close = 0;
            foreach (var c in s)
            {
                if (c == ')')
                {
                    close++;
                    if (open > 0)
                    {
                        open--;
                        close--;
                    }
                }
                else if (c == '(')
                {
                    open++;
                }
            }
            return open + close;
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
