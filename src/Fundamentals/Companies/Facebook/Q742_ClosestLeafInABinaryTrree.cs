using System.Collections.Generic;
using Fundamental.Core.Models;

namespace Fundamentals.Companies.Facebook
{
    public class Q742_ClosestLeafInABinaryTrree
    {
        /*
        Given a binary tree where every node has a unique value, and a target key k, find the value of the closest leaf node to target k in the tree.

        Here, closest to a leaf means the least number of edges travelled on the binary tree to reach any leaf of the tree. Also, a node is called a leaf if it has no children.

        In the following examples, the input tree is represented in flattened form row by row. The actual root tree given will be a TreeNode object.
        */
        public int FindCloestLeaf(TreeNode root, TreeNode k)
        {
            var g = new Dictionary<TreeNode, List<TreeNode>>();

            var queue = new Queue<TreeNode>();
            var visited = new HashSet<TreeNode>();
            queue.Enqueue(k);
            while (queue.Count > 0)
            {
                // var n = queue.Count;
                // while(n > 0) {

                // }
                var node = queue.Dequeue();
                visited.Add(node);
                if (g.ContainsKey(node))
                {
                    foreach (var neighbor in g[node])
                    {
                        if (neighbor.left == null && neighbor.right == null)
                        {
                            return neighbor.val;
                        }
                        if (!visited.Contains(neighbor))
                        {
                            queue.Enqueue(neighbor);
                        }
                    }
                }
            }
            return 0;
        }

        private void BuildGraph(TreeNode parent, TreeNode child, Dictionary<TreeNode, List<TreeNode>> g)
        {
            if (child == null)
            {
                return;
            }

            if (parent != null)
            {
                if (!g.ContainsKey(parent))
                {
                    g.Add(parent, new List<TreeNode>());
                }
                g[parent].Add(child);
                if (!g.ContainsKey(child))
                {
                    g.Add(child, new List<TreeNode>());
                }
                g[child].Add(parent);
            }
            BuildGraph(child, child.left, g);
            BuildGraph(child, child.right, g);
        }
    }
}
