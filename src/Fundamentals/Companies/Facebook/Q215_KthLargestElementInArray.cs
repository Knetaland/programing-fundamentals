using System;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Facebook
{
    /*
    https://leetcode.com/problems/kth-largest-element-in-an-array/solutions/60294/solution-explained/
    
    Find the kth largest element in an unsorted array. Note that it is the kth largest element in the sorted order, not the kth distinct element.

    Example 1:

    Input: [3,2,1,5,6,4] and k = 2
    Output: 5
    Example 2:

    Input: [3,2,3,1,2,4,5,5,6] and k = 4
    Output: 4
    Note: 
    You may assume k is always valid, 1 ≤ k ≤ array's length.
    */
    public class Q215_KthLargestElementInArray : IProblem
    {
        public int FindKthLargest(int[] nums, int k)
        {
            return QuickSelect(nums, nums.Length - k, 0, nums.Length - 1);
        }

        private int QuickSelect(int[] nums, int pos, int left, int right)
        {
            if (left == right)
            {
                return nums[left];
            }
            var pivot = Lib.Partition(nums, left, right);
            if (pivot == pos)
            {
                return nums[pivot];
            }
            else if (pivot < pos)
            {
                return QuickSelect(nums, pos, pivot + 1, right);
            }
            else
            {
                return QuickSelect(nums, pos, left, pivot - 1);
            }
        }
        
        public void Run()
        {
            var input = new[] { 3, 2, 3, 1, 2, 4, 5, 5, 6 };
            var k = 4;
            var res = FindKthLargest(input, k);
            Console.WriteLine("kth largest element is " + res);
        }
    }
}
