﻿using System;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamentals;

/*https://leetcode.com/problems/making-a-large-island/
You are given an n x n binary matrix grid. You are allowed to change at most one 0 to be 1.

Return the size of the largest island in grid after applying this operation.

An island is a 4-directionally connected group of 1s.

 

Example 1:

Input: grid = [[1,0],[0,1]]
Output: 3
Explanation: Change one 0 to 1 and connect two 1s, then we get an island with area = 3.
Example 2:

Input: grid = [[1,1],[1,0]]
Output: 4
Explanation: Change the 0 to 1 and make the island bigger, only one island with area = 4.
Example 3:

Input: grid = [[1,1],[1,1]]
Output: 4
Explanation: Can't change any 0 to 1, only one island with area = 4.
 

Constraints:

n == grid.length
n == grid[i].length
1 <= n <= 500
grid[i][j] is either 0 or 1.
*/
public class Q827_Making_A_Large_Island : IProblem
{  
    const int LAND = 1;
    const int WATER = 0;
    int[][] DIRS = [
        [0, 1],
        [0, -1],
        [1, 0],
        [-1, 0]
    ];
    public int LargestIsland(int[][] grid) {
        var n = grid.Length;
        var id = LAND + 1;
        var max = 0;
        var map = new Dictionary<int, int>(); // store the area
        // get each island's area
        for(var i = 0; i < n; i++) {
            for(var j = 0; j< n; j++ ) {
                if(grid[i][j] == LAND) {
                    var area = dfs(i, j, id);
                    max = Math.Max(area, max);
                    map.Add(id, area);
                    Console.WriteLine($"getting area at [{i},{j}]: {area}, id {id}");
                    id++;
                }
            }
        }
        // flip water to connect
        for(var i = 0; i < n; i++) {
            for(var j = 0; j< n; j++ ) {
                if (grid[i][j] == WATER) {
                    var area = makeLand(i, j);
                    max = Math.Max(area, max);
                }
            }
        }
        return max;

        int makeLand(int r, int c) {
            var area = 1; // by making current spot land
            // try connect 4 dirs
            var visitedIds = new HashSet<int>();
            foreach(var dir in DIRS) {
                var x = r + dir[0];
                var y = c + dir[1];
                if (x >=0 && y>=0 && x < n && y < n) {
                    var id = grid[x][y];
                    if (!visitedIds.Contains(id) && map.ContainsKey(id)) {
                        area += map[id];
                        visitedIds.Add(id);
                    }
                }
            }
            return area;
        }



        int dfs(int r, int c, int id) {
            if (r < 0 || r >= n || c < 0 || c >= n || grid[r][c] != LAND) {
                return 0;
            }
            grid[r][c] = id;
            var area = 1;
            foreach(var dir in DIRS) {
             area += dfs(r + dir[0], c + dir[1], id);
            }
            return area;
        }
    }

    public void Run() {
        int[][] input =  [[1,1],[1,0]];
        var res = LargestIsland(input);
        res.Print();
    }
}
