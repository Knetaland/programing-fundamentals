﻿using System;
using System.Collections.Generic;
using Fundamental.Core.Models;

namespace Fundamentals;

/*
https://leetcode.com/problems/maximum-level-sum-of-a-binary-tree/description/
Given the root of a binary tree, the level of its root is 1, the level of its children is 2, and so on.

Return the smallest level x such that the sum of all the values of nodes at level x is maximal.

*/
public class Q1161_Max_Level_Sum_Of_A_Binary_Tree
{
    public int MaxLevelSum(TreeNode root) {
        var levels = new List<List<int>>();
        var queue = new Queue<TreeNode>();
        var max = int.MinValue;
        var maxLevel = 1;
        queue.Enqueue(root);
        var curLevel = 1;
        while(queue.Count > 0) {
            var count = queue.Count;
            var sum = 0;
            while(count > 0) {
                var node = queue.Dequeue();
                sum += node.val;
                if (node.left != null) {
                    queue.Enqueue(node.left);
                }
                if (node.right != null) {
                    queue.Enqueue(node.right);
                }
                count--;
            }
            if (sum > max) {
                maxLevel = curLevel;
                max = sum;
            }
            curLevel++;
        }
        return maxLevel;
    }
}
