using System;
using System.Linq;
using System.Text;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Facebook
{
    public class Q415_AddStrings : IProblem
    {
        /*
        Given two non-negative integers num1 and num2 represented as string, return the sum of num1 and num2.

        Note:

        The length of both num1 and num2 is < 5100.
        Both num1 and num2 contains only digits 0-9.
        Both num1 and num2 does not contain any leading zero.
        You must not use any built-in BigInteger library or convert the inputs to integer directly.
        */
        public string AddStrings(string num1, string num2)
        {
            var co = 0;
            var p1 = num1.Length - 1;
            var p2 = num2.Length - 1;
            var sb = new StringBuilder();
            while (p1 >= 0 || p2 >= 0 || co > 0)
            {
                var val1 = p1 >= 0 ? num1[p1] - '0' : 0;
                var val2 = p2 >= 0 ? num2[p2] - '0' : 0;
                var val = val1 + val2 + co;
                sb.Insert(0, (val % 10).ToString());
                co = val / 10;
                p1--;
                p2--;
            }

            //Console.WriteLine("sb is " + sb.ToString());
            // return new string(sb.ToString().Reverse().ToArray());
            return sb.ToString();
        }

        // T: O(n)
        // S: O(n)
        public string AddStrings2(string num1, string num2) {
            var co = 0;
            var p1 = num1.Length - 1;
            var p2 = num2.Length - 1;
            var res = new char[Math.Max(num1.Length, num2.Length) + 1];
            var i = res.Length -1;
            while(p1 >= 0 || p2 >= 0 || co > 0) {
                var val1 = p1 < 0 ? 0 : num1[p1] - '0';   
                var val2 = p2 < 0 ? 0 : num2[p2] - '0';
                var sum = val1 + val2 + co;
                res[i] =  (char)(sum % 10 + '0');
                co = sum / 10;
                p1--;
                p2--;
                i--;
            }
            // since res length has additional 1 spot for the carryOver, i < 0 means the loop end processed the carry over, otherwise omit the leading char
            return i < 0 ?  new string(res) : new string(res, 1, res.Length - 1);
        }

        public string AddStringsWithDecimals(string num1, string num2)
        {
            var (num1Int, num1Decimal) = parse(num1);
            var (num2Int, num2Decimal) = parse(num2);
            var deciLength = Math.Max(num1Decimal.Length, num2Decimal.Length);
            num1Decimal = num1Decimal.PadRight(deciLength, '0');
            num2Decimal = num2Decimal.PadRight(deciLength, '0');
            var (decimalPart, co) = add(num1Decimal, num2Decimal, 0);
            var (intPart, intCo) = add(num1Int, num2Int, co);
            if (intCo > 0) {
                intPart = $"{intCo}{intPart}";
            }
            return $"{intPart}.{decimalPart}";

            (string intPart, string deciPart) parse(string num) {
                var res = num.Split('.');
                if (res.Length < 2) { // no decimal part
                    return (res[0], "");
                }
                return (res[0], res[1]);
            }

            (string sum, int co) add(string n1, string n2, int co) {
                int p1 = n1.Length -1, p2 = n2.Length -1;
                var res = new char[Math.Max(n1.Length , n2.Length)];
                var i = res.Length -1;
                while(p1 >=0 || p2 >= 0) {
                    var v1 = p1 < 0 ? 0 : n1[p1] - '0';
                    var v2 = p2 < 0 ? 0 : n2[p2] - '0';
                    var val = v1 + v2 + co;
                    res[i] = (char) (val % 10 + '0');
                    co = val / 10;
                    i--;
                    p1--;
                    p2--;
                }
                return (new string(res), co);
            }
        }

        public void Run()
        {
            string num1 = "999.01", num2 = "344.998";
            string a = "12345", b = "54321";
            var s = AddStringsWithDecimals(num1, num2);
            var sum = AddStrings(a, b);
            Console.WriteLine("Sum is " + sum);
            Console.WriteLine(s);
        }
    }
}
