using System;
using System.Collections.Generic;

namespace Fundamentals.Companies.Facebook
{
    public class Q340_LongestSubstringWithAtMostKDistinctCharacters
    {
        // https://www.cnblogs.com/grandyang/p/5351347.html
        int lengthOfLongestSubstringKDistinct(string s, int k)
        {
            int max = 0, left = 0;
            var map = new Dictionary<char, int>();
            for (var i = 0; i < s.Length; ++i)
            {
                if (map.ContainsKey(s[i])) {
                    map.Add(s[i], 0);    
                }
                map[s[i]]++;
                while (map.Count > k)
                {
                    map[s[left]]--;
                    if (map[s[left]] == 0) map.Remove(s[left]);
                    left++;
                }
                max = Math.Max(max, i - left + 1);
            }
            return max;
        }
    }


    /*
    Input: "eceba"
    Output: 3
    Explanation: tis "ece" which its length is 3.
    这道题给我们一个字符串，让我们求最多有两个不同字符的最长子串。
    那么我们首先想到的是用 HashMap 来做，HashMap 记录每个字符的出现次数
    ，然后如果 HashMap 中的映射数量超过两个的时候，我们需要删掉一个映射，
    比如此时 HashMap 中e有2个，c有1个，此时把b也存入了 HashMap，那么就有三对映射了，
    这时我们的 left 是0，先从e开始，映射值减1，此时e还有1个，不删除，left 自增1。
    这时 HashMap 里还有三对映射，此时 left 是1，那么到c了，映射值减1，此时e映射为0，
    将e从 HashMap 中删除，left 自增1，然后我们更新结果为 i - left + 1
    ，以此类推直至遍历完整个字符串，参见代码如下：
    */
    public class Q159_LongestSubstringWithAtMostTwoDistinctCharacters
    {
        int LengthOfLongestSubstringTwoDistinct(string s)
        {
            int res = 0, left = 0;
            var m = new Dictionary<char, int>();
            for (var i = 0; i < s.Length; ++i)
            {
                m[s[i]]++;
                while (m.Count > 2)
                {
                    if (--m[s[left]] == 0)
                    {
                        m.Remove(s[left]);
                    }

                    ++left;
                }
                res = Math.Max(res, i - left + 1);
            }
            return res;
        }

        int lengthOfLongestSubstringTwoDistinct_2(string s)
        {
            int left = 0, right = -1, res = 0;
            for (var i = 1; i < s.Length; ++i)
            {
                if (s[i] == s[i - 1])
                {
                    continue;
                }

                if (right >= 0 && s[right] != s[i])
                {
                    res = Math.Max(res, i - left);
                    left = right + 1;
                }
                right = i - 1;
            }
            return Math.Max(s.Length - left, res);
        }
    }
}
