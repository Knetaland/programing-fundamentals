using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Facebook
{
    /*
    Given a string s of '(' , ')' and lowercase English characters. 

    Your task is to remove the minimum number of parentheses ( '(' or ')', in any positions ) so that the resulting parentheses string is valid and return any valid string.

    Formally, a parentheses string is valid if and only if:

    It is the empty string, contains only lowercase characters, or
    It can be written as AB (A concatenated with B), where A and B are valid strings, or
    It can be written as (A), where A is a valid string.
    

    Example 1:

    Input: s = "lee(t(c)o)de)"
    Output: "lee(t(c)o)de"
    Explanation: "lee(t(co)de)" , "lee(t(c)ode)" would also be accepted.
    Example 2:

    Input: s = "a)b(c)d"
    Output: "ab(c)d"
    Example 3:

    Input: s = "))(("
    Output: ""
    Explanation: An empty string is also valid.
    Example 4:

    Input: s = "(a(b(c)d)"
    Output: "a(b(c)d)"
    */
    public class Q1249_MinimumRemoveToMakeValidParenthesis : IProblem
    {
        public string MinRemoveToMakeValid(string s)
        {
            if (s == null)
            {
                return s;
            }

            return UsingCount(s);
            // return UsingStack(s);
        }

        // O(n) with O(1) space
        public string UsingCount(string s)
        {
            // get total ) count
            var close = s.Count(c => c == ')');
            var open = 0;
            var res = new StringBuilder();
            foreach (var c in s)
            {
                if (c == '(')
                {
                    if (open == close)
                    {
                        continue;  // already used up all the ), dont use open 
                    }

                    open++;
                }
                else if (c == ')')
                {
                    close--; // always decrement from total ) count
                    if (open == 0)
                    {
                        continue; // no open from earlier to match this..., dont use it
                    }

                    open--; // there is a matching open, canceling out
                }

                res.Append(c);
            }
            return res.ToString();
        }

        // O(n) and O(n) space
        public string UsingStack(string s)
        {
            var toRemove = new bool[s.Length];
            var stack = new Stack<int>();
            for (var i = 0; i < s.Length; i++)
            {
                if (s[i] == ')')
                {
                    if (stack.Count == 0)
                    {
                        toRemove[i] = true;  // closing without openning, mark for deletion
                    }
                    else
                    {
                        stack.Pop();
                    }
                }
                else if (s[i] == '(')
                {
                    stack.Push(i);
                }
            }

            // mark deletion for excess open parenthesis
            while (stack.Count > 0)
            {
                toRemove[stack.Pop()] = true;
            }

            var sb = new StringBuilder();
            for (var i = 0; i < toRemove.Length; i++)
            {
                if (!toRemove[i])
                {
                    sb.Append(s[i]);
                }
            }
            return sb.ToString();
        }
        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
