using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Facebook
{
    public class Q498_DiagonalTraverse : IProblem
    {
        /*
        Given a matrix of M x N elements (M rows, N columns),
        return all elements of the matrix in diagonal order as shown in the below image.
        Input:
        [
        [ 1, 2, 3 ],
        [ 4, 5, 6 ],
        [ 7, 8, 9 ]
        ]

        Output:  [1,2,4,7,5,3,6,8,9]
        */
        public int[] FindDiagonalOrder(int[][] matrix)
        {
            int m = matrix.Length, n = matrix[0].Length;
            var goingUp = true;
            var res = new int[m * n];

            int i = 0, r = 0, c = 0;

            while (r < m && c < n)
            {
                res[i] = matrix[r][c];
                i++;
                var nextRow = r + (goingUp ? -1 : 1);
                var nextCol = c + (goingUp ? 1 : -1); 

                if (nextRow < 0 || nextRow == m || nextCol < 0 || nextCol == n) // at the boundary
                {
                    if (goingUp)
                    {
                        r += nextCol == n ? 1 : 0; // when hit the right wall, row goes down, otherwise same row
                        c += nextCol == n ? 0 : 1; // when hit the right wall, col remains the same, otherwise move right
                    }
                    else
                    {
                        r += nextRow == m ? 0 : 1; // when hitting bottom, row stays the same, otherwise row goes down
                        c += nextRow == m ? 1 : 0; // when hitting bottom, col moves right, otherwise always the first col
                    }

                    goingUp = !goingUp;
                }
                else
                {
                    r = nextRow;
                    c = nextCol;
                }
            }
            return res;
        }

        public void Run()
        {
            var matrix = new int [][] {
                [1, 2, 3],
                [4, 5, 6],
                [7, 8, 9]
            };
            var res = FindDiagonalOrder(matrix);
            Console.WriteLine("res is " + string.Join(",", res));
        }
    }
}
