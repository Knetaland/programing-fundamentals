using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q273_IntegerToEnglishWords : IProblem
    {
        private string[] LESS_THAN_20 = ["", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"];
        private string[] TENS = ["", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"];
        private string[] THOUSANDS = ["", "Thousand", "Million", "Billion"];
        public string NumberToWords(int num)
        {
            if (num == 0)
            {
                return "Zero";
            }

            var i = 0;
            var word = string.Empty;

            while (num > 0)
            {
                if (num % 1000 != 0)
                {
                    word = Helper(num % 1000) + THOUSANDS[i] + " " + word;
                }
                num /= 1000;
                i++;
            }

            return word.Trim();
        }

        private string Helper(int num)
        {
            if (num == 0)
            {
                return string.Empty;
            }

            if (num < 20)
            {
                return LESS_THAN_20[num] + " ";
            }

            if (num < 100)
            {
                return TENS[num / 10] + " " + Helper(num % 10);
            }
            else
            {
                return LESS_THAN_20[num / 100] + " Hundred " + Helper(num % 100);
            }
        }

        public void Run()
        {
            var num = 1_234_223;
            Console.WriteLine($"{num} to english is {NumberToWords(num)}");
        }
    }
}
