using System;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Facebook
{
    public class IntToEnglish : IProblem
    {
        private string[] LESS_THAN_20 = new[] { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
        private string[] TENS = new[] { "", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };
        private string[] THOUSANDS = new[] { "", "Thousand", "Million", "Billion" };

        public string ToEnglish(int n)
        {
            if (n == 0)
            {
                return "zero";
            }

            var i = 0;
            var word = string.Empty;
            while (n > 0)
            {
                var input = n % 1000;
                // in case of 1000 or 1000000 
                if (input > 0)
                {
                    word = Helper(input) + THOUSANDS[i] + " " + word;
                }

                n /= 1000;
                i++;
            }
            return word;
        }

        public string Helper(int input)
        {
            if (input == 0)
            {
                return "";
            }

            if (input < 20)
            {
                return LESS_THAN_20[input] + " ";
            }

            if (input < 100)
            {
                return TENS[input / 10] + " " + Helper(input % 10);
            }
            else
            {
                return LESS_THAN_20[input / 100] + " Hundred " + Helper(input % 100);
            }
        }

        public void Run()
        {
            var input = new[] { 10000, 132, 0, 3123, 421321321, 123, 42132, 1001 };
            foreach (var i in input)
            {
                var word = ToEnglish(i);
                Console.WriteLine($"{i} = {word}");
            }

        }
    }
}
