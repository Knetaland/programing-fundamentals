﻿using System;
using System.Collections.Generic;
using Fundamental.Core;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamentals;

/*
https://leetcode.ca/all/339.html

You are given a nested list of integers nestedList. Each element is either an integer or a list whose elements may also be integers or other lists.

The depth of an integer is the number of lists that it is inside of. For example, the nested list [1,[2,2],[[3],2],1] has each integer's value set to its depth.

Return the sum of each integer in nestedList multiplied by its depth.

Example 1:

Input: [[1,1],2,[1,1]]
Output: 10 
Explanation: Four 1's at depth 2, one 2 at depth 1.

Example 2:
Input: nestedList = [1,[4,[6]]]
Output: 27
Explanation: One 1 at depth 1, one 4 at depth 2, and one 6 at depth 3. 1*1 + 4*2 + 6*3 = 27.

*/
public class Q339_Nested_List_Weight_Sum : IProblem
{
     public int depthSum(IList<NestedInteger> nestedList) {
        var res = depthSum(nestedList, 1);
        return res;

        int depthSum(IList<NestedInteger> nestedList, int depth) {
            var sum = 0;
            foreach(var nestedInt in nestedList) {
                if (nestedInt.IsInteger()) {
                    sum += nestedInt.GetInteger().Value * depth;
                } else {
                    sum += depthSum(nestedInt.GetList(), depth + 1);
                }
            }
            return sum;
        }
     }

    public void Run() {
        var nestedList = new List<NestedInteger> {
            new NestedInteger([
                new NestedInteger(1),
                new NestedInteger(1)
            ]),
            new NestedInteger(2),
            new NestedInteger([
                new NestedInteger(1),
                new NestedInteger(1)
            ]),
        };
        //  [1,[4,[6]]]
        var nestedList2 = new List<NestedInteger> {
            new NestedInteger(1),
            new NestedInteger(new List<NestedInteger> {
                new NestedInteger(4),
                new NestedInteger([new NestedInteger(6)])
            }),
        };

         var nestedList3 = new List<NestedInteger> {
            new NestedInteger(0),
        };

        var res = depthSum(nestedList3);
        res.Print();
    }
}
