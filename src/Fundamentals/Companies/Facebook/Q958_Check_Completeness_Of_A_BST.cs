﻿using System;
using System.Collections;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace Fundamentals;

public class Q958_Check_Completeness_Of_A_BST : IProblem
{
    // O(n)
    // S: O(n)
    public bool IsCompleteTree(TreeNode root) {
        if (root == null) return true;

        var queue = new Queue<TreeNode>();
        queue.Enqueue(root);
        while(queue.Peek() != null) {
            var node = queue.Dequeue();
            queue.Enqueue(node.left);
            queue.Enqueue(node.right);
        }
        while(queue.Count > 0 && queue.Peek() == null) queue.Dequeue();
        return queue.Count == 0;
    }

    public bool IsCompleteTre2(TreeNode root) {
        if (root == null) return true;

        var queue = new Queue<TreeNode>();
        queue.Enqueue(root);
        var end = false;
        while(queue.Count > 0) {
            var node = queue.Dequeue();
            if (node == null) end = true;
            else {
                if (end) return false;
                queue.Enqueue(node.left);
                queue.Enqueue(node.right);
            }
        }
        return true;
    }

    public void Run() {
        int?[] tree = [1,2,3,4,5,6];
        //[1,2,3,4,5,null,7];
        var root = Lib.ToBinaryTree(tree);
        var res = IsCompleteTree(root);
        Console.WriteLine(res);
    }
}
