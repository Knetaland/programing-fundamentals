﻿using System;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamentals;

/*
https://leetcode.com/discuss/interview-question/4288566/E4-meta-phone-screen-qs/

There are 2 arrays which denote departing and returning flights with the respective indexes being time and the values of the array being the cost it takes for the flight.
Return the minimum cost for a round trip provided the return flight can only be taken at a time post departing flight time (i.e if departing at time i, one can catch a returning flight only from time (i+1) onwards).
For eg departing = [1,2,3,4] and returning = [4,3,2,1], the minimum cost for round trip will be 2 i.e departing[0] + returning[3].
Solve this is O(n) time

*/
public class Min_Cost_Flight_Round_Trip : IProblem
{
    public int findMin(int[] departing, int[] returning) {
        int minDep = int.MaxValue, minTotal = int.MaxValue;
        for(var i = 0; i< departing.Length; i++) {
            minDep = Math.Min(minDep, departing[i]);
            if (i+1 < returning.Length) {
                minTotal = Math.Min(minTotal, minDep + returning[i+1]);
            }
        }
        return minTotal;
    }
    public void Run() {
        int[] departing = [1,2,1,4,2], returning = [1,3,2,4];
        var res = findMin(departing, returning);
        res.Print();
    }
}
