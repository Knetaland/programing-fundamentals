﻿using System;

namespace Fundamentals;

/*
https://leetcode.com/problems/kth-missing-positive-number/description/
Given an array arr of positive integers sorted in a strictly increasing order, and an integer k.

Return the kth positive integer that is missing from this array.

 

Example 1:

Input: arr = [2,3,4,7,11], k = 5
Output: 9
Explanation: The missing positive integers are [1,5,6,8,9,10,12,13,...]. The 5th missing positive integer is 9.
Example 2:

Input: arr = [1,2,3,4], k = 2
Output: 6
Explanation: The missing positive integers are [5,6,7,...]. The 2nd missing positive integer is 6.
 

Constraints:

1 <= arr.length <= 1000
1 <= arr[i] <= 1000
1 <= k <= 1000
arr[i] < arr[j] for 1 <= i < j <= arr.length

*/
public class Q1539_Kth_Missing_Positive_Number
{
    // O(n + k): array length, worst case k is outside arry range, still need check up to k
    // S: O(1)
    public int FindKthPositive(int[] arr, int k) {
        int cur = 1, missing = 0, i = 0;
        while(true) {
            if (cur == arr[i]) {
                if (i+1 < arr.Length) {
                    i++;
                }

            } else {
                missing++;
                if (missing == k) {
                    return cur;
                }
            }
            cur++;
        }
    }

    /*
    see https://leetcode.cn/problems/kth-missing-positive-number/solutions/1448395/-by-max-lfsznscofe-0qh4/
    missing number count is:  arr[i] - i -1
    */
    public int FindKthPositiveBinarySearch(int[] arr, int k) {
        int left = 0, right = arr.Length;
        while(left < right) {
            var mid = left + (right - left) /2;
            var missingCount = arr[mid] - mid -1;
            if (k > missingCount) {
                left = mid +1;
            } else {
                right = mid;
            }
        }
        /*
        第k个缺失的数为k - (arr[i] - i - 1) + arr[i]，arr[i] - i - 1表示arr[i]位置缺少的元素个数，
        k - 缺失个数表示从arr[i]开始还缺少几个元素，再加上arr[i]，就是第k个缺失的元素。
        */
        // kth missing number is arr[i] + remaining missing number
        // where remaining missing number = k - (arr[i]- i -1)
        return arr[right-1] + (k - (arr[right-1]- (right-1) -1));
    }
}
