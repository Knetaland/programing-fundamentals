using System;
using System.Collections.Generic;
using System.Text;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Facebook
{
    public class Q68_TextJustification : IProblem
    {
        /*
        Given an array of words and a width maxWidth, format the text such that each line has exactly maxWidth characters and is fully (left and right) justified.

        You should pack your words in a greedy approach; that is, pack as many words as you can in each line. Pad extra spaces ' ' when necessary so that each line has exactly maxWidth characters.

        Extra spaces between words should be distributed as evenly as possible. If the number of spaces on a line do not divide evenly between words, the empty slots on the left will be assigned more spaces than the slots on the right.

        For the last line of text, it should be left justified and no extra space is inserted between words.

        Note:

        A word is defined as a character sequence consisting of non-space characters only.
        Each word's length is guaranteed to be greater than 0 and not exceed maxWidth.
        The input array words contains at least one word.
        Example 1:

        Input:
        words = ["This", "is", "an", "example", "of", "text", "justification."]
        maxWidth = 16
        Output:
        [
        "This    is    an",
        "example  of text",
        "justification.  "
        ]
        Example 2:

        Input:
        words = ["What","must","be","acknowledgment","shall","be"]
        maxWidth = 16
        Output:
        [
        "What   must   be",
        "acknowledgment  ",
        "shall be        "
        ]
        Explanation: Note that the last line is "shall be    " instead of "shall     be",
                    because the last line must be left-justified instead of fully-justified.
                    Note that the second line is also left-justified becase it contains only one word.
        Example 3:

        Input:
        words = ["Science","is","what","we","understand","well","enough","to","explain",
                "to","a","computer.","Art","is","everything","else","we","do"]
        maxWidth = 20
        Output:
        [
        "Science  is  what we",
        "understand      well",
        "enough to explain to",
        "a  computer.  Art is",
        "everything  else  we",
        "do                  "
        ]
        */

        // good explanation: https://www.youtube.com/watch?v=qrZLQmL6fyI

        public IList<string> FullJustify(string[] words, int maxWidth)
        {
            var res = new List<string>();
            // start from beginning, fit as many words as you can for each line
            var index = 0;
            while (index < words.Length)
            {
                var length = words[index].Length;
                var next = index + 1;
                while (next < words.Length)
                {
                    if (length + 1 + words[next].Length > maxWidth)
                    {
                        break; // try to fit next word, and need 1 char for space
                    }

                    length += 1 + words[next].Length;
                    next++;
                }
                var sb = new StringBuilder();
                var gaps = next - index - 1; // say next is 3, index is 0, current line has 3 words, so gap is 2 = 3 - 0 - 1
                if (next == words.Length || gaps == 0)
                {
                    // next is already past the array size, meaning all words are used.
                    // or no gap, meaning the line can only fit one word
                    for (var i = index; i < next - 1; i++)
                    {
                        sb.Append(words[i] + ' ');
                    }
                    sb.Append(words[next - 1]);
                    while (length < maxWidth)
                    {
                        sb.Append(' ');
                        length++;
                    }
                }
                else
                {

                    var additionalSpacesInGap = (maxWidth - length) / gaps;
                    var restOfSpaces = (maxWidth - length) % gaps;
                    // got the line 
                    for (var i = 0; index + i < next - 1; i++)
                    { // up to the word before last word on the line, so it's easier to manage space for last word
                        sb.Append(words[index + i] + ' '); //
                        for (var j = 0; j < additionalSpacesInGap + (i < restOfSpaces ? 1 : 0); j++) // say rest is 2 space for gap of 3, need to append 1 space to each of the first 2 gaps (i starts at 0)
                        {
                            sb.Append(' ');
                        }
                    }
                    sb.Append(words[next - 1]);
                }

                res.Add(sb.ToString());
                index = next;
            }

            return res;
        }

        public int FindLines(string[] words, int maxWidth)
        {
            var lines = 0;
            var index = 0;
            while (index < words.Length)
            {
                var length = words[index].Length;
                var next = index + 1;
                while (next < words.Length)
                {
                    if (length + 1 + words[next].Length > maxWidth)
                    {
                        break;
                    }

                    length += 1 + words[next].Length;
                    next++;
                }
                lines++;
                index = next;
            }
            return lines;
        }
        public void Run()
        {
            var words = new[] {"Science","is","what","we","understand","well","enough","to","explain",
         "to","a","computer.","Art","is","everything","else","we","do"};
            var maxWidth = 20;
            // var words = new [] {"What","must","be","acknowledgment","shall","be"};
            // var maxWidth = 16;

            var res = FullJustify(words, maxWidth);
            var lines = FindLines(words, maxWidth);

            Console.WriteLine("needs " + lines + " lines");
            Console.WriteLine(string.Join("\n", res));
        }
    }
}
