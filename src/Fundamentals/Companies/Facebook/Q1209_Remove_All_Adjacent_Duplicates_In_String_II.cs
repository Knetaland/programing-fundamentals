﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fundamentals;

/*
https://leetcode.com/problems/remove-all-adjacent-duplicates-in-string-ii/description/
see easy one on Q1047

You are given a string s and an integer k, a k duplicate removal consists of choosing k adjacent and equal letters from s and removing them, causing the left and the right side of the deleted substring to concatenate together.

We repeatedly make k duplicate removals on s until we no longer can.

Return the final string after all such duplicate removals have been made. It is guaranteed that the answer is unique.


Example 1:

Input: s = "abcd", k = 2
Output: "abcd"
Explanation: There's nothing to delete.
Example 2:

Input: s = "deeedbbcccbdaa", k = 3
Output: "aa"
Explanation: 
First delete "eee" and "ccc", get "ddbbbdaa"
Then delete "bbb", get "dddaa"
Finally delete "ddd", get "aa"
Example 3:

Input: s = "pbbcggttciiippooaais", k = 2
Output: "ps"
*/
public class Q1209_Remove_All_Adjacent_Duplicates_In_String_II
{
    public string RemoveDuplicates(string s, int k) {
        var stack = new Stack<Node>();
        foreach(var c in s) {
            if (stack.Count > 0 && c == stack.Peek().val) {
                var dup = stack.Peek();
                dup.count++;
                if (dup.count == k) {
                    stack.Pop();
                }
            } else {
                stack.Push(new Node(c, 1));
            }
        }
        var sb = new LinkedList<char>();
        while(stack.Count > 0) {
            var node = stack.Pop();
            for(var i = 0; i < node.count; i++) {
                sb.AddFirst(node.val);
            }
        }
        return new string(sb.ToArray());
    }


    public class Node {
        public char val { get; set; }
        public int count { get; set; }
        public Node(char v, int c) {
            val = v;
            count = c;
        }
    }
}
