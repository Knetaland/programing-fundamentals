using System;
using System.Collections.Generic;
using System.Text;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Facebook
{
    /*
    There is a new alien language which uses the latin alphabet. However, the order among letters are unknown to you.
    You receive a list of non-empty words from the dictionary, where words are sorted lexicographically by
    the rules of this new language. Derive the order of letters in this language.

    Example 1:

    Input:
    [
    "wrt",
    "wrf",
    "er",
    "ett",
    "rftt"
    ]

    Output: "wertf"
    Example 2:

    Input:
    [
    "z",
    "x"
    ]

    Output: "zx"
    Example 3:

    Input:
    [
    "z",
    "x",
    "z"
    ] 

    Output: "" 

    Explanation: The order is invalid, so return "".
    Note:

    You may assume all letters are in lowercase.
    You may assume that if a is a prefix of b, then a must appear before b in the given dictionary.
    If the order is invalid, return an empty string.
    There may be multiple valid order of letters, return any one of them is fine.
    */

    // O (N) to build graph and index (N is num of chars), O(V+E) to sort
    // 
    public class Q269_AlienDictionary : IProblem
    {
        public string AlienOrder(string[] words)
        {
            var (g, indegree) = BuildGraphAndIndegree(words);
            var res = TopoSort(g, indegree);
            if (res.Length != g.Keys.Count)
            {
                return "";
            }

            return res;
        }

        public string TopoSort(Dictionary<char, List<char>> g, int[] indegree)
        {
            var res = new StringBuilder();
            var queue = new Queue<char>();

            foreach (var startNode in g.Keys)
            {
                if (indegree[startNode - 'a'] == 0)
                {
                    queue.Enqueue(startNode);
                }
            }

            while (queue.Count > 0)
            {
                var node = queue.Dequeue();
                res.Append(node);
                foreach (var neighbor in g[node])
                {
                    indegree[neighbor - 'a']--;
                    if (indegree[neighbor - 'a'] == 0)
                    {
                        queue.Enqueue(neighbor);
                    }
                }
            }
            return res.ToString();
        }
        public (Dictionary<char, List<char>>, int[]) BuildGraphAndIndegree(string[] words)
        {
            var graph = new Dictionary<char, List<char>>();
            var indegree = new int[26];

            foreach (var w in words)
            {
                foreach (var c in w)
                {
                    if (!graph.ContainsKey(c))
                    {
                        graph.Add(c, new List<char>());
                    }
                }
            }

            for (var i = 0; i < words.Length - 1; i++)
            {
                var a = words[i];
                var b = words[i + 1];
                var j = 0;
                while (j < a.Length && j < b.Length)
                {
                    if (a[j] != b[j])
                    {
                        var start = a[j];
                        var end = b[j];
                        graph[start].Add(end);
                        indegree[end - 'a']++;
                        break;
                    }
                    j++;
                }
            }
            return (graph, indegree);
        }

        public void Run()
        {
            var words = new[] {
                "wrt",
                "wrr",
                "es",
                "ett",
                "rftt",
                "rfabc"
            };
            // var words = new [] {"zb", "za"};
            var res = AlienOrder(words);
            Console.WriteLine("res is " + res);
        }
    }
}
