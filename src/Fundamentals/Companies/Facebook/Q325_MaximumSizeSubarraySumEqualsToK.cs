using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Facebook
{
    /*
    Given an array nums and a target value k, find the maximum length of a subarray that sums to k. If there isn't one, return 0 instead.

    Example 1:

    Given nums = [1, -1, 5, -2, 3], k = 3,
    return 4. (because the subarray [1, -1, 5, -2] sums to 3 and is the longest)

    Example 2:

    Given nums = [-2, -1, 2, 1], k = 1,
    return 2. (because the subarray [-1, 2] sums to 1 and is the longest)

    Follow Up:
    Can you do it in O(n) time?
    */

    // Very similar Q560
    public class Q325_MaximumSizeSubarraySumEqualsToK : IProblem
    {
        public int MaxSize(int[] nums, int k)
        {
            var res = 0;
            var map = new Dictionary<int, int>();
            var prefixSum = 0;
            for (var i = 0; i < nums.Length; i++)
            {
                prefixSum += nums[i];
                if (prefixSum == k)
                { // found matching, record the length, this should be the longest if found
                    res = i + 1; // length = index + 1
                }
                else if (map.ContainsKey(prefixSum - k))
                {  // k = prefixsum[j] - prefixsum[i-1]
                    res = Math.Max(res, i - map[prefixSum - k]); // length = should be in between index after map[prefixSum-k] and i.  
                }
                if (!map.ContainsKey(prefixSum))
                { // if same prefixSum happens again, ignore, because the earlier record will give you max length
                    map.Add(prefixSum, i);
                }
            }
            return res;
        }
        public void Run()
        {
            var nums = new[] { 1, -1, 5, -4, 3 };
            var k = 3;
            var res = MaxSize(nums, k);
            Console.WriteLine("res is " + res);
        }
    }
}
