using System;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Facebook
{
    public class IsPalindrome : IProblem
    {
        public bool Check(string x)
        {
            if (string.IsNullOrEmpty(x))
            {
                return true;
            }

            int left = 0, right = x.Length - 1;
            while (left < right)
            {
                while (left < right && !char.IsLetterOrDigit(x[left]))
                {
                    left++;
                }

                while (right > left && !char.IsLetterOrDigit(x[right]))
                {
                    right--;
                }

                if (char.ToLower(x[left]) != char.ToLower(x[right]))
                {
                    return false;
                }

                left++;
                right--;
            }
            return true;
        }
        public void Run()
        {
            var s = "A man, a plan, a canal: Panama";
            var s1 = "obbabbo";
            var s2 = "!!!!!";
            Console.WriteLine($"[{s}] is palindrome: " + Check(s));
            Console.WriteLine($"[{s1}] is palindrome: " + Check(s1));
            Console.WriteLine($"[{s2}] is palindrome: " + Check(s2));
        }
    }
}
