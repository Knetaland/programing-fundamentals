﻿using System;
using System.Linq;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamentals;

/* 
https://leetcode.com/problems/shuffle-an-array/description/

Given an integer array nums, design an algorithm to randomly shuffle the array. All permutations of the array should be equally likely as a result of the shuffling.

Implement the Solution class:

Solution(int[] nums) Initializes the object with the integer array nums.
int[] reset() Resets the array to its original configuration and returns it.
int[] shuffle() Returns a random shuffling of the array.
 

Example 1:

Input
["Solution", "shuffle", "reset", "shuffle"]
[[[1, 2, 3]], [], [], []]
Output
[null, [3, 1, 2], [1, 2, 3], [1, 3, 2]]

Explanation
Solution solution = new Solution([1, 2, 3]);
solution.shuffle();    // Shuffle the array [1,2,3] and return its result.
                       // Any permutation of [1,2,3] must be equally likely to be returned.
                       // Example: return [3, 1, 2]
solution.reset();      // Resets the array back to its original configuration [1,2,3]. Return [1, 2, 3]
solution.shuffle();    // Returns the random shuffling of array [1,2,3]. Example: return [1, 3, 2]


*/
public class Q384_Shuffle_An_Array : IProblem
{
    public void Run() {
        int[] cards = [2,5,1,3,4,7];
        var deck = new Deck(cards);
        var shuffledDeck = deck.Shuffule();
        Console.WriteLine(String.Join(',', shuffledDeck));
        var resetDeck = deck.Reset();
        Console.WriteLine(String.Join(',', resetDeck));   
    }
}

public class Deck {
    private int[] _cards;
    private Random _random;
    public Deck(int[] nums)
    {
        _cards = nums;
        _random = new Random();
    }


    // O(1)
    public int[] Reset() {
        return _cards;
    }

    public int[] Shuffule() {
        // O(n)
        var shuffledCards = _cards.ToArray();
        // O(n)
        for(var i = 0; i < shuffledCards.Length; i++) {
            var j = _random.Next(i+1); // exclusive upper bound
            Console.WriteLine($"randomly picked index {j}");
            (shuffledCards[i], shuffledCards[j]) = (shuffledCards[j], shuffledCards[i]); // new c# feature it looks like
        }
        return shuffledCards;
    }
}
