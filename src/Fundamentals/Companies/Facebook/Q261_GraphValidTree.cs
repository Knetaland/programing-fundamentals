using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Facebook
{
    /*
    Given n nodes labeled from 0 to n - 1 and a list of undirected edges (each edge is a pair of nodes),
     write a function to check whether these edges make up a valid tree.

    For example:

    Given n = 5 and edges = [[0, 1], [0, 2], [0, 3], [1, 4]], return true.

    Given n = 5 and edges = [[0, 1], [1, 2], [2, 3], [1, 3], [1, 4]], return false.

    Hint:

    Given n = 5 and edges = [[0, 1], [1, 2], [3, 4]], what should your return? Is this case a valid tree?
    According to the definition of tree on Wikipedia: “a tree is an undirected graph in which any two vertices are connected by exactly one path. In other words, any connected graph without simple cycles is a tree.”
    Note: you can assume that no duplicate edges will appear in edges. Since all edges are undirected, [0, 1] is the same as [1, 0] and thus will not appear together in edges.
    */
    public class Q261_GraphValidTree : IProblem
    {
        public bool ValidTreeDfs(int n, IList<IList<int>> edges)
        {
            var graph = ConstructGraph(n, edges);
            var visited = new bool[n];
            if (!Dfs(graph, visited, 0, -1))
            { // start from 0 node, and since there is no prev, prev is -1
                return false;
            }
            // check all nodes are visited
            foreach (var v in visited)
            {
                if (!v)
                {
                    return false;
                }
            }
            return true;
        }

        public bool ValidTreeBfs(int n, IList<IList<int>> edges)
        {
            var graph = ConstructGraph(n, edges);
            var visited = new HashSet<int>();
            var queue = new Queue<int>();
            queue.Enqueue(0);
            visited.Add(0);
            while (queue.Count != 0)
            {
                var node = queue.Dequeue();
                foreach (var edge in graph[node])
                {
                    if (visited.Contains(edge))
                    {
                        return false;
                    }
                    visited.Add(edge);
                    queue.Enqueue(edge);
                    graph[edge].Remove(node);
                }
            }
            return true;
        }

        public bool ValidTreeUnionFind(int n, IList<IList<int>> edges)
        {
            var roots = new int[n];
            for (var i = 0; i < roots.Length; i++)
            {
                roots[i] = -1;
            }
            foreach (var edge in edges)
            {
                var x = FindRoot(roots, edge[0]);
                var y = FindRoot(roots, edge[1]);
                if (x == y)
                {
                    return false;
                }

                roots[x] = y;
            }
            return true;
        }

        private int FindRoot(int[] roots, int index)
        {
            while (roots[index] != -1)
            {
                index = roots[index];
            }

            return index;
        }

        // check if there is circular
        bool Dfs(Dictionary<int, List<int>> graph, bool[] visited, int current, int parent)
        {
            if (visited[current])
            {
                return false;
            }
            visited[current] = true;
            foreach (var n in graph[current])
            {  // dfs each neighbors, 
                if (n != parent && !Dfs(graph, visited, n, current))
                {
                    return false;
                }
            }
            return true;
        }

        private Dictionary<int, List<int>> ConstructGraph(int n, IList<IList<int>> edges)
        {
            var res = new Dictionary<int, List<int>>();
            for (var i = 0; i < n; i++)
            {
                res.Add(i, new List<int>());
            }
            foreach (var e in edges)
            {
                // undirected graph, can go both direction
                res[e[0]].Add(e[1]);
                res[e[1]].Add(e[0]);
            }
            return res;
        }

        public void Run()
        {
            var n = 5;
            var edges = new[] {
                new [] {0, 1},
                new [] {0, 2},
                new [] {0, 3},
                new [] {1, 4}
            };
            var edges2 = new[] {
                new [] {0, 1},
                new [] {1, 2},
                new [] {2, 3},
                new [] {1, 3},
                new [] {1, 4},
            };
            var res = ValidTreeDfs(n, edges);
            var res2 = ValidTreeDfs(n, edges2);


            var bfs = ValidTreeBfs(n, edges);
            var bfs2 = ValidTreeBfs(n, edges2);

            var uf = ValidTreeUnionFind(n, edges);
            var uf2 = ValidTreeUnionFind(n, edges2);

            Console.WriteLine("is valid tree DFS " + res);
            Console.WriteLine("is valid tree (bfs) " + bfs);
            Console.WriteLine("is valid tree (union find) " + uf);

            Console.WriteLine("graph 2 is valid tree (DFS) " + res2);
            Console.WriteLine("graph 2 is valid tree (bfs) " + bfs2);
            Console.WriteLine("graph 2 is valid tree (union find) " + uf2);
        }
    }
}
