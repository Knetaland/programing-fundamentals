using System;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Facebook
{
    /*
    Given two sparse matrices A and B, return the result of AB.

    You may assume that A's column number is equal to B's row number.

    Example:

    A = [
    [ 1, 0, 0],
    [-1, 0, 3]
    ]

    B = [
    [ 7, 0, 0 ],
    [ 0, 0, 0 ],
    [ 0, 0, 1 ]
    ]


         |  1 0 0 |   | 7 0 0 |   |  7 0 0 |
    AB = | -1 0 3 | x | 0 0 0 | = | -7 0 3 |
                      | 0 0 1 |
    */
    public class Q311_SparseMatrixMultiplication : IProblem
    {
        int[,] Multiply(int[,] a, int[,] b)
        {
            // matrix with size i * k multiply by matrix k * j => size with i * j
            var res = new int[a.GetLength(0), b.GetLength(1)];
            for (var i = 0; i < a.GetLength(0); i++)
            {
                for (var k = 0; k < a.GetLength(1); k++)
                {
                    if (a[i, k] != 0)
                    {
                        for (var j = 0; j < b.GetLength(1); j++)
                        {
                            if (b[k, j] != 0)
                            {
                                res[i, j] += a[i, k] * b[k, j];
                            }
                        }
                    }
                }
            }
            return res;
        }
        public void Run()
        {
            var a = new int[,] {
            {1,0,0},
            {-1,0,3}
        };
            var b = new int[,] {
            {7,0,0},
            {0,0,0},
            {0,0,1},
        };

            var res = Multiply(a, b);
        }
    }
}
