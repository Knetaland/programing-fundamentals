﻿using System;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamentals;

/*
https://leetcode.com/problems/find-peak-element/description/

A peak element is an element that is strictly greater than its neighbors.

Given a 0-indexed integer array nums, find a peak element, and return its index. If the array contains multiple peaks, return the index to any of the peaks.

You may imagine that nums[-1] = nums[n] = -∞. In other words, an element is always considered to be strictly greater than a neighbor that is outside the array.

You must write an algorithm that runs in O(log n) time.

 

Example 1:

Input: nums = [1,2,3,1]
Output: 2
Explanation: 3 is a peak element and your function should return the index number 2.
Example 2:

Input: nums = [1,2,1,3,5,6,4]
Output: 5
Explanation: Your function can return either index number 1 where the peak element is 2, or index number 5 where the peak element is 6.
*/

public class Q162_Find_Peak_Element : IProblem
{
    // O(N)
    public int FindPeakElement(int[] nums)
    {
        var i = 0;
        while (i+1 < nums.Length &&  nums[i+1]>= nums[i]) {
            i++;
        }
        return i;
    }

    // O(logN)
    public int FindPeakElementBinarySearch(int[] nums)
    {
        var left = 0;
        var right = nums.Length;

        while (left < right)
        {
            var mid = left + (right - left) / 2 ;

            var isBiggerThanLeft = mid == 0 || nums[mid] > nums[mid - 1];
            var isBiggerThanRight = mid == nums.Length - 1 || nums[mid] > nums[mid + 1];

            if (isBiggerThanLeft && isBiggerThanRight) // peak in the middle
            {
                return mid;
            }
            if (isBiggerThanLeft) // meaning peak is on the right
            {
                left = mid + 1;
            }
            else
            {
                right = mid - 1;
            }
        }

        return left;  // or right, same
    }

    public int FindValleyElementBinarySearch(int[] nums)
    {
        var left = 0;
        var right = nums.Length ;

        while (left < right)
        {
            var mid = left + (right - left) / 2 ;
            var isLessThanLeft = mid == 0 || nums[mid] < nums[mid - 1];
            var isLesshanRight = mid == nums.Length - 1 || nums[mid] < nums[mid + 1];

            if (isLessThanLeft && isLesshanRight) // valley in the middle
            {
                return mid;
            }
            if (isLessThanLeft) // meaning valley is on the right
            {
                left = mid + 1;
            }
            else
            {
                right = mid - 1;
            }
        }

        return left; // or right, same
    }

    public void Run() {
        int[] nums =
        //  [1,2,3,1];
        // [1,2,1,3,5,6,4];
        // [3,2];
        [3,4,3,2,1];
        var peak = FindPeakElementBinarySearch(nums);
        peak.Print();
        var valley = FindValleyElementBinarySearch([6,8,7,9,10]);
        valley.Print();
    }
}
