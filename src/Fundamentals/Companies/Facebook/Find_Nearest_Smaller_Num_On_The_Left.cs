﻿using System;
using System.Collections;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamentals;

/*
https://www.geeksforgeeks.org/find-the-nearest-smaller-numbers-on-left-side-in-an-array/

Given an array of integers, find the nearest smaller number for every element such that the smaller element is on the left side.

Examples: 

Input:  arr[] = {1, 6, 4, 10, 2, 5}
Output:         {_, 1, 1,  4, 1, 2}
First element ('1') has no element on left side. For 6, 
there is only one smaller element on left side '1'. 
For 10, there are three smaller elements on left side (1,
6 and 4), nearest among the three elements is 4.
Input: arr[] = {1, 3, 0, 2, 5}
Output:        {_, 1, _, 0, 2}
*/

public class Find_Nearest_Smaller_Num_On_The_Left : IProblem
{
    int[] solve(int[] nums) {
        var stack = new Stack<int>();
        var res = new List<int>();
        foreach(var num in nums) {
            while (stack.Count > 0 && stack.Peek() >= num) stack.Pop();
            res.Add(stack.Count == 0 ? -1 : stack.Peek());
            stack.Push(num);
        }
        return res.ToArray();
    }
    public void Run() {
        int[] nums = [1, 3, 0, 2, 5];
        var res = solve(nums);
        res.Print();

    }
}
