/* each char has a dictionary
 'facebook' 
  f -> f', f^, f
  a -> a, @
  c -> .....

  Print all possible words
*/

using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Facebook
{
    public class PrintAllWords : IProblem
    {
        public PrintAllWords()
        {
            dic = new Dictionary<char, List<char>>
            {
                { 'f', new List<char> { 'f' } },
                { 'a', new List<char> { 'a', '@' } },
                { 'c', new List<char> { 'c', 'x' } },
                { 'e', new List<char> { 'e', '?' } }
            };
        }
        private Dictionary<char, List<char>> dic;
        public List<string> GetAll(string f)
        {
            var res = new List<string>();
            if (string.IsNullOrEmpty(f))
            {
                return res;
            }

            Backtrack(res, f, "", 0);
            return res;
        }

        private void Backtrack(List<string> res, string s, string temp, int pos)
        {
            if (pos == s.Length)
            {
                res.Add(temp);
                return;
            }

            foreach (var k in dic[s[pos]])
            {
                temp += k;
                Backtrack(res, s, temp, pos + 1);
                temp = temp.Substring(0, temp.Length - 1);
            }
        }

        public void Run()
        {
            var input = "face";
            var s = GetAll(input);
            Console.WriteLine(string.Join(",", s));
            Console.WriteLine("count:  " + s.Count);
        }
    }
}
