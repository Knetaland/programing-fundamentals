using System;
using System.Collections.Generic;
using Fundamental.Core.DataStructure;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Facebook
{
    // please see explaination at https://leetcode.com/problems/task-scheduler/discuss/104496/concise-java-solution-on-time-o26-space
    public class Q621_TaskScheduler : IProblem
    {
        /*
        Given a char array representing tasks CPU need to do. It contains capital letters A to Z where different letters represent different tasks. Tasks could be done without original order. Each task could be done in one interval. For each interval, CPU could finish one task or just be idle.

        However, there is a non-negative cooling interval n that means between two same tasks, there must be at least n intervals that CPU are doing different tasks or just be idle.

        You need to return the least number of intervals the CPU will take to finish all the given tasks.

        Example:
        Input: tasks = ["A","A","A","B","B","B"], n = 2
        Output: 8
        Explanation: A -> B -> idle -> A -> B -> idle -> A -> B.
        
        Note:
        The number of tasks is in the range [1, 10000].
        The integer n is in the range [0, 100].
        */
        public int LeastIntervalClassic(char[] tasks, int n)
        {
            var map = new Dictionary<char, int>();
            foreach (var t in tasks)
            {
                map[t] = map.GetValueOrDefault(t, 0) + 1;
            }
            var maxHeap = new Heap<Tuple<char, int>>((a, b) => b.Item2 - a.Item2);
            foreach (var item in map)
            {
                maxHeap.Add(new Tuple<char, int>(item.Key, item.Value));
            }
            var length = 0;
            var process = new List<char>();
            while (!maxHeap.IsEmpty)
            {
                var list = new List<Tuple<char, int>>();
                for (var i = 0; i < n + 1; i++)
                {
                    if (!maxHeap.IsEmpty)
                    {
                        var task = maxHeap.Poll();
                        list.Add(task);
                        process.Add(task.Item1);
                    }
                    else
                    {
                        process.Add('*');
                    }
                }

                foreach (var t in list)
                {
                    var remain = t.Item2 - 1;
                    if (remain > 0)
                    {
                        maxHeap.Add(new Tuple<char, int>(t.Item1, remain));
                    }
                }

                length += maxHeap.IsEmpty ? list.Count : n + 1;
            }
            Console.WriteLine("process is " + string.Join(",", process));
            return length;
        }

        public int LeastIntervalSmart(char[] tasks, int n)
        {
            // task is A-Z, get the task occurences
            var occurences = new int[26];
            foreach (var t in tasks)
            {
                occurences[t - 'A']++;
            }
            Array.Sort(occurences);
            // AAABBBCC, n =3 => A~~~A~~~A => AB~~AB~~AB => ABC~AB~~AB
            int most = 25,  // last element has highest occurences
                occurence = occurences[most],  // how many time the task occurs
                chunk = occurence - 1; // chunk excluding the last occurence, because no interval needed for the last task

            var numOfTaskWithSameMostOccurences = 0;  // this will record the last chunk, which will be the number of tasks that all have most occurrences
            var i = most;
            while (i >= 0 && occurences[i] == occurences[most])
            {
                numOfTaskWithSameMostOccurences++;
                i--;
            }
            var chunkSize = n + 1; // interval + occurence
            // when n = 0, chunk size wont work correctly, and task can execute in any order, so will be the length of the tasks
            return Math.Max(tasks.Length, chunk * chunkSize + numOfTaskWithSameMostOccurences);
        }

        public void Run()
        {
            // var tasks = new [] {'A','A','A','B','B','B'};
            var tasks = "AAAABBBEEFFGG".ToCharArray();
            var cd = 3;
            var res = LeastIntervalClassic(tasks, cd);
            Console.WriteLine("min length is " + res);
        }
    }
}
