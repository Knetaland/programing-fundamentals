﻿using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace Fundamentals;

/*
https://leetcode.ca/all/708.html

Given a node from a cyclic linked list which is sorted in ascending order, write a function to insert a value into the list such that it remains a cyclic sorted list. The given node can be a reference to any single node in the list, and may not be necessarily the smallest value in the cyclic list.

If there are multiple suitable places for insertion, you may choose any place to insert the new value. After the insertion, the cyclic list should remain sorted.

If the list is empty (i.e., given node is null), you should create a new single cyclic list and return the reference to that single node. Otherwise, you should return the original given node.
*/
public class Q708_Insert_To_Circular_Linkedlist : IProblem
{
    // O(n)
    // S: O(1)
    public ListNode insert(ListNode head, int insertVal)
    {
        var newNode = new ListNode(insertVal);
        if (head == null)
        {
            newNode.next = newNode; // make it circular linkedlist
            return newNode;
        }

        var prev = head;
        var cur = head.next;
        while (cur != head)
        { // circle
            // if found in btw 2 node
            if (prev.val <= insertVal && insertVal <= cur.val)
                break;
            // the end of circular linkedList
            // 1. insert > pre 
            // 2. insert < cur
            if (prev.val > cur.val && (insertVal >= prev.val || insertVal <= cur.val))
                break;
            prev = cur;
            cur = cur.next;
        }
        prev.next = newNode;
        newNode.next = cur;
        return head;
    }

    public void Run()
    {
        var node3 = new ListNode(3);
        var node4 = new ListNode(4);
        var node1 = new ListNode(1);
        node3.next = node4;
        node4.next = node1;
        node1.next = node3;
        // 3 -> 4 -> 1
        var head = insert(node3, 1);
        var set = new HashSet<ListNode>();
        var cur = head;
        while (set.Add(cur))
        {
            Console.WriteLine(cur.val);
            cur = cur.next;
        }
    }
}
