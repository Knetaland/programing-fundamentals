﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamentals;
/*
https://leetcode.ca/2016-08-05-249-Group-Shifted-strings/
We can shift a string by shifting each of its letters to its successive letter.

For example, "abc" can be shifted to be "bcd".
We can keep shifting the string to form a sequence.

For example, we can keep shifting "abc" to form the sequence: "abc" -> "bcd" -> ... -> "xyz".
Given an array of strings strings, group all strings[i] that belong to the same shifting sequence. You may return the answer in any order.

 

Example 1:

Input: strings = ["abc","bcd","acef","xyz","az","ba","a","z"]
Output: [["acef"],["a","z"],["abc","bcd","xyz"],["az","ba"]]
Example 2:

Input: strings = ["a"]
Output: [["a"]]

*/

public class Q249_Group_Shifted_strings : IProblem
{
    public List<List<string>> groupstrings(string[] strings) {
        Dictionary<string, List<string>> mp = new ();
        foreach (var s in strings) {
            Console.WriteLine($"processing {s}");
            int diff = s[0] - 'a';
            Console.WriteLine($"diff btw {s[0]} and 'a' is  {diff}");
            char[] t = s.ToCharArray();
            for (int i = 0; i < t.Length; ++i) {
                char d = (char) (t[i] - diff);
                Console.WriteLine($"d for {t[i]} with diff {diff} is {d}");
                if (d < 'a') {
                    Console.WriteLine($"d for {d} < a, increment to {d}");
                    d = (char) (d + 26 );
                    Console.WriteLine($"d increment to {d}");
                }
                t[i] = d;
            }
            string key = new string(t);
            Console.WriteLine($"got key {key}");
            if (!mp.ContainsKey(key)) {
                mp.Add(key, new List<string>());
            }
            mp[key].Add(s);
        }
        return mp.Select(m => m.Value).ToList();
    }

    public void Run()  {
        string[] strings = ["abc","bcd","acef","xyz","az","ba","a","z"];
        var res = groupstrings(strings);
        res.Print();
    }
}
