using System;
using System.Collections.Generic;

namespace Fundamentals.Companies.Facebook
{
    public class Q139_WordBreak
    {
        /*
        Given a non-empty string s and a dictionary wordDict containing a list of non-empty words, determine if s can be segmented into a space-separated sequence of one or more dictionary words.

        Note:

        The same word in the dictionary may be reused multiple times in the segmentation.
        You may assume the dictionary does not contain duplicate words.
        Example 1:

        Input: s = "leetcode", wordDict = ["leet", "code"]
        Output: true
        Explanation: Return true because "leetcode" can be segmented as "leet code".
        Example 2:

        Input: s = "applepenapple", wordDict = ["apple", "pen"]
        Output: true
        Explanation: Return true because "applepenapple" can be segmented as "apple pen apple".
                    Note that you are allowed to reuse a dictionary word.
        Example 3:

        Input: s = "catsandog", wordDict = ["cats", "dog", "sand", "and", "cat"]
        Output: false
        */

        public bool WordBreak(string s, IList<string> wordDict)
        {
            var wordSet = new HashSet<string>(wordDict);
            var memo = new bool?[s.Length];
            // for(var i = 0; i < s.Length; i++) {
            //     memo[i] = -1;
            // }
            return BT(s, wordSet, 0, memo);

        }

        private bool BT(string s, HashSet<string> wordSet, int start, bool?[] memo)
        {
            // if (start >= s.Length) return true;
            // if (memo[start].HasValue) return memo[start].Value;
            // for(var i  = start +1; i <=s.Length; i++) {
            //     if (wordSet.Contains(s.Substring(start, i - start)) && BT(s, wordSet, i, memo)) {
            //         return memo[start] = (bool?)true;
            //     }
            // }
            // return memo[start] = (bool?)false;
            return false;
        }

        public static bool WordBreak(string s, ISet<string> wordDict)
        {
            // Check Bad Input Values
            if (wordDict == null || wordDict.Count == 0)
            {
                return false;
            }

            if (string.IsNullOrEmpty(s))
            {
                return wordDict.Contains(s);
            }

            // Check the whole word first
            if (wordDict.Contains(s))
            {
                return true;
            }

            var n = s.Length;
            // Suppose i is the start of a word and j it's end with 0 < i <= j< n
            // Lets create a table C (n*n) that check if s(i,j) is breakable
            // We have this formula for C[i,j]
            // C[i,j] = C[i,j-1] and s[j,j] is in the dictionary
            // C[i,j] = s(i,j) is in dictionary
            // C[i,j] = C[i,k] and s(k+1,j) is in dictonary, 0<k<j
            var C = new bool[n, n]; // initialiazed to false;

            // Fill the diagonal by checking if a word i,i is in the dictionary
            for (var i = 0; i < n; ++i)
            {
                C[i, i] = wordDict.Contains(s.Substring(i, 1));
            }

            // Compute C[0,j] here
            for (var j = 1; j < n; ++j)
            {
                C[0, j] = C[0, j] ||
                  (C[0, j - 1] && C[j, j]) || // C[i,j-1] and s[j,j] is in the dictionary
                  wordDict.Contains(s.Substring(0, j + 1));//s(i,j) is in dictionary
                if (!C[0, j])
                {
                    // Search for k: C[i,j] = C[i,k] and s(k+1,j) is in dictonary, 0<k<j
                    var k = 0;
                    while (k < j - 1 && !C[0, j])
                    {
                        C[k + 1, j] = wordDict.Contains(s.Substring(k + 1, j - k));
                        C[0, j] = C[0, k] && C[k + 1, j];
                        ++k;
                    }
                }
            }

            // Return true if s(0,n-1) is breakable
            return C[0, n - 1];
        }



        public IList<string> WordBreak_II(string s, ISet<string> wordDict)
        {
            IList<string> result = new List<string>();
            // Check Bad Input Values
            if (wordDict == null || wordDict.Count == 0)
            {
                return result;
            }

            if (string.IsNullOrEmpty(s))
            {
                if (wordDict.Contains(s))
                {
                    result.Add(s);
                }

                return result;
            }

            // Check the whole word first
            if (wordDict.Contains(s))
            {
                result.Add(s);
                return result;
            }

            var n = s.Length;
            // Suppose i is the start of a word and j it's end with 0 < i <= j< n
            // Lets create a table C (n*n) that check if s(i,j) is breakable
            // We have this formula for C[i,j]
            // C[i,j] = C[i,j-1] and s[j,j] is in the dictionary
            // C[i,j] = s(i,j) is in dictionary
            // C[i,j] = C[i,k] and s(k+1,j) is in dictionary, 0<k<j
            var C = new bool[n]; // initialized to false;
            IList<int>[] S = new List<int>[n];

            for (var i = 0; i < n; ++i)
            {
                S[i] = new List<int>();
            }

            //Initialize C[0]
            C[0] = wordDict.Contains(s.Substring(0, 1));
            if (C[0])
            {
                S[0].Add(-1);
            }

            // Compute C[j] here
            for (var j = 1; j < n; ++j)
            {
                var breakhere = false;
                if (wordDict.Contains(s.Substring(0, j + 1)))//s(i,j) is in dictionary
                {
                    S[j].Add(-1);
                    breakhere = true;
                }

                // Search for k: C[i,k] and s(k+1,j) is in dictonary, 0<k<j
                var k = 0;
                while (k < j)
                {
                    if (C[k] && wordDict.Contains(s.Substring(k + 1, j - k)))
                    {
                        S[j].Add(k);
                        breakhere = true;
                    }
                    ++k;
                }

                C[j] = breakhere;
            }


            // Return true if s(0,n-1) is breakable
            if (C[n - 1])
            {
                var computedString = new string[n * n];
                PrintWordPath(S[n - 1], n - 1, s, S, "", result, computedString);
            }

            return result;
        }

        public void PrintWordPath(IList<int> coords, int j, string s, IList<int>[] S, string currentPhrase, IList<string> result, string[] computed)
        {
            foreach (var i in coords)
            {
                if (i < -1)
                {
                    if (!(currentPhrase.Length == 0))
                    {
                        result.Add(currentPhrase);
                    }
                }

                var index = i + 1 + s.Length * (j - i);
                if (computed[index] == null)
                {
                    computed[index] = s.Substring(i + 1, j - i);
                }

                var word = computed[index];

                if (!(currentPhrase.Length == 0))
                {
                    word = string.Format("{0} {1}", word, currentPhrase);
                }

                if (i >= 0)
                {
                    PrintWordPath(S[i], i, s, S, word, result, computed);
                }
                else
                {
                    result.Add(word);
                }
            }
        }
    }

}
