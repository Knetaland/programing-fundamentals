﻿using System;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamentals;

/*
https://leetcode.ca/all/163.html

Given a sorted integer array nums, where the range of elements are in the inclusive range [lower, upper], return its missing ranges.

Example:

Input: nums = [0, 1, 3, 50, 75], lower = -2 and upper = 99,
Output: ["2", "4->49", "51->74", "76->99"]
// 2,4 - 49
// pre = 3
// cur = 50

// pre =  lower - 1, to satisfy {pre+1}
// == 2 ? pre + 1
// > 2 ? pre+1 -> cur -1

// last element:
//   -> pre should be at last element, upper - pre will use the same logic: pre + 1 -> upper
*/
public class Q163_Missing_Ranges : IProblem
{
    public IList<string> findMissingRangesEasy(int[] nums, int lower, int upper) {
        var res = new List<string>();
        if (nums.Length == 0) return [$"{lower}->{upper}"];

        if (nums[0] > lower) {
            res.Add(getString(lower, nums[0]));
        }
        for (var i = 1; i < nums.Length; i++) {
            if (nums[i] - nums[i-1] > 1) {
                res.Add(getString(nums[i-1], nums[i]));
            }
        }
        if (nums[nums.Length-1] < upper) {
            res.Add(getString(nums[nums.Length-1], upper));
        }
        return res;

        string getString(int left, int right) {
            if(right-left == 2) {
                return $"{left+1}";
            } else {
                return $"{left+1}->{right-1}";
            }
        }
    }

    public IList<string> findMissingRanges(int[] nums, int lower, int upper) {
        var res = new List<string>();
        
        long pre = (long)lower - 1; // in case of overflow
        var i = 0;
        while(i <  nums.Length) {
            var effectiveNum = Math.Min(nums[i], (long)upper+1);
            var diff = effectiveNum - pre ;
            if (diff > 1) {
                if (diff == 2) { // only missing 1 number
                    res.Add($"{pre+1}");
                } else if (diff > 2) {
                    res.Add($"{pre+1}->{effectiveNum-1}");
                }
            }
            if (nums[i] > upper) {
                break;
            }
            if (nums[i] > pre) {
                pre = nums[i];
            }
            i++;
        }
        if (i == nums.Length) {
            var diff = upper - pre;
            if (diff > 1) {
                if (diff == 2) { // only missing 1 number
                    res.Add($"{pre+1}");
                } else if (diff > 2) {
                    res.Add($"{pre+1}->{upper}");
                }
            }
        }
        return res;
    }

    public void Run() {
        int[] nums = [0, 1, 3, 50, 75];
        int  lower = -2 , upper = 99;
        var res = findMissingRanges(nums, lower, upper);
        Lib.Print(res);
    }
}
