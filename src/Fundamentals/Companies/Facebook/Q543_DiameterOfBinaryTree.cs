using System;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace Fundamentals.Companies.Facebook
{
    public class Q543_DiameterOfBinaryTree : IProblem
    {
        // diameter of tree will be the depth(left) + depth(right)， it's the num of edges between them
        public int DiameterOfBinaryTree(TreeNode root)
        {
            var diameter = 0;
            Depth(root);
            return diameter;

            int Depth(TreeNode n)
            {
                if (n == null)
                {
                    return 0;
                }

                var l = Depth(n.left);
                var r = Depth(n.right);
                Console.WriteLine($"at node {n.val}, left is {l}, right is {r}");
                Console.WriteLine($"diameter: {diameter} vs  {l + r}");
                diameter = Math.Max(diameter, l + r);
                return 1 + Math.Max(l, r);
            }
        }

        public void Run()  {
            var root = new TreeNode(1) {
                left = new TreeNode(2) {
                    left = new TreeNode(4) {
                        left = new TreeNode(8) {
                            left = new TreeNode(10) {}
                        }
                    },
                    right = new TreeNode(5) {
                        left = new TreeNode(9) {
                            left = new TreeNode(11)
                        }
                    }
                },
                right = new TreeNode(3),
            };
            var diameter = DiameterOfBinaryTree(root);
        }

    }
}
