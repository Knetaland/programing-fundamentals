﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamentals;

/*
https://leetcode.com/problems/remove-invalid-parentheses/description/

Given a string s that contains parentheses and letters, remove the minimum number of invalid parentheses to make the input string valid.

Return a list of unique strings that are valid with the minimum number of removals. You may return the answer in any order.

 

Example 1:

Input: s = "()())()"
Output: ["(())()","()()()"]
Example 2:

Input: s = "(a)())()"
Output: ["(a())()","(a)()()"]
Example 3:

Input: s = ")("
Output: [""]
 

Constraints:

1 <= s.length <= 25
s consists of lowercase English letters and parentheses '(' and ')'.
There will be at most 20 parentheses in s.
*/
public class Q301_Remove_Invalid_Parentheses : IProblem
{

    //DFS, https://leetcode.cn/problems/remove-invalid-parentheses/solutions/1067271/shan-chu-wu-xiao-de-gua-hao-by-leetcode-9w8au/
    // O (n* 2^n) : n length of s. valid check: n, each char has 2 possible way
    // T: O(n^2): stack for recursion, each recusion take string with length n
    public IList<string> RemoveInvalidParentheses(string s) {
        var res = new List<string>();
        int  lRemove = 0, rRemove = 0;
        // 1st scan, get the min ( ) that can be removed
        for(var i = 0; i < s.Length; i++) {
            if (s[i] == '(') {
                lRemove++;
            } else if (s[i] == ')') {
                if (lRemove == 0) {
                    rRemove++;
                } else {
                    lRemove--;
                }
            }
        }

        helper(s, 0, lRemove, rRemove);
        return res;

        void helper(string str, int start, int l, int r) {
            Console.WriteLine($"processing str {str}, start {start}, l {l}, r {r}");
            if (l== 0 && r == 0) {
                if (isValid(str)) {
                    Console.WriteLine($"{str} is valid, adding to res");
                    res.Add(str);
                } 
                Console.WriteLine($"l and r are zero, done");
                return ;
            }
            for (var i = start; i < str.Length; i++ ) {
                if (i != start && str[i] == str[i-1]) continue; // skip dup
                // remaining str not able to meet removal demand
                if (l + r > str.Length - i)  {
                    Console.WriteLine($"l {l} + r {r} > str.Length - i ({str.Length} - {i}), done");
                    return ;
                }
                // try removing open paren
                if (l > 0 && str[i] == '(') {
                    Console.WriteLine($"lremove = {l}, and (, trying removing ( at index {i} => {str.Substring(0, i) + str.Substring(i+ 1)}");
                    helper(str.Substring(0, i) + str.Substring(i+ 1), i, l - 1, r);
                }
                // try removing a close paren
                if (r > 0 && str[i] == ')') {
                    Console.WriteLine($"Rremove = {r}, and ), trying removing ) at index {i} => {str.Substring(0, i) + str.Substring(i+ 1)}");
                    helper(str.Substring(0, i) + str.Substring(i+ 1), i, l, r - 1);
                }
            }   
        }
    }


    // https://leetcode.cn/problems/remove-invalid-parentheses/solutions/108088/bfsjian-dan-er-you-xiang-xi-de-pythonjiang-jie-by-/
    // BFS, minimum removal, on each level, try removing a paren and add result to next level, to dedup => use Set
    public IList<string> RemoveInvalidParenthesesBFS(string s) {
        var level = new HashSet<string>();
        level.Add(s);
        while(true) {
            Console.WriteLine($"current level: [{string.Join(',', level)}]");
            var validItems = level.Where(item => isValid(item)).ToList();
            if (validItems.Count > 0) return validItems;
            var nextLevel = new HashSet<string>();
            foreach(var item in level) {
                for(var i = 0; i < item.Length; i++) {
                    if (item[i] == '(' || item[i] == ')') {
                        nextLevel.Add(item.Substring(0, i) + item.Substring(i+1));
                    }
                }
            }

            level = nextLevel;

            if (level.Count == 0) return [];
        }
    }

    public void Run() {
        string s =  ")((";
        // var res = RemoveInvalidParenthesesBFS(s);
        var res = RemoveInvalidParentheses(s);
        res.Print();
    }

    private bool isValid(string s) {
        var count = 0;
        foreach(var c in s) {
            if (c == '(') {
                count++;
            } else if (c == ')') {
                if (count == 0) {
                    return false;
                }
                count--;  
            }
        }
        return count == 0;
    }
}
