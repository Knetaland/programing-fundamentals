﻿using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace Fundamentals;

/*
https://leetcode.ca/all/266.html
Given a string, determine if a permutation of the string could form a palindrome.

Example 1:

Input: "code"
Output: false
Example 2:

Input: "aab"
Output: true
Example 3:

Input: "carerac"
Output: true
*/
public class Q266_Palindrome_Permutation : IProblem
{
    public bool IsPalindrome(string s) {
        if (string.IsNullOrEmpty(s)) return true;

        var map = new Dictionary<char, int>();
        foreach(var c in s) {
            if (!map.ContainsKey(c)) {
                map.Add(c, 1);
            } else {
                map[c]--;
                if (map[c] == 0) {
                    map.Remove(c);
                }
            }
        }
        return map.Count <= 1;
    }
    public void Run() {
        var list = new string[] {
            "code",
            "aab",
            "carabeerac"
        };
        foreach(var s in list) {
            var res = IsPalindrome(s);
            Console.WriteLine($"is {s} panlindrome permutation: {res}");
        }
    }
}
