using System;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace Fundamentals.Companies.Facebook
{
    public class PrintReverse : IProblem
    {
        public void Print(ListNode head)
        {
            if (head == null)
            {
                return;
            }

            Print(head.next);
            Console.WriteLine($"{head.val} ");
        }

        void IProblem.Run()
        {
        }
    }
}
