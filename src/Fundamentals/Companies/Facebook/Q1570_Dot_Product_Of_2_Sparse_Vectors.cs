﻿using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace Fundamentals;

// O(N): numb of non-zeros
// O(N): numb of non-zeros
public class Q1570_Dot_Product_Of_2_Sparse_Vectors : IProblem
{
    public void Run() {
        var vec1 = new SparseVector([1,0,0,2,3]);
        var vec2 = new SparseVector([0,3,0,4, 0]);
        var res = vec1.DotProduct(vec2);
        Console.WriteLine(res);
    }
}

public class SparseVector {
    public Dictionary<int, int> map {get;} = new Dictionary<int, int>();
    public SparseVector(int[] nums) {
        for(var i =0; i < nums.Length; i++) {
            if (nums[i] != 0) map.Add(i, nums[i]);
        }
    }

    public int DotProduct(SparseVector vec) {
        if (vec.map.Count > this.map.Count) return vec.DotProduct(this); // pick the one with less entries, to reduce the loop size
        var res = 0;
        foreach(var item in vec.map) {
            if (map.ContainsKey(item.Key)) {
                res += map[item.Key] * item.Value;
            }
        }
        return res;
    }
}