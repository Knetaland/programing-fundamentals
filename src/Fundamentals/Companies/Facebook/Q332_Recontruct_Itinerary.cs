﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamentals;

/*
https://leetcode.com/problems/reconstruct-itinerary/description/
You are given a list of airline tickets where tickets[i] = [fromi, toi] represent the departure and the arrival airports of one flight. Reconstruct the itinerary in order and return it.

All of the tickets belong to a man who departs from "JFK", thus, the itinerary must begin with "JFK". If there are multiple valid itineraries, you should return the itinerary that has the smallest lexical order when read as a single string.

For example, the itinerary ["JFK", "LGA"] has a smaller lexical order than ["JFK", "LGB"].
You may assume all tickets form at least one valid itinerary. You must use all the tickets once and only once.
*/
public class Q332_Recontruct_Itinerary : IProblem
{
    /*
    https://leetcode.com/problems/reconstruct-itinerary/solutions/4041944/95-76-dfs-recursive-iterative/
    T: O(NlogN)
    S: O(N)
    */
    public IList<string> FindItinerary(IList<IList<string>> tickets) {
        var map = new Dictionary<string, List<string>>();
        var origin = "JFK";
        foreach (var ticket in tickets) {
            var from = ticket[0];
            var to = ticket[1];
            if (!map.ContainsKey(from)) {
                map.Add(from, new List<string>());
            }
            map[from].Add(to);
        }

        // question wants lexi order, so store reverse lexi order, so last one can be removed with O(1)
        foreach(var destinations in map.Values) {
            destinations.Sort((a, b) => b.CompareTo(a));
        }

        var itinerary = new List<string>();
        dfs(origin, 0);
        itinerary.Reverse();
        return itinerary;


        void dfs(string from, int level) {
            while (map.ContainsKey(from) && map[from].Count > 0) {
                Console.WriteLine($"At level {level}: checking destinations from {from} ");
                var destinations = map[from];
                var nextStop =destinations[^1];
                Console.WriteLine($"At level {level}: next stop is {nextStop}");
                destinations.RemoveAt(destinations.Count -1);
                dfs(nextStop, level+1);
            }
            Console.WriteLine($"At level {level}: adding {from} to itinerary.");
            itinerary.Add(from);
        }
    }

    public void Run() {
        string[][] tickets = [["JFK","KUL"],["JFK","NRT"],["NRT","JFK"]];
        // [["JFK","SFO"],["JFK","ATL"],["SFO","ATL"],["ATL","JFK"],["ATL","SFO"]];
        //  [["MUC","LHR"],["JFK","MUC"],["SFO","SJC"],["LHR","SFO"]];
        var res = FindItinerary(tickets);
        res.Print();
    }
}
