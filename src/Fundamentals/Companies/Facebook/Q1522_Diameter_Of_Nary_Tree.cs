﻿using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace Fundamentals;

/*
https://leetcode.ca/all/1522.html

Given a root of an N-ary tree, you need to compute the length of the diameter of the tree.

The diameter of an N-ary tree is the length of the longest path between any two nodes in the tree. This path may or may not pass through the root.

(Nary-Tree input serialization is represented in their level order traversal, each group of children is separated by the null value.)
*/
public class Q1522_Diameter_Of_Nary_Tree : IProblem
{
    // O(n)
    public int diameter(NaryTreeNode root) {
        var res = 0;
        dfs(root);
        return res;

        // on a given node, get longest and second longest paths
        int dfs(NaryTreeNode node) {
            Console.WriteLine($"processing node {node.val}");
            if (node == null) return 0;
            var max = 0;
            var secondMax = 0;
            foreach(var child in node.children) {
                Console.WriteLine($"processing child node {child.val}");
                var height = dfs(child);
                Console.WriteLine($"got max height for node {child.val}: {height}");
                if (height >  max) {
                    Console.WriteLine($"height {height} is greater than max {max}, setting max= {height}, secondMax = {max}");
                    secondMax = max;
                    max = height;
                } else if (height > secondMax) {
                    Console.WriteLine($"height {height} is greater than secondMax {secondMax}, setting secondMax = {height}");
                    secondMax = height;
                }
            }
            res = Math.Max(res, max + secondMax);
            return 1 + max;
        }
    }

    public void Run() {
        /*
                  1
            2   3   4    5
               / \  |   / \
              6  7  8  9   10
                 |  |  |
                 11 12 13
                 |
                 14
        */
        var root = new NaryTreeNode(1) {
            children = {
                new NaryTreeNode(2),
                new NaryTreeNode(3) {
                    children = {
                        new NaryTreeNode(6),
                        new NaryTreeNode(7) {
                            children = {
                                new NaryTreeNode(11) {
                                    children =  {new NaryTreeNode(14)}
                                }
                            }
                        }
                    }
                },
                new NaryTreeNode(4) {
                    children = {
                        new NaryTreeNode(8) {
                            children = {
                                new NaryTreeNode(12)
                            }
                        }
                    }
                },
                new NaryTreeNode(5) {
                    children = {
                        new NaryTreeNode(9) {
                            children = {
                                new NaryTreeNode(13)
                            }
                        },
                        new NaryTreeNode(10)
                    }
                }
            }
        };
        var res = diameter(root);

        Console.WriteLine(res);
    }
}
