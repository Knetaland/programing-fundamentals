using System;
using Fundamental.Core.DataStructure;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Facebook
{
    /*
    Given a non-empty array of integers, return the third maximum number in this array. If it does not exist, return the maximum number. The time complexity must be in O(n).

    Example 1:

    Input: [3, 2, 1]

    Output: 1

    Explanation: The third maximum is 1.
    Example 2:

    Input: [1, 2]

    Output: 2

    Explanation: The third maximum does not exist, so the maximum (2) is returned instead.
    Example 3:

    Input: [2, 2, 3, 1]

    Output: 1

    Explanation: Note that the third maximum here means the third maximum distinct number.
    Both numbers with value 2 are both considered as second maximum.
    */
    public class Q414_ThirdMaximumNumber : IProblem
    {
        public int ThirdMax(int[] nums)
        {
            int? max = null, second = null, third = null;
            foreach (var num in nums)
            {
                if (max.HasValue && max == num ||
                (second.HasValue && second == num) ||
                (third.HasValue && third == num))
                {
                    continue;
                }
                if (max == null || num > max)
                {
                    third = second;
                    second = max;
                    max = num;
                }
                else if (second == null || num > second)
                {
                    third = second;
                    second = num;
                }
                else if (third == null || num > third)
                {
                    third = num;
                }
            }
            return third ?? max.Value;
        }

        public int ThirdMaxWithHeap(int[] nums)
        {
            var heap = new Heap<int>((a, b) => a.CompareTo(b));
            foreach (var num in nums)
            {
                heap.Add(num);
                if (heap.Size > 3)
                {
                    heap.Poll();
                }
            }
            return heap.Top;
        }

        public void Run()
        {
            var nums = new[] { 2, 1, int.MinValue };
            var res = ThirdMax(nums);
            var res2 = ThirdMaxWithHeap(nums);
            Console.WriteLine("res is " + res);
            Console.WriteLine("res is " + res2);
        }
    }
}
