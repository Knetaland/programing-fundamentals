﻿using System;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamentals;


/*
https://leetcode.com/problems/maximum-swap/description/

You are given an integer num. You can swap two digits at most once to get the maximum valued number.

Return the maximum valued number you can get.

 

Example 1:

Input: num = 2736
Output: 7236
Explanation: Swap the number 2 and the number 7.
Example 2:

Input: num = 9973
Output: 9973
Explanation: No swap.
 

Constraints:

0 <= num <= 108
*/
public class Q670_Maximum_Swap : IProblem
{
    public int MaximumSwap(int num) {
        var indexArr = new int[10];
        var s = num.ToString().ToCharArray();
        for(var i = 0; i< s.Length; i++) { // record last occurence of digit
            indexArr[s[i]-'0'] = i;
        }
        // Lib.Print(indexArr);

        // loop again, find a num that's max occurs after current index, swap, done.
        for(var i = 0;  i < s.Length; i++) {
            var cur = s[i];
            for(var c = '9'; c > cur; c--) { // from 9 going down to cur
                if (indexArr[c-'0'] > i ) {
                    // swap
                    (s[i], s[indexArr[indexArr[c-'0']]]) = (s[indexArr[indexArr[c-'0']]], s[i]);
                    return int.Parse(new string(s));
                }
            }
        }
        // no swap
        return num;
    }

    public void Run()  {
        var num = 2736;
        var res = MaximumSwap(num);
        res.Print();
    }
}
