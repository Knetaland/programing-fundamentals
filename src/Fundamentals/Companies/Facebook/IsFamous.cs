using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Facebook
{
    public class IsFamous : IProblem
    {
        public bool HasCelebrity(List<int> people)
        {
            var candidate = people[0];
            for (var i = 1; i < people.Count; i++)
            {
                if (Knows(candidate, people[i]) && !Knows(people[i], candidate))
                {
                    candidate = people[i];
                }
            }
            for (var i = 0; i < people.Count; i++)
            {
                if (people[i] != candidate && (Knows(candidate, people[i]) || !Knows(people[i], candidate)))
                {
                    return false;
                }
            }
            return true;
        }

        // a fake knows method, find max
        public bool Knows(int a, int b)
        {
            return a < b;
        }
        public void Run()
        {
            var sd = new List<int> { 1, 2, 6, 5, 7, 9, 10, 23, 12, 15, 20 };
            HasCelebrity(sd);
        }
    }
}
