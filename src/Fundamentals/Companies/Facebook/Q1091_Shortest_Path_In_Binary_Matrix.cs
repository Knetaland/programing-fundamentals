﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
namespace Fundamentals;

/**
https://leetcode.com/problems/shortest-path-in-binary-matrix/description/

Given an n x n binary matrix grid, return the length of the shortest clear path in the matrix. If there is no clear path, return -1.

A clear path in a binary matrix is a path from the top-left cell (i.e., (0, 0)) to the bottom-right cell (i.e., (n - 1, n - 1)) such that:

All the visited cells of the path are 0.
All the adjacent cells of the path are 8-directionally connected (i.e., they are different and they share an edge or a corner).
The length of a clear path is the number of visited cells of this path.
**/

public class Q1091_Shortest_Path_In_Binary_Matrix : IProblem
{
    // 0 is clear path,  path length is # of cells
    const int CLEAR = 0;
    // 8 directions
    int[][] DIRS = [
         [1, 0],
                [0, 1],
                [0, -1],
               
                [-1, 0],
                // [-1, -1], 
                // [-1, 1],
                // [1, -1],
                // [1, 1]
            ];

    public int ShortestPathBinaryMatrix(int[][] grid) {
        return ShortestPathBinaryMatrixWithSteps(grid).Length;
    }

    // DFS, no need to get shortest path
    public List<List<int[]>> hasPath(int[][] maze) {
        var res = new List<List<int[]>>();
        var m = maze.Length;
        var n = maze[0].Length;
        var found = dfs(0,0, new List<int[]>());
        return found ? res : [];

        bool dfs(int x, int y, List<int[]> path) {
            if (x < 0 || y < 0 || x >= m || y >=n || maze[x][y] != CLEAR) return false;

            path.Add([x,y]);
            maze[x][y] = -1;
            if (x == m -1 && y == n-1) {
                res.Add(new List<int[]>(path));
                return true;
            };
            var found = false;
            foreach(var dir in DIRS) {
                found = found || dfs(x + dir[0], y + dir[1], new List<int[]>(path));
            }
            return found;
        }
    }

    // BFS for the shortest path
    // O(m x n): visit every cell
    // S: (m x n): queue
    public int[][] ShortestPathBinaryMatrixWithSteps(int[][] grid) {
        if (grid[0][0] != CLEAR) return [];
        var m = grid.Length;
        var n = grid[0].Length;
        var path = new int[m,n][]; // store the path
        var queue = new Queue<int[]>();
        queue.Enqueue([0, 0]);
        var step = 0;
        while(queue.Count > 0) {
            var k = queue.Count;
            step++;
            while(k>0) {
                var pos = queue.Dequeue();
                if (pos[0]== m-1 && pos[1] == n-1) {
                    var i = m -1;
                    var j = n -1;
                    var res = new LinkedList<int[]>();
                    res.AddFirst([i, j]);
                    while(i >0 || j > 0) {
                        var prevStep = path[i,j];
                        if (prevStep != null) {
                            res.AddFirst(prevStep);
                            i = prevStep[0];
                            j = prevStep[1];
                        }
                    }
                    return res.ToArray();
                }
                k--;
                // get all the valid adjacent cells
                foreach(var dir in DIRS) {
                    var x = pos[0]+ dir[0];
                    var y = pos[1] + dir[1];
                    if (IsCleared(grid, x, y)) {
                        queue.Enqueue([x,y]);
                        path[x,y] = [pos[0], pos[1]];
                        grid[x][y] = -1; // so that it is no longer cleared
                    }
                }
            }
        }
        return [];
    }

    private bool IsCleared(int[][] grid, int x, int y) {
        return x>=0 && x<grid.Length && y>=0 && y < grid[0].Length && grid[x][y] == 0;
    }

    public void Run() {
        int[][] grid =  [[0,0,0, 0],[0,0, 0, 0],[0,0,0,0]];
        // var res = ShortestPathBinaryMatrixWithSteps(grid);
        var res = hasPath(grid);
        // Lib.Print(res);
        foreach(var step in res) {
            foreach(var s in step) {
                Console.WriteLine(string.Join(',', s));
            }
            Console.WriteLine("------");
        }
    }
}
