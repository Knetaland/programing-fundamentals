using System;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.Facebook
{
    /*
    You are given a map in form of a two-dimensional integer grid where 1 represents land and 0 represents water.

    Grid cells are connected horizontally/vertically (not diagonally). The grid is completely surrounded by water, and there is exactly one island (i.e., one or more connected land cells).

    The island doesn't have "lakes" (water inside that isn't connected to the water around the island). One cell is a square with side length 1. The grid is rectangular, width and height don't exceed 100. Determine the perimeter of the island.

    

    Example:

    Input:
    [
        [0,1,0,0],
        [1,1,1,0],
        [0,1,0,0],
        [1,1,0,0]
    ]

    Output: 16
    */
    public class Q463_IslandPerimeter : IProblem
    {
        int[][] DIRS = [
                [0, 1],
                [0, -1],
                [1, 0],
                [-1, 0]
            ];
        // works for multiple islands
        public int IslandPerimeter(int[][] grid)
        {
            if (grid == null || grid.Length == 0)
            {
                return 0;
            }

            var perimeter = 0;
            for (var i = 0; i < grid.Length; i++)
            {
                for (var j = 0; j < grid[0].Length; j++)
                {
                    if (grid[i][j] == 1)
                    {
                        perimeter += 4; // add 4 sides for each island
                        // if top or left (already visited) is 1, means it's the adjacent island
                        if (i > 0 && grid[i - 1][j] == 1)
                        {
                            perimeter -= 2;
                        }

                        if (j > 0 && grid[i][j - 1] == 1)
                        {
                            perimeter -= 2;
                        }
                    }
                }
            }
            return perimeter;
        }
 
        public int IslandPerimeterDfs(int[][] grid) {
            var perimeter = 0;
            int VISITED = -1;
            for(var i = 0; i < grid.Length; i++) {
                for(var j = 0; j < grid[0].Length; j++) {
                    if (grid[i][j] == 1) {
                        perimeter += dfs(grid, i, j);
                    }
                }
            }

            return perimeter;

            int dfs(int[][] g, int row, int col) {
                if (row < 0 || row >= g.Length || col < 0 || col >= g[0].Length || g[row][col] == 0) return 1; // at the outter edge or by the water, perimeter + 1
                if (g[row][col] == VISITED) return 0; // edge within two land, no perimeter
                g[row][col] = VISITED;
                var edges = 0;
                foreach(var dir in DIRS) {
                    edges += dfs(g, row + dir[0], col + dir[1]);
                } 
                return edges;
            }
        }
         

        public void Run()
        {
            var grid = new int[][] {
                [0,1,0,0],
                [1,1,1,0],
                [0,1,0,0],
                [1,1,0,0]
            };

            var res =  IslandPerimeterDfs(grid);

            Console.WriteLine("perimeter is " + res);
        }
    }
}
