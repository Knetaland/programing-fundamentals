﻿using System;
using Fundamental.Core.Interfaces;
using Microsoft.VisualBasic;

namespace Fundamentals;

/*
https://leetcode.ca/2017-01-11-408-Valid-Word-Abbreviation

A string can be abbreviated by replacing any number of non-adjacent, non-empty substrings with their lengths. The lengths should not have leading zeros.

For example, a string such as "substitution" could be abbreviated as (but not limited to):

"s10n" ("s ubstitutio n")
"sub4u4" ("sub stit u tion")
"12" ("substitution")
"su3i1u2on" ("su bst i t u ti on")
"substitution" (no substrings replaced)

The following are not valid abbreviations:

"s55n" ("s ubsti tutio n", the replaced substrings are adjacent)
"s010n" (has leading zeros)
"s0ubstitution" (replaces an empty substring)
Given a string word and an abbreviation abbr, return whether the string matches the given abbreviation.

A substring is a contiguous non-empty sequence of characters within a string.

Example 1:

Input: word = "internationalization", abbr = "i12iz4n"
Output: true
Explanation: The word "internationalization" can be abbreviated as "i12iz4n" ("i nternational iz atio n").
Example 2:

Input: word = "apple", abbr = "a2e"
Output: false
Explanation: The word "apple" cannot be abbreviated as "a2e".


*/
public class Q408_Valid_Word_Abbr : IProblem
{
    public void Run() {
        var word = "inte$n:tionalization";
        var abbr = "i12iz4n";
        var res = validWordAbbreviation(word, abbr);
        Console.WriteLine(res);
    }

    // Input: word = "internationalization", abbr = "i12iz4n"
    // O(n)
    // space O(1), no additional space
    /*
        m = 20
        a = 13
        b = i
          length = 12
        */
    public bool validWordAbbreviation(string word, string abbr) {
        word = Strings.Trim(word);
        var a = 0;
        var b = 0;
        while(a < word.Length && b< abbr.Length) {
            // Console.WriteLine($"comparing word[{a}]:{word[a]} against abbr[{b}]:{abbr[b]} ");
            if (!char.IsDigit(abbr[b])) {
                if (word[a]!=abbr[b]) return false;
                // Console.WriteLine("char matches, moving on");
                b++;
                a++;
            } else {
                var length = 0;
                while(b < abbr.Length && char.IsDigit(abbr[b])) {
                    length =  length * 10 + abbr[b] - '0';
                    b++;
                }
                a += length;
                // Console.WriteLine($"abbr has length of {length}, moving a to index {a}");
            }
        }
        // Console.WriteLine($"finished while loop, comparing a:{a} with word {word.Length}, and {b} with abbr {abbr.Length}");
        return a == word.Length && b == abbr.Length;
    }
}
