﻿using System;
using System.Collections.Generic;
using Fundamental.Core.DataStructure;
using Fundamental.Core.Interfaces;

namespace Fundamentals;

/*
https://leetcode.com/problems/sliding-window-median/description/

The median is the middle value in an ordered integer list. If the size of the list is even, there is no middle value. So the median is the mean of the two middle values.

For examples, if arr = [2,3,4], the median is 3.
For examples, if arr = [1,2,3,4], the median is (2 + 3) / 2 = 2.5.
You are given an integer array nums and an integer k. There is a sliding window of size k which is moving from the very left of the array to the very right. You can only see the k numbers in the window. Each time the sliding window moves right by one position.

Return the median array for each window in the original array. Answers within 10-5 of the actual value will be accepted.

 

Example 1:

Input: nums = [1,3,-1,-3,5,3,6,7], k = 3
Output: [1.00000,-1.00000,-1.00000,3.00000,5.00000,6.00000]
Explanation: 
Window position                Median
---------------                -----
[1  3  -1] -3  5  3  6  7        1
 1 [3  -1  -3] 5  3  6  7       -1
 1  3 [-1  -3  5] 3  6  7       -1
 1  3  -1 [-3  5  3] 6  7        3
 1  3  -1  -3 [5  3  6] 7        5
 1  3  -1  -3  5 [3  6  7]       6
Example 2:

Input: nums = [1,2,3,4,2,3,1,4,2], k = 3
Output: [2.00000,3.00000,3.00000,3.00000,2.00000,3.00000,2.00000]
*/
public class Q480_Sliding_Window_Median : IProblem
{
    public double[] MedianSlidingWindow(int[] nums, int k) {
        var res = new double[nums.Length - k + 1];
        var medianRetriever = new MedianRetriever(k);
        for(var i  = 0; i < k; i++) {
            medianRetriever.add(nums[i]);
        }
        res[0] = medianRetriever.getMedian();

        for(var i = k; i < nums.Length; i++) {
            medianRetriever.remove(nums[i-k]);
            medianRetriever.add(nums[i]);
            res[i-k+1] = medianRetriever.getMedian();
        }
        return res;
    }

    public void Run() {
        int[] nums =  [1,2,3,4,2,3,1,4,2];
        int k = 3;
        var res = MedianSlidingWindow(nums, k);
        Console.WriteLine(string.Join(',', res));
    }



    public class MedianRetriever {
        private readonly int _capacity;
        private Heap<int> _small;
        private Heap<int> _large;
        private Dictionary<int, int> _delayDelete;
        int _smallSize, _largeSize;

        public MedianRetriever(int k) {
            _capacity = k;
            _large = new Heap<int>((a,b) => a.CompareTo(b));
            _small = new Heap<int>((a,b) => b.CompareTo(a));
            _delayDelete = new Dictionary<int, int>();
        }

        public void add(int number) {
            if (_small.Size == 0 || number <= _small.Top)  {
                _small.Add(number);
                _smallSize++;
            } else {
                _large.Add(number);
                _largeSize++;
            }
            rebalance();
        }

        private void prune(Heap<int> heap) {
            while(heap.Size > 0 && _delayDelete.ContainsKey(heap.Top) && _delayDelete[heap.Top] > 0) {
                _delayDelete[heap.Top]--;
                heap.Poll();
            }
        }

        private void rebalance() {
            if (_smallSize > _largeSize + 1) {
                _large.Add(_small.Poll());
                _smallSize--;
                _largeSize++;
                prune(_small);
            } else if (_largeSize > _smallSize) {
                _small.Add(_large.Poll());
                _largeSize--;
                _smallSize++;
                prune(_large);
            }
        }

        public void remove(int number) {
            _delayDelete[number] = _delayDelete.GetValueOrDefault(number, 0) + 1;
            if (number <= _small.Top) {
                _smallSize--;
                prune(_small);
            } else {
                _largeSize--;
                prune(_large);
            }
            rebalance();
        }

        public double getMedian() {
            if (_capacity % 2 == 0) {
                return (_small.Top + _large.Top) / 2.0; 
            } else {
                return _small.Top;
            }
        }
    }
}
