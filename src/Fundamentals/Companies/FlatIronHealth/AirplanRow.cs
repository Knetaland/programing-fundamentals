using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace Fundamentals.Companies.FlatIronHealth
{
    public class AirPlaneRow : IProblem
    {
        public List<string> FindSeats(string s, int tickets)
        {
            var res = new List<string>();
            if (string.IsNullOrEmpty(s) || tickets < 1)
            {
                return res;
            }

            Backtrack(res, s, tickets, 0, "");
            return res;
        }

        private void Backtrack(List<string> res, string s, int tickets, int pos, string temp)
        {
            if (temp.Length == tickets)
            {
                res.Add(temp);
                return;
            }
            if (pos == s.Length)
            {
                return;
            }

            var c = s[pos];
            temp += c;
            Backtrack(res, s, tickets, pos + 1, temp);
            temp = temp.Substring(0, temp.Length - 1);
            Backtrack(res, s, tickets, pos + 1, temp);
        }

        public void Run()
        {
            var s = "A";
            var tickets = 1;
            var res = FindSeats(s, tickets);
        }
    }
}
