﻿using System;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamentals;

public class Playground : IProblem
{
    public void Run() {
        // partitionTest();
        // MorseCode("...");
        BST();
    }

    private void BST() {
        var nums = new int[] { 1, 3, 8, 9};
        var target = 3;
        var res = nums.BinarySearch(target);
        Console.WriteLine(res);
    }

    private void partitionTest() {
        // Partiion test
        var rand = new Random();

        int[] test = new int[500];
        for(var i = 0; i < test.Length; i++) {
            test[i] = rand.Next(1000);
        }
        var partition = test.Partition(0, test.Length-1);
        // Console.WriteLine($"partiion is {partition}");
        // test.Print();
        for(var i = 0; i < partition; i++) {
            if (test[i] > test[partition]) Console.WriteLine($"error! index{i} value {test[i]} is greater than pivot {test[partition]}");
        }
        for(var i = partition+1; i < test.Length; i++) {
            if (test[i] < test[partition]) Console.WriteLine($"error! index{i} value {test[i]} is less than pivot {test[partition]}");
        }
    }


    // Oscar Health
    private List<string> MorseCode(string morse) {
        var map = new Dictionary<string, char>()
        {
            {".-", 'A'},
            {"-...", 'B'},
            {"-.-.", 'C'},
            {"-..", 'D'},
            {".", 'E'},
            {"..-.", 'F'},
            {"--.", 'G'},
            {"....", 'H'},
            {"..", 'I'},
            {".---", 'J'},
            {"-.-", 'K'},
            {".-..", 'L'},
            {"--", 'M'},
            {"-.", 'N'},
            {"---", 'O'},
            {".--.", 'P'},
            {"--.-", 'Q'},
            {".-.", 'R'},
            {"...", 'S'},
            {"-", 'T'},
            {"..-", 'U'},
            {"...-", 'V'},
            {".--", 'W'},
            {"-..-", 'X'},
            {"-.--", 'Y'},
            {"--..", 'Z'}
        };

        var res = new List<string>();
        backtrack(morse, "");;
        Console.WriteLine(string.Join(',', res));
        return res;

        void backtrack(string s, string decoded) {
            if (s == "") {
                res.Add(decoded);
                return ;
            }

            foreach(var code in map) {
                if(s.StartsWith(code.Key)) {
                    backtrack(s.Substring(code.Key.Length), decoded + map[code.Key]);
                }
            }
        }

        // void DecodeMorseRecursive(string morse, string decoded, List<string> result)
        // {
        //     if (morse == "")
        //     {
        //         result.Add(decoded);
        //         return;
        //     }

        //     foreach (var pair in map)
        //     {
        //         if (morse.StartsWith(pair.Key))
        //         {
        //             DecodeMorseRecursive(morse.Substring(pair.Key.Length), decoded + pair.Value, result);
        //         }
        //     }
        // }
    }

    

}
