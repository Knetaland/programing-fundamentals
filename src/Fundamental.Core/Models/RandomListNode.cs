namespace Fundamental.Core.Models
{
    public class RandomListNode
    {
        public int label;
        public RandomListNode next, random;
        public RandomListNode(int x) { label = x; }
    };
}
