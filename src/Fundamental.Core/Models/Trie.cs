using System.Collections.Generic;

namespace Fundamental.Core.Models
{
    public class Trie
    {
        public Trie()
        {
            children = new Dictionary<char, Trie>();
        }
        public Dictionary<char, Trie> children;
        public bool endOfWord;
    }
}
