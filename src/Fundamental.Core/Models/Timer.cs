using System;
using System.Diagnostics;

namespace Fundamental.Core.Models
{
    public class Timer : IDisposable
    {
        private readonly Stopwatch _stopWatch;
        private bool _disposed;

        public Timer()
        {
            _stopWatch = new Stopwatch();
            _stopWatch.Start();
        }

        public void Dispose()
        {
            if (_disposed)
            {
                return;
            }

            _disposed = true;
            _stopWatch.Stop();
            Console.WriteLine($"Time elapsed: {_stopWatch.Elapsed.Duration()}");
        }
    }
}
