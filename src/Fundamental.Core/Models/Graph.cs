using System.Collections.Generic;

namespace Fundamental.Core.Models
{
    public class Graph
    {
        public IList<Edge> Edges { get; set; } = new List<Edge>();

        public void AddEdge(int @in, int @out)
        {
            Edges.Add(new Edge(@in, @out));
        }

    }

    public class Edge
    {
        public Edge(int @in, int @out)
        {
            In = @in;
            Out = @out;
        }
        public int In { get; set; }
        public int Out { get; set; }
    }
}
