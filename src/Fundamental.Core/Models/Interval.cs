namespace Fundamental.Core.Models
{
    public class Interval
    {
        public int start;
        public int end;
        public Interval() { start = 0; end = 0; }
        public Interval(int s, int e) { start = s; end = e; }
    }


    public class IntervalSegment
    {
        public int val;
        public bool isStart;

        public IntervalSegment() { }

        public IntervalSegment(int v, bool s = true)
        {
            val = v;
            isStart = s;
        }
    }
}
