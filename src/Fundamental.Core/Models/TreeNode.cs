using System.Collections.Generic;

namespace Fundamental.Core.Models
{
    public class TreeNode
    {
        public TreeNode(int x)
        {
            val = x;
        }

        public int val { get; set; }
        public TreeNode left { get; set; }
        public TreeNode right { get; set; }
    }

    public class GTreeNode
    {
        public int val { get; set; }
        public List<GTreeNode> children { get; set; }
    }

    public class GTreeNodeWrapper
    {
        public int maxHeight { get; set; }
        public GTreeNode node { get; set; }
        public GTreeNodeWrapper(GTreeNode root, int height)
        {
            maxHeight = height;
            node = root;
        }
    }

    public class NaryTreeNode
    {
        public int val;
        public IList<NaryTreeNode> children = new List<NaryTreeNode>();

        public NaryTreeNode(int _val)
        {
            val = _val;
        }

        public NaryTreeNode(int _val, IList<NaryTreeNode> _children)
        {
            val = _val;
            children = _children;
        }
    }

    public class TreeNodeWithParent {
        public int val;
        public TreeNodeWithParent left;
        public TreeNodeWithParent right;
        public TreeNodeWithParent parent;
    }
}
