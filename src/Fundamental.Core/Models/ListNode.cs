namespace Fundamental.Core.Models
{
    public class ListNode
    {
        public int val { get; set; }
        public ListNode next { get; set; }

        public ListNode(int x)
        {
            val = x;
        }
    }
}
