﻿using System.Collections.Generic;

namespace Fundamental.Core;

public class NestedInteger
{
    private readonly int? _intVal;
    private readonly IList<NestedInteger> _nestedIntVals;

    public NestedInteger(int intVal)
    {   
        _intVal = intVal;
    }

    public NestedInteger(IList<NestedInteger> nestedIntegers)
    {
        _nestedIntVals = nestedIntegers;
    }



    // @return true if this NestedInteger holds a single integer, rather than a nested list.
    public bool IsInteger() {
        return _intVal.HasValue;
    }

    // @return the single integer that this NestedInteger holds, if it holds a single integer
    public int? GetInteger() {
        return _intVal;
    }

     // @return the nested list that this NestedInteger holds, if it holds a nested list
    public IList<NestedInteger> GetList() {
        return _nestedIntVals;
    }
}
