﻿using System;
using Fundamental.Core.Models;

namespace Fundamental.Core;

public class BinarySearchTree
{
    public TreeNode root { get; set; }

    public BinarySearchTree(TreeNode node)
    {
        root = node;
    }
    
    public TreeNode insert(int val) {
        return insert(root, val);

        TreeNode insert(TreeNode node, int target) {
            if (node == null) return new TreeNode(target);
            if (target < node.val) node.left = insert(node.left, target);
            else node.right = insert(node.right, target);
            return node;
        }
    }

    public TreeNode insertIterative(int val) {
        var newNode =  new TreeNode(val);
        if (root == null) return newNode;

        var cur = root;
        var parent = root;
        while(cur != null) {
            parent = cur;
            if (val < cur.val) cur = cur.left;
            else cur = cur.right;
        }
        if (val < parent.val) parent.left = newNode;
        else parent.right = newNode;

        return root;
    }

    public TreeNode min(TreeNode node) {
         while (node != null && node.left != null) node = node.left;
         return node;
    }

    public TreeNode delete(int target) {
        return delete(root, target);
    }

    public TreeNode delete(TreeNode node, int target) {
        if (node == null) return null;

        if (target < node.val) node.left = delete(node.left, target);
        else if (target > node.val) node.right = delete(node.right, target);
        // target node found, remove
        else {
            if (node.left == null) return node.right; 
            if (node.right == null) return node.left;
            // both children, get the leftMost of right tree

            // Solution 1 update val and recursively delete min
            // var leftMostNodeOfRightSubTree = min(node.right);
            // node.val = leftMostNodeOfRightSubTree.val;
            // node.right = delete(node.right, leftMostNodeOfRightSubTree.val);

            // Solution 2, find the min node and update the left/right of the node accordingly
            var minNode = node.right; // go to the right to find the min node
            var parent = node; // set parent to be the current node
            while(minNode.left != null) {
                parent = minNode;
                minNode = minNode.left;
            }
            // if the min node is the direct child, just move it up to the deleted node and set left = root.left, keep the right unchanged
            // following is when the min is at least 2 lvl deep
            if (parent != node) { 
                parent.left = minNode.right;  // move min.right to the parent.left
                minNode.right = node.right;  // set leftmost.right = root.right
            }
            minNode.left = node.left;  // set the leftmost.left = root.left
            return minNode;
        }
        return node;
    }



}
