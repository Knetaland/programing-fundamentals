using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Fundamental.Core.Models;

namespace Fundamental.Core.Helpers
{
    public static class Lib
    {
        private static Random _rand = new Random();
        public static IDisposable UseTimer()
        {
            return (IDisposable)new Timer();
        }

        public static ListNode ToListNode(int[] input)
        {
            ListNode head = null, pointer = head;
            foreach (var i in input)
            {
                var node = new ListNode(i);

                if (head == null)
                {
                    head = node;
                }
                else
                {
                    pointer.next = node;
                }

                pointer = node;
            }
            return head;
        }

        public static TreeNode ToBinarySearchTree(int[] nums, int left, int right)
        {
            if (left > right)
            {
                return null;
            }

            var mid = left + (right - left) / 2;
            var root = new TreeNode(nums[mid])
            {
                left = ToBinarySearchTree(nums, left, mid - 1),
                right = ToBinarySearchTree(nums, mid + 1, right)
            };
            return root;
        }

        // O(n)
        public static TreeNode ToBinarySearchTree(List<int> nums, int left, int right)
        {
            if (left > right)
            {
                return null;
            }

            var mid = left + (right - left) / 2;
            var root = new TreeNode(nums[mid])
            {
                left = ToBinarySearchTree(nums, left, mid - 1),
                right = ToBinarySearchTree(nums, mid + 1, right)
            };
            return root;
        }

        public static TreeNode ToBinaryTree(int?[] nums, int pos = 0)
        {
            if (pos >= nums.Length || pos < 0 || nums[pos] == null)
            {
                return null;
            }

            var root = new TreeNode(nums[pos].Value)
            {
                left = ToBinaryTree(nums, pos * 2 + 1),
                right = ToBinaryTree(nums, pos * 2 + 2)
            };
            return root;
        }

        public static TreeNode ToBinaryTree(TreeNode[] nodes, int pos)
        {
            if (pos >= nodes.Length || pos < 0)
            {
                return null;
            }
            var root = nodes[pos];
            root.left = ToBinaryTree(nodes, pos * 2 + 1);
            root.right = ToBinaryTree(nodes, pos * 2 + 2);
            return root;
        }

        public static TreeNode Find(TreeNode root, int target) {
            if (root == null) return null;
            if (root.val == target) return root;
            return Find(root.left, target) ?? Find(root.right, target);
        }

        

        public static void Swap<T>(T[] nums, int x, int y)
        {
            var temp = nums[x];
            nums[x] = nums[y];
            nums[y] = temp;
        }

        public static void Swap<T>(IList<T> nums, int x, int y)
        {
            var temp = nums[x];
            nums[x] = nums[y];
            nums[y] = temp;
        }

        public static void Reverse(int[] nums, int x, int y)
        {
            while (x < y)
            {
                Swap(nums, x++, y--);
            }
        }

        

        public static Interval[] ToIntervals(this int[,] input)
        {
            var res = new Interval[input.GetLength(0)];
            for (var i = 0; i < input.GetLength(0); i++)
            {
                res[i] = new Interval
                {
                    start = input[i, 0],
                    end = input[i, 1]
                };
            }
            return res;
        }

        /* Find the index of first number that is >= target */
        public static int FindFirstIndexGreaterOrEqualsToTarget(this int[] nums, int target) {
            int left = 0, right = nums.Length;
            while(left < right) {
                var mid = left + (right - left) /2;
                if (nums[mid] < target) {
                    left = mid + 1;
                } else {
                    // even if ==, set the right boundary
                    right = mid;
                }
            }
            return right; // can return left here, as left will be === right when existing the while loop
        }

        /* Find the index of last number that is < target */
        public static int FindLastIndexLessThanTarget(this int[] nums, int target) {
            var index = nums.FindLastIndexLessThanTarget(target);
            return index - 1;
        }

        /* Find the index of first number that is > target */
        public static int FindFirstIndexGreaterThanTarget(this int[] nums, int target) {
            int left  = 0, right = nums.Length;
            while (left < right) {
                var mid = left + (right - left) / 2;
                if (nums[mid] <= target) {
                    left = mid + 1;
                } else {
                    right = mid;
                }
            }
            return right;
        }

        /* Find the index of last number that is <= target */
        public static int FindLastIndexLessOrEqualsToTarget(this int[] nums, int target) {
            var index = nums.FindFirstIndexGreaterThanTarget(target);
            return index - 1;
        }

        public static int BinarySearch(this int[] nums, int target) {
            int left = 0, right = nums.Length;
            while(left < right) {
                var mid = left + (right - left)/2;
                if (nums[mid] == target) return mid;
                if (nums[mid] < target) {
                    left = mid + 1;
                } else {
                    right = mid; // can't set to mid -1, as the pattern is using right = length
                }
            }
            return -1;
        }

        /* Partion an array and returnf a pivot, left of pivot will be less, to the right will be greater or equals to pivot*/
        public static int Partition(this int[] nums, int left, int right) {
            var pivotIndex = _rand.Next(left, right);
            Swap(nums, pivotIndex, right);
            var pivot = nums[right];
            var wall = left; 
            while(left < right) {
                if (nums[left] < pivot) {
                    // if (wall != left)  // can add this as improvement, since no swap is needed
                    Swap(nums, wall, left);
                    wall++;
                }
                left++;
            }
            Swap(nums, wall, right);
            return wall;
        }

        #region print
        public static void Print(this string msg)
        {
            Console.WriteLine(msg);
        }

        public static void Print(this int res) {
            Console.WriteLine(res);
        }

        public static void PrintList(ListNode head)
        {
            var list = new List<int>();
            while (head != null)
            {
                list.Add(head.val);
                head = head.next;
            }
            Console.WriteLine($"linkedlist {string.Join(",", list.ToArray())}");
        }

        public static void Print(IEnumerable<IEnumerable<int>> list)
        {
            foreach (var o in list)
            {
                Console.WriteLine(string.Join(",", o));
            }
        }

        public static void Print (this IEnumerable<int> list)
        { 
            Console.WriteLine(string.Join(',', list));
        }

        public static void Print (this IEnumerable<string> list)
        { 
            Console.WriteLine(string.Join(',', list));
        }

        public static void Print<T> (this IEnumerable<IEnumerable<T>> list) {
            Console.WriteLine("[");
            foreach(var l in list) {
                Console.WriteLine(string.Join(',', l));
            }
            Console.WriteLine("]");
        }
        #endregion
    }
}
