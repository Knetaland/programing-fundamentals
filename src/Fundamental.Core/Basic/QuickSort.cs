using System;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace Fundamental.Core.Basic
{
    public class QuickSort : IProblem
    {
        public void Sort(int[] nums)
        {

            Sort(nums, 0, nums.Length - 1);
        }

        private void Sort(int[] nums, int left, int right)
        {
            if (nums.Length <= 1)
            {
                return;
            }

            if (left >= right)
            {
                return;
            }

            var pivot = PartitionII(nums, left, right);
            Sort(nums, left, pivot - 1);
            Sort(nums, pivot + 1, right);
        }

        private int Partition(int[] nums, int left, int right)
        {
            var pivot = nums[right];
            var i = left - 1;
            for (var j = left; j < right; j++)
            {
                if (nums[j] <= pivot)
                {
                    i++;
                    if (i != j)
                    {
                        Lib.Swap(nums, i, j);
                    }

                }
            }
            Lib.Swap(nums, i + 1, right);
            return i + 1;
        }

        private int PartitionII(int[] nums, int left, int right)
        {
            int pivot = nums[left], l = left + 1, r = right;
            while (l <= r)
            {
                while (l <= r && nums[l] <= pivot)
                {
                    l++;
                }

                while (l <= r && nums[r] >= pivot)
                {
                    r--;
                }

                if (l <= r)
                {
                    Lib.Swap(nums, l, r);
                    l++;
                    r--;
                }
            }
            Lib.Swap(nums, left, r);
            return r;
        }

        public void Run()
        {
            var ran = new Random();
            var count = 20;
            var nums = new int[count];

            for (var i = 0; i < count; i++)
            {
                nums[i] = ran.Next(count);
            }
            // var nums = new [] {4,2,4,3,2};
            Console.WriteLine("Original list is " + string.Join(",", nums));
            Sort(nums);
            Console.WriteLine("after sorting, list is " + string.Join(",", nums));
        }
    }
}
