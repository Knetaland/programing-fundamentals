﻿using System;
using System.Linq;

namespace Fundamental.Core;

// Disjoint Set Union/ Union Find
public class DSU
{
    protected int[] parent;

    public DSU(int size)
    {
        parent = new int[size];
        for (var i = 0; i < size; i++) parent[i] = i; // so the root is self
    }

    public int find(int x) {
        if (parent[x] != x) {
            parent[x] = find(parent[x]); // til find the top parent, path compression
        }
        return parent[x];
    }

    public virtual void union(int x, int y) {
        var xRoot = find(x);
        var yRoot = find(y);
        if (xRoot !=  yRoot) {
            parent[yRoot] = xRoot;
        }
    }
}

public class DsuWithSize : DSU
{
    protected int[] size;
    public DsuWithSize(int n) : base(n)
    {
        size = new int[n];
        Array.Fill(size, 1);
    }

     public override void union(int x, int y) {
        var xRoot = find(x);
        var yRoot = find(y);
        if (xRoot == yRoot) return ;
        if (size[xRoot] > size[yRoot]) {
            parent[yRoot] = xRoot;
            size[xRoot] += size[yRoot];
        } else {
            parent[xRoot] = yRoot;
            size[yRoot] += size[xRoot];
        }
    }

    public int max() {
        return size.Max();
    }
}
