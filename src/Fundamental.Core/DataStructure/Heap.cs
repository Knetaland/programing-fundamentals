using System;
using System.Collections.Generic;

namespace Fundamental.Core.DataStructure
{
    public class Heap<T>
    {
        private readonly Comparison<T> _comparison;
        private List<T> _list;

        public Heap(Comparison<T> comparison)
        {
            _comparison = comparison;
            _list = new List<T>();
        }

        public bool IsEmpty => _list.Count == 0;
        public int Size => _list.Count;
        public T Top => IsEmpty ? throw new NullReferenceException() : _list[0];

        public void Add(T item)
        {
            _list.Add(item);
            Swim(item);
        }

        public T Poll()
        {
            if (_list.Count == 0)
            {
                throw new NullReferenceException();
            }

            var item = _list[0];
            _list[0] = _list[_list.Count - 1];
            _list.RemoveAt(_list.Count - 1);
            Sink();
            return item;
        }

        private void Sink()
        {
            if (_list.Count <= 1)
            {
                return;
            }

            var count = _list.Count;
            int current = 0, child = 1;
            var item = _list[0];
            if (count == 2)
            {
                child = 1;
            }
            else
            {
                child = _comparison(_list[1], _list[2]) < 0 ? 1 : 2;
            }

            while (_comparison(item, _list[child]) > 0)
            {
                _list[current] = _list[child];
                _list[child] = item;

                current = child;

                var left = child * 2 + 1;
                if (left > count - 1)
                { // no more child
                    break;
                }
                var right = child * 2 + 2;
                if (right > count - 1)
                {  // no right child, use left
                    child = left;
                }
                else
                {
                    child = _comparison(_list[left], _list[right]) < 0 ? left : right;
                }
            }
        }

        private void Swim(T item)
        {
            var count = _list.Count;
            int current = count - 1, parent = GetParent(current);
            while (_comparison(item, _list[parent]) < 0 && parent >= 0)
            {
                _list[current] = _list[parent];
                _list[parent] = item;
                current = parent;
                parent = GetParent(parent);
            }

            int GetParent(int index) => (index - 1) / 2;
        }
    }
}
