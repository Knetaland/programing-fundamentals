namespace Fundamental.Core.Interfaces
{
    public interface IProblem
    {
        void Run();
    }
}
