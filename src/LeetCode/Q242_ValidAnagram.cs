using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q242_ValidAnagram : IProblem
    {
        /*
         Given two strings s and t , write a function to determine if t is an anagram of s.
             */
        public bool IsAnagram(string s, string t)
        {
            if (s.Length != t.Length)
            {
                return false;
            }

            var map = new Dictionary<char, int>();
            foreach (var c in s)
            {
                if (!map.ContainsKey(c))
                {
                    map.Add(c, 0);
                }
                map[c]++;
            }
            foreach (var c in t)
            {
                if (!map.ContainsKey(c))
                {
                    return false;
                }
                map[c]--;
            }
            foreach (var val in map.Values)
            {
                if (val > 0)
                {
                    return false;
                }
            }
            return true;
        }

        public bool IsAnagramOptimized(string s, string t)
        {
            if (s.Length != t.Length)
            {
                return false;
            }

            var store = new int[26];
            foreach (var c in s)
            {
                store[c - 'a']++;
            }
            foreach (var c in t)
            {
                store[c - 'a']--;
            }
            return !store.Any(v => v > 0);
        }

        public void Run()
        {
            var s = "anagram";
            var t = "nagaram";
            using (Lib.UseTimer())
            {
                var res = IsAnagram(s, t);
                Console.WriteLine($"{s} is Anagram of {t}? {res}");
            }
            using (Lib.UseTimer())
            {
                var res2 = IsAnagramOptimized(s, t);
                Console.WriteLine($"{s} is Anagram of {t}? {res2}");
            }
        }
    }
}
