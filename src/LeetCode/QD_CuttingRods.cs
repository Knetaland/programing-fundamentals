using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class QD_CuttingRods : IProblem
    {
        public int Cut(int[] values)
        {
            var res = new int[values.Length + 1, values.Length + 1];
            for (var i = 1; i <= values.Length; i++)
            {
                for (var j = 1; j <= values.Length; j++)
                {
                    if (j >= i)
                    {
                        res[i, j] = Math.Max(res[i - 1, j], values[i - 1] + res[i, j - i]);
                    }
                    else
                    {
                        res[i, j] = res[i - 1, j];
                    }
                }
            }

            return res[values.Length, values.Length];
        }

        public int CutSingleArray(int[] values)
        {
            var res = new int[values.Length + 1];
            var rods = new int[values.Length + 1];
            for (var i = 1; i <= values.Length; i++)
            {
                for (var j = i; j <= values.Length; j++)
                {
                    if (values[i - 1] + res[j - i] > res[j])
                    {
                        rods[j] = i;
                        res[j] = Math.Max(res[j], values[i - 1] + res[j - i]);
                    }

                }
            }
            var x = values.Length;
            while (x > 0)
            {
                Console.WriteLine("use rod " + rods[x] + " with value " + values[rods[x] - 1]);
                x = x - rods[x];
            }
            return res[values.Length];
        }
        public void Run()
        {
            var values = new[] { 2, 5, 7, 8 };
            var max = CutSingleArray(values);
            Console.WriteLine("max value will be " + max);
        }
    }
}
