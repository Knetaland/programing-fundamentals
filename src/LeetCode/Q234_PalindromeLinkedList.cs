using System;
using System.Collections;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    /*
     * 234. Palindrome Linked List
     * Given a singly linked list, determine if it is a palindrome.

        Example 1:

        Input: 1->2
        Output: false
        Example 2:

        Input: 1->2->2->1
        Output: true
        Follow up:
        Could you do it in O(n) time and O(1) space?
     */
    public class Q234_PalindromeLinkedList : IProblem
    {
        public bool IsPalindrome(ListNode head)
        {
            var stack = new Stack<ListNode>();
            var pt = head;
            var count = 0;
            while (pt != null)
            {
                stack.Push(pt);
                pt = pt.next;
                count++;
            }

            pt = head;

            for (var i = 0; i < count / 2; i++)
            {
                var node = stack.Pop();
                if (pt.val != node.val)
                {
                    return false;
                }
                pt = pt.next;
            }
            return true;
        }

        public bool IsPalindromeO1Space(ListNode head)
        {
            // use 2 pointers, 1 fast, 1 slow.
            // after fast reaches the end, slow is half way through, now reverse the 2nd half, compare the reversed 2nd half with head
            var fast = head;
            var slow = head;
            while (fast != null && fast.next != null)
            {
                fast = fast.next.next;
                slow = slow.next;
            }
            // means odd number, 2nd half should start the next node)
            if (fast != null)
            {
                slow = slow.next;
            }
            var h2 = Reverse(slow);
            var pt = head;
            while (h2 != null)
            {
                if (pt.val != h2.val)
                {
                    return false;
                }
                pt = pt.next;
                h2 = h2.next;
            }

            return true;

            ListNode Reverse(ListNode n)
            {
                ListNode prev = null;
                var cur = n;
                while (cur != null)
                {
                    var next = cur.next;
                    cur.next = prev;
                    prev = cur;
                    cur = next;
                }
                return prev;
            }
        }



        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
