using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given a non-empty array of integers, every element appears three times except for one, which appears exactly once. Find that single one.

    Note:

    Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?

    Example 1:

    Input: [2,2,3,2]
    Output: 3
    Example 2:

    Input: [0,1,0,1,0,1,99]
    Output: 99
    */
    public class Q137_SingleNumber_II : IProblem
    {
        public void Run()
        {
            var nums = new[] { 2, 2, 3, 2 };
            var s = SingleNumber(nums);
        }

        /*
        [1,1,1,5]
        => 001
        001
        001
        101
        => we can see that for the ones appears 3 times, the # of 1's on position % 3 == 0
        => the remaining binary after the treament will be the result   
        */
        public int SingleNumber(int[] nums)
        {
            var res = 0;
            for (var i = 0; i < 32; i++)
            {
                var sum = 0;
                foreach (var num in nums)
                {
                    if ((num >> i & 1) == 1)
                    {  // ith position is 1
                        sum++;
                    }
                }
                sum %= 3;
                res |= sum << i; // set ith position to 1
            }
            return res;
        }
    }
}
