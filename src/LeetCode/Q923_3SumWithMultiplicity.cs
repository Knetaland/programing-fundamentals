using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q923_3SumWithMultiplicity : IProblem
    {
        public void Run()
        {
            var A = new[] { 1, 1, 2, 2, 3, 3, 4, 4, 5, 5 };
            var target = 8;
            var res = ThreeSumMulti(A, target);
            Console.WriteLine("3 sum multi is " + res);

            var res2 = ThreeSumMultiWithHash(A, target);

            Console.WriteLine("3 sum multi is " + res2);
        }

        public int ThreeSumMultiWithHash(int[] A, int target)
        {
            var res = 0;
            var map = new Dictionary<int, int>();
            foreach (var n in A)
            {
                map[n] = map.GetValueOrDefault(n, 0) + 1;
            }
            foreach (var a in map)
            {
                foreach (var b in map)
                {
                    var i = a.Key;
                    var j = b.Key;
                    var k = target - i - j;
                    if (map.ContainsKey(k))
                    {
                        if (i == j && j == k)
                        {
                            res += map[i] * (map[i] - 1) * (map[i] - 2) / 6;
                        }
                        else if (i == j && j != k)
                        {
                            res += map[i] * (map[i] - 1) / 2 * map[k];
                        }
                        else if (i < j && j < k)
                        {
                            res += map[i] * map[j] * map[k];
                        }
                    }
                }
            }
            return res;
        }
        public int ThreeSumMulti(int[] A, int target)
        {
            var m = 1e9 + 7;
            var res = new List<List<int>>();
            long count = 0;
            Array.Sort(A);
            for (var i = 0; i < A.Length - 2; i++)
            {
                var remain = target - A[i];
                var j = i + 1;
                var k = A.Length - 1;
                while (j < k)
                {
                    var sum = A[j] + A[k];

                    if (sum > remain)
                    {
                        k--;
                    }
                    else if (sum < remain)
                    {
                        j++;
                    }
                    else
                    {
                        // count the same number
                        int left = 1, right = 1;
                        while (j + left < k && A[j] == A[j + left])
                        {
                            left++;
                        }
                        while (k - right >= j + left && A[k] == A[k - right])
                        {
                            right++;
                        }
                        if (A[j] != A[k])
                        {
                            count += left * right;
                        }
                        else
                        {
                            count += (k - j) * (k - j + 1) / 2;
                        }
                        j += left;
                        k -= right;
                    }
                }
            }
            return (int)(count % m);
        }
    }
}
