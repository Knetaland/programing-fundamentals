using System;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    // Time O(N): traverse each node once
    // Space O(logN): height recursive call when p q at lowest level
    public class Q236_LowestCommonAncestorOfBinaryTree : IProblem
    {
        public TreeNode LowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q)
        {
            Console.WriteLine($"Going down root {root?.val}, find lca for p [{p.val}] and q [{q.val}]");
            if (root == null || root == p || root == q) // find the target
            {
                Console.WriteLine($"Found root = {root?.val}, returning root");
                return root;
            }
            var left = LowestCommonAncestor(root.left, p, q); // if target found, won't be null
            var right = LowestCommonAncestor(root.right, p, q); 
            if (left != null && right != null)  // means p and q on two different sides
            {
                Console.WriteLine($"left: {left.val} and right: {right.val}, found lca at {root.val}");
                return root;
            }
            else
            {
                Console.WriteLine($"left is {left?.val}, right is {right?.val}");
                return left ?? right;
            }
        }

        // construct parent map, build ancestor set for one node, and look from bottom up for the other node
        // O(N): build the parent map
        // O(N): space to store parent map + O(LogN) height for the ancestors set
        public TreeNode LowestCommonAncestor2(TreeNode root, TreeNode p, TreeNode q) {
            var parentMap = new Dictionary<TreeNode, TreeNode>();
            dfs(null, root);
            var pAncestors = new HashSet<TreeNode>();
            while(p != null) {
                pAncestors.Add(p);
                p = parentMap[p];
            }
            while(!pAncestors.Contains(q)) {
                q = parentMap[q];
            }
            return q;

            void dfs( TreeNode cur, TreeNode parent) {
                if (cur == null) return ;
                parentMap.Add(cur, parent);
                dfs(cur.left, cur);
                dfs(cur.right, cur);
            }
        }
        public void Run()
        {
            var input = new int?[] { 3, 5, 1, 6, 2, 0, 8, null, null, 7, 4 };
            var tree = Lib.ToBinaryTree(input, 0);
            // var p = tree.left.right.left;
            // var q = tree.left.right.right;
            var p = tree.left.right.left;
            var q = tree.right;
            var lca = LowestCommonAncestor(tree, p, q);
            Console.WriteLine("LCA is " + lca.val);
        }


        public TreeNode LCA(TreeNode root, TreeNode p, TreeNode q) {
            if (root == null) return null;
            if (root == p || root == q) return root;
            var left = LCA(root.left, p,  q);
            var right = LCA(root.right, p,  q);
            
            if(left != null && right != null)  return root;
            return left ?? right;
        }
    }
}
