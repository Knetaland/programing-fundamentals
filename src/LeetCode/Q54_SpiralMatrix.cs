using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given a matrix of m x n elements (m rows, n columns), return all elements of the matrix in spiral order.

    Example 1:

    Input:
    [
    [ 1, 2, 3 ],
    [ 4, 5, 6 ],
    [ 7, 8, 9 ]
    ]
    Output: [1,2,3,6,9,8,7,4,5]
    Example 2:

    Input:
    [
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9,10,11,12]
    ]
    Output: [1,2,3,4,8,12,11,10,9,5,6,7]
    */

    public enum Edge
    {
        TOP,
        RIGHT,
        BOTTOM,
        LEFT,
    }

    public class Q54_SpiralMatrix : IProblem
    {
        public IList<int> SpiralOrder(int[][] matrix)
        {
            var res = new List<int>();
            if (matrix == null || matrix.Length == 0)
            {
                return res;
            }

            int m = matrix.Length, n = matrix[0].Length;
            int top = 0, right = n - 1, bottom = m - 1, left = 0;
            var edge = Edge.TOP;
            while (left <= right && top <= bottom)
            {
                switch (edge)
                {
                    case Edge.TOP:
                        for (var i = left; i <= right; i++)
                        {
                            res.Add(matrix[top][i]);
                        }
                        top++; // go down
                        break;
                    case Edge.RIGHT:
                        for (var i = top; i <= bottom; i++)
                        {
                            res.Add(matrix[i][right]);
                        }
                        right--; // go left
                        break;
                    case Edge.BOTTOM:
                        for (var i = right; i >= left; i--)
                        {
                            res.Add(matrix[bottom][i]);
                        }
                        bottom--; // going up
                        break;
                    case Edge.LEFT:
                        for (var i = bottom; i >= top; i--)
                        {
                            res.Add(matrix[i][left]);
                        }
                        left++;
                        break;
                }
                edge = (Edge)(((int)edge + 1) % 4);
            }
            return res;
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
