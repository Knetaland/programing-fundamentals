using System;
using System.Linq;

namespace LeetCode
{
    /*here are a number of spherical balloons spread in two-dimensional space. For each balloon, provided input is the start and end coordinates of the horizontal diameter. Since it's horizontal, y-coordinates don't matter and hence the x-coordinates of start and end of the diameter suffice. Start is always smaller than end. There will be at most 104 balloons.

An arrow can be shot up exactly vertically from different points along the x-axis. A balloon with xstart and xend bursts by an arrow shot at x if xstart ≤ x ≤ xend. There is no limit to the number of arrows that can be shot. An arrow once shot keeps travelling up infinitely. The problem is to find the minimum number of arrows that must be shot to burst all balloons.

Example:

Input:
[[10,16], [2,8], [1,6], [7,12]]

Output:
2

Explanation:
One way is to shoot one arrow for example at x = 6 (bursting the balloons [2,8] and [1,6]) and another arrow at x = 11 (bursting the other two balloons).*/

    // similar to 56
    public class Q452_MinimumNumberOfArrowsToBurstBallons
    {
        public int FindMinArrowShots(int[][] points)
        {
            if (points == null || points.Length == 0)
            {
                return 0;
            }

            var list = points.ToList();
            list.Sort((a, b) => a[0] - b[0]);
            var start = list[0][0];
            var end = list[0][1];
            var min = 0;
            foreach (var point in list)
            {
                if (point[0] <= end)
                {
                    end = Math.Min(end, point[1]);
                }
                else
                {
                    min++;
                    start = point[0];
                    end = point[1];
                }
            }
            min++;
            return min;
        }
    }
}
