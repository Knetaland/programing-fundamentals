using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given a sorted array, two integers k and x, find the k closest elements to x in the array. The result should also be sorted in ascending order. If there is a tie, the smaller elements are always preferred.

    Example 1:

    Input: [1,2,3,4,5], k=4, x=3
    Output: [1,2,3,4]
    

    Example 2:

    Input: [1,2,3,4,5], k=4, x=-1
    Output: [1,2,3,4]    
    */
    public class Q658_FindKClosestElements : IProblem
    {
        public IList<int> FindClosestElements(int[] arr, int k, int x)
        {
            return BinaryWithTwoPointers(arr, k, x);
            // below is more efficient but I dont get it
            // var res = new List<int>();
            // int left = 0, right = arr.Length - k; // so that mid + k wont be out of bound
            // while (left < right) {
            //     int mid = left + (right - left) / 2;
            //     /* In each iteration, if mid + k is closer to x, it means elements from mid + 1 to
            //      mid + k are all equal or closer to x due to ascending order.
            //      Thus we can discard mid by assigning left to mid + 1. */
            //     if (x - arr[mid] > arr[mid + k] - x) left = mid + 1;
            //     else right = mid;
            // }
            // for(var i = 0; i < k; i++) {
            //     res.Add(arr[left + i]);
            // }
            // return res;
        }

        public IList<int> BinaryWithTwoPointers(int[] arr, int k, int x)
        {
            var res = new List<int>();
            var index = FindLeft(arr, 0, arr.Length, x);
            if (index == 0)
            { // everything is > x, k closest will be first k elements
                res.AddRange(arr.Take(k));
                return res;
            }
            if (index == arr.Length)
            { // similar to above, just take last k elements
                res.AddRange(arr.Skip(arr.Length - k).Take(k));
                return res;
            }

            if (arr[index] != x)
            { // meaning x is not in arr
                if (Math.Abs(arr[index - 1] - x) < Math.Abs(arr[index] - x))
                {
                    // prev element is closer, set index to that
                    index--;
                }
            }
            int count = 0, l = index - 1, r = index + 1;
            res.Add(arr[index]);
            count++;
            while (count < k)
            {
                if (l < 0)
                {
                    res.Add(arr[r]);
                    r++;
                }
                else if (r >= arr.Length)
                {
                    res.Add(arr[l]);
                    l--;
                }
                else
                {
                    var ld = Math.Abs(arr[l] - x);
                    var rd = Math.Abs(arr[r] - x);
                    if (ld <= rd)
                    {
                        res.Add(arr[l]);
                        l--;
                    }
                    else
                    {
                        res.Add(arr[r]);
                        r++;
                    }
                }
                count++;
            }
            res.Sort();
            return res;
        }

        int FindLeft(int[] arr, int l, int r, int target)
        {
            while (l < r)
            {
                var mid = l + (r - l) / 2;
                if (target > arr[mid])
                {
                    l = mid + 1;
                }
                else
                {
                    r = mid;
                }
            }
            return r;
        }
        public void Run()
        {
            var arr = new[] { 1, 1, 1, 10, 10, 10 };
            int k = 1, x = 9;
            var res = BinaryWithTwoPointers(arr, k, x);
            Console.WriteLine(string.Join(",", res));
        }
    }
}
