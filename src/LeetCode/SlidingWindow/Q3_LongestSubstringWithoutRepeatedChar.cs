using System;
using System.Collections.Generic;
using System.Text;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given a string, find the length of the longest substring without repeating characters.

    Example 1:

    Input: "abcabcbb"
    Output: 3 
    Explanation: The answer is "abc", with the length of 3. 
    Example 2:

    Input: "bbbbb"
    Output: 1
    Explanation: The answer is "b", with the length of 1.
    Example 3:

    Input: "pwwkew"
    Output: 3
    Explanation: The answer is "wke", with the length of 3. 
                Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
    */
    public class Q3_LongestSubstringWithoutRepeatedChar : IProblem
    {
        public int Longest(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return 0;
            }

            var map = new Dictionary<char, int>();
            int newNonDupStart = 0, max = 0, count = 0;
            int left = 0, right = 0; // not needed for the question, just to record the resulted substring
            for (var i = 0; i < s.Length; i++)
            {
                var currentChar = s[i];
                if (map.ContainsKey(currentChar))
                {
                    newNonDupStart = Math.Max(newNonDupStart, map[currentChar]+1);
                    count = i - newNonDupStart + 1;
                }
                else
                {
                    count++;
                }
                map[currentChar] = i;
                if (count > max)
                {
                    max = count;
                    left = i - count + 1;
                    right = i;
                }
            }

            Console.WriteLine($"substring is {s.Substring(left, right - left + 1)}");
            return max;
        }

        public int Longest_With_Hashset(string s) {
            var visited = new HashSet<char>();
            int max = 0, left = 0;
            for(var i = 0; i < s.Length; i++) {
                while(!visited.Add(s[i])) { // false when char already in set
                    visited.Remove(s[left]);
                    left++;
                }
                max = Math.Max(max, i - left + 1);
            }
            return max;
        }

        public void Run()
        {
            var input = "bbbbb"; // bbbbb, pwwkew
            Console.WriteLine($"longest length of {input} is {Longest(input)}");
        }
    }
}
