using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode.SlidingWindow
{
    /*
    https://leetcode.ca/all/159.html

    Given a string s , find the length of the longest substring t  that contains at most 2 distinct characters.

    Example 1:

    Input: "eceba"
    Output: 3
    e: 1
    b:1
    a: 1
    Left: 2
    max: 3
    cur: eb
    Explanation: t is "ece" which its length is 3.
    Example 2:

    Input: "ccaabbb"
    Output: 5
    Explanation: t is "aabbb" which its length is 5.
    */
    public class Q159_Longest_Substring_With_Two_Disctinc_Characters : IProblem
    {
        public int lengthOfLongestSubstringTwoDistinct(string s) {
            var map = new Dictionary<char, int>();
            var k = 2;
            var left = 0;
            var max = 0;
            for(var i = 0; i < s.Length; i++) {
                var c = s[i];
                map[c] = map.GetValueOrDefault(c, 0) + 1;
                while(map.Count > k) { // slide left
                    var leftChar = s[left];
                    map[leftChar]--;
                    if (map[leftChar] ==0) map.Remove(leftChar);
                    left++;
                }
                max = Math.Max(max, i - left + 1);
            }
            return max;
        }
        public void Run() {
            var s = "ccaabbb";
            var res = lengthOfLongestSubstringTwoDistinct(s);
            Console.WriteLine($"res is {res}");
        }
    }
}