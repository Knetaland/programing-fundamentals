using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode.SlidingWindow
{
    /*
    Given two strings s1 and s2, write a function to return true if s2 contains the permutation of s1. In other words, one of the first string's permutations is the substring of the second string.

    Example 1:

    Input: s1 = "ab" s2 = "eidbaooo"
    Output: True
    Explanation: s2 contains one permutation of s1 ("ba").
    Example 2:

    Input:s1= "ab" s2 = "eidboaoo"
    Output: False
    

    Note:

    The input strings only contain lower case letters.
    The length of both given strings is in range [1, 10,000].
    */
    public class Q567_PermutationInString : IProblem
    {
        public bool CheckInclusion(string s1, string s2) {
            if (s2.Length < s1.Length) return false;
            var map = new Dictionary<char,int>();
            var charNeeded = 0;
            foreach(var c in s1.ToCharArray()) {
                if(!map.ContainsKey(c)) {
                    map.Add(c, 0);
                }
                map[c]++;
                charNeeded++;
            }
            var left = 0;
            for(var i = 0; i < s2.Length; i++) {
                var c = s2[i];

                if (map.ContainsKey(c)) {
                    if (map[c]>0) {
                        charNeeded--;    
                    }
                    map[c]--;
                }

                while (charNeeded == 0) {
                    if (i - left +1 == s1.Length) return true;
                    var leftChar = s2[left];
                    if (map.ContainsKey(leftChar)) {
                        map[leftChar]++;
                        if (map[leftChar] > 0) charNeeded++;
                    }
                    left++;
                }
            }
            return false;
        }

        public bool CheckInclusion2(string s1, string s2)
        {
            if (s1.Length > s2.Length)
            {
                return false;
            }

            var map = new Dictionary<char, int>();
            foreach (var c in s1)
            {
                map[c] = map.GetValueOrDefault(c, 0) + 1;
            }

            var count = map.Count;
            int left = 0, right = 0;
            while (right < s2.Length)
            {
                var c = s2[right];
                if (map.ContainsKey(c))
                {
                    map[c]--;
                    if (map[c] == 0)
                    {
                        count--;
                    }
                }
                while (count == 0)
                {
                    var l = s2[left];
                    if (map.ContainsKey(l))
                    {
                        map[l]++;
                        if (map[l] > 0)
                        {
                            count++;
                        }
                    }
                    if (right - left + 1 == s1.Length)
                    {
                        return true;
                    }
                    left++;
                }
                right++;
            }
            return false;
        }

        public void Run() {
            var s1 = "a";
            var s2 = "ab";
            var res = CheckInclusion(s1, s2);
            Console.WriteLine($"res is {res}");
        }
    }
}
