using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode.SlidingWindow
{
    /*
    Given a string s and a non-empty string p, find all the start indices of p's anagrams in s.

    Strings consists of lowercase English letters only and the length of both strings s and p will not be larger than 20,100.

    The order of output does not matter.

    Example 1:

    Input:
    s: "cbaebabacd" p: "abc"

    Output:
    [0, 6]

    Explanation:
    The substring with start index = 0 is "cba", which is an anagram of "abc".
    The substring with start index = 6 is "bac", which is an anagram of "abc".
    Example 2:

    Input:
    s: "abab" p: "ab"

    Output:
    [0, 1, 2]

    Explanation:
    The substring with start index = 0 is "ab", which is an anagram of "ab".
    The substring with start index = 1 is "ba", which is an anagram of "ab".
    The substring with start index = 2 is "ab", which is an anagram of "ab".
    */
    public class Q438_FindAllAnagramsInAString : IProblem
    {
        public IList<int> FindAnagrams(string s, string p)
        {
            var res = new List<int>();
            var map = new Dictionary<char, int>();
            foreach (var c in p)
            {
                map[c] = map.GetValueOrDefault(c, 0) + 1;
            }

            int begin = 0, end = 0;
            var count = map.Count;
            while (end < s.Length)
            {
                if (map.ContainsKey(s[end]))
                {
                    map[s[end]]--;
                    if (map[s[end]] == 0)
                    {
                        count--;
                    }
                }

                while (count == 0)
                {
                    var c = s[begin];
                    if (map.ContainsKey(c))
                    {
                        map[c]++;
                        if (map[c] > 0)
                        {
                            count++;
                        }
                    }
                    if (end - begin + 1 == p.Length)
                    {
                        res.Add(begin);
                    }
                    begin++;
                }
                end++;
            }
            return res;
        }

        public IList<int> practice(string s, string p) {
           if (string.IsNullOrEmpty(s) || string.IsNullOrEmpty(p)) return [];
           var res = new List<int>();
           var map = new Dictionary<char, int>();
           var needed = 0;
           int left = 0;
           foreach (var c in p) {
                map[c] = map.GetValueOrDefault(c, 0) + 1;
                needed++;
            }

            for(var i = 0; i < s.Length; i++) {
                var c = s[i];
                if (map.ContainsKey(c)) {
                    if (map[c] > 0) needed--;
                    map[c]--;
                }
                while(needed == 0) {
                    var len = i - left + 1;
                    if (len == p.Length) {
                        res.Add(left);
                    }
                    var leftC = s[left];
                    if (map.ContainsKey(leftC)) {
                        map[leftC]++;
                        if(map[leftC] > 0) needed++;
                    }
                    left++;
                }
            }
            return res;
        }
        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
