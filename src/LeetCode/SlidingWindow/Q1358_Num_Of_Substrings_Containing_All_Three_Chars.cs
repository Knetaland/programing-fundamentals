﻿using System;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode;

/*
https://leetcode.com/problems/number-of-substrings-containing-all-three-characters/description/

Given a string s consisting only of characters a, b and c.

Return the number of substrings containing at least one occurrence of all these characters a, b and c.

 

Example 1:

Input: s = "abcabc"
Output: 10
Explanation: The substrings containing at least one occurrence of the characters a, b and c are "abc", "abca", "abcab", "abcabc", "bca", "bcab", "bcabc", "cab", "cabc" and "abc" (again). 
Example 2:

Input: s = "aaacb"
Output: 3
Explanation: The substrings containing at least one occurrence of the characters a, b and c are "aaacb", "aacb" and "acb". 
Example 3:

Input: s = "abc"
Output: 1
 

Constraints:

3 <= s.length <= 5 x 10^4
s only consists of a, b or c characters.
*/

// solution: https://leetcode.cn/problems/number-of-substrings-containing-all-three-characters/solutions/107665/si-kao-de-guo-cheng-bi-da-an-zhong-yao-xiang-xi-tu/
public class Q1358_Num_Of_Substrings_Containing_All_Three_Chars : IProblem
{
    // O(N^2)
    public int NumberOfSubstringsNaive(string s) {
        var map = new Dictionary<int, int>();
        
        int res = 0, left = 0, right = 0, n = s.Length;
        while(right < n) {
            map[s[right]-'a'] = map.GetValueOrDefault(s[right]-'a', 0) + 1;
            if (map.GetValueOrDefault(0, 0) > 0 && map.GetValueOrDefault(1, 0)  >0 && map.GetValueOrDefault(2, 0) > 0) { // all 3 chars are in substring, everything going forward will have 3 chars
                res += n - right;
                map = new Dictionary<int, int>(); // reset map
                left++; 
                right = left; // moving to start from next index
                continue;
            }
            right++;
        }
        return res;
    }

    // note that s only consists of a, b or c characters.
    // O(N)
    // S: O(3) -> O(1)
    public int NumberOfSubstrings(string s) {
        var map = new Dictionary<int, int>();
        int res = 0, left = 0, right = 0, n = s.Length;
        while(right < n) {
            map[s[right]-'a'] = map.GetValueOrDefault(s[right]-'a', 0) + 1;
            while (map.GetValueOrDefault(0, 0) > 0 && map.GetValueOrDefault(1, 0)  >0 && map.GetValueOrDefault(2, 0) > 0) { // all 3 char are in the substring, now sliding the left window
                res += n - right; // every substring after current right will be qualified, don't need to count, just add to res
                map[s[left]-'a']--; // sliding left index
                left++;
            }
            right++;
        }

        return res;

    }

    public void Run() {
        var s =  "aaacb";
        var res  = NumberOfSubstrings(s);
        res.Print();
    }
}
