﻿using System;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode;

/*
https://leetcode.com/problems/max-consecutive-ones-iii/

Given a binary array nums and an integer k, return the maximum number of consecutive 1's in the array if you can flip at most k 0's.

 

Example 1:

Input: nums = [1,1,1,0,0,0,1,1,1,1,0], k = 2
Output: 6
Explanation: [1,1,1,0,0,1,1,1,1,1,1]
Bolded numbers were flipped from 0 to 1. The longest subarray is underlined.
Example 2:

Input: nums = [0,0,1,1,0,0,1,1,1,0,1,1,0,0,0,1,1,1,1], k = 3
Output: 10
Explanation: [0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,1,1,1,1]
Bolded numbers were flipped from 0 to 1. The longest subarray is underlined.
*/
public class Q1004_Consecutive_Zeros_III : IProblem
{
    // if zero in btw i,j is < k, we can form continuous 1s. Once zeros > k, we need to slide the left until condition is met.
    public int LongestOnes(int[] nums, int k) {
        var left = 0;
        var zeroCount = 0;
        var res = 0;
        for(var i =0; i < nums.Length; i++) {
            if (nums[i] == 0) zeroCount++;
            while(zeroCount > k) {
                if (nums[left]==0) zeroCount--;
                left++;
            }
            res = Math.Max(res, i - left + 1);
        }
        return res;
    }

    public void Run() {
        int[] nums =  [0,0,1,1,0,0,1,1,1,0,1,1,0,0,0,1,1,1,1];
        int k = 3;
        var res = LongestOnes(nums, k);
        res.Print();
    }
}
