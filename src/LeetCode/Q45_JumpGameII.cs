using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given an array of non-negative integers, you are initially positioned at the first index of the array.

    Each element in the array represents your maximum jump length at that position.

    Your goal is to reach the last index in the minimum number of jumps.

    Example:

    Input: [2,3,1,1,4]
    Output: 2
    Explanation: The minimum number of jumps to reach the last index is 2.
        Jump 1 step from index 0 to 1, then 3 steps to the last index.
    */
    public class Q45_JumpGameII : IProblem
    {
        /*
        * We use "last" to keep track of the maximum distance that has been reached
        * by using the minimum steps "jumps", whereas "curMax" is the maximum distance
        * that can be reached by using "jumps+1" steps. Thus,
        * curMax = max(i+A[i]) where 0 <= i <= last.
        */
        public int Jump(int[] nums)
        {
            int last = 0, curMax = 0, jumps = 0;
            for (var i = 0; i < nums.Length; i++)
            {
                if (i > last)
                {
                    last = curMax;
                    jumps++;
                }
                curMax = Math.Max(curMax, i + nums[i]);
            }
            return jumps;
        }


        // http://www.cnblogs.com/lichen782/p/leetcode_Jump_Game_II.html
        public int JumpDp(int[] A)
        {
            var jumps = new int[A.Length];
            var pos = new int[A.Length];
            jumps[0] = 0;
            // init the table to have max value
            for (var i = 1; i < A.Length; i++)
            {
                jumps[i] = int.MaxValue;
                pos[i] = int.MaxValue;
            }
            for (var i = 1; i < A.Length; i++)
            {
                for (var j = 0; j < i; j++)
                {
                    if (j + A[j] >= i)
                    {
                        if (jumps[j] + 1 < jumps[i])
                        {
                            jumps[i] = Math.Min(jumps[i], 1 + jumps[j]);
                            pos[i] = j;
                        }
                    }
                }
            }
            var p = pos[A.Length - 1];
            var path = new Stack<int>();
            while (p != 0)
            {
                path.Push(p);
                p = pos[p];
            }
            //Console.WriteLine(String.Join(",", pos));
            while (path.Count > 0)
            {
                Console.WriteLine("Jump " + path.Pop());
            }
            return jumps[A.Length - 1];
        }

        public int Jump2(int[] nums)
        {
            var jumps = new int[nums.Length];
            var pos = new int[nums.Length];
            jumps[0] = 0;
            for (var i = 1; i < nums.Length; i++)
            {
                pos[i] = int.MaxValue;
                jumps[i] = int.MaxValue;
            }

            for (var i = 1; i < nums.Length; i++)
            {
                for (var j = 0; j < i; j++)
                {
                    if (nums[j] + j >= i)
                    {
                        if (jumps[j] + 1 < jumps[i])
                        {
                            jumps[i] = jumps[j] + 1;
                            pos[i] = j;
                        }
                    }
                }
            }
            var p = pos[nums.Length - 1];
            var path = new Stack<int>();
            while (p != 0)
            {
                path.Push(p);
                p = pos[p];
                if (p == 0)
                {
                    path.Push(p);
                }
            }
            //Console.WriteLine(String.Join(",", pos));
            while (path.Count > 0)
            {
                Console.WriteLine("Jump " + path.Pop());
            }
            return jumps[nums.Length - 1];
        }

        public void Run()
        {
            var nums = new[] { 3, 4, 2, 3, 1, 1, 2, 4 };
            Console.WriteLine($"min jums is {Jump2(nums)}");
        }
    }
}
