using System;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    public class Q235_LowestCommonAncestorOfBinarySearchTree : IProblem
    {
        public TreeNode LowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q)
        {
            // if root is p or q
            if (root.val == p.val || root.val == q.val)
            {
                return root;
            }

            if (p.val < root.val ^ q.val < root.val)
            {
                return root;
            }

            if (p.val < root.val && q.val < root.val)
            {
                return LowestCommonAncestor(root.left, p, q);
            }
            else
            {
                return LowestCommonAncestor(root.right, p, q);
            }
        }

         public TreeNode LowestCommonAncestorIterative(TreeNode root, TreeNode p, TreeNode q) {
            while(true) {
                if (root == null) return root;
                if (p.val < root.val && q.val < root.val ) root = root.left;
                else if (p.val > root.val && q.val > root.val) root = root.right;
                else return root;
            }
         }
        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
