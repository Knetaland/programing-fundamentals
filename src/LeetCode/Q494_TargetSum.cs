using System;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q494_TargetSum : IProblem
    {
        /*
        You are given a list of non-negative integers, a1, a2, ..., an, and a target, S. Now you have 2 symbols + and -. For each integer, you should choose one from + and - as its new symbol.

        Find out how many ways to assign symbols to make sum of integers equal to target S.

        Example 1:

        Input: nums is [1, 1, 1, 1, 1], S is 3. 
        Output: 5
        Explanation:

        -1+1+1+1+1 = 3
        +1-1+1+1+1 = 3
        +1+1-1+1+1 = 3
        +1+1+1-1+1 = 3
        +1+1+1+1-1 = 3

        There are 5 ways to assign symbols to make the sum of nums be target 3.
        */

        // simple version of Q282_ExpressionAddOperations
        public int FindTargetSumWays(int[] nums, int s)
        {
            var count = 0;
            Helper(nums, s, 0, 0);
            return count;

            void Helper(int[] numbs, int target, int pos, int sum)
            {
                if (pos == numbs.Length)
                {
                    if (target == sum)
                    {
                        count++;
                    }
                    return;
                }

                Helper(numbs, target, pos + 1, sum + numbs[pos]);
                Helper(numbs, target, pos + 1, sum - numbs[pos]);
            }
        }

        public IList<string> FindAllWaysToTarget(int[] nums, int s)
        {
            var res = new List<string>();
            Dfs(res, "", nums, s, 0, 0);
            return res;
        }

        private void Dfs(List<string> res, string curExpression, int[] nums, int target, int pos, int sum)
        {
            if (pos == nums.Length)
            {
                if (sum == target)
                {
                    res.Add(curExpression);
                }
                return;
            }

            Dfs(res, curExpression + $"+{nums[pos]}", nums, target, pos + 1, sum + nums[pos]);
            Dfs(res, curExpression + $"-{nums[pos]}", nums, target, pos + 1, sum - nums[pos]);
        }

        public void Run()
        {
            var inputs = new[] { 1, 2, 3, 4, 3, 2 };
            var s = 3;
            var res = FindAllWaysToTarget(inputs, s);
            foreach (var r in res)
            {
                Console.WriteLine(r);
            }
        }
    }
}
