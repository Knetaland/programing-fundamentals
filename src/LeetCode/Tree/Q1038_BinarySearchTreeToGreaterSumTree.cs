using System.Collections;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    public class Q1038_BinarySearchTreeToGreaterSumTree : IProblem
    {
        public TreeNode BstToGst(TreeNode root)
        {
            var sum = 0;
            var stack = new Stack<TreeNode>();
            var cur = root;
            while (stack.Count > 0 || cur != null)
            {
                while (cur != null)
                {
                    stack.Push(cur);
                    cur = cur.right;
                }
                var n = stack.Pop();
                n.val += sum;
                sum = n.val;
                cur = n.left;
            }
            return root;
        }

        public TreeNode BstToGstRecursive(TreeNode root)
        {
            var prevSum = 0;
            Helper(root);
            return root;
            void Helper(TreeNode n)
            {
                if (n == null)
                {
                    return;
                }
                Helper(n.right);
                n.val += prevSum;
                prevSum = n.val;
                Helper(n.left);
            }
        }
        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
