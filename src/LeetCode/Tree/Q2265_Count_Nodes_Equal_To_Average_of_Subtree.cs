﻿using System;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode;

/*
https://leetcode.com/problems/count-nodes-equal-to-average-of-subtree/description/

Given the root of a binary tree, return the number of nodes where the value of the node is equal to the average of the values in its subtree.

Note:

The average of n elements is the sum of the n elements divided by n and rounded down to the nearest integer.
A subtree of root is a tree consisting of root and all of its descendants.


*/
public class Q2265_Count_Nodes_Equal_To_Average_of_Subtree : IProblem
{
    // O(n^2)
    public int AverageOfSubtree(TreeNode root) {
        var res = 0;

        Solve(root);
        return res;


        void Solve(TreeNode node) {
            if (node == null) return ;
            var (sum, count) = Sum(node); // for a given node, get sum and count in order to get average
            if (sum/count == node.val) {
                res++;
            }
            Solve(node.left);
            Solve(node.right);
        }


        // get the sum and count on a given node
        (int sum, int count) Sum(TreeNode node) {
            if (node == null) return (0,0);
            var left = Sum(node.left);
            var right = Sum(node.right);
            return (node.val + left.sum + right.sum, 1 + left.count + right.count);
        }
    }

    // O(N)
    public int AverageOfSubtreeOptimized(TreeNode root) {
        var res = 0;
        Solve(root);
        return res;

        // get the sum and count on a given node
        (int sum, int count) Solve(TreeNode node) {
            Console.WriteLine($"solving for node {(node == null ? "null" : node.val.ToString())}");
            if (node == null) return (0,0);
            var left = Solve(node.left);
            var right = Solve(node.right);
            var sum = node.val + left.sum + right.sum;
            var count = 1 + left.count + right.count;
            if (sum/count == node.val) res++;
            return (sum, count);
        }
    }

    public void Run() {
        int?[] treee = [4,8,5,0,1,null,6];
        var res = AverageOfSubtreeOptimized(Lib.ToBinaryTree(treee));
        res.Print();
    }
}
