﻿using System;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Models;

namespace LeetCode;

/*
https://leetcode.com/problems/balance-a-binary-search-tree/description/

Given the root of a binary search tree, return a balanced binary search tree with the same node values. If there is more than one answer, return any of them.

A binary search tree is balanced if the depth of the two subtrees of every node never differs by more than 1.

*/
public class Q1382_Balance_A_Binary_Search_Tree
{
    public TreeNode BalanceBST(TreeNode root) {
        var list = new List<int>();
        inorder(root);
        

        return Lib.ToBinarySearchTree(list, 0, list.Count - 1);

        // O(n)
        void inorder(TreeNode node) {
            if (node == null) return ;
            inorder(node.left);
            list.Add(node.val);
            inorder(node.right);
        }
    }
}
