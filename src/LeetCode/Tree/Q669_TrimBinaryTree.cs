using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    public class Q669_TrimBinaryTree : IProblem
    {
        /*
            Given a binary search tree and the lowest and highest boundaries as L and R, trim the tree so that all its elements lies in [L, R] (R >= L). You might need to change the root of the tree, so the result should return the new root of the trimmed binary search tree.
        */
        public TreeNode TrimBST(TreeNode root, int low, int high) {
            if (root == null) return null;
            if (root.val < low) return TrimBST(root.right, low, high);
            if (root.val > high) return TrimBST(root.left, low, high);
            root.left = TrimBST(root.left, low, high);
            root.right = TrimBST(root.right, low, high);
            return root;
        }

        public void Run() => throw new System.NotImplementedException();
    }
}