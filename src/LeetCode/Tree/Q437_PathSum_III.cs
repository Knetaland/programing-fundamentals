using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    /*
    Given the root of a binary tree and an integer targetSum, return the number of paths where the sum of the values along the path equals targetSum.

    The path does not need to start or end at the root or a leaf, but it must go downwards (i.e., traveling only from parent nodes to child nodes).

    

    Example 1:


    Input: root = [10,5,-3,3,2,null,11,3,-2,null,1], targetSum = 8
    Output: 3
    Explanation: The paths that sum to 8 are shown.
    Example 2:

    Input: root = [5,4,8,11,null,13,4,7,2,null,null,5,1], targetSum = 22
    Output: 3
    

    Constraints:

    The number of nodes in the tree is in the range [0, 1000].
    -109 <= Node.val <= 109
    -1000 <= targetSum <= 1000
    */
    public class Q437_PathSum_III : IProblem
    {

        // O(n^2) - worst case: tree with 1 side, best case: O(nlogn): balance tree: n * h
        // https://zxi.mytechroad.com/blog/tree/leetcode-437-path-sum-iii/
        public int PathSum(TreeNode root, int sum)
        {
            if (root == null)
            {
                return 0;
            }

            return NumOfPaths(root, sum) + PathSum(root.left, sum) + PathSum(root.right, sum);

            int NumOfPaths(TreeNode node, int remainder)
            {
                if (node == null)
                {
                    return 0;
                }

                remainder -= node.val;
                return remainder == 0 ? 1 : 0 + NumOfPaths(node.left, remainder) + NumOfPaths(node.right, remainder);
            }
        }

        // https://www.youtube.com/watch?v=uZzvivFkgtM
        // O(n)
        public int PathSum_PrefixSum(TreeNode root, int sum) {
            if (root == null) return 0;
            var prefixSumMap = new Dictionary<int, int>();
            prefixSumMap.Add(0, 1); // default to have 1 path for 0, e.g. root == target
            return GetNumPath(root, 0, sum, prefixSumMap);
        }

        private int GetNumPath(TreeNode root, int runningSum, int target, Dictionary<int, int> prefixSumMap) 
        {
            if (root == null) return 0;
            runningSum += root.val;
            var count = prefixSumMap.GetValueOrDefault( runningSum - target, 0);
            if (!prefixSumMap.ContainsKey(runningSum)) {
                prefixSumMap.Add(runningSum, 0);
            }
            prefixSumMap[runningSum]++;
            count += GetNumPath(root.left, runningSum, target, prefixSumMap) + GetNumPath(root.right, runningSum, target, prefixSumMap);
            prefixSumMap[runningSum]--;
            return count;
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
