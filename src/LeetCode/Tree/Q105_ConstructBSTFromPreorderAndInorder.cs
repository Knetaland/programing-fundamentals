using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    public class Q105_ConstructBSTFromPreorderAndInorder : IProblem
    {
        public TreeNode BuildTree(int[] preorder, int[] inorder)
        {
            if (preorder == null || inorder == null || preorder.Length == 0 || inorder.Length == 0)
            {
                return null;
            }

            return BuildTree(preorder, inorder, 0, 0, inorder.Length - 1);
        }
        // https://www.youtube.com/watch?v=S1wNG5hx-30
        public TreeNode BuildTree(int[] preorder, int[] inorder, int pStart, int inStart, int inEnd)
        {
            if (pStart > preorder.Length || inStart > inEnd)
            {
                return null;
            }

            var cur = new TreeNode(preorder[pStart]);
            var i = inStart;
            // find the root at i in inorder, anything on the left will be on its left subtree
            while (i <= inEnd)
            {
                if (preorder[pStart] == inorder[i])
                {
                    break;
                }

                i++;
            }
            var leftSize = i - inStart;
            cur.left = BuildTree(preorder, inorder, pStart + 1, inStart, i - 1);
            cur.right = BuildTree(preorder, inorder, pStart + 1 + leftSize, i + 1, inEnd);
            return cur;
        }

        public TreeNode BuildTreeImprove(int[] preorder, int[] inorder) {

            var inOrderPositionMap = new Dictionary<int, int>();
            for(var i = 0; i < inorder.Length; i++) {
                inOrderPositionMap.Add(inorder[i], i);
            }

            var root =  buildTree(0, 0, inorder.Length - 1);
            return root;

            // preorder has root as first element, find root in inorder array, to the left is left subtree, to the right is the right subtree.
            TreeNode buildTree(int prestart, int inStart, int inEnd) {
                if (prestart >= preorder.Length || inStart > inEnd) return null;
                var root = new TreeNode(preorder[prestart]);
                var rootIndexInInorder = inOrderPositionMap[root.val];
                var leftSize = rootIndexInInorder - inStart;
                root.left = buildTree(prestart+1, inStart,  rootIndexInInorder -1);
                root.right = buildTree(prestart+1+leftSize, rootIndexInInorder +1, inEnd);
                return root;
            }
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
