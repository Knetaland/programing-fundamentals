using System.Collections.Generic;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    public class Q112_PathSum
    {
        /*
        Time complexity: O(n)

        Space complexity: O(n)
        */
        public bool HasPathSum(TreeNode root, int sum)
        {
            if (root == null)
            {
                return false;
            }

            if (root.left == null && root.right == null && sum == root.val)
            {
                return true;
            }

            return HasPathSum(root.left, sum - root.val) || HasPathSum(root.right, sum - root.val);
        }

        public bool HasPathSum_Iterative(TreeNode root, int targetSum) {
            if (root == null) return false;
            var stack = new Stack<TreeNode>();
            stack.Push(root);
            while(stack.Count > 0) {
                var node = stack.Pop();
                if (node.left == null && node.right == null && node.val == targetSum) {
                    return true;
                }
                if (node.left != null) {
                    node.left.val += node.val;
                    stack.Push(node.left);
                }
                if (node.right != null) {
                    node.right.val += node.val;
                    stack.Push(node.right);
                }
            }
            return false;
        }
    }
}
