using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    public class Q814_Binary_Tree_Pruning
    {
        public TreeNode PruneTree(TreeNode root) {
                if (root == null) return null;
                root.left = PruneTree(root.left);
                root.right = PruneTree(root.right);
                // post order because we need to trim from bottom up
                if (root.left == null && root.right == null && root.val != 1) return null;
                return root;
        }
    }
}