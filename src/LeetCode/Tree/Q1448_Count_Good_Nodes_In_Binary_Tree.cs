﻿using System;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode;

/*
https://leetcode.com/problems/count-good-nodes-in-binary-tree/description/
Given a binary tree root, a node X in the tree is named good if in the path from root to X there are no nodes with a value greater than X.

Return the number of good nodes in the binary tree.


*/
public class Q1448_Count_Good_Nodes_In_Binary_Tree : IProblem
{
    public int GoodNodes(TreeNode root) {
        if (root == null) return 0;
        var res =0;
        backtracking(root, int.MinValue);
        return res;
        void backtracking(TreeNode node, int max) {
            if (node == null) return ;
            if (node.val >= max) { 
                Console.WriteLine($"node {node.val} > max {max}");
                res++;
                max = node.val;
            }
            
            Console.WriteLine($"checking left of node {node.val}");
            backtracking(node.left, max);
            Console.WriteLine($"checking right of node {node.val}");
            backtracking(node.right, max);
        }
    }
    public void Run() {
        int?[] treee = [2,null,4,null,null,10,8,null,null,4];
        /*
          2
            \
            4
           /  \
          10   8
              /
             4
        */
        var root = Lib.ToBinaryTree(treee);
        var res = GoodNodes(root);
        res.Print();
    }
}