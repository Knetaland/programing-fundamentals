using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    /*
    Given the root node of a binary search tree, return the sum of values of all nodes with value between L and R (inclusive).

    The binary search tree is guaranteed to have unique values.

    

    Example 1:

    Input: root = [10,5,15,3,7,null,18], L = 7, R = 15
    Output: 32
    Example 2:

    Input: root = [10,5,15,3,7,13,18,1,null,6], L = 6, R = 10
    Output: 23
    

    Note:

    The number of nodes in the tree is at most 10000.
    The final answer is guaranteed to be less than 2^31.
    */
    public class Q938_RangeSumOfBST
    {
        public int RangeSumBST(TreeNode root, int L, int R)
        {
            if (root == null)
            {
                return 0;
            }

            var res = 0;
            // solution 1, same approach as code in place
            // dfs(root)
            // solution 2, inorder
            // inorder(root);

            if (L <= root.val && root.val <= R)
            {
                res += root.val;
            }

            if (L < root.val)
            {
                res += RangeSumBST(root.left, L, R); // can still go left
            }

            if (root.val < R)
            {
                res += RangeSumBST(root.right, L, R); // can still go right
            }

            return res;

            // O(logN)
            void dfs(TreeNode node) {
                if (node == null) return ;
                if (L <= node.val && node.val <= node.val) {
                    res += node.val;
                }
                if (L < node.val) dfs(node.left); // keep searching left, as node is still > L
                if (node.val < R) dfs(node.right); // keep searching right, as node is still < R
            }

            // in order traversal, all the nodes in btw will get added to res;
            // O(N)
            void inorder(TreeNode node) {
                if (node == null) return ;
                inorder(node.left);
                if (L <= node.val && node.val <= node.val) res += node.val;
                inorder(node.right);
            }
        }
    }
}
