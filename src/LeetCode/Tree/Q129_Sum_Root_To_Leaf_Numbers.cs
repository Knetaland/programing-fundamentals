using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    public class Q129_Sum_Root_To_Leaf_Numbers : IProblem
    {
        /*
        Given a binary tree containing digits from 0-9 only, each root-to-leaf path could represent a number.

        An example is the root-to-leaf path 1->2->3 which represents the number 123.

        Find the total sum of all root-to-leaf numbers.

        Note: A leaf is a node with no children.

        Example:

        1->2
        12
        1->3
        13
        25
        Example 2:

        4->9->5
        4->9->1
        4->0
        1026
        */
        public int SumNumbers(TreeNode root) {
            if (root == null) return 0;
            var paths = new List<string>();
            Backtrack(root, "", paths);
            var num = 0;
            foreach(var path in paths) {
                var val = int.Parse(path);
                num += val;
            }
            return num;
        }

        private void Backtrack(TreeNode root, string curPath, List<string> paths) {
            if (root ==  null) return ;
            curPath += root.val;
            if (root.left == null && root.right == null) {
                paths.Add(curPath);
            }
            Backtrack(root.left, curPath, paths);
            Backtrack(root.right, curPath, paths);
            curPath = curPath.Substring(0, curPath.Length-1);
        }



        public int SumNumbers_Ver2(TreeNode root) {
            return Helper(root, 0, 0 );

            int Helper(TreeNode node, int currentSum, int negCount) {
                if (node == null) return 0;
                if (node.val < 0) negCount++;
                currentSum = currentSum * 10 + Math.Abs(node.val);
                if (node.left == null && node.right == null) {
                    var res = negCount % 2 == 0 ? currentSum : -currentSum;
                    Console.WriteLine($"leaf value {res}");
                    return res;
                } 
                var left = Helper(node.left, currentSum, negCount);
                var right = Helper(node.right, currentSum, negCount);
                return left + right;
            }
        }

        // solution 3
        public int Sum(TreeNode root) {
            var res = new List<int>();
            dfs(root, 0);
            return res.Sum();

            void dfs(TreeNode cur, int total) {
                if (cur == null) return ;
                total = total * 10 + cur.val;
                if (cur.left == null && cur.right == null) {
                    res.Add(total);
                    return ;
                }
                dfs(cur.left, total);
                dfs(cur.right, total);
            }
        }

        public void Run() {
            int?[] nums = [-4,9,0,5,-1];
            var res = SumNumbers_Ver2(Lib.ToBinaryTree(nums));
            res.Print();
        }

    }
}