using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    public class Q965_UnivaluedBinaryTree
    {
        /*
        A binary tree is uni-valued if every node in the tree has the same value.

        Given the root of a binary tree, return true if the given tree is uni-valued, or false otherwise.
        */
        public bool IsUnivalTree(TreeNode root)
        {
            if (root == null)
                return true;
            var l = root.left == null || (root.val == root.left.val && IsUnivalTree(root.left));
            var r = root.right == null || (root.val == root.right.val && IsUnivalTree(root.right));
            return l && r;
        }
    }
}
