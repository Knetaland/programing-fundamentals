﻿using System;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode;

/*
https://leetcode.ca/2020-07-02-1676-Lowest-Common-Ancestor-of-a-Binary-Tree-IV
Given the root of a binary tree and an array of TreeNode objects nodes,
return the lowest common ancestor (LCA) of all the nodes in nodes. All the nodes will exist in the tree, and all values of the tree's nodes are unique.

Extending the definition of LCA on Wikipedia: "The lowest common ancestor of n nodes p1, p2, ..., pn in a binary tree T is the lowest node that has every pi as a descendant (where we allow a node to be a descendant of itself) for every valid i". A descendant of a node x is a node y that is on the path from node x to some leaf node.


Input: root = [3,5,1,6,2,0,8,null,null,7,4], nodes = [4,7]
Output: 2
Explanation: The lowest common ancestor of nodes 4 and 7 is node 2.
*/
public class Q1676_LCA_Of_Binary_Tree_IV : IProblem
{
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode[] nodes)
    {
        var set = new HashSet<TreeNode>(nodes);
        return lca(root);

        TreeNode lca(TreeNode node)
        {
            if (node == null)
                return null;
            if (set.Contains(node))
                return node;
            var left = lca(node.left);
            var right = lca(node.right);
            if (left != null && right != null)
                return node;
            return left ?? right;
        }
    }

    public void Run()
    {
        int?[] tree = [3, 5, 1, 6, 2, 0, 8, null, null, 7, 4];
        var nodes = new List<int[]> {
            new int[] { 4, 7 },
            new int[] { 1 },
            new int[] {7,6,2,4},
        };
        foreach (var list in nodes)
        {
            var root = Lib.ToBinaryTree(tree);
            var targetNodes = new List<TreeNode>();
            foreach (var n in list)
            {
                targetNodes.Add(Lib.Find(root, n));
            }
            var lca = lowestCommonAncestor(root, targetNodes.ToArray());
            Console.WriteLine($"lca is {lca.val}");
        }
    }
}
