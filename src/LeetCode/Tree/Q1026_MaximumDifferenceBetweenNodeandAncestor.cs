using System;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    /*
    Given the root of a binary tree, find the maximum value V for which there exists different nodes A and B where V = |A.val - B.val| and A is an ancestor of B.

    (A node A is an ancestor of B if either: any child of A is equal to B, or any child of A is an ancestor of B.)

    Example 1:
    Input: [8,3,10,1,6,null,14,null,null,4,7,13]
    Output: 7
    Explanation: 
    We have various ancestor-node differences, some of which are given below :
    |8 - 3| = 5
    |3 - 7| = 4
    |8 - 1| = 7
    |10 - 13| = 3
    Among all possible differences, the maximum value of 7 is obtained by |8 - 1| = 7.
    Note:
    The number of nodes in the tree is between 2 and 5000.
    Each node will have value between 0 and 100000.
    */
    public class Q1026_MaximumDifferenceBetweenNodeandAncestor : IProblem
    {
        private int maxDiff = 0;
        public int MaxAncestorDiff(TreeNode root)
        {
            if (root == null)
            {
                return 0;
            }

            Dfs(root, root.val, root.val);
            return maxDiff;
        }
        // max diff happens between max node min node, when traverse through the tree,
        // pass along current min and current max and diff with current node to get the max diff
        private void Dfs(TreeNode root, int min, int max)
        {
            if (root == null)
            {
                return;
            }

            var diff1 = Math.Abs(root.val - min);
            var diff2 = Math.Abs(root.val - max);
            maxDiff = Math.Max(maxDiff, Math.Max(diff1, diff2));
            Dfs(root.left, Math.Min(min, root.val), Math.Max(max, root.val));
            Dfs(root.right, Math.Min(min, root.val), Math.Max(max, root.val));
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
