using System;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    public class Q110_BalancedTree : IProblem
    {
        // O(n)
        public bool IsBalancedBetter(TreeNode root)
        {
            if (GetHeight(root) == -1)
            {
                return false;
            }

            return true;

            int GetHeight(TreeNode n)
            {
                if (n == null) return 0;

                var left = GetHeight(n.left);
                if (left == -1) return -1;

                var right = GetHeight(n.right);
                if (right == -1) return -1;

                if (Math.Abs(left - right) > 1) return -1;

                return 1 + Math.Max(left, right);
            }
        }

        // O(nlogn)
        public bool IsBalancedSlow(TreeNode root)
        {
            if (root == null)
            {
                return true;
            }

            var left = GetHeight(root.left);
            var right = GetHeight(root.right);
            return Math.Abs(left - right) <= 1 && IsBalancedSlow(root.left) && IsBalancedSlow(root.right);

            int GetHeight(TreeNode n)
            {
                if (n == null)
                {
                    return 0;
                }

                var l = 1 + GetHeight(n.left);
                var r = 1 + GetHeight(n.right);
                return Math.Max(l, r);
            }
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
