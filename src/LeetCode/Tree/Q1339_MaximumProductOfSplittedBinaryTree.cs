using System;
using System.Collections.Generic;
using System.Text;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    public class Q1339_MaximumProductOfSplittedBinaryTree : IProblem
    {
        /*
         Given a binary tree root. Split the binary tree into two subtrees by removing 1 edge such that the product of the sums of the subtrees are maximized.

Since the answer may be too large, return it modulo 10^9 + 7.
             
             */
        public int MaxProduct(TreeNode root)
        {
            var mod = 1e9 + 7;
            var total = Sum(root);
            var ans = long.MinValue;
            Solve(root);
            return (int)(ans % mod);

            long Solve(TreeNode node)
            {
                if (node == null)
                {
                    return 0;
                }

                var l = Solve(node.left);
                var r = Solve(node.right);

                ans = Math.Max(ans, Math.Max((total - l) * l, (total - r) * r));
                return node.val + l + r;
            }
        }

        private long Sum(TreeNode root)
        {
            if (root == null)
            {
                return 0;
            }
            return root.val + Sum(root.left) + Sum(root.right);
        }

        public void Run()
        {

        }
    }
}
