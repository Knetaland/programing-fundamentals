using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    public class Q617_MergeTwoBinaryTrees : IProblem
    {
        public TreeNode MergeTrees(TreeNode t1, TreeNode t2)
        {
            if (t1 == null)
            {
                return t2;
            }

            if (t2 == null)
            {
                return t1;
            }

            var root = new TreeNode(t1.val + t2.val)
            {
                left = MergeTrees(t1.left, t2.left),
                right = MergeTrees(t1.right, t2.right)
            };
            return root;
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
