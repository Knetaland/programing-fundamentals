using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{

    // https://www.youtube.com/watch?v=YZ7Lm8AKwPs
    public class Q426_BSTToSortedDoublyLinkedList : IProblem
    {
        public TreeNode ConvertRecursive(TreeNode root)
        {
            if (root == null) return null;
            var dummy = new TreeNode(-1);
            var prev = dummy;
            // connect nodes
            inorder(root);
            // connect head and tail, prev now should be stopped on tail now
            var head = dummy.right;
            prev.right = head;
            head.left = prev;
            return head;

            void inorder(TreeNode cur)
            {
                if (cur == null) return;

                inorder(cur.left);
                prev.right = cur;
                cur.left = prev;
                prev = cur;
                inorder(cur.right);
            }
        }

        public TreeNode ConvertIterative(TreeNode root)
        {
            if (root == null)
            {
                return null;
            }

            var stack = new Stack<TreeNode>();
            var cur = root;
            var dummy = new TreeNode(-1);
            var prev = dummy;
            while (stack.Count > 0 || cur != null)
            {
                while (cur != null)
                {
                    stack.Push(cur);
                    cur = cur.left;
                }
                var node = stack.Pop();
                prev.right = node;
                node.left = prev;
                prev = node;
                cur = node.right;
            }

            var head = dummy.right;
            prev.right = head;
            head.left = prev;

            return head;
        }

        public void Run()
        {
            var tree = new TreeNode(4)
            {
                left = new TreeNode(2)
                {
                    left = new TreeNode(1),
                    right = new TreeNode(3),
                },
                right = new TreeNode(5)
            };

            var node = ConvertRecursive(tree);
            // var node = ConvertIterative(tree);
            var n = 45;
            while (node.right != null && n >= 0)
            {
                Console.WriteLine(node.val);
                node = node.right;
                n--;
            }
        }
    }
}
