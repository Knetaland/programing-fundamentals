using System;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    /*
    Given a binary tree, find the length of the longest path where each node in the path has the same value. This path may or may not pass through the root.

    The length of path between two nodes is represented by the number of edges between them.



    Example 1:

    Input:

                5
                / \
                4   5
            / \   \
            1   1   5
    Output: 2



    Example 2:

    Input:

                1
                / \
                4   5
            / \   \
            4   4   5
    */

    public class Q687_LongestUnivaluePath : IProblem
    {
        public void Run()
        {
            throw new System.NotImplementedException();
        }

        public int LongestUnivaluePath(TreeNode root)
        {
            var longest = 0;
            Helper(root);
            return longest;

            int Helper(TreeNode n)
            {
                if (n == null)
                {
                    return 0;
                }

                var left = Helper(n.left);
                var right = Helper(n.right);
                left = n.left != null && n.left.val == n.val ? left + 1 : 0;
                right = n.right != null && n.right.val == n.val ? right + 1 : 0;
                longest = Math.Max(longest, left + right); // path is the edge connecting nodes
                return Math.Max(left, right);
            }
        }
    }
}
