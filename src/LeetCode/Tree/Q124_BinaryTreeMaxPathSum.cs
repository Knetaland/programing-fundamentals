using System;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    /*
    Given a non-empty binary tree, find the maximum path sum.

    For this problem, a path is defined as any sequence of nodes from some
    starting node to any node in the tree along the parent-child connections.
    The path must contain at least one node and does not need to go through the root.

    Example 1:

    Input: [1,2,3]

        1
        / \
        2   3

    Output: 6
    Example 2:

    Input: [-10,9,20,null,null,15,7]

    -10
    / \
    9  20
        /  \
    15   7

    Output: 42
    */

    // related prob: Q543_DiameterOfBinaryTree
    // O(n) with space O(h)
    public class Q124_BinaryTreeMaxPathSum
    {
        public int MaxPathSum(TreeNode root)
        { 
            var max = int.MinValue;
            Dfs(root);
            return max;

            // max single path (either left or right) sum for root
            int Dfs(TreeNode n)
            {
                if (n == null) return 0;
                var left = Math.Max(0, Dfs(n.left)); // if < 0, dont take. 
                var right = Math.Max(0, Dfs(n.right));
                max = Math.Max(max, n.val + left + right);
                return n.val + Math.Max(left, right);
            }
        }
    }
}
