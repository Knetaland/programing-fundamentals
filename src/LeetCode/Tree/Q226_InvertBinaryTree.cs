using System.Collections;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    public class Q226_InvertBinaryTree : IProblem
    {
        public TreeNode InvertTree(TreeNode root)
        {
            if (root == null)
            {
                return null;
            }

            var left = root.left;
            root.left = InvertTree(root.right);
            root.right = InvertTree(left);
            return root;
        }

        public TreeNode InvertTreeIterative(TreeNode root) {
                if (root == null) return null;
                var queue = new Queue<TreeNode>();
                queue.Enqueue(root);
                while(queue.Count > 0) {
                    var node =queue.Dequeue();
                    // swap
                    (node.left, node.right) = (node.right, node.left);
                    if (node.left != null) queue.Enqueue(node.left);
                    if (node.right != null) queue.Enqueue(node.right);
                }

                return root;
        }
        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
