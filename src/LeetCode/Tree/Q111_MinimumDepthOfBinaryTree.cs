using System;
using System.Collections.Generic;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    public class Q111_MinimumDepthOfBinaryTree
    {
        /*
        Given a binary tree, find its minimum depth.

        The minimum depth is the number of nodes along the shortest path from the root node down to the nearest leaf node.

        Note: A leaf is a node with no children.

        

        Example 1:


        Input: root = [3,9,20,null,null,15,7]
        Output: 2
        Example 2:

        Input: root = [2,null,3,null,4,null,5,null,6]
        Output: 5
        

        Constraints:

        The number of nodes in the tree is in the range [0, 105].
        -1000 <= Node.val <= 1000
        */

        public int MinDepth(TreeNode root) {
            if (root == null) return 0;
            var l = MinDepth(root.left);
            var r = MinDepth(root.right);
            return (l == 0 || r == 0) ? l + r + 1 : 1 + Math.Min(l, r);
        }

        // BFS, short-circuit when found leaf
        public int MinDepth_Optimized(TreeNode root) {
            if (root == null) return 0;

            var queue = new Queue<TreeNode>();
            queue.Enqueue(root);
            var level = 1;
            while(queue.Count > 0) {
                var count = queue.Count;
                while(count > 0) {
                    var node = queue.Dequeue();
                    if (node.left == null && node.right == null) return level;
                    if (node.left != null) queue.Enqueue(node.left);
                    if (node.right != null) queue.Enqueue(node.right);
                    count--;
                }
                level++;
            }
            return level;
        }
    }
}
