﻿using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode;

/*
https://leetcode.ca/all/428.html

Serialization is the process of converting a data structure or object into a sequence of bits so that it can be stored in a file or memory buffer, or transmitted across a network connection link to be reconstructed later in the same or another computer environment.

Design an algorithm to serialize and deserialize an N-ary tree. An N-ary tree is a rooted tree in which each node has no more than N children. There is no restriction on how your serialization/deserialization algorithm should work. You just need to ensure that an N-ary tree can be serialized to a string and this string can be deserialized to the original tree structure.

For example, you may serialize the following 3-ary tree
*/
public class Q428_Serialize_And_Deserialize_N_ary_Tree : IProblem
{
    public string Serialize(NaryTreeNode root) {
        var res = new List<string>();
        
        preOrder(root);

        void preOrder(NaryTreeNode node) {
            if (node == null) return ;
            var childrenSize = node.children.Count;
            res.Add($"{node.val}");
            res.Add($"{childrenSize}");

            foreach(var child in node.children) {
                preOrder(child);
            }
        }
        return string.Join(',', res);
    }

    public NaryTreeNode Deserialize(string input) {
        var queue = new Queue<string>(input.Split(','));
        var root =  dfs(queue);
        return root;

        NaryTreeNode dfs(Queue<string> q) {
            if (q.Count == 0) return null;
            var val = int.Parse(queue.Dequeue()); // node val
            var size = int.Parse(queue.Dequeue()); // size
            var node = new NaryTreeNode(val);
            for(var i = 0; i < size; i++) {
                node.children.Add(dfs(queue));
            }
            return node;
        }
    }

    public void Run() {
        var root = new NaryTreeNode(1) {
            children =  {
                new NaryTreeNode(3) {
                    children = {
                        new NaryTreeNode(5),
                        new NaryTreeNode(6)
                    }
                },
                new NaryTreeNode(2),
                new NaryTreeNode(4) {
                    children = {
                        new NaryTreeNode(7)
                    }
                },
            }
        };

        var serilizedString = Serialize(root);

        var deserializedTree = Deserialize(serilizedString);
        var dResSer = Serialize(deserializedTree);

        Console.WriteLine(serilizedString == dResSer);


    }
}
