﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Models;

namespace LeetCode;

/*
https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/description/

Given the root of a binary tree, return the zigzag level order traversal of its nodes' values. (i.e., from left to right, then right to left for the next level and alternate between).

Example 1:

Input: root = [3,9,20,null,null,15,7]
Output: [[3],[20,9],[15,7]]

Example 2:

Input: root = [1]
Output: [[1]]

Example 3:

Input: root = []
Output: []
 


*/
public class Q103_Binary_Tree_Zigzag_Level_Order_Traversal
{
    public IList<IList<int>> ZigzagLevelOrder(TreeNode root) {
        var res = new List<IList<int>>();

        if (root == null) return res;

        var queue = new Queue<TreeNode>();
        queue.Enqueue(root);
        var curLevel = 0;
        while( queue.Count > 0) {
            var level = new LinkedList<int>(); // linked list so O(1) to append and prepend
            var count = queue.Count;
            var goingRight = curLevel % 2 == 0;
            while(count >0) {
                var node = queue.Dequeue();
                if (goingRight) {
                    level.AddLast(node.val);
                } else {
                    level.AddFirst(node.val);
                }
                if (node.left != null) queue.Enqueue(node.left);
                if (node.right != null) queue.Enqueue(node.right);
                count--;
            }
            res.Add(level.ToList());
            curLevel++;
        }
        return res;
    }
}
