using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    public class TreeNodeWithCount : TreeNode
    {
        public TreeNodeWithCount(int x) : base(x)
        {
        }
        public int Count { get; set; }

        public new TreeNodeWithCount left { get; set; }
        public new TreeNodeWithCount right { get; set; }
    }

    public class BinaryTreeWithSize {
        TreeNodeWithCount root;
        public int size(TreeNodeWithCount node) {
            if (node == null) return 0;
            return node.Count;
        }

        public void put(TreeNodeWithCount node) {
           root = put(root, node);
        }

        public TreeNodeWithCount put(TreeNodeWithCount x, TreeNodeWithCount node) {
            if (x == null) {
                node.Count = 1;
                return node;
            }

            if (node.val < x.val) {
                x.left = put(x.left, node);
            } else if (node.val > x.val) {
                x.right = put(x.right, node);
            }
            x.Count = 1 + size(x.left) + size(x.right);
            return x;
        }

        public int find(int k) {
            return find(root, k);
        }

        public int find(TreeNodeWithCount x, int k) {
            if (x == null) throw new Exception("some error");
            var leftSize = 0;
            if (x.left != null) {
                leftSize = x.left.Count;
            }

            if (k <= leftSize) {
                return find(x.left, k);
            } else if (k > leftSize + 1) {
                return find(x.right, k - leftSize - 1);
            }
            return x.val;
        }

        public void build(TreeNode node) {
            if (node == null) return;
            put(new TreeNodeWithCount(node.val));
            build(node.left);
            build(node.right);
        }
    }

    public class Q230_FindKthSmallest : IProblem
    {
        public int KthSmallest(TreeNode root, int k)
        {
            // var node = Build(root);

            // var s = new Q102_TreeLevelTraversal();
            // var res = s.LevelOrder(node);
            // var sb = new StringBuilder();
            // for (var i = 0; i < res.Count; i++)
            // {
            //     sb.AppendLine(string.Join(",", res[i]));
            // }
            // Console.WriteLine($"level order is \n {sb}");

            
            // var res = Find(node, k - 1);
            // return res;

            var tree = new BinaryTreeWithSize();
            tree.build(root);
            var res = tree.find(k);
            return res;
        }

        // O(N), space O(N)
        private int GetWithStack(TreeNode root, int k)
        {
            var stack = new Stack<TreeNode>();

            while (root != null || stack.Count > 0)
            {
                while (root != null)
                {
                    stack.Push(root);
                    root = root.left;
                }
                root = stack.Pop();
                k--;
                if (k == 0)
                    break;
                root = root.right;
            }
            return root.val;
        }

        private int inOrderTraversal(TreeNode root, int k)
        {
            if (root == null)
                return 0;
            var res = new List<int>();
            inOrder(root);
            return k - 1 < res.Count ? res[k - 1] : res[res.Count - 1];

            void inOrder(TreeNode node)
            {
                if (node == null)
                    return;
                inOrder(node.left);
                res.Add(node.val);
                inOrder(node.right);
            }
        }

        public void Run()
        {
            var root = new TreeNode(8)
            {
                left = new TreeNode(4)
                {
                    right = new TreeNode(6)
                    {
                        left = new TreeNode(5),
                        right = new TreeNode(7)
                    },
                    left = new TreeNode(2)
                    {
                        left = new TreeNode(1),
                        right = new TreeNode(3)
                    }
                },
                right = new TreeNode(12)
            };
            var res = KthSmallest(root, 9);
            Console.WriteLine($"res is {res}");
        }

        private int Find(TreeNodeWithCount root, int k)
        {
            if (root == null)
                return -1;
            if (k == root.Count)
                return root.val;
            if (k > root.Count)
            {
                return Find(root.right, k - 1 - root.Count);
            }
            else
            {
                return Find(root.left, k);
            }
        }

        Dictionary<TreeNode, int> map = new Dictionary<TreeNode, int>();

        private TreeNodeWithCount Build(TreeNode root)
        {
            if (root == null)
                return null;
            var node = new TreeNodeWithCount(root.val);

            var count = GetCounts(root.left);

            node.Count = count;
            node.left = Build(root.left);
            node.right = Build(root.right);
            return node;
        }

        private int GetCounts(TreeNode root)
        {
            if (root == null)
                return 0;
            if (map.ContainsKey(root)) {
                Console.WriteLine($"found existing node {root.val} with count {map[root]}");
                return map[root];    
            }
                
            Console.WriteLine($"getting count for node {root.val}");

            var left = GetCounts(root.left);
            var right = GetCounts(root.right);
            var count = left + right + 1;
            map.Add(root, count);
            Console.WriteLine($"setting count on node {root.val} with count {count} ");
            return count;
        }
    }
}