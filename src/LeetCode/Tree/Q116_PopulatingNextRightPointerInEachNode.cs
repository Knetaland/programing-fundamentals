namespace LeetCode.Tree
{
    /*
    https://leetcode.com/problems/populating-next-right-pointers-in-each-node/
    */
    public class Q116_PopulatingNextRightPointerInEachNode
    {
        public DoublyLinkedListTreeNode Connect(DoublyLinkedListTreeNode root)
        {
            if (root == null || root.left == null)
            {
                return root;
            }

            root.left.next = root.right;
            if (root.next != null)
            {
                root.right.next = root.next.left;
            }
            Connect(root.left);
            Connect(root.right);
            return root;
        }
    }

    public class DoublyLinkedListTreeNode
    {
        public int val;
        public DoublyLinkedListTreeNode left;
        public DoublyLinkedListTreeNode right;
        public DoublyLinkedListTreeNode next;

        public DoublyLinkedListTreeNode() { }

        public DoublyLinkedListTreeNode(int _val)
        {
            val = _val;
        }

        public DoublyLinkedListTreeNode(int _val, DoublyLinkedListTreeNode _left, DoublyLinkedListTreeNode _right, DoublyLinkedListTreeNode _next)
        {
            val = _val;
            left = _left;
            right = _right;
            next = _next;
        }
    }
}
