using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    /*
    
    Given a binary search tree and a node in it, find the in-order successor of that node in the BST.

    The successor of a node p is the node with the smallest key greater than p.val.

*/
    public class Q285_InOrderSuccessorInBST : IProblem
    {
        public void Run()
        {
            var tree = new TreeNode(4)
            {
                left = new TreeNode(2)
                {
                    left = new TreeNode(1),
                    right = new TreeNode(3),
                },
                right = new TreeNode(5)
            };

            var p = tree.left;

            var res = InorderSuccessor(tree, p);
            var res2 = InorderSuccessorBS(tree, p);
            if (res == null)
            {
                Console.WriteLine("next node is null");
                Console.WriteLine("next node 2 is null");
            }
            else
            {
                Console.WriteLine("next node is " + res.val);
                Console.WriteLine("next node 2 is " + res2.val);
            }
        }

        TreeNode InorderSuccessorBS(TreeNode root, TreeNode p)
        {
            if (p == null || root == null)
            {
                return null;
            }

            TreeNode res = null;
            var cur = root;
            while (cur != null)
            {
                if (cur.val > p.val)
                {
                    res = cur;
                    cur = cur.left;
                }
                else
                {
                    cur = cur.right;
                }
            }
            return res;
        }

        TreeNode InorderSuccessor(TreeNode root, TreeNode p)
        {
            if (p == null || root == null)
            {
                return null;
            }

            var visited = false;

            var stack = new Stack<TreeNode>();
            var cur = root;
            while (stack.Count > 0 || cur != null)
            {
                while (cur != null)
                {
                    stack.Push(cur);
                    cur = cur.left;
                }
                var node = stack.Pop();
                if (visited)
                {
                    return node;
                }

                if (node == p)
                {
                    visited = true;
                }
                cur = node.right;
            }
            return null;
        }
    }
}
