using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    public class Q637_AverageOfLevelsInBinaryTree : IProblem
    {
        public IList<double> AverageOfLevels(TreeNode root)
        {
            var res = new List<double>();
            if (root == null)
            {
                return res;
            }

            var queue = new Queue<TreeNode>();
            queue.Enqueue(root);
            var sum = 0;
            while (queue.Count > 0)
            {
                var count = queue.Count;
                for (var i = 0; i < count; i++)
                {
                    var node = queue.Dequeue();
                    sum += node.val;
                    if (node.left != null)
                    {
                        queue.Enqueue(node.left);
                    }

                    if (node.right != null)
                    {
                        queue.Enqueue(node.right);
                    }
                }
                res.Add(sum / count);
            }
            return res;
        }
        public void Run()
        {

            throw new System.NotImplementedException();
        }
    }
}
