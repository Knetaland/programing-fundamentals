using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    // tree top view, pick top element on each horizontal distance
    public class Q987_VerticalOrderTraversalOfBinaryTree
    {
        public IList<IList<int>> VerticalTraversalBetter(TreeNode root)
        {
            var res = new List<IList<int>>();

            if (root == null)
                return res;

            var queue = new Queue<(TreeNode, int)>(); // node + col
            var col = 0; // start from col 0
            var map = new Dictionary<int, IList<int>>(); // store the col -> list
            queue.Enqueue((root, col));
            var minCol = 0;
            var maxCol = 0;
            while (queue.Count > 0)
            {
                var node = queue.Dequeue();
                var curCol = node.Item2;
                var curNode = node.Item1;
                if (!map.ContainsKey(curCol))
                {
                    map.Add(curCol, []);
                }
                map[curCol].Add(curNode.val);

                if (curNode.left != null)
                {
                    queue.Enqueue((curNode.left, curCol - 1));
                    minCol = Math.Min(minCol, curCol - 1);
                }
                if (curNode.right != null)
                {
                    queue.Enqueue((curNode.right, curCol + 1));
                    maxCol = Math.Max(maxCol, curCol + 1);
                }
            }

            for (var i = minCol; i <= maxCol; i++)
            {
                res.Add(map[i]);
            }

            return res;
        }
        public IList<IList<int>> VerticalTraversal(TreeNode root)
        {
            var res = new List<IList<int>>();
            if (root == null)
            {
                return res;
            }

            var queue = new Queue<Location>();
            var map = new Dictionary<int, List<Location>>();
            queue.Enqueue(new Location(root, 0, 0));
            int min = 0, max = 0;
            while (queue.Count > 0)
            {
                var n = queue.Dequeue();
                if (!map.ContainsKey(n.x))
                {
                    map.Add(n.x, new List<Location>());
                }
                map[n.x].Add(n);
                min = Math.Min(min, n.x);
                max = Math.Max(max, n.x);
                if (n.node.left != null)
                {
                    queue.Enqueue(new Location(n.node.left, n.x - 1, n.y - 1));
                }
                if (n.node.right != null)
                {
                    queue.Enqueue(new Location(n.node.right, n.x + 1, n.y - 1));
                }
            }

            for (var i = min; i <= max; i++)
            {
                if (map.ContainsKey(i))
                {
                    map[i].Sort((a, b) =>
                    {
                        if (a.y == b.y)
                        {
                            return a.node.val - b.node.val;
                        }
                        return b.y - a.y;
                    });
                    res.Add(map[i].Select(n => n.node.val).ToList());
                }
            }
            return res;
        }


        private class Location
        {
            public TreeNode node { get; set; }
            public int x { get; set; }
            public int y { get; set; }
            public Location(TreeNode n, int xVal, int yVal)
            {
                node = n;
                x = xVal;
                y = yVal;
            }
        }

    }
}
