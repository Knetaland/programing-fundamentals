﻿using System;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode;

/*
https://leetcode.ca/2016-10-28-333-Largest-BST-Subtree/

Given the root of a binary tree, find the largest subtree, which is also a Binary Search Tree (BST),
 where the largest means subtree has the largest number of nodes.

A Binary Search Tree (BST) is a tree in which all the nodes follow the below-mentioned properties:

The left subtree values are less than the value of their parent (root) node's value.
The right subtree values are greater than the value of their parent (root) node's value.
Note: A subtree must include all of its descendants.
*/
public class Q333_Largest_BST_Subtree : IProblem
{
    int max = 0;
    public int largestBSTSubtree(TreeNode root) {
        var max = 0;
        getCount(root.left);
        getCount(root.right);
        return max;
    }
    int getCount(TreeNode root) {
        if (root == null) return 0;
        var left = getCount(root.left);
        var right = getCount(root.right);
        var count =  1 + left + right;
        if (count > max) {
            max = count;
        }
        return count;
    }

    public void Run() {

    }
}
