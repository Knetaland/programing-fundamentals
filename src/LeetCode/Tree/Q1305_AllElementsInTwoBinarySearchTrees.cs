using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    public class Q1305_AllElementsInTwoBinarySearchTrees : IProblem
    {
        /*
        Return a list containing all the integers from both trees sorted in ascending order.

        Example 1:


        Input: root1 = [2,1,4], root2 = [1,0,3]
        Output: [0,1,1,2,3,4]
        */
        public IList<int> GetAllElements1(TreeNode root1, TreeNode root2)
        {
            // 1. inorder traverse both trees, merge 2 sorted arrays
            List<TreeNode> l1 = new List<TreeNode>(), l2 = new List<TreeNode>();
            Inorder(root1, l1);
            Inorder(root2, l2);
            var res = Merge(l1, l2);
            return res;
        }

        public IList<int> GetAllElements2(TreeNode root1, TreeNode root2)
        {
            // 1. find left most node for each tree, which is smaller in BST, store them 
            Stack<TreeNode> s1 = new Stack<TreeNode>(), s2 = new Stack<TreeNode>();
            AddLeftMostNodes(root1, s1);
            AddLeftMostNodes(root2, s2);
            var res = new List<int>();
            while (s1.Count > 0 || s2.Count > 0)
            {
                var s = s1.Count == 0 ? s2 : s2.Count == 0 ? s1 : s1.Peek().val < s2.Peek().val ? s1 : s2;
                var n = s.Pop();
                res.Add(n.val);
                AddLeftMostNodes(n.right, s);
            }
            return res;
        }

        private List<int> Merge(List<TreeNode> l1, List<TreeNode> l2)
        {
            var res = new List<int>();
            int p1 = 0, p2 = 0;
            while (p1 < l1.Count || p2 < l2.Count)
            {
                if (p1 == l1.Count)
                {
                    res.Add(l2[p2++].val);
                }
                else if (p2 == l2.Count)
                {
                    res.Add(l1[p1++].val);
                }
                else
                {
                    if (l1[p1].val < l2[p2].val)
                    {
                        res.Add(l1[p1++].val);
                    }
                    else
                    {
                        res.Add(l2[p2++].val);
                    }
                }
            }
            return res;
        }

        private TreeNode AddLeftMostNodes(TreeNode root, Stack<TreeNode> s)
        {
            if (root == null)
            {
                return null;
            }

            var cur = root;
            while (cur != null)
            {
                s.Push(cur);
                cur = cur.left;
            }
            return s.Peek();
        }

        private void Inorder(TreeNode root, List<TreeNode> list)
        {
            if (root == null)
            {
                return;
            }

            Inorder(root.left, list);
            list.Add(root);
            Inorder(root.right, list);
        }
        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
