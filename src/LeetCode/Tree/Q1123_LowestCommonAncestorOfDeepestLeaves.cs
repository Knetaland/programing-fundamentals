using System;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    public class Q865_Q1123_LowestCommonAncestorOfDeepestLeaves : IProblem
    {
        /*
        Given a rooted binary tree, return the lowest common ancestor of its deepest leaves.

Recall that:

The node of a binary tree is a leaf if and only if it has no children
The depth of the root of the tree is 0, and if the depth of a node is d, the depth of each of its children is d+1.
The lowest common ancestor of a set S of nodes is the node A with the largest depth such that every node in S is in the subtree with root A.
 

Example 1:

Input: root = [1,2,3]
Output: [1,2,3]
Explanation: 
The deepest leaves are the nodes with values 2 and 3.
The lowest common ancestor of these leaves is the node with value 1.
The answer returned is a TreeNode object (not an array) with serialization "[1,2,3]".
Example 2:

Input: root = [1,2,3,4]
Output: [4]
Example 3:

Input: root = [1,2,3,4,5]
Output: [2,4,5]
 

Constraints:

The given tree will have between 1 and 1000 nodes.
Each node of the tree will have a distinct value between 1 and 1000.

        */
        public TreeNode LcaDeepestLeaves(TreeNode root)
        {
            if (root == null)
            {
                return null;
            }

            var left = GetDepth(root.left);
            var right = GetDepth(root.right);
            if (left == right)
            {
                return root;
            }
            return left > right ? LcaDeepestLeaves(root.left) : LcaDeepestLeaves(root.right);
        }

        private int GetDepth(TreeNode root)
        {
            if (root == null)
            {
                return 0;
            }

            return 1 + Math.Max(GetDepth(root.left), GetDepth(root.right));
        }

        // return node along with it's depth
        // if both sides have same height, return root
        // if left > right, lca is on left
        // if left < right, lca is on right
        private Tuple<TreeNode, int> FindLca(TreeNode root)
        {
            if (root == null)
            {
                return new Tuple<TreeNode, int>(null, 0);
            }

            var left = FindLca(root.left);
            var right = FindLca(root.right);
            if (left.Item2 == right.Item2)
            {
                return new Tuple<TreeNode, int>(root, left.Item2 + 1);
            }
            if (left.Item2 > right.Item2)
            {
                return new Tuple<TreeNode, int>(left.Item1, 1 + left.Item2);
            }
            return new Tuple<TreeNode, int>(right.Item1, 1 + right.Item2);
        }
        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
