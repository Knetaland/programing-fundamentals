using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    public class Q113_PathSum_II : IProblem
    {
        /*
        Given the root of a binary tree and an integer targetSum, return all root-to-leaf paths where the sum of the node values in the path equals targetSum. Each path should be returned as a list of the node values, not node references.

        A root-to-leaf path is a path starting from the root and ending at any leaf node. A leaf is a node with no children.
        */
        public IList<IList<int>> PathSum(TreeNode root, int sum)
        {
            var res = new List<IList<int>>();
            BackTrack(res, [], sum, root);
            return res;
        }

        public void BackTrack(IList<IList<int>> res, IList<int> cur, int target, TreeNode node)
        {
            if (node == null)
            {
                return;
            }

            cur.Add(node.val);
            // Lib.Print(string.Join(",", cur));
            if (node.val == target && node.left == null && node.right == null)
            {
                res.Add(new List<int>(cur));
                // return; // still need to remove last element
            } else {
                BackTrack(res, cur, target - node.val, node.left);
                BackTrack(res, cur, target - node.val, node.right);
            }

            cur.RemoveAt(cur.Count - 1);
        }

        public void Run(){
            var arr = new int?[] { 5,4,8,11,null,13,4,7,2,null,null,5,1 };
            var root = Lib.ToBinaryTree(arr);
            var targetSum = 22;
            var res = PathSum(root, targetSum);
        }
    }
}
