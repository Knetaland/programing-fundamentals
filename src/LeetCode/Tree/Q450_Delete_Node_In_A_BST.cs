﻿using System;
using Fundamental.Core;
using Fundamental.Core.Models;

namespace LeetCode;

/*
Given a root node reference of a BST and a key, delete the node with the given key in the BST. Return the root node reference (possibly updated) of the BST.

Basically, the deletion can be divided into two stages:

Search for a node to remove.
If the node is found, delete the node.
*/

public class Q450_Delete_Node_In_A_BST
{
    public TreeNode DeleteNode(TreeNode root, int key) {
        var tree = new BinarySearchTree(root);
        return tree.delete(key);
    }
}
