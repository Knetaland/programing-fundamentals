﻿using System;
using System.Collections.Generic;

namespace LeetCode;
using TreeNode = Fundamental.Core.Models.TreeNodeWithParent;

/*
https://leetcode.ca/2020-06-06-1650-Lowest-Common-Ancestor-of-a-Binary-Tree-III/

Given two nodes of a binary tree p and q, return their lowest common ancestor (LCA).

Each node will have a reference to its parent node. The definition for Node is below:

class Node {
public int val;
public Node left;
public Node right;
public Node parent;
}
According to the definition of LCA on Wikipedia: “The lowest common ancestor of two nodes p and q in a tree T is the lowest node that has both p and q as descendants (where we allow a node to be a descendant of itself).”


*/
public class Q1650_LCA_Of_Binary_Tree_III
{
    // O(logN)
    // S: O(lgN)

    public TreeNode lowestCommonAncestor(TreeNode p, TreeNode q) {
        var pAncestor = new HashSet<TreeNode>();
        while (p != null) {
            pAncestor.Add(p);
            p = p.parent;
        }
        while(q != null) {
            if (pAncestor.Contains(q)) return q;
            q = q.parent;
        }
        return null;
    }

    // O(N)
    // O(1)
    public TreeNode lowestCommonAncestorUsingIntersection(TreeNode p, TreeNode q) {
        TreeNode a = p, b = q;
        while ( a != b) {
            a = a == null? q : a.parent;
            b = b == null? p : b.parent;
        }
        return a;
    }
    
}
