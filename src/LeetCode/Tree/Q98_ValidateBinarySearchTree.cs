// Given a binary tree, determine if it is a valid binary search tree (BST).

// Assume a BST is defined as follows:

// The left subtree of a node contains only nodes with keys less than the node's key.
// The right subtree of a node contains only nodes with keys greater than the node's key.
// Both the left and right subtrees must also be binary search trees.
// Example 1:
//     2
//    / \
//   1   3
// Binary tree [2,1,3], return true.
// Example 2:
//     1
//    / \
//   2   3
// Binary tree [1,2,3], return false.



using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;
/**
* Definition for a binary tree node.
* public class TreeNode {
*     int val;
*     TreeNode left;
*     TreeNode right;
*     TreeNode(int x) { val = x; }
* }
*/
namespace LeetCode
{
    public class Q98_ValidateBinarySearchTree : IProblem
    {
        public bool IsValidBSTRecursive(TreeNode root)
        {
            return RecursiveHelper(root, null, null);
        }

        private bool RecursiveHelper(TreeNode root, int? lower, int? higher)
        {
            if (root == null)
            {
                return true;
            }

            if ((higher.HasValue && root.val >= higher) || (lower.HasValue && root.val <= lower))
            {
                return false;
            }

            return RecursiveHelper(root.left, lower, root.val) && RecursiveHelper(root.right, root.val, higher);
        }

        // Method 2 Iterative
        public bool IsValidIterative(TreeNode root)
        {
            // in order traversal
            var stack = new Stack<TreeNode>();
            var cur = root;
            TreeNode prev = null;
            while (cur != null || stack.Count > 0)
            {
                while (cur != null)
                {
                    stack.Push(cur);
                    cur = cur.left;
                }
                var node = stack.Pop();
                if (prev != null && prev.val >= node.val)
                {
                    return false;
                }

                prev = node;
                cur = node.right;
            }
            return true;
        }
        public void Run()
        {
            var tree = new TreeNode(1)
            {
                left = new TreeNode(1),
            };

            IsValidBSTRecursive(tree);
            IsValidIterative(tree);
        }
    }
}
