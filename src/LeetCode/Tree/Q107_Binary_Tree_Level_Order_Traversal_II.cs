﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode;

/*
https://leetcode.com/problems/binary-tree-level-order-traversal-ii/description/

Given the root of a binary tree, return the bottom-up level order traversal of its nodes' values. (i.e., from left to right, level by level from leaf to root).


*/
public class Q107_Binary_Tree_Level_Order_Traversal_II : IProblem
{
    public IList<IList<int>> LevelOrderBottom(TreeNode root) {
        // use same logic as Q102, just reverse the list at the end
        var res = traverse(root);
        // reverse(res);
        return res;
    }

    private void reverse(IList<IList<int>> res) {
        var left = 0;
        var right = res.Count-1;
        while(left < right) {
            var temp = res[left];
            res[left] = res[right];
            res[right] = temp;
            left++;
            right--;
        }
    }

    public IList<IList<int>> traverse(TreeNode root) {
        var res = new LinkedList<IList<int>>();
        if (root == null) return res.ToList();

        var queue = new Queue<TreeNode>();
        queue.Enqueue(root);
        while(queue.Count > 0) {
            var level = new List<int>();
            var count = queue.Count;
            while(count >0) {
                var node = queue.Dequeue();
                level.Add(node.val);
                if (node.left != null) queue.Enqueue(node.left);
                if (node.right != null) queue.Enqueue(node.right);
                count--;
            }
            // Solution 1, insert to the top of list, can use LinkedList.AddFirst. Then we don't need to reverse
            // res.Insert(0, level);
            // res.AddFirst(level);

            res.AddFirst(level);
        }
        return res.ToList();
    }
    public void Run() => throw new NotImplementedException();
}
