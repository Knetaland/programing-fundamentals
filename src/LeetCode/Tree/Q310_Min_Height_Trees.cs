﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode;

/*
https://leetcode.com/problems/minimum-height-trees/description/

A tree is an undirected graph in which any two vertices are connected by exactly one path. In other words, any connected graph without simple cycles is a tree.

Given a tree of n nodes labelled from 0 to n - 1, and an array of n - 1 edges where edges[i] = [ai, bi] indicates that there is an undirected edge between the two nodes ai and bi in the tree, you can choose any node of the tree as the root. When you select a node x as the root, the result tree has height h. Among all possible rooted trees, those with minimum height (i.e. min(h))  are called minimum height trees (MHTs).

Return a list of all MHTs' root labels. You can return the answer in any order.

The height of a rooted tree is the number of edges on the longest downward path between the root and a leaf.
*/

public class Q310_Min_Height_Trees : IProblem
{
    /*
        https://www.youtube.com/watch?v=OsvbLAaRmu8
        In order to get the min height tree, the roots will be on the two longest path (root to leave). To find that,
        we can keep trimming the leaves, so when there are only 1 or 2 nodes left (furthest from leaves), they are candidates for
        the root of the min height tree.
    */

    // O(N)
    // S: O(N)
    public IList<int> FindMinHeightTrees(int n, int[][] edges) {
        if (n == 1) return [0];
        var graph = new Dictionary<int, ISet<int>>();
        // build graph
        foreach(var edge in edges) {
            if (!graph.ContainsKey(edge[0])) {
                graph[edge[0]] = new HashSet<int>();
            }
            graph[edge[0]].Add(edge[1]);
            if (!graph.ContainsKey(edge[1])) {
                graph[edge[1]] = new HashSet<int>();
            }
            graph[edge[1]].Add(edge[0]);
        }   

        while (n > 2) {
            // find all nodes that only has one edge connect to it, which are the leaves
            var leaves = graph.Where(g => g.Value.Count == 1).Select(e => e.Key).ToList();
            n -= leaves.Count;
            foreach(var leaf in leaves) {
                var parent = graph[leaf].FirstOrDefault(); // leaf only has one neighbor
                graph.Remove(leaf); // remove the leaf
                graph[parent].Remove(leaf); // remove leaf connection
            }
        }
        return graph.Keys.ToList();
    }

    public void Run() {
        List<(int n, int[][] edges)> input = new List<(int, int[][])>  {
            (4, [[1,0],[1,2],[1,3]]),
            (6,[[3,0],[3,1],[3,2],[3,4],[5,4]])
        };
        foreach(var q in input) {
            var res = FindMinHeightTrees(q.n, q.edges);
            res.Print();
        }
    }
}
