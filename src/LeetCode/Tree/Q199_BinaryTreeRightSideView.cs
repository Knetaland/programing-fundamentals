using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{

    /*
    Given a binary tree, imagine yourself standing on the right side of it, return the values of the nodes you can see ordered from top to bottom.

    Example:

    Input: [1,2,3,null,5,null,4]
    Output: [1, 3, 4]
    Explanation:

    1            <---
    /   \
    2     3         <---
    \     \
    5     4       <---
    */
    public class Q199_BinaryTreeRightSideView : IProblem
    {
        public IList<int> RightSideViewBFS(TreeNode root)
        {
            // traverse each level, find right most element on each level
            var queue = new Queue<TreeNode>();
            var res = new List<int>();
            if (root == null)
            {
                return res;
            }
            queue.Enqueue(root);
            while (queue.Count > 0)
            {
                var count = queue.Count;
                TreeNode node = null;
                while (count > 0)
                {
                    node = queue.Dequeue();
                    if (node.left != null)
                    {
                        queue.Enqueue(node.left);
                    }

                    if (node.right != null)
                    {
                        queue.Enqueue(node.right);
                    }

                    count--;
                }
                res.Add(node.val);
            }
            return res;
        }

        public IList<int> RightSideViewDFS(TreeNode root)
        {
            var res = new List<int>();
            if (root == null)
            {
                return res;
            }
            Dfs(res, root, 0);
            return res;
        }

        void Dfs(IList<int> res, TreeNode node, int level)
        {
            if (node == null)
            {
                return;
            }
            // depth first on right child, first time reaches the level will be the right most node on that level
            if (res.Count == level)
            {
                res.Add(node.val);
            }
            Dfs(res, node.right, level + 1);
            Dfs(res, node.left, level + 1);
        }


        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
