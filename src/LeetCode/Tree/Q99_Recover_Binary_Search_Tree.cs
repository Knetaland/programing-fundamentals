﻿using System;
using System.Collections;
using System.Collections.Generic;
using Fundamental.Core.Models;

namespace LeetCode;

/*
https://leetcode.com/problems/recover-binary-search-tree/description/

You are given the root of a binary search tree (BST), where the values of exactly two nodes of the tree were swapped by mistake. Recover the tree without changing its structure.
*/
public class Q99_Recover_Binary_Search_Tree
{
    public void RecoverTree(TreeNode root) {
        var stack = new Stack<TreeNode>();
        var cur = root;
        var prev = new TreeNode(int.MinValue);
        TreeNode first = null, second = null;
        while(stack.Count > 0 || cur !=null) {
            while(cur!= null) {
                stack.Push(cur);
                cur = cur.left;
            }
            var node = stack.Pop();
            if (node.val < prev.val) {
                if (first == null) {
                    first = prev;  // since only two nodes are misplaced, when found out of order, the prev node must be one of them.   
                }
                second = node;  // second is a candidate here, have to continue to set the second if prev is still < node 
            }

            /* following 7 and 4 are out of place
                    7
                /       \
                1       8
                      /  \
                     6
                    / \
                    5  4       
            */
            prev = node;
            cur = node.right;
        }

        var temp = first.val;
        first.val = second.val;
        second.val = temp;
    }
}
