﻿using System;
using System.Linq.Expressions;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode;


/*
https://leetcode.ca/all/1644.html 

Given the root of a binary tree, return the lowest common ancestor (LCA) of two given nodes, p and q. If either node p or q does not exist in the tree, return null. All values of the nodes in the tree are unique.

According to the definition of LCA on Wikipedia: "The lowest common ancestor of two nodes p and q in a binary tree T is the lowest node that has both p and q as descendants (where we allow a node to be a descendant of itself)". A descendant of a node x is a node y that is on the path from node x to some leaf node.
*/

public class Q1644_LCA_of_Binary_Tree_II : IProblem
{
    int count = 0;
    public TreeNode Lca(TreeNode root, TreeNode p, TreeNode q) {
        // var foundP = findNode(root, p);
        // var foundQ = findNode(root, q);
        // if (!foundP  || !foundQ) return null;

        // return LcaNormal(root, p, q);

        // solution 2
        var lca = lcaCountNode(root, p, q);
        return count == 2 ? lca  : null;
    }

    public TreeNode lcaCountNode(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null) return null;
        Console.WriteLine($"checking node {root.val}");
        if (root == p || root == q) {
            count++;
            Console.WriteLine($"detecting node is p or q: {root.val}, returning {root.val}");
            return root;
        }
        var left = lcaCountNode(root.left, p, q);
        var right = lcaCountNode(root.right, p, q);
        Console.WriteLine($"left is {left?.val}, right is {right?.val}");
        if (left!= null && right != null) return root;
        return left ?? right;
    }

    

    private bool findNode(TreeNode root, TreeNode target) {
        if (root == null) return false;
        if (target == root) return true;
        return findNode(root.left, target) || findNode(root.right, target);
    }

    private TreeNode LcaNormal(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null || root == p || root == q) return root;
        var left = LcaNormal(root.left, p, q);
        var right = LcaNormal(root.right, p, q);
        if (left != null && right != null) return root;
        return left ?? right;
    }


    public void Run() {
        var input = new int?[] { 3, 5, 1, 6, 2, 0, 8, null, null, 7, 4 };
            var tree = Lib.ToBinaryTree(input, 0);
            // var p = tree.left.right.left;
            // var q = tree.left.right.right;
            var p = tree.left;
            var q = new TreeNode(10);
            var lca = Lca(tree, p, q);
            if (lca != null) {
                Console.WriteLine("LCA is " + lca .val);
            } else {
                Console.WriteLine("no lca");
            }
            
    }
}
