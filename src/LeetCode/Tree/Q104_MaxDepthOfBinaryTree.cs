using System;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    public class Q104_MaxDepthOfBinaryTree : IProblem
    {
        public int MaxDepth(TreeNode root)
        {
            if (root == null)
            {
                return 0;
            }

            var leftHeight = MaxDepth(root.left);
            var rightHeight = MaxDepth(root.right);

            return 1 + Math.Max(leftHeight, rightHeight);
        }

        public void Run()
        {
            var tree = new TreeNode(3)
            {
                left = new TreeNode(9),
                right = new TreeNode(20)
                {
                    left = new TreeNode(15),
                    right = new TreeNode(7)
                }
            };

            var max = MaxDepth(tree);
            Console.WriteLine($"max depth is {max}");
        }
    }
}
