using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{

    // opposite of Q606
    /*
    You need to construct a binary tree from a string consisting of parenthesis and integers.

    The whole input represents a binary tree. It contains an integer followed by zero, one or two pairs of parenthesis. The integer represents the root's value and a pair of parenthesis contains a child binary tree with the same structure.

    You always start to construct the left child node of the parent first if it exists.

    Example:

    Input: "4(2(3)(1))(6(5))"
    Output: return the tree root node representing the following tree:

        4
        /   \
        2     6
    / \   / 
    3   1 5   
    

    Note:

    There will only be '(', ')', '-' and '0' ~ '9' in the input string.
    An empty tree is represented by "" instead of "()".
    */
    public class Q536_Construct_Binary_Tree_From_String : IProblem
    {
        public TreeNode Construct(string input) {
            if (string.IsNullOrEmpty(input)) return null;
            var index = 0;
            return Recursive();

            TreeNode Recursive() {
                if (index > input.Length) return null;
                var sign = 1;
                if (input[index] == '-') {
                    sign = -1;
                    index++;
                }
                var num = 0;
                while(index< input.Length && char.IsDigit(input[index])) {
                    num = num * 10 + input[index] - '0';
                    index++;
                }
                num = num * sign;

                var node = new TreeNode(num);
                if (index >= input.Length) return node;

                if (index < input.Length && input[index] == '(') {
                    index++;
                    node.left = Recursive();
                }
                if (index < input.Length && input[index] == ')') {
                    index++;
                    return node;
                }

                if (index < input.Length && input[index] == '(') {
                    index++;
                    node.right = Recursive();
                }

                if (index < input.Length && input[index] == ')') {
                    index++;
                    return node;
                }
                return node;
            }
        }

        public TreeNode Construct_Iterative(string input) {
            if (string.IsNullOrEmpty(input)) return null;
            var stack = new Stack<TreeNode>();

            var index = 0;
            while(index < input.Length) {
                if (input[index] == '-' || char.IsDigit(input[index])) {
                     var sign = 1;
                    if (input[index] == '-') {
                        sign = -1;
                        index++;
                    }
                    var num = 0;
                    while(index< input.Length && char.IsDigit(input[index])) {
                        num = num * 10 + input[index] - '0';
                        index++;
                    }
                    num = num * sign;
                    var node = new TreeNode(num);
                    if (stack.Count > 0) {
                        var root = stack.Peek();
                        if (root.left == null) {
                            root.left = node;
                        } else {
                            root.right = node;
                        }
                    }
                    stack.Push(node);
                    continue;
                } else if (input[index] == ')') {
                    stack.Pop();
                }
                index++;
            }
            return stack.Pop();            
        }

        public void Run() {
            var str = "4(2(3)(1))(6(5))";
            // var root = Construct(str);
            var root = Construct_Iterative(str);
            var helper = new Q102_TreeLevelTraversal();
            var print = helper.LevelOrder(root);
            Lib.Print(print);
        }
    }
}