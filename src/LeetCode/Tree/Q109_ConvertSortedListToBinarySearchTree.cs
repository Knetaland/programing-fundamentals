using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    public class Q109_ConvertSortedListToBinarySearchTree : IProblem
    {
        private ListNode _head;
        public void Run()
        {
            throw new System.NotImplementedException();
        }

        public TreeNode SortedListToBSTNaive(ListNode head)
        {
            if (head == null)
            {
                return null;
            }

            var mid = findMiddleElement(head);
            var node = new TreeNode(mid.val);
            if (head == mid)
            {
                return node;
            }
            node.left = SortedListToBST(head);
            node.right = SortedListToBST(mid.next);
            return node;
        }
        private ListNode findMiddleElement(ListNode head)
        {

            // The pointer used to disconnect the left half from the mid node.
            ListNode prevPtr = null;
            var slowPtr = head;
            var fastPtr = head;

            // Iterate until fastPr doesn't reach the end of the linked list.
            while (fastPtr != null && fastPtr.next != null)
            {
                prevPtr = slowPtr;
                slowPtr = slowPtr.next;
                fastPtr = fastPtr.next.next;
            }

            // Handling the case when slowPtr was equal to head.
            if (prevPtr != null)
            {
                prevPtr.next = null;
            }

            return slowPtr;
        }

        public TreeNode SortedListToBST(ListNode head)
        {
            var size = GetSize(head);
            _head = head;
            return Convert(0, size - 1);
        }

        private TreeNode Convert(int l, int r)
        {
            if (l > r)
            {
                return null;
            }

            var mid = l + (r - l) / 2;
            // First step of simulated inorder traversal. Recursively form the left half
            var left = Convert(l, mid - 1);
            var node = new TreeNode(_head.val)
            {
                left = left
            };

            _head = _head.next;

            node.right = Convert(mid + 1, r);
            return node;
        }

        private int GetSize(ListNode head)
        {
            var pt = head;
            var count = 0;
            while (pt != null)
            {
                pt = pt.next;
                count++;
            }
            return count;
        }
    }
}
