﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode;

/*

Design an algorithm to serialize and deserialize a binary search tree. There is no restriction on how your serialization/deserialization algorithm should work. You need to ensure that a binary search tree can be serialized to a string, and this string can be deserialized to the original tree structure.

The encoded string should be as compact as possible.


Example 1:

Input: root = [2,1,3]
Output: [2,1,3]
Example 2:

Input: root = []
Output: []

*/

public class Q449_Serialize_Deserialize_BST : IProblem
{
    // Note that the question wants the serialized string as compact as possible
    public string serialize(TreeNode root) {
        var list = new List<string>();
        dfs(root);

        return string.Join(',', list);

        void dfs(TreeNode node) {
            if (node == null) return ;
            list.Add($"{node.val}");
            dfs(node.left);
            dfs(node.right);
        }

        // or use stringbuilder.
        // void dfs(TreeNode node) {
        //     if (node == null) return ;
        //     sb.Append($"{node.val}");
        //     if (node.left) {
        //          sb.Append(',');
        //          dfs(node.left);
        //      }    
        //     if (node.right) {
        //          sb.Append(',');
        //          dfs(node.right);
        //      }    
        // }
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(string data) {
        if (string.IsNullOrEmpty(data)) return  null;

        var queue = new Queue<string>(data.Split(','));

        var root = dfs(queue, int.MinValue, int.MaxValue);
        return root;
        
        TreeNode dfs(Queue<string> input, int low, int high) {
            if (queue.Count == 0) return null;
            var topVal = int.Parse(queue.Peek());
            Console.WriteLine($"peek value is {topVal}, trying to add node btw {low}-{high}");

            if (topVal < low || topVal > high) {
                Console.WriteLine($"top val {topVal} is not in btw {low} and {high}, skip");
                return null;
            }
            queue.Dequeue();
            Console.WriteLine($"assigning node {topVal}");
            var node = new TreeNode(topVal) {
                left = dfs(queue, low, topVal),
                right = dfs(queue, topVal, high)
            };
            return node;
        }
    }

    public void Run() {
        int?[] s = [3, 1, 6, 0, null, 4, 7];
        var root = Lib.ToBinaryTree(s);

        var serialized = serialize(root);
        var d = serialize(deserialize(serialized));

        Console.WriteLine(serialized);
        Console.WriteLine(d);
    }
}
