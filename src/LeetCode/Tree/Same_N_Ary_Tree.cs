using System;
using System.Collections;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

using Node = Fundamental.Core.Models.NaryTreeNode;

namespace LeetCode.Tree
{
    public class Same_N_Ary_Tree : IProblem
    {
        public bool IsSameTree(Node root, Node root2) {
            if (root == null && root2 == null) return true;
            if (root == null || root2 == null) return false;
            var queue1 = new Queue<Node>();
            var queue2 = new Queue<Node>();

            queue1.Enqueue(root);
            queue2.Enqueue(root2);
            while(queue1.Count > 0 && queue2.Count >0) {
                var node1 = queue1.Dequeue();
                var node2 = queue2.Dequeue();

                if (node1 == null && node2 == null) {
                    continue;
                }
                if (node1 == null || node2 == null || node1.val != node2.val) return false;
                if (node1.children == null || node1.children.Count == 0) {
                    queue1.Enqueue(null);
                } else {
                    foreach(var child  in node1.children) {
                        queue1.Enqueue(child);
                    }
                }
                if (node2.children == null || node2.children.Count == 0) {
                    queue2.Enqueue(null);
                } else {
                    foreach(var child  in node2.children) {
                        queue2.Enqueue(child);
                    }
                }
            }
            return queue1.Count == queue2.Count;
        }

        public void Run() {
            var tree1 = new Node(1, new List<Node>{
                new Node(2, new List<Node> { new Node(4) }),
                new Node(3)
            });
            var tree2 = new Node(1, new List<Node>{
                new Node(2, new List<Node> { new Node(4) }),
                new Node(3)
            });

            var isSameTree = IsSameTree(tree1, tree2);
            Console.WriteLine($"is same tree? {isSameTree}");
        }
    }
}
