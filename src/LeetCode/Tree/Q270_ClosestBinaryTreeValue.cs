using System;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Tree
{
    public class Q270_ClosestBinaryTreeValue : IProblem
    {

        /*
            Given a non-empty binary search tree and a target value, find the value in the BST that is closest to the target.

            Note:

            Given target value is a floating point.
            You are guaranteed to have only one unique value in the BST that is closest to the target.
        */

        public int ClosestValue(TreeNode root, double target)
        {
            if (root == null)
            {
                return 0;
            }
            var min = double.MaxValue;
            var res = root.val;
            var cur = root;
            while (cur != null)
            {
                if (Math.Abs(target - cur.val) < min)
                {
                    res = cur.val;
                    min = Math.Abs(target - cur.val);
                }
                cur = target < cur.val ? cur.left : cur.right;  // if target < cur, closer target will be on node < cur, so it will be on the left
            }
            return res;
        }

        public void Run() {
            var root = new TreeNode(4) {
                left = new TreeNode(2) {
                    left = new TreeNode(1),
                    right = new TreeNode(3)
                },
                right = new TreeNode(5)
            };
            /*
                4
               / \
              2   5
             / \
            1   3
            */
            var res = ClosestValue(root, 3.1415);

            Console.WriteLine(res);
        }
    }
}
