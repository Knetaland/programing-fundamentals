using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given n non-negative integers a1, a2, ..., an , where each represents a point at coordinate (i, ai). n vertical lines are drawn such that the two endpoints of line i is at (i, ai) and (i, 0). Find two lines, which together with x-axis forms a container, such that the container contains the most water.

    Note: You may not slant the container and n is at least 2.
    Example:

    Input: [1,8,6,2,5,4,8,3,7]
    Output: 49
    */
    public class Q11_ContainerWithMostWater : IProblem
    {
        public int MaxArea(int[] height)
        {
            if (height.Length < 2)
            {
                return 0;
            }

            int max = 0, left = 0, right = height.Length - 1;
            while (left < right)
            {
                var h = Math.Min(height[left], height[right]);
                max = Math.Max(h * (right - left), max);
                if (height[left] < height[right])
                {
                    left++;
                }
                else
                {
                    right--;
                }
            }
            return max;
        }

        public void Run()
        {
            var heights = new[] { 9, 2, 3, 6, 18, 4, 21, 54, 23, 89 };
            Console.WriteLine($"container with most water area is {MaxArea(heights)}");
        }
    }
}
