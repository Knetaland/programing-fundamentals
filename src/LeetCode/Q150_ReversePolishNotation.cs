using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Valid operators are +, -, *, /. Each operand may be an integer or another expression.

    Some examples:
    ["2", "1", "+", "3", "*"] -> ((2 + 1) * 3) -> 9
    ["4", "13", "5", "/", "+"] -> (4 + (13 / 5)) -> 6
    Subscribe to see which companies asked this question.
     */
    public class Q150_ReversePolishNotation : IProblem
    {
        public int EvalRPN(string[] tokens)
        {
            if (tokens == null || tokens.Length == 0)
            {
                return 0;
            }

            var stack = new Stack<int>();
            int a = 0, b = 0;
            foreach (var s in tokens)
            {
                if (s == "+")
                {
                    stack.Push(stack.Pop() + stack.Pop());
                }
                else if (s == "-")
                {
                    b = stack.Pop();
                    a = stack.Pop();
                    stack.Push(a - b);
                }
                else if (s == "*")
                {
                    stack.Push(stack.Pop() * stack.Pop());
                }
                else if (s == "/")
                {
                    b = stack.Pop();
                    a = stack.Pop();
                    stack.Push(a / b);
                }
                else
                {
                    stack.Push(Convert.ToInt32(s));
                }
            }
            return stack.Pop();
        }
        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
