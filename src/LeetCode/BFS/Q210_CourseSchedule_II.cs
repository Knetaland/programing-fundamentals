﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Fundamental.Core.Interfaces;

namespace LeetCode;

/*

There are a total of numCourses courses you have to take, labeled from 0 to numCourses - 1. You are given an array prerequisites where prerequisites[i] = [ai, bi] indicates that you must take course bi first if you want to take course ai.

For example, the pair [0, 1], indicates that to take course 0 you have to first take course 1.
Return the ordering of courses you should take to finish all courses. If there are many valid answers, return any of them. If it is impossible to finish all courses, return an empty array.

 

Example 1:

Input: numCourses = 2, prerequisites = [[1,0]]
Output: [0,1]
Explanation: There are a total of 2 courses to take. To take course 1 you should have finished course 0. So the correct course order is [0,1].
Example 2:

Input: numCourses = 4, prerequisites = [[1,0],[2,0],[3,1],[3,2]]
Output: [0,2,1,3]
Explanation: There are a total of 4 courses to take. To take course 3 you should have finished both courses 1 and 2. Both courses 1 and 2 should be taken after you finished course 0.
So one correct course order is [0,1,2,3]. Another correct ordering is [0,2,1,3].
Example 3:

Input: numCourses = 1, prerequisites = []
Output: [0]
 

Constraints:

1 <= numCourses <= 2000
0 <= prerequisites.length <= numCourses * (numCourses - 1)
prerequisites[i].length == 2
0 <= ai, bi < numCourses
ai != bi
All the pairs [ai, bi] are distinct.
*/

public class Q210_CourseSchedule_II : IProblem
{
    public int[] FindOrder(int numCourses, int[][] prerequisites) {
        var indegree = new Dictionary<int, int>();
        var graph = new Dictionary<int, List<int>>();
        foreach(var prer in prerequisites) {
            var from = prer[1];
            var to = prer[0];
            if (!graph.ContainsKey(from)) {
                graph.Add(from, new List<int>());
            }
            graph[from].Add(to);
            indegree[to] = indegree.GetValueOrDefault(to, 0) + 1;
        }

        var q = new Queue<int>();
        for(var i = 0; i < numCourses; i++) {
            if (!indegree.ContainsKey(i)) {
                q.Enqueue(i);
            }
        }
        var res = new List<int>();
        while(q.Count > 0) {
            var course = q.Dequeue();
            res.Add(course);
            if (graph.ContainsKey(course)) {
                var nextLevelCourses = graph[course];
                foreach(var c in nextLevelCourses) {
                    if (indegree.ContainsKey(c)) {
                        indegree[c]--;
                        if (indegree[c] == 0) {
                            q.Enqueue(c);
                        }
                    }
                }
            }
        }
        if (res.Count != numCourses) return [];
        return res.ToArray();
    }

    public void Run() => throw new NotImplementedException();
}
