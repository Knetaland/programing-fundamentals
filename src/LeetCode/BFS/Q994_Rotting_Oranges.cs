using System;
using System.Collections;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode.BFS;
/// <summary>
/// https://leetcode.com/problems/rotting-oranges/
/** 
    You are given an m x n grid where each cell can have one of three values:

    0 representing an empty cell,
    1 representing a fresh orange, or
    2 representing a rotten orange.
    Every minute, any fresh orange that is 4-directionally adjacent to a rotten orange becomes rotten.

    Return the minimum number of minutes that must elapse until no cell has a fresh orange. If this is impossible, return -1.
**/ 
/// </summary>
public class Q994_Rotting_Oranges : IProblem
{
    const int ROTTEN = 2;
    const int EMPTY = 0;
    const int FRESH = 1;
    public int OrangesRotting(int[][] grid) {
        int[][] DIRS = [
            [0,1],
            [0,-1],
            [1,0],
            [-1,0]
        ];
        int fresh = 0;
        var queue = new Queue<(int, int)>();
        var m = grid.Length;
        var n = grid[0].Length;
        var time = -1;
        for(var i = 0; i < grid.Length; i++) {
            for(var j = 0; j < grid[0].Length; j++) {
                if(grid[i][j] == ROTTEN) {
                    queue.Enqueue((i, j));
                } else if (grid[i][j] == FRESH) {
                    fresh++;
                }
            }
        }
        if (fresh == 0) { return 0; } 

        while(queue.Count > 0) {
            var count  = queue.Count;
            while(count > 0) {
                var rotten = queue.Dequeue();
                foreach(var dir in DIRS) {
                    var x = rotten.Item1 + dir[0];
                    var y = rotten.Item2 + dir[1];
                    if (x >=0 && x < m && y >=0 && y < n && grid[x][y] == FRESH) {
                        Console.WriteLine($"rotting at [{x},{y}]");
                        queue.Enqueue((x, y));
                        grid[x][y] = ROTTEN;
                        fresh--;
                    }
                }
                Console.WriteLine($"time: {time}");
                count--;
                
            }
            time++;
        }

        return fresh != 0 ? -1 : time;
    }

    public void Run() {
        var grid = new int[][] {
            [2,1,1],[1,1,0],[0,1,1]
        };
        var res = OrangesRotting(grid);

        res.Print();
    }
}
