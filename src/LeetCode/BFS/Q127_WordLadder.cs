using System;
using System.Collections.Generic;
using System.Text;
using Fundamental.Core.Interfaces;

namespace LeetCode.BFS
{
    public class Q127_WordLadder : IProblem
    {
        /*
        Given two words (beginWord and endWord), and a dictionary's word list, find the length of shortest transformation sequence from beginWord to endWord, such that:

        Only one letter can be changed at a time.
        Each transformed word must exist in the word list. Note that beginWord is not a transformed word.
        Note:

        Return 0 if there is no such transformation sequence.
        All words have the same length.
        All words contain only lowercase alphabetic characters.
        You may assume no duplicates in the word list.
        You may assume beginWord and endWord are non-empty and are not the same.
        Example 1:

        Input:
        beginWord = "hit",
        endWord = "cog",
        wordList = ["hot","dot","dog","lot","log","cog"]

        Output: 5

        Explanation: As one shortest transformation is "hit" -> "hot" -> "dot" -> "dog" -> "cog",
        return its length 5.
        Example 2:

        Input:
        beginWord = "hit"
        endWord = "cog"
        wordList = ["hot","dot","dog","lot","log"]

        Output: 0

        Explanation: The endWord "cog" is not in wordList, therefore no possible transformation.
        */
        public int LadderLength(string beginWord, string endWord, IList<string> wordList)
        {
            var map = BuildMap(beginWord, wordList);
            return BFS(beginWord, endWord, map);
        }

        Dictionary<string, List<string>> BuildMap(string beginWord, IList<string> wordList)
        {
            var map = new Dictionary<string, List<string>>();
            var wordSet = new HashSet<string>(wordList);
            wordSet.Add(beginWord);
            // build the graph
            foreach (var w in wordSet)
            {
                var n = w.Length;
                for (var i = 0; i < n; i++)
                {
                    var sb = new StringBuilder(w);
                    for (var t = 'a'; t <= 'z'; t++)
                    {
                        if (sb[i] != t)
                        {
                            sb[i] = t;
                            var newWord = sb.ToString();
                            if (wordSet.Contains(newWord))
                            {
                                if (!map.ContainsKey(w))
                                {
                                    map.Add(w, new List<string>());
                                }
                                map[w].Add(newWord);
                            }
                        }
                    }
                }
            }
            return map;
        }

        public int BFS(string beginWord, string endWord, Dictionary<string, List<string>> map)
        {
            var queue = new Queue<string>();
            var visited = new HashSet<string>
            {
                beginWord
            };
            queue.Enqueue(beginWord);
            var pathLength = 1;
            while (queue.Count > 0)
            {
                pathLength++;
                var size = queue.Count;
                while (size > 0)
                {
                    var word = queue.Dequeue();
                    foreach (var transform in map[word])
                    {
                        if (transform == endWord)
                        {
                            return pathLength;
                        }
                        if (!visited.Contains(transform))
                        {
                            queue.Enqueue(transform);
                            visited.Add(transform);
                        }
                    }
                    size--;
                }
            }
            return 0;
        }

        public int LadderLengthWithGeneric(string beginWord, string endWord, IList<string> wordList)
        {
            var map = new Dictionary<string, HashSet<string>>();
            foreach (var w in wordList)
            {
                for (var i = 0; i < w.Length; i++)
                {
                    var generic = GetGeneric(w, i);
                    if (!map.ContainsKey(generic))
                    {
                        map.Add(generic, new HashSet<string>());
                    }
                    map[generic].Add(w);
                }
            }
            var queue = new Queue<Tuple<string, int>>();
            var visited = new HashSet<string>();
            queue.Enqueue(new Tuple<string, int>(beginWord, 1));
            visited.Add(beginWord);
            while (queue.Count > 0)
            {
                var size = queue.Count;
                var cur = queue.Dequeue();
                if (cur.Item1 == endWord)
                {
                    return cur.Item2;
                }
                for (var i = 0; i < cur.Item1.Length; i++)
                {
                    var generic = GetGeneric(cur.Item1, i);
                    if (map.ContainsKey(generic))
                    {
                        foreach (var next in map[generic])
                        {
                            if (!visited.Contains(next))
                            {
                                visited.Add(next);
                                queue.Enqueue(new Tuple<string, int>(next, cur.Item2 + 1));
                            }
                        }
                    }
                }
            }
            return 0;
        }

        public string GetGeneric(string s, int i)
        {
            var sb = new StringBuilder(s);
            sb[i] = '*';
            return sb.ToString();
        }


        // Method 3
        public int LadderLength3(string beginWord, string endWord, IList<string> wordList)
        {
            var queue = new Queue<string>();
            queue.Enqueue(beginWord);
            var wordSet = new HashSet<string>(wordList);

            var wordLength = beginWord.Length;
            var steps = 1;
            // BFS
            while (queue.Count > 0)
            {
                steps++;
                var size = queue.Count;
                while (size > 0)
                {
                    var curWord = queue.Dequeue();
                    for (var i = 0; i < wordLength; i++)
                    {
                        var newWordSB = new StringBuilder(curWord);
                        for (var letter = 'a'; letter <= 'z'; letter++)
                        {
                            newWordSB[i] = letter;
                            var newWord = newWordSB.ToString();
                            if (wordSet.Contains(newWord))
                            { // in dictionary
                                if (newWord == endWord)
                                    return steps;
                                queue.Enqueue(newWord);
                                wordSet.Remove(newWord); // visited, remove to reduce re-processing
                            }
                        }
                    }
                    size--;
                }

            }
            return 0;
        }

        public void Run()
        {
            string beginWord = "hit", endWord = "cog";
            var wordList = new[] { "hot", "dot", "dog", "lot", "log", "cog" };
            var res = LadderLengthWithGeneric(beginWord, endWord, wordList);
            Console.WriteLine($"res is  {res}");
        }
    }
}
