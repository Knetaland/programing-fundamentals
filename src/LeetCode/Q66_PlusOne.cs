using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
     * Given a non-empty array of digits representing a non-negative integer, plus one to the integer.

        The digits are stored such that the most significant digit is at the head of the list, and each element in the array contain a single digit.

        You may assume the integer does not contain any leading zero, except the number 0 itself.

        Example 1:

        Input: [1,2,3]
        Output: [1,2,4]
        Explanation: The array represents the integer 123.
        Example 2:

        Input: [4,3,2,1]
        Output: [4,3,2,2]
        Explanation: The array represents the integer 4321.
     * 
     * 
     */
    public class Q66_PlusOne : IProblem
    {
        public int[] PlusOne(int[] digits)
        {
            var carryOver = 1;
            for (var i = digits.Length - 1; i >= 0; i--)
            {
                var sum = carryOver + digits[i];
                digits[i] = sum % 10;
                carryOver = sum / 10;
                if (carryOver == 0)
                {
                    break;
                }
            }

            if (carryOver > 0)
            {
                digits.Prepend(carryOver).ToArray();
            }
            return digits;
        }

        public char[] PlusOne(char[] number) {
            if (number == null || number.Length == 0) return ['1'];
            var isNegative = number[0] == '-';
            var startIndex =  isNegative ? 1 : 0;
            var i =  number.Length-1;
            var carryOver = isNegative ? -1 : 1;
            while( i >=startIndex) {
                // Console.WriteLine($"co is {carryOver}");
                var sum =  number[i] - '0' + carryOver;
                // Console.WriteLine($"sum is {sum}");
                if (sum < 0) {
                    sum += 10;
                    carryOver = -1;
                } else if (sum >= 10) {
                    carryOver = sum / 10;
                    sum = sum % 10;
                } else {
                    carryOver = 0;
                }
                // Console.WriteLine($"char to insert is {sum} or { (char)(sum + '0')}");
                number[i] = (char)(sum + '0');
                i--;
                if (carryOver == 0) {
                    break;
                }
             
            }
            Console.WriteLine($"i is {i}");
            if (i <=0) {
                if (isNegative) {
                    char[] result = new char[number.Length - 1];
                    Array.Fill(result, '9');
                    result[0] = '-';
                    // Array.Copy(number, startIndex, result, 2, number.Length - 1 - startIndex);
                    return result;
                } else if (carryOver > 0) { // positive
                     return number.Prepend( (char)(carryOver +'0')).ToArray();
                }
            }
            return number;
        }

        public char[] PlusOneDealWithNegative(char[] num) {
            var sign = num[0];
            bool isNegative = false;
            if (sign == '-') isNegative = true;

            for(var i = num.Length-1; i >=0; i--) {
                if (isNegative) {
                    if (num[i] == '0') {
                        num[i] = '9';
                    } else {
                        num[i]--;
                        if (i == 1) {
                            break;
                        }
                        return num;
                    }
                } else {
                    if (num[i] == '9') {
                        num[i] = '0';
                    } else {
                        num[i]++;
                        return num;
                    }
                }
            }
            
            if (isNegative) {
                // reach here meaning there is a leading `0` after negative char
                //  num[0]
                if (num.Length > 2) {
                    var result = new char[num.Length-1];
                    Array.Fill(result, '9');
                    
                    result[0] = '-';
                    return result;
                }
                return num;
            } 
            var res = new char[num.Length+1];
            Array.Fill(res, '0');
            res[0] = '1';
            return res;
        }

        public int[] PlusOneSmart(int[] digits)
        {
            for (var i = digits.Length - 1; i >= 0; i--)
            {
                if (digits[i] == 9)
                {
                    digits[i] = 0;
                }
                else
                {
                    digits[i]++;
                    return digits;
                }
            }

            // reach here, all 9s
            var res = new int[digits.Length + 1];
            
            res[0] = 1;
            return res;
        }

        public void Run()
        {
            // var input1 = new[] { 4, 3, 2, 1 };
            // var input2 = new int[0];
            // var res1 = PlusOne(input1);
            // var res2 = PlusOne(input2);
            // var all9s = new[] { 9, 9, 9, 9, 9, 9 };
            // var input3 = new[] { 1, 2, 3, 4, 5 };
            // var output = PlusOneSmart(all9s);
            // Console.WriteLine($"{string.Join("", input1)} plus 1 is {string.Join("", res1)}");
            // Console.WriteLine($"{string.Join("", input2)} plus 1 is {string.Join("", res2)}");


            List<char[]> input4 = new List<char[]> {
                new char[] { '-', '9', '9'},
                new char[] { '-', '1', '0'},
                new char[] { '-', '1'},
                new char[] { '1'},
                new char[] { '9'},
                new char[] { '0'},
                new char[] { '9','9','9'},
                new char[] { '9','9','8'},
            };
            foreach(var input in input4) {
                Console.WriteLine($"plus 1 on {new string(input)} is:");
                var res4 = PlusOne(input);
                Console.WriteLine( new string(res4));
            }
        }
    }
}
