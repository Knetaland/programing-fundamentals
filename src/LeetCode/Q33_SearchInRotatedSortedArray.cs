using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q33_SearchInRotatedSortedArray : IProblem
    {
        public int Search(int[] nums, int target)
        {
            if (nums == null || nums.Length == 0)
            {
                return -1;
            }

            int left = 0, right = nums.Length - 1;
            while (left <= right)
            {
                var mid = left + (right - left) / 2;
                if (target == nums[mid])
                {
                    return mid;
                }
                // left is sorted
                if (nums[left] <= nums[mid])
                {
                    if (target < nums[mid] && target >= nums[left]) // taret falls in btw
                    {
                        right = mid - 1;
                    }
                    else
                    {
                        left = mid + 1;
                    }
                }
                else
                { // right is sorted
                    if (target > nums[mid] && target <= nums[right])
                    {
                        left = mid + 1;
                    }
                    else
                    {
                        right = mid - 1;
                    }
                }
            }
            return -1;
        }
        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
