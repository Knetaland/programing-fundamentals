﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Interfaces;

namespace LeetCode;

/*
https://leetcode.ca/2021-04-14-1762-Buildings-With-an-Ocean-View/

There are n buildings in a line. You are given an integer array heights of size n that represents the heights of the buildings in the line.

The ocean is to the right of the buildings. A building has an ocean view if the building can see the ocean without obstructions. Formally, a building has an ocean view if all the buildings to its right have a smaller height.

Return a list of indices (0-indexed) of buildings that have an ocean view, sorted in increasing order.

Example 1:

Input: heights = [4,2,3,1]

Output: [0,2,3]

Explanation: Building 1 (0-indexed) does not have an ocean view because building 2 is taller.

Example 2:

Input: heights = [4,3,2,1]

Output: [0,1,2,3]

Explanation: All the buildings have an ocean view.

Example 3:

Input: heights = [1,3,2,4]

Output: [3]

Explanation: Only building 3 has an ocean view.

Example 4:

Input: heights = [2,2,2,2]

Output: [3]

Explanation: Buildings cannot see the ocean if there are buildings of the same height to its right.
*/
public class Q1762_Buildings_With_Ocean_View : IProblem
{
    // O(n)
    // O(n) stack
    public int[] findBuildings(int[] heights) {
        var stack = new Stack<int>();
        
        for(var i = 0; i < heights.Length; i++) {
            while (stack.Count > 0 &&  heights[stack.Peek()] <= heights[i]) stack.Pop();
            stack.Push(i);
        }

        var res = new int[stack.Count];
        for(var i = res.Length -1; i >=0; i--) {
            res[i] = stack.Pop();
        }
        return res;
     }


    // O(n)
    // O(n) , O(1) if the res space is not count
     public int[] findBuildings2(int[] heights) 
     {
        if (heights == null || heights.Length == 0) return Array.Empty<int>();
        var maxHeight = int.MinValue;
        var oceanView = new LinkedList<int>();
        for(var i = heights.Length -1; i >=0; i-- ) {
            if (heights[i] > maxHeight) {
                maxHeight = heights[i];
                oceanView.AddFirst(i);
            }
        }
      
        return oceanView.ToArray();
     }

    public void Run() {
        int[] heights =   [4,3,2,1];
        var res = findBuildings2(heights);
        Console.WriteLine(string.Join(',', res));
    }
}
