﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Fundamental.Core.Interfaces;
using Microsoft.VisualBasic;

namespace LeetCode;

/*
https://leetcode.com/problems/largest-rectangle-in-histogram/description/

Given an array of integers heights representing the histogram's bar height where the width of each bar is 1, return the area of the largest rectangle in the histogram.


**/

public class Q84_LargestRectangleArea : IProblem
{
    public int LargestRectangleArea(int[] heights) {
        return DivideAndConq(heights, 0, heights.Length - 1);
    }

    public int Calculate(int[] heights) {
        // var stack = new Stack<int>();
        
        // var max = 0;
        // for(var i = 0; i < heights.Length; i++) {
        //     while(stack.Count > 0 &&  heights[stack.Peek()] > heights[i]) {
        //         var index = stack.Pop();
        //         var preMaxHeight = heights[index];
        //         var width = i - index;
        //         var area = preMaxHeight * width;
        //         max = Math.Max(area, max);
        //     }
        //     stack.Push(i);
        // }

        // while(stack.Count > 0) {
        //     var preHeightIndex = stack.Pop();
        //     var width = heights.Length  - preHeightIndex;
        //     max = Math.Max(heights[preHeightIndex] * width, max);
        // }

        // return max;
         int maxArea = 0;

        Stack<(int index, int height)> stack = new();
        int n = heights.Length;

        for(int i = 0; i < n; i++)
        {
            int index = i, height = heights[i];
            Console.WriteLine($"processing index  {i} with height {heights[i]}");
            while(stack.Count != 0 && stack.Peek().height > heights[i])
            {
                (index, height) = stack.Pop();
                Console.WriteLine($"height { heights[i]}  < stack peek {height}, popped ({index}, {height})");
                maxArea = Math.Max((i - index) * height, maxArea);
            }
            Console.WriteLine($"pushing to stack ({index}, {heights[i]})");
            stack.Push((index, heights[i]));
        }

        while(stack.Count != 0)
        {
            (int index, int height) = stack.Pop();
            maxArea = Math.Max((n - index) * height, maxArea);
        }

        return maxArea;
    }


    public int CalculateImproved(int[] heights) {
        var stack = new Stack<int>();
        var i = 0;
        var n = heights.Length;
        var max = 0;

        while(i <= n) {
            var h = i == n ? 0 : heights[i]; // already pushed all heights
            if (stack.Count == 0 || h > heights[stack.Peek()]) { // if incrementing, pushing to stack as the area is growing
                stack.Push(i);
                i++;
            } else { 
                var preHeight = heights[stack.Pop()];
                var width = i - (stack.Count == 0 ? 0 : stack.Peek() + 1);
                var area = width * preHeight;
                max = Math.Max(max, area);
            }
        }

        return max;
    }

    // Divide and Conq
    public int DivideAndConq(int[] heights, int start, int end) {
        if (start > end) return 0;
        // get the min height 
        var minHeightIndex = start;
        for(var i = start; i <= end; i++) {
            if (heights[i] <= heights[minHeightIndex] ) {
                minHeightIndex = i;
            }
        }
        // cal current min using the min height * range
        var curArea = heights[minHeightIndex] * (end - start + 1); // at leats of width 1
        var left = DivideAndConq(heights, start, minHeightIndex -1);
        var right = DivideAndConq(heights, minHeightIndex + 1, end);
        return Math.Max(curArea, Math.Max(left, right));
    }

    public void Run() {
        var heights = new int[] {
            2,1,5,6,2,3
        };

        var res = Calculate(heights);

    }
}
