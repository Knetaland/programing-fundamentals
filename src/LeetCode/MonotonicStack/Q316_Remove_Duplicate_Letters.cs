﻿using System;
using System.Collections.Generic;
using System.Text;
using Fundamental.Core.Interfaces;

namespace LeetCode;


/*
https://leetcode.com/problems/remove-duplicate-letters/description/

Given a string s, remove duplicate letters so that every letter appears once and only once. You must make sure your result is 
the smallest in lexicographical order among all possible results.



Example 1:

Input: s = "bcabc"
Output: "abc"
Example 2:

Input: s = "cbacdcbc"
Output: "acdb"


Constraints:

1 <= s.length <= 104
s consists of lowercase English letters.

*/


public class Q316_Remove_Duplicate_Letters : IProblem
{
    public string RemoveDuplicateLetters(string s) {
        var used = new HashSet<char>();
        var lastIndexMap = new int[26];
        for (var i= 0; i < s.Length; i++) {
            var c = s[i];
            lastIndexMap[c-'a'] = i;
        }

        var stack = new Stack<char>();
        for(var i = 0; i < s.Length; i++) {
            if (used.Contains(s[i])) continue;
            while(
                stack.Count > 0 &&
                s[i] <= stack.Peek() && // current char is before 
                i < lastIndexMap[stack.Peek()- 'a'] // more of same char appears later, so it can be popped
            ) {
                used.Remove(stack.Pop()); // also remove from used
            }
            stack.Push(s[i]);
            used.Add(s[i]);
        }
        var res = new char[stack.Count];
        for (var i = res.Length-1; i >=0; i--) {
            res[i] = stack.Pop();
        }
        return new string(res);
    }

    public void Run() {
        var s = "cbacdcbc";
        var res = RemoveDuplicateLetters(s);
        Console.WriteLine(res);
    }
}
