# Monotonic stack

496. Next Greater Element I
503. Next Greater Element II
1019. Next Greater Node In Linked List
739. Daily Temperatures
316. Remove Duplicate Letters
1081. Smallest Subsequence of Distinct Characters
402. Remove K Digits
42. Trapping Rain Water
84. Largest Rectangle in Histogram


``` 
var res = new int[nums.Length];
var stack;
for(var i = nums.length - 1; i >= 0; i--) {
    while(stack.Count > 0 && nums[i] >= stack.Peek()) stack.Pop();
    res[i] = stack.Count == 0 ? -1 : stack.Peek();
    stack.Push(nums[i]);
}
```
## Leetcode discussion
https://leetcode.com/discuss/study-guide/2347639/A-comprehensive-guide-and-template-for-monotonic-stack-based-problems 