﻿using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode;

/*
You are given the head of a linked list with n nodes.

For each node in the list, find the value of the next greater node. That is, for each node, find the value of the first node that is next to it and has a strictly larger value than it.

Return an integer array answer where answer[i] is the value of the next greater node of the ith node (1-indexed). If the ith node does not have a next greater node, set answer[i] = 0.

https://leetcode.com/problems/next-greater-node-in-linked-list/description/

*/
public class Q1019_NextGreaterNodeInLinkedList : IProblem
{
    public int[] NextLargerNodes(ListNode head) {
        var numsList = new List<int>();
        while(head != null) {
            numsList.Add(head.val);
            head = head.next;
        }

        // var nums = numsList.ToArray();
        var stack = new Stack<int>();
        var res =  new int[numsList.Count];
        for(var i = numsList.Count -1; i >=0; i--) {
            while (stack.Count > 0 && numsList[i] >= stack.Peek()) stack.Pop();
            res[i] =  stack.Count > 0 ?stack.Peek() : 0;
            stack.Push(numsList[i]);
        }
        return res;
    }

    public void Run() => throw new NotImplementedException();
}
