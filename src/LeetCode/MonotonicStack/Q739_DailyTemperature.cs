﻿using System;
using System.Collections;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode;

/*

https://leetcode.com/problems/daily-temperatures/

Given an array of integers temperatures represents the daily temperatures, return an array answer such that answer[i] is the number of days you have to wait after the ith day to get a warmer temperature. If there is no future day for which this is possible, keep answer[i] == 0 instead.

 

Example 1:

Input: temperatures = [73,74,75,71,69,72,76,73]
Output: [1,1,4,2,1,1,0,0]
Example 2:

Input: temperatures = [30,40,50,60]
Output: [1,1,1,0]
Example 3:

Input: temperatures = [30,60,90]
Output: [1,1,0]
*/

public class Q739_DailyTemperature : IProblem
{

    public struct Temperature {
        public int degree;
        public int index;
        
    }
    public int[] DailyTemperatures(int[] temperatures) {
        var res = new int[temperatures.Length];
        var stack = new Stack<Temperature>();

        for(var i = temperatures.Length -1; i >=0; i--) {
            while(stack.Count > 0 && stack.Peek().degree <= temperatures[i]) stack.Pop();
            res[i] = stack.Count > 0 ? stack.Peek().index - i  : 0;
            stack.Push(new Temperature {
                degree = temperatures[i],
                index = i
            });
        }
        return res;
    }

    public int[] DailyTemperaturesOptimize(int[] temperatures) {
        var res = new int[temperatures.Length];
        var stack = new Stack<int>();

        for(var i = temperatures.Length -1; i >=0; i--) {
            while(stack.Count > 0 && temperatures[stack.Peek()] <= temperatures[i]) stack.Pop();
            res[i] = stack.Count > 0 ? stack.Peek() - i  : 0;
            stack.Push(i);
        }
        return res;
    }



    public void Run() {
        int [] temperatures = [73,74,75,71,69,72,76,73];
        var res = DailyTemperaturesOptimize(temperatures);
        Console.WriteLine(string.Join(",", res));
    }
}
