﻿using System;
using System.Collections;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode;

/* 
https://leetcode.com/problems/next-greater-element-ii/

Given a circular integer array nums (i.e., the next element of nums[nums.length - 1] is nums[0]), return the next greater number for every element in nums.

The next greater number of a number x is the first greater number to its traversing-order next in the array, which means you could search circularly to find its next greater number. If it doesn't exist, return -1 for this number.

 

Example 1:

Input: nums = [1,2,1]
Output: [2,-1,2]
Explanation: The first 1's next greater number is 2; 
The number 2 can't find next greater number. 
The second 1's next greater number needs to search circularly, which is also 2.
Example 2:

Input: nums = [1,2,3,4,3]
Output: [2,3,4,-1,4]
 

Constraints:

1 <= nums.length <= 104
-109 <= nums[i] <= 109
*/
public class Q503_NextGreaterElement_II : IProblem
{
    public int[] NextGreaterElements(int[] nums) {
        // Naive is to copy the circular array to 2 * length, then use NextGreaterElementI way to solve it.
        // smarter version is to use mod
        return Better(nums);
    }


    public int[] Better (int[] nums ) {
        var stack = new Stack<int>();
        var n  = nums.Length;
        var res = new int[n];
        for(var i = 2*n -1; i >=0; i--) {
            while(stack.Count > 0 && nums[i%n] >= stack.Peek())  {
                stack.Pop();
            }
            res[i%n] = stack.Count > 0 ? stack.Peek() : -1;
            stack.Push(nums[i%n]);
        }
        return res;
    }
    // O(2N)
    // space O(2N)
    public int[] Naive(int[] nums ) {
        var newLength  = nums.Length *2;
        var longerNums = new int[newLength];
        for(var i = 0; i < newLength; i++) {
            longerNums[i] = nums[i % nums.Length];
        }
        var stack = new Stack<int>();
        
        for(var i = newLength - 1; i >= nums.Length; i--) {
            while(stack.Count >0 && longerNums[i] >= stack.Peek()) {
                stack.Pop();
            } 
            stack.Push(longerNums[i]);
            
        }

        var res = new int[nums.Length];
        for (var i = nums.Length -1; i >=0; i --) {
            while(stack.Count > 0 && nums[i] >= stack.Peek()) {
                stack.Pop();
            }
            res[i] = stack.Count > 0 ? stack.Peek() : -1;
            stack.Push(nums[i]);
        }

        return res;

    }
    

    public void Run()  {
        var nums = new int[] { 1, 2, 3, 4, 3 };
        var res = Naive(nums);

        Console.WriteLine(String.Join(",", res));
    } 
}
