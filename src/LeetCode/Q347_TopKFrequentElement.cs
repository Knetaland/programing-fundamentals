using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given a non-empty array of integers, return the k most frequent elements.

    Example 1:

    Input: nums = [1,1,1,2,2,3], k = 2
    Output: [1,2]
    Example 2:

    Input: nums = [1], k = 1
    Output: [1]
    Note:

    You may assume k is always valid, 1 ≤ k ≤ number of unique elements.
    Your algorithm's time complexity must be better than O(n log n), where n is the array's size.
    */
    public class Q347_TopKFrequentElement : IProblem
    {
        // O(n)
        // O(n)
        public IList<int> TopKFrequent(int[] nums, int k)
        {
            var res = new List<int>();
            if (nums == null || nums.Length == 0)
            {
                return res;
            }

            var map = new Dictionary<int, int>();
            foreach (var num in nums)
            {
                map[num] = map.GetValueOrDefault(num, 0) + 1;
            }

            var bucket = new List<int>[nums.Length + 1]; // +1 to length here because the case of all same num in array. freq = length
            foreach (var e in map)
            {
                var freq = e.Value;
                if (bucket[freq] == null)
                {
                    bucket[freq] = new List<int> { e.Key };
                }
                else
                {
                    bucket[freq].Add(e.Key);
                }
            }

            for (var i = bucket.Length - 1; i >= 0; i--)
            {
                if (bucket[i] != null)
                {
                    res.AddRange(bucket[i]);
                    if (res.Count == k)
                    {
                        break;
                    }
                }
            }

            return res;
        }

        // O(nlogk)
        // O(n)
        public IList<int> TopKFrequentWithHeap(int[] nums, int k)
        {
            var res = new List<int>();
            if (nums == null || nums.Length == 0 || k ==0) {
                return res;
            } 
            var map = new Dictionary<int, int>();
            foreach(var num in nums) {
                map[num] = map.GetValueOrDefault(num, 0) + 1;
            }
            // var queue = new PriorityQueue<int, int>(Comparer<int>.Create((a, b) =>  b - a));
            var queue = new PriorityQueue<int, int>();
            foreach(var item in map) {
                queue.Enqueue(item.Key, item.Value);
                if (queue.Count > k ) queue.Dequeue();
            }
            var i =0;
            while(i < k) {
                var num = queue.Dequeue();
                res.Add(num);
                i++;
            }

            return res;
        }
        
        public void Run()
        {
            var nums = new int[] {
                9,9,10,1,1,1,1,2,2,3,11,11,11,11
            };
            var k = 2;
            var res = TopKFrequentWithHeap(nums, k);
            Console.WriteLine($"{String.Join(",", res)}");
        }
    }
}
