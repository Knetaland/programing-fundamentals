using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given a list of non-negative numbers and a target integer k, write a function to check if the array has
    a continuous subarray of size at least 2 that sums up to a multiple of k, that is, sums up to n*k where n is also an integer.

    Example 1:

    Input: [23, 2, 4, 6, 7],  k=6
    Output: True
    Explanation: Because [2, 4] is a continuous subarray of size 2 and sums up to 6.
    Example 2:

    Input: [23, 2, 6, 4, 7],  k=6
    Output: True
    Explanation: Because [23, 2, 6, 4, 7] is an continuous subarray of size 5 and sums up to 42.
    
    Note:

    The length of the array won't exceed 10,000.
    You may assume the sum of all the numbers is in the range of a signed 32-bit integer.
    */

    /*
    
    */
    public class Q523_ContinuousSubarraySum : IProblem
    {
        public bool CheckSubarraySum(int[] nums, int k)
        {
            var map = new Dictionary<int, int>
            {
                { 0, -1 } // for 2 element subarray [1,1], k =1, if found, i - map[x]
            };
            var presum = 0;
            for (var i = 0; i < nums.Length - 1; i++)
            {
                if (nums[i] + nums[i + 1] == 0)
                { // two element add up to 0, 0 is multiple of any number, also eliminate the case where k = 0 when true [5,0,0]
                    return true;
                }
            }

            for (var i = 0; i < nums.Length; i++)
            {
                presum += nums[i];
                if (presum == 0 && i >= 1) // any k can * 0 to get 0 
                {
                    return true;
                }

                if (k != 0)
                {
                    // here we use math: if (a-b)%k = 0 => a%k - b%k =0 =>  a % k == b % k 
                    var mod = presum % k;
                    if (map.ContainsKey(mod))
                    {
                        if (i - map[mod] >= 2)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        map.Add(mod, i);
                    }
                }
            }
            return false;
        }
        public void Run()
        {
            int[] nums = [1, 0];
            var k = 2;
            var res = CheckSubarraySum(nums, k);
            Console.WriteLine(res);
        }
    }
}
