using System;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    public class Q314_BinaryTreeVerticalOrder : IProblem
    {
        /*
        Given a binary tree, return the vertical order traversal of its nodes' values. (ie, from top to bottom, column by column).

        If two nodes are in the same row and column, the order should be from left to right. */

        // https://discuss.leetcode.com/topic/31954/5ms-java-clean-solution

        /*
        BFS, put node, col into queues at the same time
        Every left child access col - 1 while right child col + 1
        This maps node into different col buckets
        Get col boundary min and max on the fly
        Retrieve result from cols 
        */
        // public IList<IList<int>> VerticalOrderOld(TreeNode root)
        // {
        //     var res = new List<IList<int>>();
        //     if (root == null)
        //     {
        //         return res;
        //     }

        //     var map = new Dictionary<int, IList<int>>();
        //     var nodeQueue = new Queue<TreeNode>();
        //     var colQueue = new Queue<int>();
        //     int min = 0, max = 0;
        //     nodeQueue.Enqueue(root);
        //     colQueue.Enqueue(0);
        //     while (nodeQueue.Count > 0)
        //     {
        //         var curNode = nodeQueue.Dequeue();
        //         var curCol = colQueue.Dequeue();
        //         if (!map.ContainsKey(curCol))
        //         {
        //             map.Add(curCol, new List<int>());
        //         }

        //         map[curCol].Add(curNode.val);
        //         if (curNode.left != null)
        //         {
        //             nodeQueue.Enqueue(curNode.left);
        //             colQueue.Enqueue(curCol - 1);
        //             min = Math.Min(min, curCol - 1);
        //         }
        //         if (curNode.right != null)
        //         {
        //             nodeQueue.Enqueue(curNode.right);
        //             colQueue.Enqueue(curCol + 1);
        //             max = Math.Max(max, curCol + 1);
        //         }
        //     }

        //     for (var i = min; i <= max; i++)
        //     {
        //         res.Add(map[i]);
        //     }
        //     return res;
        // }

        public IList<IList<int>> VerticalOrder2(TreeNode root) {
            var res = new List<IList<int>>();

            if (root == null) return res;

            var queue = new Queue<(TreeNode, int)>(); // node + col
            var col = 0; // start from col 0
            var map = new Dictionary<int, IList<int>>(); // store the col -> list
            queue.Enqueue((root, col));
            var minCol = 0;
            var maxCol = 0;
            while(queue.Count > 0) {
                var node = queue.Dequeue();
                var curNode = node.Item1;
                var curCol = node.Item2;
                if (!map.ContainsKey(curCol)) {
                    map.Add(curCol, new List<int>());
                }
                map[curCol].Add(curNode.val);

                if (curNode.left != null) { 
                    queue.Enqueue((curNode.left, curCol -1));
                    minCol = Math.Min(minCol, curCol -1);
                }
                if (curNode.right != null) {
                    queue.Enqueue((curNode.right, curCol + 1));
                    maxCol = Math.Max(maxCol, curCol  + 1);
                }
            }

            for(var i = minCol; i <= maxCol; i++) {
                res.Add(map[i]);
            }

            return res;
        }

        public void Run()
        {
            // var input = new int?[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
            int?[] input = [3, 9, 8, 4, 0, 1, 7, null, null, null, null, 2, 5 ];
            var tree = Lib.ToBinaryTree(input, 0);
            Console.WriteLine($"Travelling vertically {string.Join(",", input)}");
            var res = VerticalOrder2(tree);
            Console.WriteLine($"[");
            foreach (var r in res)
            {
                Console.WriteLine(string.Join(",", r));
            }
            Console.WriteLine($"]");
        }
    }
}
