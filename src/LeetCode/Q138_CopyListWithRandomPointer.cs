using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    public class Q138_CopyListWithRandomPointer : IProblem
    {
        /*
        A linked list is given such that each node contains an additional random pointer which could point to any node in the list or null.

        Return a deep copy of the list.

        The Linked List is represented in the input/output as a list of n nodes. Each node is represented as a pair of [val, random_index] where:

        val: an integer representing Node.val
        random_index: the index of the node (range from 0 to n-1) where random pointer points to, or null if it does not point to any node.
        */
        public RandomListNode CopyRandomList(RandomListNode head)
        {
            if (head == null) return null;

            RandomListNode dummy = new RandomListNode(-1), prev = dummy, pointer = head;
            var map = new Dictionary<RandomListNode, RandomListNode>();
            while (pointer != null)
            {
                if (!map.ContainsKey(pointer))
                {
                    map.Add(pointer, new RandomListNode(pointer.label));
                }
                // copy original node
                var copy = map[pointer];
                if (pointer.random != null)
                {
                    if (!map.ContainsKey(pointer.random))
                    {
                        map.Add(pointer.random, new RandomListNode(pointer.random.label));
                    }
                    // copy random node
                   copy.random = map[pointer.random];
                }
                prev.next = copy;
                prev = prev.next;
                pointer = pointer.next;
            }
            return dummy.next;
        }

        public RandomListNode CopyRandomListEasy(RandomListNode head) {
            var map = new Dictionary<RandomListNode, RandomListNode>();
            var cur = head;
            while(cur != null) {
                map.Add(cur, new RandomListNode(cur.label));
                cur = cur.next;
            }
            cur = head;
            while(cur != null) {
                if (cur.next != null) map[cur].next = map[cur.next]; 
                if (cur.random != null) map[cur].random = map[cur.random];
                cur = cur.next;
            }
            return map[head];
        }

        public RandomListNode CopyRandomListOptimize(RandomListNode head)
        {
            if (head == null)
            {
                return null;
            }

            var cur = head;
            // 1st pass, insert copy node to the next of original node
            while (cur != null)
            {
                var copy = new RandomListNode(cur.label);
                var next = cur.next;
                copy.next = next;
                cur.next = copy;
                cur = cur.next.next;
            }
            // 2nd pass, copy random
            cur = head;
            while (cur != null)
            {
                if (cur.random != null)
                {
                    var copy = cur.next;
                    copy.random = cur.random.next; // the copy of the random will be on the next of original random 
                }
                cur = cur.next.next;
            }

            var dummy = new RandomListNode(-1);
            cur = head;
            var prev = dummy;
            while (cur != null)
            {
                prev.next = cur.next; // pick copied node
                cur.next = cur.next.next;
                prev = prev.next; // set prev to current copied node
                cur = cur.next;
            }
            return dummy.next;
        }
        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
