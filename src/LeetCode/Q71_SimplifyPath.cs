using System;
using System.Collections.Generic;
using System.Text;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    https://leetcode.com/problems/simplify-path/description/

   Given an absolute path for a file (Unix-style), simplify it.

   For example,
   path = "/home/", => "/home"
   path = "/a/./b/../../c/", => "/c"
   */
    public class Q71_SimplifyPath : IProblem
    {
        // O(N)
        // O(N)
        public string SimplifyPath(string path)
        {
            var stack = new Stack<string>();
            var dirs = path.Split('/', StringSplitOptions.RemoveEmptyEntries);
            foreach (var dir in dirs)
            {
                if (dir == "..")
                {
                    if (stack.Count > 0)
                    {
                        stack.Pop();
                    }
                }
                else if (dir != "." && !string.IsNullOrWhiteSpace(dir))
                {
                    stack.Push(dir);
                }
            }

            if (stack.Count == 0)
            {
                return "/";
            }

            // var sb = new StringBuilder();
            var res = new LinkedList<string>();
            while (stack.Count > 0)
            {
                var dir = stack.Pop();
                // sb.Insert(0, $"/{dir}");
                res.AddFirst($"/{dir}");
            }
            return string.Join("", res);
            // return sb.ToString();
        }
        public void Run()
        {
            var path = "/a/./b/../../c/";
            var res = SimplifyPath(path);
            Console.WriteLine(res);
        }
    }
}
