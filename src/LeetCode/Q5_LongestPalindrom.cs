using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.

    Example 1:

    Input: "babad"
    Output: "bab"
    Note: "aba" is also a valid answer.
    Example 2:

    Input: "cbbd"
    Output: "bb"
    */
    public class Q5_LongestPalindrom : IProblem
    {
        public string LongestPalindrom(string s)
        {
            if (string.IsNullOrEmpty(s) || s.Length < 2)
            {
                return s;
            }

            int i = 0, leftMost = 0, rightMost = 0, n = s.Length;

            while (i < n)
            {
                int start = i;
                while (i+1 < n && s[start] == s[i+1])
                {
                    i++;
                }

                var end = i;
                while (start >= 0 && end < n && s[start] == s[end])
                {
                    if (rightMost - leftMost < end - start)
                    {
                        rightMost = end;
                        leftMost = start;
                    }
                    start--;
                    end++;
                }
                i++;
            }
            return s.Substring(leftMost, rightMost - leftMost + 1);
        }

        public void Run()
        {
            var input = "aa";
            var s = LongestPalindrom(input);
        }
    }
}
