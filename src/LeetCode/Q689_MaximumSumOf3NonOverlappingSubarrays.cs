using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q689_MaximumSumOf3NonOverlappingSubarrays : IProblem
    {
        /*
        In a given array nums of positive integers, find three non-overlapping subarrays with maximum sum.

        Each subarray will be of size k, and we want to maximize the sum of all 3*k entries.

        Return the result as a list of indices representing the starting position of each interval (0-indexed). If there are multiple answers, return the lexicographically smallest one.

        Example:

        Input: [1,2,1,2,6,7,5,1], 2
        Output: [0, 3, 5]
        Explanation: Subarrays [1, 2], [2, 6], [7, 5] correspond to the starting indices [0, 3, 5].
        We could have also taken [2, 1], but an answer of [1, 3, 5] would be lexicographically larger.
        
        */

        //https://www.cnblogs.com/grandyang/p/8453386.html
        public int[] MaxSumOfThreeSubarrays(int[] nums, int k)
        {
            if (nums == null || nums.Length == 0)
            {
                return new int[0];
            }

            var preSum = new int[nums.Length - k + 1];
            var n = preSum.Length;
            for (var i = 0; i < k; i++)
            {
                preSum[0] += nums[i];
            }
            for (var i = 1; i < n; i++)
            {
                preSum[i] = preSum[i - 1] + nums[i + k - 1] - nums[i - 1]; // add current index on nums (i + k -1), and remove prev num outside k boundary
            }
            Console.WriteLine($"{string.Join(",", preSum)}");
            int[] maxPositionL = new int[n], maxPositionR = new int[n];
            maxPositionL[0] = 0; // max is at 0 to begin with
            for (var i = 1; i < n; i++)
            {
                if (preSum[i] > preSum[maxPositionL[i - 1]])
                { // greater than previous max sum, index was stored at maxPosition
                    maxPositionL[i] = i;
                }
                else
                {
                    maxPositionL[i] = maxPositionL[i - 1];
                }
            }
            maxPositionR[n - 1] = n - 1; // max is at last index when going from right
            for (var i = n - 2; i >= 0; i--)
            {
                if (preSum[i] >= preSum[maxPositionR[i + 1]])
                {
                    maxPositionR[i] = i;
                }
                else
                {
                    maxPositionR[i] = maxPositionR[i + 1];
                }
            }

            int x = -1, y = -1, z = -1, max = int.MinValue;
            for (var i = k; i < n - k; i++)
            { // need to have k element in front and in the back
                var sum = preSum[maxPositionL[i - k]] + preSum[i] + preSum[maxPositionR[i + k]];
                if (sum > max)
                {
                    max = sum;
                    x = maxPositionL[i - k];
                    y = i;
                    z = maxPositionR[i + k];
                }
            }
            return new int[] { x, y, z };
        }

        public void Run()
        {
            var a = new[] { 17, 8, 14, 11, 13, 9, 4, 19, 20, 6, 1, 20, 6, 5, 19, 8, 5, 16, 20, 17 };
            var k = 5;
            var res = MaxSumOfThreeSubarrays(a, k);
            Console.WriteLine($"res is {string.Join(",", res)}");
        }
    }
}
