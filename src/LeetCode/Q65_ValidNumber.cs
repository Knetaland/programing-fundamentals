using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q65_ValidNumber : IProblem
    {
        public bool IsNumber(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return false;
            }
            // check leading space
            int i = 0, n = s.Length;
            while (i < n && char.IsWhiteSpace(s[i]))
            {
                i++;
            }
            // check +/- sign
            if (i < n && (s[i] == '+' || s[i] == '-'))
            {
                i++;
            }

            var hasDigits = false;
            while (i < n && char.IsDigit(s[i]))
            {
                i++;
                hasDigits = true;
            }
            // check decimals
            if (i < n && s[i] == '.')
            {
                i++; // go past .
                // check decimals
                while (i < n && char.IsDigit(s[i]))
                {
                    hasDigits = true;
                    i++;
                }
            }
            // check e
            if (i < n && (s[i] == 'e' || s[i] == 'E') && hasDigits) // must have digit in front of e
            {
                i++;
                hasDigits = false;
                // check +/- sign
                if (i < n && (s[i] == '+' || s[i] == '-'))
                {
                    i++;
                }

                while (i < n && char.IsDigit(s[i]))
                {
                    i++;
                    hasDigits = true;
                }
            }

            // handle trailing space
            while (i < n && char.IsWhiteSpace(s[i]))
            {
                i++;
            }

            return hasDigits && i == s.Length;
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
