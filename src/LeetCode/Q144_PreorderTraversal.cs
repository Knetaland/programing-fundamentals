using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    public class Q144_PreorderTraversal : IProblem
    {
        public IList<int> PreorderTraversal(TreeNode root)
        {
            var res = new List<int>();
            if (root == null)
            {
                return res;
            }

            var stack = new Stack<TreeNode>();
            stack.Push(root);
            while (stack.Count > 0)
            {
                var node = stack.Pop();
                res.Add(node.val);
                if (node.right != null)
                {
                    stack.Push(node.right);
                }

                if (node.left != null)
                {
                    stack.Push(node.left);
                }
            }
            return res;
        }

        private IList<int> PreOrderRecursive(TreeNode root)
        {
            var res = new List<int>();
            Recurse(root, res);
            return res;
        }
        private void Recurse(TreeNode root, IList<int> list)
        {
            if (root == null)
            {
                return;
            }

            list.Add(root.val);
            Recurse(root.left, list);
            Recurse(root.right, list);
        }
        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
