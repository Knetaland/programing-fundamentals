using System;
using System.Collections.Generic;
using System.Text;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

/*
O(nk Log k) , k = list count
 */
namespace LeetCode
{
    public class Q23_MergeKSortedLists : IProblem
    {
        //http://bangbingsyb.blogspot.com/2014/11/leetcode-merge-k-sorted-lists.html
        public ListNode MergeKLists(ListNode[] lists)
        {
            if (lists == null || lists.Length == 0)
            {
                return null;
            }
            return divideConquer(lists, 0, lists.Length -1);
        }
        // each divide split in half => logk splits
        // first k/2 nodes, each merge 2*n, where n is the size of each linkedlist,
        // then k/4 node, each merge 4*n => k/4 * 4n = kn
        // => kn * logk
        // Space: O(logk) recursive call stack
        private ListNode divideConquer(ListNode[] lists, int l, int r) {
            Console.WriteLine($"D&C for {l} and {r}");
            if (l > r) return null; // not valid
            if (l == r) return lists[l]; // only 1 element
            
            var mid = l + (r-l) /2;
            Console.WriteLine("mid is " + mid);
            var left = divideConquer(lists, l , mid);
            var head = new ListNode(-1);
            head.next = left;
            var sb = new StringBuilder();
            while(left != null) {
                sb.Append($"{left.val},");
                left = left.next;
            }
            Console.WriteLine($"left is {sb.ToString()}");
            left = head.next;
            var right = divideConquer(lists, mid+1, r);
            head.next = right;
            sb = new StringBuilder();
             while(right != null) {
                sb.Append($"{right.val},");
                right = right.next;
            }
            
            Console.WriteLine($"right is {sb.ToString()}");
            right = head.next;
            return MergeTwoLists(left, right);
        }


        // pq enqueue/dequeue logk, need to loop through each node k*n, => knlogk
        // space: k
        private ListNode MergeKListsWithPq(ListNode[] lists) {
            if (lists == null || lists.Length == 0)
            {
                return null;
            }

            var pq = new PriorityQueue<ListNode, int>();
            foreach(var head in lists) {
                if (head != null) pq.Enqueue(head, head.val);
            }
            var dummy = new ListNode(-1);
            var pointer = dummy;
            while(pq.Count > 0) {
                var top = pq.Dequeue();
                pointer.next = top;
                pointer = pointer.next;

                if (top.next != null) {
                    pq.Enqueue(top.next, top.next.val);
                }
            }
            return dummy.next;
        }

        private ListNode MergeTwoLists(ListNode l1, ListNode l2)
        {
            var q21 = new Q21_MergeTwoSortedList();
            return q21.MergeTwoLists(l1, l2);
        }

        public void Run()
        {
            var input = new[] {
                Lib.ToListNode(new[] {0,2,7,10,32,41}),
                Lib.ToListNode(new[] {1,3,4,6,9,10}),
                Lib.ToListNode(new[] {5,9,21,23,24,31}),
                Lib.ToListNode(new[] {11,14,16,19,81,100}),
                Lib.ToListNode(new[] {21,22,23,24,24,24})
            };
            Console.WriteLine("Input [");
            foreach (var node in input)
            {
                Lib.PrintList(node);
            }
            Console.WriteLine("] ==> ");

            Lib.PrintList(MergeKLists(input));
        }
    }
}
