using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q1152_TrackUser : IProblem
    {
        /*
        We are given some website visits: the user with name username[i] visited the website website[i] at time timestamp[i].

        A 3-sequence is a list of websites of Length 3 sorted in ascending order by the time of their visits.  (The websites in a 3-sequence are not necessarily distinct.)

        Find the 3-sequence visited by the largest number of users. If there is more than one solution, return the lexicographically smallest such 3-sequence.


        Input: 
            username = ["joe","joe","joe","james","james","james","james","mary","mary","mary"],
            timestamp = [1,2,3,4,5,6,7,8,9,10],
            website = ["home","about","career","home","cart","maps","home","home","about","career"]
        Output: ["home","about","career"]
        Explanation: 
        The tuples in this example are:
        ["joe", 1, "home"]
        ["joe", 2, "about"]
        ["joe", 3, "career"]
        ["james", 4, "home"]
        ["james", 5, "cart"]
        ["james", 6, "maps"]
        ["james", 7, "home"]
        ["mary", 8, "home"]
        ["mary", 9, "about"]
        ["mary", 10, "career"]
        The 3-sequence ("home", "about", "career") was visited at least once by 2 users.
        The 3-sequence ("home", "cart", "maps") was visited at least once by 1 user.
        The 3-sequence ("home", "cart", "home") was visited at least once by 1 user.
        The 3-sequence ("home", "maps", "home") was visited at least once by 1 user.
        The 3-sequence ("cart", "maps", "home") was visited at least once by 1 user.
        */

        public IList<string> mostVisitedPattern(string[] username, int[] timestamp, string[] website) {
            var map = new Dictionary<string, SortedDictionary<int, string>>();
            for (int i = 0; i < username.Length; i++) {
                var user = username[i];
                if (!map.ContainsKey(user)) {
                    map.Add(user, new SortedDictionary<int, string>());
                }
                map[user].Add(timestamp[i], website[i]);
            }
                 
            int maxCount = 0;
            List<string> res = new List<string>();
            var trackMap = new Dictionary<string, int>();
            var strListMap = new Dictionary<string, List<string>>();

            foreach(var item in map) {
                var sites = item.Value.Values.ToList();
                var sequence = new List<List<string>>();
                // getSequence(sites, 0, new List<string>(), sequence);
                getSequenceContinuous(sites, sequence);
                foreach(var se in sequence) {
                    Console.WriteLine($"[{string.Join("-", se)}]");
                    var key = string.Join("->", se);
                    if (!trackMap.ContainsKey(key)) {
                        trackMap.Add(key,0);
                    }
                    trackMap[key]++;
                    if (trackMap[key] > maxCount) {
                        maxCount = trackMap[key];
                        res = se;
                    }
                }
            }

            return res;
        }

        public void getSequence(List<string> list, int start, List<string> curList, List<List<string>> res) {
            if (curList.Count == 3) {
                res.Add(new List<string>(curList));
                return ;
            }
            for(var i = start; i < list.Count; i++) {
                curList.Add(list[i]);
                getSequence(list, i+1, curList, res);
                curList.RemoveAt(curList.Count -1);
            }
        }

        public void getSequenceContinuous(List<string> list, List<List<string>> res) {
            for(var i = 0; i < list.Count - 2; i++) {
                res.Add(new List<string>{list[i], list[i+1], list[i+2]});
            }
        }

        public void Run() {
            var username = new [] {"joe","joe","joe","james","james","james","james","mary","mary","mary"};
            var timestamp = new [] {1,2,3,4,5,6,7,8,9,10};
            var website = new [] { "home","about","career","home","cart","maps","home","home","about","career"};

            var res = mostVisitedPattern(username, timestamp, website);
            Console.WriteLine($"[{string.Join(",", res)}]");

        }
    }
}