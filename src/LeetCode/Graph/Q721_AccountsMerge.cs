using System;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode.Graph
{
    public class Q721_AccountsMerge
    {
        /*
        Given a list accounts, each element accounts[i] is a list of strings, where the first element accounts[i][0] is a name, and the rest of the elements are emails representing emails of the account.

        Now, we would like to merge these accounts. Two accounts definitely belong to the same person if there is some email that is common to both accounts. Note that even if two accounts have the same name, they may belong to different people as people could have the same name. A person can have any number of accounts initially, but all of their accounts definitely have the same name.

        After merging the accounts, return the accounts in the following format: the first element of each account is the name, and the rest of the elements are emails in sorted order. The accounts themselves can be returned in any order.

        Example 1:
        Input: 
        accounts = [["John", "johnsmith@mail.com", "john00@mail.com"], ["John", "johnnybravo@mail.com"], ["John", "johnsmith@mail.com", "john_newyork@mail.com"], ["Mary", "mary@mail.com"]]
        Output: [["John", 'john00@mail.com', 'john_newyork@mail.com', 'johnsmith@mail.com'],  ["John", "johnnybravo@mail.com"], ["Mary", "mary@mail.com"]]
        Explanation: 
        The first and third John's are the same person as they have the common email "johnsmith@mail.com".
        The second John and Mary are different people as none of their email addresses are used by other accounts.
        We could return these lists in any order, for example the answer [['Mary', 'mary@mail.com'], ['John', 'johnnybravo@mail.com'], 
        ['John', 'john00@mail.com', 'john_newyork@mail.com', 'johnsmith@mail.com']] would still be accepted.
        Note:

        The length of accounts will be in the range [1, 1000].
        The length of accounts[i] will be in the range [1, 10].
        The length of accounts[i][j] will be in the range [1, 30].
        */
        public IList<IList<string>> AccountsMerge(IList<IList<string>> accounts)
        {
            var (g, accountMap) = Build(accounts);
            var visited = new HashSet<string>();
            var res = new List<IList<string>>();
            foreach (var email in accountMap.Keys)
            {
                if (!visited.Contains(email))
                {
                    var list = new List<string>();
                    Dfs(list, visited, g, email);
                    list.Sort(StringComparer.Ordinal);
                    list.Insert(0, accountMap[email]);
                    res.Add(list);
                }
            }
            return res;
        }

        public IList<IList<string>> UnionFind(IList<IList<string>> accounts)
        {
            var parents = new Dictionary<string, string>();
            var owner = new Dictionary<string, string>();

            foreach (var a in accounts)
            {
                var name = a[0];
                for (var i = 1; i < a.Count; i++)
                {
                    owner[a[i]] = name;
                    parents[a[i]] = a[i]; //  point to itself first
                }
            }

            foreach (var a in accounts)
            {
                var parent = Find(parents, a[1]);
                for (var i = 2; i < a.Count; i++)
                {
                    parents[Find(parents, a[i])] = parent;
                }
            }

            var unions = new Dictionary<string, HashSet<string>>();
            foreach (var a in accounts)
            {
                var parent = Find(parents, a[1]);
                for (var i = 1; i < a.Count; i++)
                {
                    if (!unions.ContainsKey(parent))
                    {
                        unions.Add(parent, new HashSet<string>());
                    }
                    unions[parent].Add(a[i]);
                }
            }

            var res = new List<IList<string>>();
            foreach (var item in unions)
            {
                var emails = item.Value.ToList();
                emails.Sort(StringComparer.Ordinal);
                emails.Insert(0, owner[item.Key]);
                res.Add(emails);
            }
            return res;
        }

        string Find(Dictionary<string, string> p, string email)
        {
            return p[email] == email ? email : Find(p, p[email]);
        }

        private void Dfs(IList<string> list, HashSet<string> visited, Dictionary<string, HashSet<string>> g, string email)
        {
            if (!visited.Contains(email))
            {
                visited.Add(email);
                list.Add(email);
                foreach (var neighbor in g[email])
                {
                    Dfs(list, visited, g, neighbor);
                }
            }
        }

        private (Dictionary<string, HashSet<string>>, Dictionary<string, string>) Build(IList<IList<string>> accounts)
        {
            var g = new Dictionary<string, HashSet<string>>();
            var accountMap = new Dictionary<string, string>();
            foreach (var emailAccounts in accounts)
            {
                var name = emailAccounts[0];

                for (var i = 1; i < emailAccounts.Count; i++)
                {
                    var email = emailAccounts[i];
                    if (!g.ContainsKey(email))
                    {
                        g.Add(email, new HashSet<string>());
                    }
                    accountMap[email] = name;
                    if (i > 1)
                    {
                        g[email].Add(emailAccounts[i - 1]);
                        g[emailAccounts[i - 1]].Add(email);
                    }
                }
            }
            return (g, accountMap);
        }
    }
}
