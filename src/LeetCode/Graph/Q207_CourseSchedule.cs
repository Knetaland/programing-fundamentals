using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Interfaces;

namespace LeetCode.Graph
{
    /*
    There are a total of n courses you have to take, labeled from 0 to n-1.

    Some courses may have prerequisites, for example to take course 0 you have to first take course 1, which is expressed as a pair: [0,1]

    Given the total number of courses and a list of prerequisite pairs, is it possible for you to finish all courses?

    Example 1:

    Input: 2, [[1,0]] 
    Output: true
    Explanation: There are a total of 2 courses to take. 
                To take course 1 you should have finished course 0. So it is possible.
    Example 2:

    Input: 2, [[1,0],[0,1]]
    Output: false
    Explanation: There are a total of 2 courses to take. 
                To take course 1 you should have finished course 0, and to take course 0 you should
                also have finished course 1. So it is impossible.
    Note:

    The input prerequisites is a graph represented by a list of edges, not adjacency matrices. Read more about how a graph is represented.
    You may assume that there are no duplicate edges in the input prerequisites.
    */
    public class Q210_Q207_CourseSchedule : IProblem
    {
        // 207
        public bool CanFinish(int numCourses, int[][] prerequisites)
        {
            var (graph, indegree) = BuildGraph(prerequisites, numCourses);
            var (count, _) = TopologicalSort(graph, indegree);
            return count == numCourses;
        }

        public int[] FindOrder(int numCourses, int[][] prerequisites)
        {
            var (graph, indegree) = BuildGraph(prerequisites, numCourses);
            var (count, list) = TopologicalSort(graph, indegree);
            if (count == numCourses)
            {
                return list.ToArray();
            }
            return new int[0];
        }

        public bool CanFinishUsingCycle(int numCourses, int[][] prerequisites)
        {
            var (graph, indegree) = BuildGraph(prerequisites, numCourses);
            var visitStatus = new int[numCourses];
            for (var i = 0; i < numCourses; i++)
            {
                if (HasCycle(graph, visitStatus, i))
                {
                    return false;
                }
            }
            return true;
        }

        bool HasCycle(Dictionary<int, List<int>> g, int[] v, int course)
        {
            if (v[course] == Visiting)
            {
                return true; // has cycle
            }

            if (v[course] == Visited)
            {
                return false;
            }

            v[course] = Visiting;
            if (g.ContainsKey(course))
            {
                foreach (var e in g[course])
                {
                    if (HasCycle(g, v, e))
                    {
                        return true;
                    }
                }
            }
            v[course] = Visited;
            return false;
        }
        public (int, List<int>) TopologicalSort(Dictionary<int, List<int>> g, int[] indegree)
        {
            var res = new List<int>();
            var count = 0;
            var queue = new Queue<int>();
            for (var i = 0; i < indegree.Length; i++)
            {
                if (indegree[i] == 0)
                {
                    queue.Enqueue(i);  // enqueue the index number, which is the course number
                }
            }
            while (queue.Count > 0)
            {
                var course = queue.Dequeue();
                res.Add(course);
                count++;
                if (g.ContainsKey(course))
                {
                    var higherLevelCourses = g[course];
                    foreach (var c in higherLevelCourses)
                    {
                        indegree[c]--;
                        if (indegree[c] == 0)
                        {
                            queue.Enqueue(c);
                        }
                    }
                }
            }
            return (count, res);
        }

        public (Dictionary<int, List<int>>, int[]) BuildGraph(int[][] prerequisites, int numCourses)
        {
            var inDegreeMap = new int[numCourses];
            var graph = new Dictionary<int, List<int>>();
            foreach (var pre in prerequisites)
            {
                var start = pre[1];
                var end = pre[0];

                if (!graph.ContainsKey(start))
                {
                    graph.Add(start, new List<int>());
                }
                graph[start].Add(end);
                inDegreeMap[end]++;
            }
            return (graph, inDegreeMap);
        }

        public void Run()
        {
            var preq = new[] {
                new [] {2,0},
                new [] {1,0},
                new int[] {3,2},
                new int[] {3,1}
            };
            var n = 4;
            var res = CanFinish(n, preq);
            var res2 = CanFinishUsingCycle(n, preq);
            Console.WriteLine("can finished " + res);
            Console.WriteLine("can finished " + res2);

            var orders = FindOrder(n, preq);
            Console.WriteLine("order: " + string.Join(",", orders));
        }

        private const int Visiting = 1;
        private const int Visited = 2;
    }
}
