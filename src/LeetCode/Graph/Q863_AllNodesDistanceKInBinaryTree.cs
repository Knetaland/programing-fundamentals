using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.Graph
{
    /*
    We are given a binary tree (with root node root), a target node, and an integer value K.

    Return a list of the values of all nodes that have a distance K from the target node.  The answer can be returned in any order.

    

    Example 1:

    Input: root = [3,5,1,6,2,0,8,null,null,7,4], target = 5, K = 2

    Output: [7,4,1]

    Explanation: 
    The nodes that are a distance 2 from the target node (with value 5)
    have values 7, 4, and 1.

    Note that the inputs "root" and "target" are actually TreeNodes.
    The descriptions of the inputs above are just serializations of these objects.
    */
    public class Q863_AllNodesDistanceKInBinaryTree : IProblem
    {
        public IList<int> DistanceK(TreeNode root, TreeNode target, int K)
        {
            var graph = new Dictionary<TreeNode, IList<TreeNode>>();
            BuildGraph(null, root, graph);

            var visited = new HashSet<TreeNode>();
            // var n = 0;
            var queue = new Queue<TreeNode>();
            var res = new List<int>();
            queue.Enqueue(target);
            while (queue.Count > 0)
            {
                if (K == 0)
                {
                    while (queue.Count > 0)
                    {
                        res.Add(queue.Dequeue().val);
                    }
                    return res;
                }
                var queueCount = queue.Count;
                while (queueCount > 0)
                {
                    var node = queue.Dequeue();
                    visited.Add(node);
                    //  if (n == K) { // 0 distance is itself
                    //     res.Add(node.val);
                    // }
                    if (graph.ContainsKey(node))
                    {
                        foreach (var neighbor in graph[node])
                        {
                            if (!visited.Contains(neighbor))
                            {
                                visited.Add(neighbor);
                                queue.Enqueue(neighbor);
                            }
                        }
                    }
                    queueCount--;
                }
                // n++;
                K--;
            }
            return res;
        }

        private void BuildGraph(TreeNode parent, TreeNode child, Dictionary<TreeNode, IList<TreeNode>> graph)
        {
            if (parent != null)
            {
                if (!graph.ContainsKey(parent))
                {
                    graph.Add(parent, new List<TreeNode>());
                }
                graph[parent].Add(child);

                if (!graph.ContainsKey(child))
                {
                    graph.Add(child, new List<TreeNode>());
                }
                graph[child].Add(parent);
            }
            if (child.left != null)
            {
                BuildGraph(child, child.left, graph);
            }

            if (child.right != null)
            {
                BuildGraph(child, child.right, graph);
            }
        }

        public void Run()
        {
            var tree = new TreeNode(0)
            {
                left = new TreeNode(1)
                {
                    right = new TreeNode(2)
                    {
                        right = new TreeNode(3)
                        {
                            right = new TreeNode(4)
                            {
                                left = new TreeNode(6),
                                right = new TreeNode(7)
                            }
                        }
                    }
                },
                right = new TreeNode(5)
            };
            var target = tree.left.right;  //2
            var distance = 3;

            var res = DistanceK(tree, target, distance);
            Console.WriteLine("res is " + string.Join(",", res));
        }
    }
}
