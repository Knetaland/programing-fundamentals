using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode.Graph
{
    /*
    Given an undirected graph, return true if and only if it is bipartite.

Recall that a graph is bipartite if we can split it's set of nodes into two independent subsets A and B such that every edge in the graph has one node in A and another node in B.

The graph is given in the following form: graph[i] is a list of indexes j for which the edge between nodes i and j exists.  Each node is an integer between 0 and graph.length - 1.  There are no self edges or parallel edges: graph[i] does not contain i, and it doesn't contain any element twice.

Example 1:
Input: [[1,3], [0,2], [1,3], [0,2]]
Output: true
Explanation: 
The graph looks like this:
0----1
|    |
|    |
3----2
We can divide the vertices into two groups: {0, 2} and {1, 3}.
Example 2:
Input: [[1,2,3], [0,2], [0,1,3], [0,2]]
Output: false
Explanation: 
The graph looks like this:
0----1
| \  |
|  \ |
3----2
We cannot find a way to divide the set of nodes into two independent subsets.
 

Note:

graph will have length in range [1, 100].
graph[i] will contain integers in range [0, graph.length - 1].
graph[i] will not contain i or duplicate values.
The graph is undirected: if any element j is in graph[i], then i will be in graph[j].
    */
    public class Q785_IsGraphBipartite : IProblem
    {
        public bool IsBipartite(int[][] graph)
        {
            var colors = new int[graph.Length]; // 0 uncolored, 1 color A, -1 color B.
            // color node with A, and neighbors with B. if a colored node has a different color, then not Bipartite.
            for (var i = 0; i < graph.Length; i++)
            {
                if (colors[i] == 0 && !IsValid(graph, colors, i, 1))
                {
                    return false;
                }
            }
            return true;
        }

        public bool IsValid(int[][] g, int[] colors, int node, int color)
        {
            if (colors[node] != 0)
            {
                return colors[node] == color;
            }

            colors[node] = color;
            foreach (var neighbor in g[node])
            {
                if (!IsValid(g, colors, neighbor, -1 * color))
                {
                    return false;
                }
            }
            return true;
        }

        public bool IsBipartiteBfs(int[][] graph)
        {
            var colors = new int[graph.Length];
            for (var i = 0; i < graph.Length; i++)
            {
                if (colors[i] != 0)
                {
                    continue;
                }

                colors[i] = 1;
                var queue = new Queue<int>();
                queue.Enqueue(i);
                while (queue.Count > 0)
                {
                    var node = queue.Dequeue();
                    foreach (var neighbor in graph[node])
                    {
                        if (colors[neighbor] == colors[node])
                        {
                            return false;
                        }

                        if (colors[neighbor] == 0)
                        {
                            colors[neighbor] = -1 * colors[node];
                            queue.Enqueue(neighbor);
                        }
                    }
                }
            }
            return true;
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
