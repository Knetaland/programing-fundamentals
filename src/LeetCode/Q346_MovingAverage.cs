using System.Collections.Generic;

namespace LeetCode
{
    public class Q346_MovingAverage
    {
        /*
        Given a stream of integers and a window size, calculate the moving average of all integers in the sliding window.

        Example:

        MovingAverage m = new MovingAverage(3);
        m.next(1) = 1
        m.next(10) = (1 + 10) / 2
        m.next(3) = (1 + 10 + 3) / 3
        m.next(5) = (10 + 3 + 5) / 3
        */
    }

    public class MovingAverage {
        int _size;
        Queue<int> _queue;
        double sum;
        
        public MovingAverage(int size)
        {
            _size = size;
            _queue = new Queue<int>();
            sum = 0;
        }

        public double next(int val) {
            if (_queue.Count >= _size) {
                sum -= _queue.Dequeue();
            }
            _queue.Enqueue(val);
            sum += val;
            return sum / _queue.Count;
        }
    }
}