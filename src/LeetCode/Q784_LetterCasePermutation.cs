using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q784_LetterCasePermutation : IProblem
    {
        public List<string> LetterCasePermutation(string s)
        {
            var res = new List<string>();
            BackTrack(res, "", s, 0);
            return res;
        }

        void BackTrack(List<string> res, string cur, string s, int pos)
        {
            if (pos == s.Length)
            {
                res.Add(cur);
                return;
            }

            BackTrack(res, cur + s[pos], s, pos + 1);
            if (char.IsLetter(s[pos]))
            {
                if (char.IsUpper(s[pos]))
                {
                    BackTrack(res, cur + char.ToLower(s[pos]), s, pos + 1);
                }
                else
                {
                    BackTrack(res, cur + char.ToUpper(s[pos]), s, pos + 1);
                }
            }
        }
        public void Run()
        {
            var input = "a1b2";
            var res = LetterCasePermutation(input);

            Console.WriteLine(string.Join(",", res));
        }
    }
}
