using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q628_MaxProductOfThreeNumbers : IProblem
    {
        /*
        Given an integer array, find three numbers whose product is maximum and output the maximum product.

        Example 1:

        Input: [1,2,3]
        Output: 6
        

        Example 2:

        Input: [1,2,3,4]
        Output: 24
        

        Note:

        The length of the given array will be in range [3,104] and all elements are in the range [-1000, 1000].
        Multiplication of any three numbers in the input won't exceed the range of 32-bit signed integer.
        */

        // O(NlogN)
        public int MaximumProduct(int[] nums)
        {
            Array.Sort(nums);
            return Math.Max(nums[nums.Length - 1] * nums[nums.Length - 2] * nums[nums.Length - 3], nums[0] * nums[1] * nums[nums.Length - 1]);
        }

        // O(N)
        public int MaxProductOneIteration(int[] nums)
        {
            int smallest = int.MaxValue, secondSmallest = int.MaxValue;
            int biggest = int.MinValue, secondBiggest = int.MinValue, thirdBiggest = int.MinValue;
            for (var i = 0; i < nums.Length; i++)
            {
                var num = nums[i];
                if (num > biggest)
                {
                    thirdBiggest = secondBiggest;
                    secondBiggest = biggest;
                    biggest = num;
                }
                else if (num > secondBiggest)
                {
                    thirdBiggest = secondBiggest;
                    secondBiggest = num;
                }
                else if (num > thirdBiggest)
                {
                    thirdBiggest = num;
                }

                if (num < smallest)
                {
                    secondSmallest = smallest;
                    smallest = num;
                }
                else if (num < secondSmallest)
                {
                    secondSmallest = num;
                }
            }

            return Math.Max(smallest * secondSmallest * biggest, biggest * secondBiggest * thirdBiggest);
        }

        public void Run()
        {

        }
    }
}
