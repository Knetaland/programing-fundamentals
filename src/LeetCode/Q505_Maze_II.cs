using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    There is a ball in a maze with empty spaces and walls. 
    The ball can go through empty spaces by rolling up, down, left or right, but it won't stop rolling until hitting a wall.
    When the ball stops, it could choose the next direction.

    Given the ball's start position, the destination and the maze, find the shortest distance for 
    the ball to stop at the destination. The distance is defined by the number of empty spaces traveled by the ball
    from the start position (excluded) to the destination (included). If the ball cannot stop at the destination, return -1.

    The maze is represented by a binary 2D array. 1 means the wall and 0 means the empty space. 
    You may assume that the borders of the maze are all walls. The start and destination coordinates are represented by row and 
    column indexes.

    // ask to find find the shortest distance for the ball to stop at the destination
    */

    // to use dijkstra, see java with priority queue, pick the shorter distance
    public class Q505_Maze_II : IProblem
    {
        private int[][] dirs = new[]{
            new[] {1,0},
            new[] {-1,0},
            new[] {0,-1},
            new[] {0,1}
        };

        public int ShortestDistance(int[,] maze, int[] start, int[] destination)
        {
            return Bfs(maze, start, destination);
        }

        private int Bfs(int[,] maze, int[] start, int[] destination)
        {
            int m = maze.GetLength(0), n = maze.GetLength(1);
            var distance = InitDistance(m, n);
            var queue = new Queue<Tuple<int, int>>();
            queue.Enqueue(new Tuple<int, int>(start[0], start[1]));
            distance[start[0], start[1]] = 0;
            while (queue.Count > 0)
            {
                var pos = queue.Dequeue();
                foreach (var dir in dirs)
                {
                    int x = pos.Item1, y = pos.Item2;
                    var dist = distance[x, y];
                    while (x >= 0 && y >= 0 && x < m && y < n && maze[x, y] != 1)
                    {
                        x += dir[0];
                        y += dir[1];
                        dist++;
                    }
                    x -= dir[0];
                    y -= dir[1];
                    dist--;
                    if (dist < distance[x, y])
                    {
                        distance[x, y] = dist;
                        // enqueue only if not destination. If destination, we dont want to put it in queue for extra calculation
                        if (!(x == destination[0] && y == destination[1]))
                        {
                            queue.Enqueue(new Tuple<int, int>(x, y));
                        }
                    }
                }
            }

            var res = distance[destination[0], destination[1]];
            return res == int.MaxValue ? -1 : res;
        }

        private int[,] InitDistance(int m, int n)
        {
            var distance = new int[m, n];
            for (var i = 0; i < m; i++)
            {
                for (var j = 0; j < n; j++)
                {
                    distance[i, j] = int.MaxValue;
                }
            }
            return distance;
        }

        public void Run()
        {
            var maze = new[,] {
                {0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 0, 1, 0},
                {1, 1, 0, 1, 1},
                {0, 0, 0, 0, 0},
            };
            var source = new[] { 0, 4 };
            var destination = new[] { 4, 4 };

            var res = ShortestDistance(maze, source, destination);
            Console.WriteLine("min path " + res);
        }
    }
}
