using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    /*
    Given a binary tree, collect a tree's nodes as if you were doing this: Collect and remove all leaves, repeat until the tree is empty.
    
    Example:
    Input: [1,2,3,4,5]
            1
            / \
            2   3
        / \     
        4   5    

    Output: [[4,5,3],[2],[1]]
    
    Explanation:
    1. Removing the leaves [4,5,3] would result in this tree:

            1
            / 
            2          
    2. Now removing the leaf [2] would result in this tree:
            1          
    3. Now removing the leaf [1] would result in the empty tree:
            []  
    */
    public class Q366_FindBinaryTreeLeavesGoogle : IProblem
    {
        public void Run() {
            var root = new TreeNode(1) {
                left = new TreeNode(2) {
                    left = new TreeNode(4),
                    right = new TreeNode(5) {
                        left = new TreeNode(6),
                        right = new TreeNode(8),
                    }
                },
                right = new TreeNode(3)
            };
            var res = FindLeaves(root);
            // var res = FindLeaves_ByDepth(root);
            foreach(var r in res) {
                Console.WriteLine($"[{string.Join(",", r)}]");
            }
        }

        IList<IList<int>> FindLeaves_ByDepth(TreeNode root) {
            var res = new List<IList<int>>();
            dfs(root);

            return res;

            int dfs(TreeNode node) {
                if (node == null) return -1;
                var height = Math.Max(dfs(node.left), dfs(node.right)) + 1;
                if (res.Count == height) {
                    res.Add(new List<int>());
                }
                res[height].Add(node.val);
                Console.WriteLine($"node {node.val} at height {height} added to list[height]");
                return height;
            }
        }

        // Time Complexity: n + (n-1) + (n-2) + (n-3) + ... + 3 + 2 + 1 => n(n+1)/2 = n^2
        IList<IList<int>> FindLeaves(TreeNode root) {
            var res = new List<IList<int>>();
            while(root != null) {
                var level = new List<int>();
                root = dfs(root, level);
                res.Add(level);
            }

            return res;

            TreeNode dfs(TreeNode node, IList<int> list) {
                if (node == null) return null;
                if (node.left == null && node.right == null) {
                    list.Add(node.val);
                    return null;
                }
                node.left = dfs(node.left, list);
                node.right = dfs(node.right, list);
                return node;
            }
        }
    }
}