using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q24_RemoveDuplicateInSortedArray : IProblem
    {
        public int RemoveDuplicates(int[] nums)
        {
            if (nums == null || nums.Length == 0)
            {
                return 0;
            }

            var index = 0;
            for (var i = 0; i < nums.Length; i++)
            {
                if (nums[index] != nums[i])
                {
                    nums[++index] = nums[i];
                }
            }

            return index + 1;
        }
        public void Run()
        {

        }
    }
}
