using System;
using System.Text;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q38_CountAndSay : IProblem
    {
        public string CountAndSay(int n)
        {
            if (n <= 0)
            {
                return string.Empty;
            }

            var start = 1;
            var res = "1";
            while (start < n)
            {
                var count = 1;
                var cur = res[0];
                var sb = new StringBuilder();
                for (var i = 1; i < res.Length; i++)
                {
                    if (res[i] != cur) // char changed, record count and char
                    {
                        sb.Append($"{count}{cur}");
                        cur = res[i];
                        count = 1;
                    }
                    else
                    {
                        count++;
                    }
                }
                sb.Append($"{count}{cur}"); // end of string, append count+char
                res = sb.ToString();
                start++;
            }
            return res;
        }

        public void Run()
        {
            var n = 5;
            Console.WriteLine($"{n}th sequence is {CountAndSay(n)}");
        }
    }
}
