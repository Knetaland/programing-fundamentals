using System;
using System.Collections;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given n non-negative integers representing an elevation map where the width of each bar is 1, compute how much water it is able to trap after raining.


    The above elevation map is represented by array [0,1,0,2,1,0,1,3,2,1,2,1]. In this case, 6 units of rain water (blue section) are being trapped. Thanks Marcos for contributing this image!

    Example:

    Input: [0,1,0,2,1,0,1,3,2,1,2,1]
    Output: 6
    */
    public class Q42_TrappingRainWater : IProblem
    {
        public int Trap(int[] height)
        {
            int left = 0, right = height.Length - 1, level = 0, total = 0;
            while (left + 1 < right && height[left] <= height[left + 1])
            {
                left++;
            }

            while (right - 1 > left && height[right] <= height[right - 1])
            {
                right--;
            }

            while (left < right)
            {
                if (height[left] < height[right])
                {
                    level = height[left] > level ? height[left] : level;
                    var water = Math.Max(0, level - height[left]);
                    total += water;
                    left++;
                }
                else
                {
                    level = height[right] > level ? height[right] : level;
                    var water = Math.Max(0, level - height[right]);
                    total += water;
                    right--;
                }
            }
            return total;
        }

        public int TrapWithMonotonicStack(int[] height) {
            // needs left wall, bottom, right wall to store water
            var stack = new Stack<int>();
            var total = 0;
            for(var i = 0; i < height.Length; i++) {
                while(stack.Count > 0 && 
                 height[stack.Peek()] < height[i] // wall elevated, can store water now
                 ) {
                    var bottom = height[stack.Pop()];
                    if (stack.Count ==0) {
                        // no left wall, can't store water
                        break;
                    }
                    var leftWall = stack.Peek();
                    var wallHeight = Math.Min(height[leftWall], height[i]); // min height of left and right wall
                    var depth = wallHeight - bottom;
                    var width = i - leftWall - 1;
                    total += depth * width;
                 }
                stack.Push(i); // push the index
            }
            return total;
        }
        public void Run()
        {
            int[] height = [0,1,0,2,1,0,1,3,2,1,2,1];

            var res = TrapWithMonotonicStack(height);

            Console.WriteLine(res);
        }
    }
}
