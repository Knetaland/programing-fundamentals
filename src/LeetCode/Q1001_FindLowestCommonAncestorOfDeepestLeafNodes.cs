using System;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    // http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=148413&pid=2068421&page=1&extra=page%3D1%26filter%3Dsortid%26sortid%3D311#pid2068421
    public class Q1001_FindLowestCommonAncestorOfDeepestLeafNodes : IProblem
    {
        public GTreeNode Find(GTreeNode root)
        {
            if (root == null || root.children.Count == 0)
            {
                return root;
            }

            return Helper(root).node;
        }

        private GTreeNodeWrapper Helper(GTreeNode root)
        {
            if (root.children.Count == 0)
            {
                return new GTreeNodeWrapper(root, 1);
            }

            var maxDepth = int.MinValue;
            var r = new GTreeNodeWrapper(root, maxDepth);
            foreach (var child in root.children)
            {
                var temp = Helper(child);
                if (temp.maxHeight > maxDepth)
                {
                    maxDepth = temp.maxHeight;
                    r.node = temp.node;
                    r.maxHeight = temp.maxHeight + 1;
                }
                else if (temp.maxHeight == maxDepth)
                {
                    r.node = root;
                }
            }
            return r;
        }

        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
