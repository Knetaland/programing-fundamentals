using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given an array of non-negative integers, you are initially positioned at the first index of the array.

    Each element in the array represents your maximum jump length at that position.

    Determine if you are able to reach the last index.

    Example 1:

    Input: [2,3,1,1,4]
    Output: true
    Explanation: Jump 1 step from index 0 to 1, then 3 steps to the last index.
    Example 2:

    Input: [3,2,1,0,4]
    Output: false
    Explanation: You will always arrive at index 3 no matter what. Its maximum
                jump length is 0, which makes it impossible to reach the last index.
    */
    public class Q55_JumpGame : IProblem
    {
        public bool CanJump(int[] nums)
        {
            if (nums == null || nums.Length == 0)
            {
                return false;
            }
            // max is the max distance to jump to 
            var max = nums[0];
            for (var i = 0; i <= max; i++)
            {
                // max can past last element
                if (max >= nums.Length - 1)
                {
                    return true;
                }
                max = Math.Max(i + nums[i], max);
            }
            return false;
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
