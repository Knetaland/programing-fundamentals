using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    In an alien language, surprisingly they also use english lowercase letters, but possibly in a different order. The order of the alphabet is some permutation of lowercase letters.

    ven a sequence of words written in the alien language, and the order of the alphabet, return true if and only if the given words are sorted lexicographicaly in this alien language.
    */
    public class Q953_VerifyingAnAlienDictionary : IProblem
    {
        public void Run()
        {
            throw new System.NotImplementedException();
        }

        public bool IsAlienSorted(string[] words, string order)
        {
            var map = BuildMap(order);
            for (var i = 0; i < words.Length - 1; i++)
            {
                if (IsBigger(words[i], words[i + 1], map))
                {
                    return false;
                }
            }
            return true;
        }

        public bool IsBigger(string a, string b, Dictionary<char, int> map)
        {
            var i = 0;
            while (i < a.Length && i < b.Length)
            {
                if (a[i] != b[i])
                {
                    if (map[a[i]] > map[b[i]])
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                i++;
            }
            return a.Length > b.Length;
        }

        public Dictionary<char, int> BuildMap(string order)
        {
            var map = new Dictionary<char, int>();
            for (var i = 0; i < order.Length; i++)
            {
                map[order[i]] = i;
            }
            return map;
        }
    }
}
