using System;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

    Example:

    Input: [-2,1,-3,4,-1,2,1,-5,4],
    Output: 6
    Explanation: [4,-1,2,1] has the largest sum = 6.
    Follow up:

    If you have figured out the O(n) solution, try coding another solution using the divide and conquer approach, which is more subtle.
    */
    public class Q53_MaxSubArray : IProblem
    {
        public int MaxSubArray(int[] nums)
        {
            var res = new List<int>();
            var path = new List<int>();
            var max = nums[0];
            var sum = nums[0];
            res.Add(0);
            for (var i = 1; i < nums.Length; i++)
            {
                if (sum + nums[i] > nums[i])
                {
                    Console.WriteLine($"adding number {nums[i]} at index {i} to get sum {sum + nums[i]}");
                    res.Add(nums[i]);
                }
                else
                {
                    res.Clear();
                    Console.WriteLine($"number {nums[i]} at index {i} is new greater sum");
                    res.Add(nums[i]);
                }
                sum = Math.Max(sum + nums[i], nums[i]);
                if (sum > max)
                {
                    Console.WriteLine($"new max is detected {sum}: {string.Join(",", res)}");
                    path = new List<int>(res);
                }
                max = Math.Max(max, sum);
            }
            Console.WriteLine($"new max is detected {max}: {string.Join(",", path)}");
            return max;
        }

        public int Dp(int[] nums)
        {
            var dp = new int[nums.Length];
            dp[0] = nums[0];
            var max = nums[0];
            for (var i = 1; i < nums.Length; i++)
            {
                dp[i] = Math.Max(dp[i - 1] + nums[i], nums[i]);
                max = Math.Max(dp[i], max);
            }
            return max;
        }

        public int DivideAndConquer(int[] nums)
        {
            return DACHelper(nums, 0, nums.Length - 1);
        }
        public int DACHelper(int[] nums, int left, int right)
        {
            if (right <= left)
            {
                return nums[left];
            }

            var mid = left + (right - left) / 2;
            var lMax = DACHelper(nums, left, mid - 1);
            var rMax = DACHelper(nums, mid + 1, right);
            int mMax = nums[mid], temp = nums[mid];
            for (var i = mid - 1; i >= left; i--)
            {
                temp += nums[i];
                mMax = Math.Max(mMax, temp);
            }
            temp = mMax;
            for (var i = mid + 1; i <= right; i++)
            {
                temp += nums[i];
                mMax = Math.Max(mMax, temp);
            }
            return Math.Max(mMax, Math.Max(lMax, rMax));
        }

        public void Run()
        {
            var input = new[] { -2, 1, -3, 4, -1, 2, 1, -5, 4 };
            using (Lib.UseTimer())
            {
                var res = MaxSubArray(input);
                Console.WriteLine("Max sum of subarary is " + res);
            }
            using (Lib.UseTimer())
            {
                var res = Dp(input);
                Console.WriteLine("Max sum of subarary is " + res);
            }
            using (Lib.UseTimer())
            {
                var res = DivideAndConquer(input);
                Console.WriteLine("Max sum of subarary is " + res);
            }
        }
    }
}
