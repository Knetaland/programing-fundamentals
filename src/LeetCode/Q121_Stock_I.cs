using System;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q121_Stock_I : IProblem
    {
        /*
        Say you have an array for which the ith element is the price of a given stock on day i.

        If you were only permitted to complete at most one transaction (i.e., buy one and sell one share of the stock), design an algorithm to find the maximum profit.

        Note that you cannot sell a stock before you buy one.

        Example 1:

        Input: [7,1,5,3,6,4]
        Output: 5
        Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
                    Not 7-1 = 6, as selling price needs to be larger than buying price.
        Example 2:

        Input: [7,6,4,3,1]
        Output: 0
        Explanation: In this case, no transaction is done, i.e. max profit = 0.
        */
        public int MaxProfit(int[] prices)
        {
            if (prices == null || prices.Length <= 1)
            {
                return 0;
            }

            var lowest = prices[0];
            var max = 0;
            var lowestIndex = 0;
            for (var i = 1; i < prices.Length; i++)
            {
                if (prices[i] < lowest)
                {
                    lowest = prices[i];
                    lowestIndex = i;
                }
                else
                {
                    var curProfit = prices[i] - lowest;
                    if (curProfit > max)
                    {
                        max = curProfit;
                        Console.WriteLine($"Bought on day {lowestIndex} with price {lowest} and sold on day {i} with price {prices[i]} for profit of {curProfit}");
                    }
                }
            }
            return max;
        }

        public bool BiggestDropInRange(int[] nums, int range) {
            if (nums.Length == 0 || nums == null) return false;
            
            int max = nums[0], maxIndex = 0, biggestDrop = 0, maxDropRange = 0;
            for(var i = 0; i < nums.Length; i++) {
                if (nums[i] >= max) {
                    max = nums[i];
                    maxIndex = i;
                } else {
                    var drop = max - nums[i];
                    if (drop >= biggestDrop) {
                        biggestDrop = drop;
                        maxDropRange = i - maxIndex;
                        // record the index
                        Console.WriteLine($"max drop happens btw indexes {maxIndex} and {i}");
                    }
                }
            }
            return maxDropRange <= range;
        }
        public void Run()
        {
            int[] s =  [7,8,5,3,6,4];
            var range = 2;
            var res = BiggestDropInRange(s, range);
            Console.WriteLine($"within range {res}");
        }
    }
}
