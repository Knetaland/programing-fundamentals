using System;
using System.Collections.Generic;
using System.Text;
using Fundamental.Core.DataStructure;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given a non-empty string str and an integer k, rearrange the string such that the same characters are at least distance k from each other.

    All input strings are given in lowercase letters. If it is not possible to rearrange the string, return an empty string "".

    Example 1:
    str = "aabbcc", k = 3

    Result: "abcabc"

    The same letters are at least distance 3 from each other.
    Example 2:
    str = "aaabc", k = 3 

    Answer: ""

    It is not possible to rearrange the string.
    Example 3:
    str = "aaadbbcc", k = 2

    Answer: "abacabcd"

    Another possible answer is: "abcabcda"

    The same letters are at least distance 2 from each other.
   */
    public class Q358_ReArrangeStringsKDistanceApart : IProblem
    {
        public string ReArrange(string s, int k)
        {
            if (k <= 1 || string.IsNullOrEmpty(s))
            {
                return s;
            }

            var map = new Dictionary<char, int>();
            foreach (var c in s)
            {
                map[c] = map.GetValueOrDefault(c, 0) + 1;
            }

            var heap = new Heap<Tuple<char, int>>((a, b) => b.Item2 - a.Item2);
            foreach (var c in map)
            {
                heap.Add(new Tuple<char, int>(c.Key, c.Value));
            }

            var res = "";
            while (!heap.IsEmpty)
            {
                var sb = new StringBuilder();
                var list = new List<Tuple<char, int>>();
                for (var i = 0; i < k; i++)
                {
                    if (heap.IsEmpty)
                    {
                        return "";
                    }

                    list.Add(heap.Poll());
                }

                foreach (var item in list)
                {
                    sb.Append(item.Item1);
                    var count = item.Item2 - 1;
                    if (count > 0)
                    {
                        heap.Add(new Tuple<char, int>(item.Item1, count));
                    }
                }
                res += sb.ToString();
            }
            return res;
        }
        public void Run()
        {
            var str = "aaaddbcc";
            var k = 2;
            var res = ReArrange(str, k);
            Console.WriteLine(res);
        }
    }
}
