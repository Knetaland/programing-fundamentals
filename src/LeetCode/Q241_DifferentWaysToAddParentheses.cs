using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q241_DifferentWaysToAddParentheses : IProblem
    {
        /*
        Given a string of numbers and operators, return all possible results from computing all the different possible ways to group numbers and operators. The valid operators are +, - and *.

        Example 1:

        Input: "2-1-1"
        Output: [0, 2]
        Explanation: 
        ((2-1)-1) = 0 
        (2-(1-1)) = 2
        Example 2:

        Input: "2*3-4*5"
        Output: [-34, -14, -10, -10, 10]
        Explanation: 
        (2*(3-(4*5))) = -34 
        ((2*3)-(4*5)) = -14 
        ((2*(3-4))*5) = -10 
        (2*((3-4)*5)) = -10 
        (((2*3)-4)*5) = 10
        */
        private Dictionary<string, List<int>> _mem = new Dictionary<string, List<int>>();
        public IList<int> DiffWaysToCompute(string input)
        {
            if (_mem.ContainsKey(input))
            {
                return _mem[input];
            }

            if (string.IsNullOrEmpty(input))
            {
                return new List<int>();
            }

            var res = new List<int>();
            for (var i = 0; i < input.Length; i++)
            {
                var c = input[i];
                if (c == '+' || c == '-' || c == '*')
                {
                    var left = DiffWaysToCompute(input.Substring(0, i));
                    //  Console.WriteLine("Left is " + string.Join(",", left));
                    var right = DiffWaysToCompute(input.Substring(i + 1));
                    //  Console.WriteLine("right is " + string.Join(",", right));
                    foreach (var l in left)
                    {
                        foreach (var r in right)
                        {
                            if (c == '+')
                            {
                                res.Add(l + r);
                                //  Console.WriteLine("adding to res " + l + "+" +r);
                            }
                            else if (c == '-')
                            {
                                res.Add(l - r);
                                //  Console.WriteLine("adding to res " + l + "-" +r);
                            }
                            else if (c == '*')
                            {
                                res.Add(l * r);
                                //  Console.WriteLine("adding to res " + l + "*" +r);
                            }
                        }
                    }
                }
            }
            if (res.Count == 0)
            {
                res.Add(int.Parse(input));
            }
            return _mem[input] = res;
        }

        public void Run()
        {
            var input = "2-1-1";
            var res = DiffWaysToCompute(input);
            Console.WriteLine("res is " + string.Join(",", res));
        }
    }
}
