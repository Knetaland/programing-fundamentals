using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q37_SudokuSolver : IProblem
    {
        public void Run()
        {
            var board = new[] {
                new [] {'5','3','.','.','7','.','.','.','.'},
                new [] {'6','.','.','1','9','5','.','.','.'},
                new [] {'.','9','8','.','.','.','.','6','.'},
                new [] {'8','.','.','.','6','.','.','.','3'},
                new [] {'4','.','.','8','.','3','.','.','1'},
                new [] {'7','.','.','.','2','.','.','.','6'},
                new [] {'.','6','.','.','.','.','2','8','.'},
                new [] {'.','.','.','4','1','9','.','.','5'},
                new [] {'.','.','.','.','8','.','.','7','9'},
            };

            SolveSudoku(board);
        }

        public void SolveSudoku(char[][] board)
        {
            if (board == null || board.Length != 9 || board[0].Length != 9)
            {
                return;
            }

            var solved = Backtrack(board, 0, 0);
        }
        public bool Backtrack(char[][] board, int row, int col)
        {
            // scan line by line to find `.`
            while (row < 9 && col < 9)
            {
                if (board[row][col] == '.')
                {
                    break;
                }

                if (col == 8)
                {
                    col = 0;
                    row++;
                }
                else
                {
                    col++;
                }
            }

            if (row >= 9)
            {
                return true;
            }

            var newRow = row + col / 8; // only when col == 8, newRow = next row
            var newCol = (col + 1) % 9; // col 0 - 8
            for (var num = 1; num <= 9; num++)
            {
                if (IsValid(board, row, col, num))
                {
                    board[row][col] = (char)(num + '0'); // place a number on current `.`
                    var res = Backtrack(board, newRow, newCol);
                    if (res)
                    {
                        return true;
                    }

                    board[row][col] = '.';
                }
            }
            return false;
        }

        public bool IsValid(char[][] board, int row, int col, int num)
        {
            var numChar = num + '0';
            for (var i = 0; i < 9; i++)
            {
                if (board[row][i] == numChar || board[i][col] == numChar)
                {
                    return false;
                }
            }
            // check square
            var rowOffset = (row / 3) * 3;
            var colOffset = (col / 3) * 3;
            for (var i = 0; i < 3; i++)
            {
                for (var j = 0; j < 3; j++)
                {
                    if (board[rowOffset + i][colOffset + j] == numChar)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
