using Fundamental.Core.DataStructure;

namespace LeetCode
{
    /*
    Design a class to find the kth largest element in a stream. Note that it is the kth largest element in the sorted order, not the kth distinct element.

    Your KthLargest class will have a constructor which accepts an integer k and an integer array nums, which contains initial elements from the stream. For each call to the method KthLargest.add, return the element representing the kth largest element in the stream.

    Example:

    int k = 3;
    int[] arr = [4,5,8,2];
    KthLargest kthLargest = new KthLargest(3, arr);
    kthLargest.add(3);   // returns 4
    kthLargest.add(5);   // returns 5
    kthLargest.add(10);  // returns 5
    kthLargest.add(9);   // returns 8
    kthLargest.add(4);   // returns 8
    */
    public class Q703_KthLargestElementInAStream
    {
        public class KthLargest
        {
            private readonly int _capacity;
            private Heap<int> _heap;
            public KthLargest(int k, int[] arr)
            {
                _capacity = k;
                _heap = new Heap<int>((a, b) => a - b);
                foreach (var i in arr)
                {
                    Add(i);
                }
            }

            public int Add(int num)
            {
                if (_heap.Size < _capacity)
                {
                    _heap.Add(num);
                }
                else if (num > _heap.Top)
                {
                    _heap.Poll();
                    _heap.Add(num);
                }
                return _heap.Top;
            }
        }
    }
}
