using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q15_ThreeSum : IProblem
    {
        public IList<IList<int>> ThreeSum(int[] nums)
        {
            var res = new List<IList<int>>();
            if (nums == null || nums.Length < 3)
            {
                return res;
            }

            Array.Sort(nums);
            for (var i = 0; i < nums.Length - 1; i++)
            {
                // find target < num2 + num3
                var target = 0 - nums[i];
                int left = i + 1, right = nums.Length - 1;
                while (left < right)
                {
                    var sum = nums[left] + nums[right];
                    if (sum == target)
                    {
                        res.Add(new List<int> { nums[i], nums[left], nums[right] });
                        // skip same elements
                        #region one way
                        int currentLeft = left, currentRight = right;
                        left++;
                        right--;
                        while (left < right && nums[currentLeft] == nums[left])
                        {
                            left++;
                        }

                        while (right > left && nums[currentRight] == nums[right])
                        {
                            right--;
                        }
                        #endregion
                        #region the other way
                        //while( left + 1 < right && nums[left] == nums[left+1]) left++;
                        //while( right -1 > left && nums[right] == nums[right-1]) right--;
                        //left++;
                        //right--;
                        #endregion
                    }
                    if (sum < target)
                    {
                        left++;
                    }
                    else if (sum > target)
                    {
                        right--;
                    }
                }
                // skip same  element
                while (i + 1 < nums.Length - 1 && nums[i] == nums[i + 1])
                {
                    i++;
                }
            }
            return res;
        }
        public void Run()
        {
            var nums = new[] { -1, 0, 1, 2, -1, -4 };
            Console.WriteLine($"3 sums for array [ {string.Join(",", nums)}] with target 0 are :");
            var list = ThreeSum(nums);
            foreach (var l in list)
            {
                Console.WriteLine($"[{string.Join(",", l)}]");
            }
        }
    }

}
