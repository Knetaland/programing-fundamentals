using System;
using System.Collections.Generic;

namespace LeetCode
{
    /*
    Given an array of integers with possible duplicates, randomly output the index of a given target number. You can assume that the given target number must exist in the array.

    Note:
    The array size can be very large. Solution that uses too much extra space will not pass the judge.

    Example:

    int[] nums = new int[] {1,2,3,3,3};
    Solution solution = new Solution(nums);

    // pick(3) should return either index 2, 3, or 4 randomly. Each index should have equal probability of returning.
    solution.pick(3);

    // pick(1) should return 0. Since in the array only nums[0] is equal to 1.
    solution.pick(1);

    */
    public class Q398_RandomPickIndex
    {

    }

    public class RandomPicker
    {
        private readonly int[] _nums;
        private readonly Random _rand;
        private Dictionary<int, List<int>> _map;
        public RandomPicker(int[] nums)
        {
            _nums = nums;
            _rand = new Random();
            _map = new Dictionary<int, List<int>>();
            for (var i = 0; i < nums.Length; i++)
            {
                if (!_map.ContainsKey(nums[i]))
                {
                    _map.Add(nums[i], new List<int>());
                }
                _map[nums[i]].Add(i);
            }
        }

        public int Pick(int target)
        {
            if (!_map.ContainsKey(target))
            {
                return -1;
            }
            var list = _map[target];
            // random.Next randomly 
            // return list[_rand.Next(list.Count)];
            var count = 0;
            var res = list[0];
            foreach (var i in list)
            {
                count++;
                if (_rand.Next(count) == 0)
                {
                    res = i;
                }
            }
            return res;

            // var count = 0;
            // var res = -1;
            // for(var i = 0; i < _nums.Length; i++) {
            //     if (target != _nums[i]) continue;
            //     count++;
            //     // for example, [5,5,1,2,5,5,5]
            //     // why we can find target 5 with 1/5 possibility:
            //     // each time we use the random(0,i), the early index gets higher rate of getting picked, but also higher rate of getting replaced
            //     // 1st 5, 100%(picked) * 1/2 (replaced) * 2/3(replaced) * 3/4(replaced) * 4/5(replaced)
            //     // 2nd 5, 1/2 (picked) * 2/3(replaced) * 3/4(replaced) * 4/5(replaced) => 1/5
            //     // 3rd 5, 1/3 (picked) * 3/4(replaced) * 4/5(replaced) => 1/5
            //     // 4th 5, 1/4 (picked) * 4/5(replaced) => 1/5
            //     // 5th 5, 1/5 (picked)
            //     if(_rand.Next(count) == 0) res = i;  
            // }
            // return res;   
        }
    }
}
