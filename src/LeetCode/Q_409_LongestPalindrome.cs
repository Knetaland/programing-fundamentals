﻿using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode;

public class Q_409_LongestPalindrome : IProblem
{
    /*
    Given a string s which consists of lowercase or uppercase letters, return the length of the longest palindrome that can be built with those letters.

    Letters are case sensitive, for example, "Aa" is not considered a palindrome here

    Example 1:

    Input: s = "abccccdd"
    Output: 7
    Explanation: One longest palindrome that can be built is "dccaccd", whose length is 7.
    Example 2:

    Input: s = "a"
    Output: 1
    Explanation: The longest palindrome that can be built is "a", whose length is 1.

    */

    public int LongestPalindrome(string s) {
        var count = 0;
        if (string.IsNullOrEmpty(s)) {
            return count;
        }

        var set = new HashSet<char>();
        foreach(var c in s) {
            // found a pair, can be placed in palindrome
            if (set.Contains(c)) {
                count += 2;
                // already picked, remove so same char can be picked as palindrome later
                set.Remove(c);
            } else {
                set.Add(c);
            }
        }

        // means any char left is one single char, can pick one as part of the palindrome
        return set.Count == 0 ? count : count + 1;

    }

    public void Run() => throw new NotImplementedException();
}
