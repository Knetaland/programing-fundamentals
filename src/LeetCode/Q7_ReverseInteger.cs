using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q7_ReverseInteger : IProblem
    {
        public int Reverse(int x)
        {
            var maxBeforeBlowup = int.MaxValue / 10;
            var maxMod = int.MaxValue % 10;
            var minMod = int.MinValue % 10;
            var minBeforeBlowup = int.MinValue / 10;
            var ans = 0;
            while (x != 0)
            {
                var mod = x % 10;
                if (ans > maxBeforeBlowup ||
                    ans < minBeforeBlowup ||
                    (ans == maxBeforeBlowup && mod > maxMod) ||
                    (ans == minBeforeBlowup && mod < minMod))
                {
                    return 0;
                }
                ans = ans * 10 + mod;
                x /= 10;
            }
            return ans;
        }

        public void Run()
        {
            var x = -1534236469;
            Console.WriteLine($"{x} reverse to {Reverse(x)}");
        }
    }
}
