using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q18_FourSum : IProblem
    {
        public IList<IList<int>> FourSum(int[] nums, int target)
        {
            var res = new List<IList<int>>();
            if (nums == null && nums.Length < 4)
            {
                return res;
            }

            Array.Sort(nums);

            for (var i = 0; i < nums.Length - 3; i++)
            {
                var sum = target - nums[i];
                int left = i + 1, right = nums.Length - 1;
                while (left < right - 1)
                {
                    int innerL = left + 1, innerR = right;
                    while (innerL < innerR)
                    {
                        if (sum == nums[left] + nums[innerL] + nums[innerR])
                        {
                            res.Add(new List<int> { nums[i], nums[left], nums[innerL], nums[innerR] });
                            while (innerL + 1 < innerR && nums[innerL] == nums[innerL + 1])
                            {
                                innerL++;
                            }

                            while (innerR - 1 > innerL && nums[innerR] == nums[innerR - 1])
                            {
                                innerR--;
                            }

                            innerL++;
                            innerR--;
                        }
                        else if (nums[left] + nums[innerL] + nums[innerR] < sum)
                        {
                            innerL++;
                        }
                        else
                        {
                            innerR--;
                        }
                    }
                    while (left + 1 < right && nums[left] == nums[left + 1])
                    {
                        left++;
                    }

                    left++;
                }
                while (i + 1 < nums.Length && nums[i] == nums[i + 1])
                {
                    i++;
                }
            }
            return res;
        }

        public void Run()
        {
            var nums = new[] { 1, 0, -1, 0, -2, 2 };
            var target = 0;
            Console.WriteLine($"[{string.Join(",", nums)}] with taget {target} gives us: ");
            foreach (var v in FourSum(nums, target))
            {
                Console.WriteLine($"[{string.Join(",", v)}]");
            }
        }
    }
}
