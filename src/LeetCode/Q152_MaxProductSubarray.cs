using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q152_MaxProductSubarray : IProblem
    {
        /*

        Given an integer array nums, find the contiguous subarray within an array (containing at least one number) which has the largest product.

        Example 1:

        Input: [2,3,-2,4]
        Output: 6
        Explanation: [2,3] has the largest product 6.
        Example 2:

        Input: [-2,0,-1]
        Output: 0
        Explanation: The result cannot be 2, because [-2,-1] is not a subarray.
        */

        // solution: https://www.youtube.com/watch?v=AtzfZHb35YI
        /* similar to Q53, but this needs to consider 0 and negative number
        need to maintain max_prod, min_prod and prod.
        when looping:
             max_prod = max(max_prod * num, min_prod * num, num)
             min_pord = min(max_prod * num, min_prod * num, num)
             prod = max(prod, max_prod)
        */
        public int MaxProduct(int[] nums)
        {
            int maxProd = nums[0], minProd = nums[0], product = nums[0];
            for (var i = 1; i < nums.Length; i++)
            {
                var num = nums[i];
                var curMaxProd = maxProd * num;
                var curMinProd = minProd * num;
                maxProd = Math.Max(curMaxProd, Math.Max(curMinProd, num));
                minProd = Math.Min(curMaxProd, Math.Min(curMinProd, num));
                product = Math.Max(product, maxProd);
            }
            return product;
        }

        public void Run()
        {
            var nums = new[] { -4, -3, -2 };
            var res = MaxProduct(nums);
            Console.WriteLine($"max prod is {res}");
        }
    }
}
