using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Node = UndirectedGraphNode;
namespace LeetCode
{
    public class Q133_CloneGraph : IProblem
    {
        // O(n)
        public Node CloneGraph(Node node)
        {
            if (node == null)
            {
                return null;
            }

            var copy = new Node(node.label);
            var map = new Dictionary<Node, Node>
            {
                { node, copy }
            };
            var queue = new Queue<Node>();
            queue.Enqueue(node);
            while (queue.Count > 0)
            {
                var n = queue.Dequeue();
                foreach (var nb in n.neighbors)
                {
                    if (!map.ContainsKey(nb))
                    {
                        queue.Enqueue(nb);
                        map.Add(nb, new Node(nb.label));
                    }
                    map[n].neighbors.Add(map[nb]);
                }
            }
            return copy;
        }

        public void Run()
        {
            throw new NotImplementedException();
        }

        Dictionary<Node,Node> map = new Dictionary<Node ,Node>();

        // recursive 
        public Node Clone(Node node) {
            if (node == null) return null;
            if (map.ContainsKey(node)) return map[node];
            var copy =  new Node(node.label);
            map.Add(node, copy);
            foreach(var nb in node.neighbors) {
                copy.neighbors.Add(Clone(nb));
            }
            return copy;
        }
    }
}
