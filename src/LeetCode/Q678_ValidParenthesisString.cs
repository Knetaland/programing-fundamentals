using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q678_ValidParenthesisString : IProblem
    {
        /*
        Given a string containing only three types of characters: '(', ')' and '*', write a function to check whether this string is valid. We define the validity of a string by these rules:

        Any left parenthesis '(' must have a corresponding right parenthesis ')'.
        Any right parenthesis ')' must have a corresponding left parenthesis '('.
        Left parenthesis '(' must go before the corresponding right parenthesis ')'.
        '*' could be treated as a single right parenthesis ')' or a single left parenthesis '(' or an empty string.
        An empty string is also valid.
        Example 1:
        Input: "()"
        Output: True
        Example 2:
        Input: "(*)"
        Output: True
        Example 3:
        Input: "(*))"
        Output: True
        */

        //O(n) , O(n)
        public bool CheckValidString(string s)
        {
            var left = new Stack<int>();
            var star = new Stack<int>();
            for (var i = 0; i < s.Length; i++)
            {
                if (s[i] == '(')
                {
                    left.Push(i);
                }
                else if (s[i] == '*')
                {
                    star.Push(i);
                }
                else if (s[i] == ')')
                {
                    if (left.Count == 0 && star.Count == 0)
                    {
                        return false;
                    }

                    if (left.Count > 0)
                    {
                        left.Pop();
                    }
                    else
                    {
                        star.Pop();
                    }
                }
            }
            while (left.Count > 0 && star.Count > 0)
            {
                if (star.Peek() < left.Peek())
                {
                    return false;  // in the case of *(, it will be false.
                }

                left.Pop();
                star.Pop();
            }
            return left.Count == 0;
        }

        public bool CheckValidTwoScans(string s)
        {
            int left = 0, right = 0;
            // scan left -> right, all * as '('
            foreach (var c in s)
            {
                if (c == '(' || c == '*')
                {
                    left++;
                }
                else
                {
                    left--;
                }
                if (left < 0)
                {
                    return false; // even if we use all * as (, still not enough to offset )
                }
            }
            if (left == 0)
            {
                return true; // meaning it works, return right away
            }
            // if left > 0, we can't guarantee valid, since the count could be from *, or from (
            for (var i = s.Length - 1; i >= 0; i--)
            {
                if (s[i] == ')' || s[i] == '*')
                {
                    right++;
                }
                else
                {
                    right--;
                }
                if (right < 0)
                {
                    return false; // even if we use all * as ), still too many (
                }
            }
            // if right == 0, valid
            // if right > 0 
            // 因为之前正向遍历的时候，我们的左括号多了，我们之前说过了，多余的左括号可能是星号变的，也可能是本身就多的左括号。本身就多的左括号这种情况会在反向遍历时被检测出来，如果没有检测出来，说明多余的左括号一定是星号变的。而这些星号在反向遍历时又变做了右括号，最终导致了右括号有剩余，
            // 所以当这些星号都当作空的时候，左右括号都是对应的，即是合法的
            return true;
        }
        public void Run()
        {
            var s = "(())((())()()(*)(*()(())())())()()((()())((()))(*";
            var res = CheckValidString(s);
        }
    }
}
