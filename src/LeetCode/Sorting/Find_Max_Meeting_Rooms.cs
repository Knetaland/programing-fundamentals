﻿using System;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode;

public class Find_Max_Meeting_Rooms : IProblem
{
    public List<int> getRooms(int[] s, int[] f) {
        var meetings = new List<(int, int, int)>();
        for(var i = 0; i < s.Length; i++) {
            meetings.Add((s[i], f[i], i+1));
        }
        meetings.Sort((a,b) => a.Item2 - b.Item2);
        var preEnd = int.MinValue;
        var res = new List<int>();
        foreach(var meeting in meetings) {
            if (meeting.Item1 >= preEnd) {
                res.Add(meeting.Item3);
                preEnd = meeting.Item2;
            }
        }
        return res;
    }
    public void Run() {
        // int[] s = [1, 3, 0, 5, 8, 5], f = [2, 4, 6, 7, 9, 9];
        int[] s =[75250, 50074, 43659, 8931, 11273, 27545, 50879, 77924], f =[112960, 114515, 81825, 93424, 54316, 35533, 73383, 160252];
        var res  = getRooms(s, f);
        res.Print();
    }
}
