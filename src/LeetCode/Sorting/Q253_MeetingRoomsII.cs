using System;
using System.Collections.Generic;
using Fundamental.Core.DataStructure;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    /* https://leetcode.ca/all/253.html
     * Given an array of meeting time intervals consisting of start and end times [[s1,e1],[s2,e2],...]
      find the minimum number of conference rooms required.
      
      Example 1:

        Input: [[0, 30],[5, 10],[15, 20]]
        Output: 2
        Example 2:

        Input: [[7,10],[2,4]]
        Output: 1
       */

    public class Q253_MeetingRoomsII : IProblem
    {
        public int minMeetingRooms(int[][] intervals) {
            if (intervals == null || intervals.Length == 0) return 0;
            
            var list = new List<(int, int)>();
            foreach(var interval in intervals) {
                list.Add((interval[0], 1));
                list.Add((interval[1], -1));
            }
            list.Sort((a, b) => {
                if (a.Item1 != b.Item1) { 
                    return a.Item1 - b.Item1;
                } else {// if same time, end first, then start
                    return a.Item2 == -1 ? -1 : 1; // if is end, then first
                }
            });
            var max = 0;
            var cap = 0;
            foreach(var time in list) {
                cap += time.Item2;
                if (cap > max) max = cap;
            }
            return max;
        }

        public int minMeetingRooms(Interval[] intervals)
        {
            if (intervals == null || intervals.Length == 0)
            {
                return 0;
            }

            var list = new List<IntervalSegment>();
            // add all to list, with end indicated with bool of false
            foreach (var interval in intervals)
            {
                list.Add(new IntervalSegment(interval.start));
                list.Add(new IntervalSegment(interval.end, false));
            }
            // sort by time, also when the time are the same, the end should go before start
            list.Sort((x, y) =>
            {
                if (x.val != y.val)
                {
                    return x.val - y.val;
                }
                else
                {
                    return x.isStart ? 1 : -1; // if happens at the same time, end goes first
                }
            });
            int cur = 0, min = int.MinValue;
            foreach (var time in list)
            {
                // add 1 to stack when it is start, and remove if it is end
                if (time.isStart)
                {
                    cur++;
                }
                else
                {
                    cur--;
                }

                min = Math.Max(cur, min);
            }
            return min;
        }

        public int MinRooms(Interval[] intervals)
        {
            if (intervals == null || intervals.Length == 0)
            {
                return 0;
            }

            var startTime = new int[intervals.Length];
            var endTime = new int[intervals.Length];
            for (var i = 0; i < intervals.Length; i++)
            {
                startTime[i] = intervals[i].start;
                endTime[i] = intervals[i].end;
            }
            Array.Sort(startTime);
            Array.Sort(endTime);
            var endPos = 0;
            var res = 0;
            for (var i = 0; i < intervals.Length; i++)
            {
                if (startTime[i] < endTime[endPos])
                {
                    res++;
                }
                else
                {
                    endPos++;
                }
            }
            return res;
        }

        public int MinRoomWithHeap(Interval[] intervals) // priority queue
        {
            Array.Sort(intervals, (a, b) => a.start - b.start);
            var heap = new Heap<Interval>((a, b) => a.end - b.end);
            heap.Add(intervals[0]);
            for (var i = 1; i < intervals.Length; i++)
            {
                var earliest = heap.Poll();
                var current = intervals[i];
                if (current.start >= earliest.end)
                {
                    earliest.end = current.end;
                }
                else
                {
                    heap.Add(current);
                }
                heap.Add(earliest);
            }
            return heap.Size;
        }

        public void Run()
        {
            var input = new int[,]
            {
                {2, 3},
                {0, 2},
                {4, 7},
                {6, 7},
                {1, 2},
                {1, 2},
                {1, 2},
                {2, 4},
                {3, 5},
                {4, 6},
            };

            var intervals = input.ToIntervals();
            var min = minMeetingRooms(intervals);
            Console.WriteLine($"min rooms needed {min}");

            var min2 = MinRooms(intervals);
            Console.WriteLine($"min rooms needed {min2}");

            var min3 = MinRoomWithHeap(intervals);
            Console.WriteLine($"min rooms needed {min3}");

            int[][] itv2 = [
                [2, 3],
                [0, 2],
                [4, 7],
                [6, 7],
                [1, 2],
                [1, 2],
                [1, 2],
                [2, 4],
                [3, 5],
                [4, 6],
            ];
            var min4 = minMeetingRooms(itv2);
            min4.Print();
        }
    }
}
