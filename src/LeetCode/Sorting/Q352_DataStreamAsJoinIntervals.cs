using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    /*
    Given a data stream input of non-negative integers a1, a2, ..., an, ..., summarize the numbers seen so far as a list of disjoint intervals.

    For example, suppose the integers from the data stream are 1, 3, 7, 2, 6, ..., then the summary will be:

    [1, 1]
    [1, 1], [3, 3]
    [1, 1], [3, 3], [7, 7]
    [1, 3], [7, 7]
    [1, 3], [6, 7]
    Follow up:
    What if there are lots of merges and the number of disjoint intervals are small compared to the data stream's size?
 */
    public class Q352_DataStreamAsJoinIntervals : IProblem
    {
        public class SummaryRanges
        {
            private List<Interval> intervals;
            public SummaryRanges()
            {
                intervals = new List<Interval>();
            }

            // Time: O(n), can be improved to be O(logN), see java using treeMap/treeset
            public void addNum(int val)
            {
                var newInterval = new Interval(val, val);
                var res = new List<Interval>();
                foreach (var cur in intervals)
                {
                    // on the left, no overlap, add new interval, set cur as new interval
                    if (newInterval.end + 1 < cur.start)
                    {
                        res.Add(newInterval);
                        newInterval = cur;
                    }
                    // on the right, no overlap, add current interval, the new interval still pendding for insert
                    else if (newInterval.start > cur.end + 1)
                    {
                        res.Add(cur);
                    }
                    // overlap, set temp interval with min and max, go for next insert
                    else
                    {
                        newInterval.start = Math.Min(newInterval.start, cur.start);
                        newInterval.end = Math.Max(newInterval.end, cur.end);
                    }
                }
                res.Add(newInterval);
                intervals = res;
            }

            public List<Interval> getIntervals()
            {
                return intervals;
            }
        }

        public void Run()
        {
            var s = new SummaryRanges();
            var input = new[] { 1, 3, 7, 2, 6 };
            foreach (var i in input)
            {
                s.addNum(i);
            }

            var ss = s.getIntervals();
            foreach (var sss in ss)
            {
                Console.WriteLine($"[{sss.start}, {sss.end}]");
            }
            // Console.WriteLine($"OUtput {String.Join(",",s.getIntervals())}");
        }
    }
}
