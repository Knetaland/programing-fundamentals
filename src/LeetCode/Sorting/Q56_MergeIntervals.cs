using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    public class Q56_MergeIntervals : IProblem
    {
        // O(nlogn + n): sort, scan
        // O(1): res doesn't counts
        public IList<Interval> Merge(IList<Interval> intervals)
        {
            var res = new List<Interval>();
            if (intervals == null || intervals.Count <= 1)
            {
                return res;
            }

            var intervalsList = intervals.ToList();
            intervalsList.Sort((x, y) => x.start - y.start);

            int start = intervalsList[0].start, end = intervalsList[0].end;
            for (var i = 1; i < intervalsList.Count; i++)
            {
                var interval = intervalsList[i];
                if (interval.start <= end)
                {
                    end = Math.Max(end, intervalsList[i].end);
                }
                else
                {
                    res.Add(new Interval(start, end));
                    start = interval.start;
                    end = interval.end;
                }
            }
            res.Add(new Interval(start, end));
            return res;
        }
        public void Run()
        {
            var input = new int[,] { { 1, 3 }, { 2, 6 }, { 8, 10 }, { 15, 18 } };
            var intervals = input.ToIntervals();

            var output = Merge(intervals);

            foreach (var interval in output)
            {
                Console.WriteLine($"({interval.start}, {interval.end})");
            }
        }
    }

}
