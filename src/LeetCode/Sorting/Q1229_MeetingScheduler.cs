using System;
using Fundamental.Core.Interfaces;

namespace LeetCode.Sorting
{
    public class Q1229_MeetingScheduler: IProblem
    {
        /*
        Given the availability time slots arrays slots1 and slots2 of two people and a meeting duration duration, return the earliest time slot that works for both of them and is of duration duration.

        If there is no common time slot that satisfies the requirements, return an empty array.

        The format of a time slot is an array of two elements [start, end] representing an inclusive time range from start to end.  

        It is guaranteed that no two availability slots of the same person intersect with each other. That is, for any two time slots [start1, end1] and [start2, end2] of the same person, either start1 > end2 or start2 > end1.

        Example 1:

        Input: slots1 = [[10,50],[60,120],[140,210]], slots2 = [[0,15],[60,70]], duration = 8
        Output: [60,68]
        Example 2:

        Input: slots1 = [[10,50],[60,120],[140,210]], slots2 = [[0,15],[60,70]], duration = 12
        Output: []
        Constraints:

        1 <= slots1.length, slots2.length <= 10^4
        slots1[i].length, slots2[i].length == 2
        slots1[i][0] < slots1[i][1]
        slots2[i][0] < slots2[i][1]
        0 <= slots1[i][j], slots2[i][j] <= 10^9
        1 <= duration <= 10^6 
        */

        public int[] minAvailableDuration(int[][] slots1, int[][] slots2, int duration) {
            Array.Sort(slots1, (a, b) => a[0] - b[0]);
            Array.Sort(slots2, (a, b) => a[0] - b[0]);

            int i =0, j =0, l1 = slots1.Length, l2 = slots2.Length;
            while(i < l1 && j < l2) {
                var overlapStart = Math.Max(slots1[i][0], slots2[j][0]); // later start time
                var overlapEnd = Math.Min(slots1[i][1], slots2[j][1]); // ealier end time
                // if no overlap, overlapEnd - overlapStart will get negative number
                Console.WriteLine($"start:{overlapStart}, end:{overlapEnd}");
                if (overlapEnd - overlapStart >= duration) {
                    return new [] {overlapStart, overlapStart+duration};
                } else if (slots1[i][1] < slots2[j][1]) {  //move pointer
                    i++;
                } else {
                    j++;
                }
            }

            return new int[]{};
        }

        public void Run() {
            var slots1 = new int[][] {
                new [] {10,50},
                new [] {60,120},
                new [] {140,210},
            };
            var slots2 = new int[][] {
                new [] {0,15},
                new [] {60,70},
            };
            var duration = 8;
            var res = minAvailableDuration(slots1, slots2, duration);

            Console.WriteLine($"[{string.Join(",",res)}]");
        }
    }
}
