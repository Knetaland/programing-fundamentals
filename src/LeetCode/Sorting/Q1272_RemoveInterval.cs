using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode.Sorting
{
    /*
    Given a sorted list of disjoint intervals, each interval intervals[i] = [a, b] represents the set of real numbers x such that a <= x < b.

    We remove the intersections between any interval in intervals and the interval toBeRemoved.

    Return a sorted list of intervals after all such removals.

    Example 1:

    Input: intervals = [[0,2],[3,4],[5,7]], toBeRemoved = [1,6]
    Output: [[0,1],[6,7]]
    Example 2:

    Input: intervals = [[0,5]], toBeRemoved = [2,3]
    Output: [[0,2],[3,5]]
    Constraints:

    1 <= intervals.length <= 10^4
    -10^9 <= intervals[i][0] < intervals[i][1] <= 10^9
    Solution: Geometry
    Time complexity: O(n)
    Space complexity: O(n)
    */
    public class Q1272_RemoveInterval : IProblem
    {
        /*
        0 1 2 3 4 5 6 7
        - - 
              - -
                  - - -
                 * * * 
        - -   - -    --

        no overlap:
            - cur.end <= new.start || cur.start >= new.end
        noverlap:
            1. overlap after start
            - xxxx
                dddd
            =>xx (cur.start to new.start)
            2. overlap before end
            - xxxxxx
            - ddd
            =>   xxx (new.end to cur.end)
            3. overlap in the middle
            - xxxxxxx
            -   ddd
            =>xx   xx (case 1 + case 2)
            4. totoal overlap
            -   xxx
            -  ddddd
            - (do nothing)

        */

        // if new.start >= cur.end, add cur
        // if new.end <= cur.start, add cur, 
        // if cur.start >= new.start,  cur.start ~ new.start

        public int[][] RemoveInterval(int[][] intervals, int[] toBeRemoved) {
            var res = new List<int[]>();
            foreach(var cur in intervals) {
                // no  overlap
                if (cur[1] <= toBeRemoved[0] || cur[0] >= toBeRemoved[1]) {
                    res.Add(cur);
                } else { // overlap 
                    if (toBeRemoved[0] > cur[0]) {
                        res.Add([cur[0], toBeRemoved[0]]);
                    }
                    if (toBeRemoved[1] < cur[1]) {
                        res.Add([toBeRemoved[1], cur[1]]);
                    }
                }
            }
            return res.ToArray();
        }

        public void Run() {
            var intervals = new int [][] {
                new [] { 0, 2 },
                new [] { 3, 4 },
                new [] { 5, 7 },
            };
            var newInterval = new [] {1, 6};

            var res = RemoveInterval(intervals, newInterval);
            foreach(var interval in res) {
                Console.WriteLine($"[{interval[0]}, {interval[1]}]");
            }
            Console.WriteLine("--------- Sample 2 -----------");
            // 2
            intervals = new int [][] {
                new [] { 0, 5 },
            };
            newInterval = new [] {2, 3};
            res = RemoveInterval(intervals, newInterval);
            foreach(var interval in res) {
                Console.WriteLine($"[{interval[0]}, {interval[1]}]");
            }
        }
    }
}
