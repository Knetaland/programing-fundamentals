using System;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    /*
     * Given an array of meeting time intervals consisting of start and end times [[s1,e1],[s2,e2],...] (si < ei),
      determine if a person could attend all meetings.

        For example, Given [[0, 30],[5, 10],[15, 20]], return false.
     */
    public class Q252_MeetingRooms : IProblem
    {
        public bool canAttendMeetings(Interval[] intervals)
        {
            if (intervals == null || intervals.Length == 0)
            {
                return true;
            }
            // sort the meetings based on start time
            Array.Sort(intervals, (x, y) => x.start - y.start);

            for (var i = 0; i < intervals.Length - 1; i++)
            {
                if (intervals[i].end > intervals[i + 1].start)
                {
                    return false;
                }
            }
            return true;
        }

        public void Run()
        {
            var input = new int[,] { { 15, 20 }, { 0, 30 }, { 5, 10 }, { 2, 6 } };
            var intervals = input.ToIntervals();
            var possible = canAttendMeetings(intervals);
            Console.WriteLine("Possible? " + possible);

        }
    }
}
