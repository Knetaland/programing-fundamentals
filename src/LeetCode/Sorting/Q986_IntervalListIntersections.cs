using System;
using System.Collections.Generic;

namespace LeetCode
{
    /*
    Given two lists of closed intervals, each list of intervals is pairwise disjoint and in sorted order.

    Return the intersection of these two interval lists.

    (Formally, a closed interval [a, b] (with a <= b) denotes the set of real numbers x with a <= x <= b.  The intersection of two closed intervals is a set of real numbers that is either empty, or can be represented as a closed interval.  For example, the intersection of [1, 3] and [2, 4] is [2, 3].)

    

    Example 1:



    Input: A = [[0,2],[5,10],[13,23],[24,25]], B = [[1,5],[8,12],[15,24],[25,26]]
    Output: [[1,2],[5,5],[8,10],[15,23],[24,24],[25,25]]
    Reminder: The inputs and the desired output are lists of Interval objects, and not arrays or lists.
    */
    public class Q986_IntervalListIntersections
    {
        public int[][] IntervalIntersection(int[][] A, int[][] B)
        {
            var res = new List<int[]>();
            if (A == null || B == null || A.Length == 0 || B.Length == 0)
            {
                return [];
            }

            int a = 0, b = 0;
            while (a < A.Length && b < B.Length)
            {
                var start = Math.Max(A[a][0], B[b][0]);
                var end = Math.Min(A[a][1], B[b][1]);

                if (start <= end)
                {
                    res.Add([ start, end ]);
                }
                if (A[a][1] < B[b][1])
                {
                    a++;
                }
                else
                {
                    b++;
                }
            }
            return res.ToArray();
        }
    }
}
