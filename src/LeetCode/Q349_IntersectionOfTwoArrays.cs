using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given two arrays, write a function to compute their intersection.

    Example 1:

    Input: nums1 = [1,2,2,1], nums2 = [2,2]
    Output: [2]
    Example 2:

    Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
    Output: [9,4]
    Note:

    Each element in the result must be unique.
    The result can be in any order.
        */
    public class Q349_IntersectionOfTwoArrays : IProblem
    {
        public int[] Intersection(int[] nums1, int[] nums2)
        {
            var map = new HashSet<int>();
            foreach (var num in nums1)
            {
                map.Add(num);
            }
            var res = new HashSet<int>();
            foreach (var num in nums2)
            {
                if (map.Contains(num))
                {
                    res.Add(num);
                }
            }
            return res.ToArray();
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
