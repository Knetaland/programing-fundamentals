using System;
using Fundamental.Core.DataStructure;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given a n x n matrix where each of the rows and columns are sorted in ascending order, find the kth smallest element in the matrix.

    Note that it is the kth smallest element in the sorted order, not the kth distinct element.

    Example:

    matrix = [
    [ 1,  5,  9],
    [10, 11, 13],
    [12, 13, 15]
    ],
    k = 8,

    return 13.
    Note: 
    You may assume k is always valid, 1 ≤ k ≤ n2.
    */
    public class Q378_KthSmallestElementInASortedMatrix : IProblem
    {
        public int KthSmallestWithHeap(int[][] matrix, int k)
        {
            var heap = new Heap<int>((a, b) => b - a);
            for (var i = 0; i < matrix.Length; i++)
            {
                for (var j = 0; j < matrix[0].Length; j++)
                {
                    heap.Add(matrix[i][j]);
                    if (heap.Size > k)
                    {
                        heap.Poll();
                    }
                }
            }
            return heap.Top;
        }
        public int KthSmallest(int[][] matrix, int k)
        {
            int m = matrix.Length, n = matrix[0].Length;
            int lo = matrix[0][0], hi = matrix[m - 1][n - 1];
            while (lo < hi)
            {
                var mid = lo + (hi - lo) / 2;
                var count = 0;
                var col = n - 1;
                for (var row = 0; row < m; row++)
                { // go through each row, find count of element < mid
                    while (col >= 0 && matrix[row][col] > mid)
                    {
                        col--;  // from right to left, optimization here is since col is sorted, if value at col < mid in row i, it will be < mid in row i+1
                    }

                    count += (col + 1);
                }
                if (count < k)
                {
                    lo = mid + 1;
                }
                else
                {
                    hi = mid;
                }
            }
            return lo;
        }

        public void Run()
        {
            var matrix = new[] {
                new int[] { 1,  5,  9},
                new int[] {10, 11, 13},
                new int[] {12, 13, 15}
            };

            var k = 3;
            var res = KthSmallest(matrix, k);
            var res2 = KthSmallestWithHeap(matrix, k);
            Console.WriteLine("res is " + res);
            Console.WriteLine("res2 is " + res2);
        }
    }
}
