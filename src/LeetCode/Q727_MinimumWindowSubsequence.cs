using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given strings S and T, find the minimum (contiguous) substring W of S, so that T is a subsequence of W.

    If there is no such window in S that covers all characters in T, return the empty string "". If there are multiple such minimum-length windows, return the one with the left-most starting index.

    Example 1:

    Input: 
    S = "abcdebdde", T = "bde"
    Output: "bcde"
    Explanation: 
    "bcde" is the answer because it occurs before "bdde" which has the same length.
    "deb" is not a smaller window because the elements of T in the window must occur in order.
    

    Note:

    All the strings in the input will only contain lowercase letters.
    The length of S will be in the range [1, 20000].
    The length of T will be in the range [1, 100].
    */
    public class Q727_MinimumWindowSubsequence : IProblem
    {
        private int FindRight(string s, string t, int index)
        {
            var tIndex = 0;
            while (index < s.Length)
            {
                if (s[index] == t[tIndex])
                {
                    tIndex++;
                }
                if (tIndex == t.Length)
                {
                    break;
                }

                index++;
            }
            return index;
        }

        private int FindLeft(string s, string t, int index)
        {
            var tIndex = t.Length - 1;
            while (index >= 0)
            {
                if (s[index] == t[tIndex])
                {
                    tIndex--;
                }
                if (tIndex < 0)
                {
                    break;
                }

                index--;
            }
            return index;
        }
        public string MinWindow(string s, string t)
        {
            int min = int.MaxValue, startIndex = 0, right = 0;

            while (right < s.Length)
            {
                // use fast pointer to find the last character of t in s
                right = FindRight(s, t, right);
                // if right pointer is over than boundary
                if (right == s.Length)
                {
                    break;
                }
                // use another slow pointer to traverse from right to left until find first character of t in s
                var left = FindLeft(s, t, right);

                // if we found another subsequence with smaller length, update result
                if (right - left + 1 < min)
                {
                    min = right - left + 1;
                    startIndex = left;
                }
                // WARNING: we have to move right pointer to the next position of left pointer, NOT the next position
                // of right pointer
                right = left + 1;
            }
            return min == int.MaxValue ? "" : s.Substring(startIndex, min);

        }
        public void Run()
        {
            string s = "abcdebdde", t = "bde";
            var res = MinWindow(s, t);
            Console.WriteLine("res is " + res);
        }
    }
}
