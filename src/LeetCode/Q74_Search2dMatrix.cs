using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Write an efficient algorithm that searches for a value in an m x n matrix. This matrix has the following properties:

    Integers in each row are sorted from left to right.
    The first integer of each row is greater than the last integer of the previous row.
    For example,

    Consider the following matrix:

    [
      [1,   3,  5,  7],
      [10, 11, 16, 20],
      [23, 30, 34, 50]
    ]
    Given target = 3, return true.
        */
    public class Q74_Search2dMatrix : IProblem
    {
        public bool SearchMatrix(int[][] matrix, int target)
        {
            if (matrix == null || matrix.Length == 0 || matrix[0].Length == 0)
            {
                return false;
            }

            int row = 0, col = matrix[0].Length - 1;
            while (true)
            {
                if (row >= matrix.Length || col < 0)
                {
                    return false;
                }
                var value = matrix[row][col];
                if (value == target)
                {
                    return true;
                }

                if (target < value)
                {
                    col--;
                }
                else
                {
                    row++;
                }
            }
        }

        public bool SearchMatrixBS(int[][] matrix, int target)
        {
            if (matrix == null || matrix.Length == 0 || matrix[0].Length == 0)
            {
                return false;
            }

            int startRow = 0, endRow = matrix.Length, endCol = matrix[0].Length - 1;
            while (startRow < endRow)
            {
                var mid = startRow + (endRow - startRow) / 2;
                if (matrix[mid][endCol] < target)
                {
                    startRow = mid + 1;
                }
                else
                {
                    endRow = mid;
                }
            }
            if (endRow == matrix.Length)
            {
                return false;
            }

            var start = 0;
            var end = matrix[0].Length;
            while (start < end)
            {
                var mid = start + (end - start) / 2;
                if (matrix[endRow][mid] == target)
                {
                    return true;
                }

                if (target > matrix[endRow][mid])
                {
                    start = mid + 1;
                }
                else
                {
                    end = mid;
                }
            }
            return false;
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
