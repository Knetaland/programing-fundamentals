using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q247_StrobogrammicNumber_II : IProblem
    {
        /*
        A strobogrammatic number is a number that looks the same when rotated 180 degrees (looked at upside down).

        Find all strobogrammatic numbers that are of length = n.

        Example:

        Input:  n = 2
        Output: ["11","69","88","96"]
        */
        // see https://www.cnblogs.com/grandyang/p/5200919.html
        // recurse from i -2, 
        /*
        n = 0:   none

        n = 1:   0, 1, 8

        n = 2:   11, 69, 88, 96

        n = 3:   101, 609, 808, 906, 111, 619, 818, 916, 181, 689, 888, 986

        n = 4:   1001, 6009, 8008, 9006, 1111, 6119, 8118, 9116, 1691, 6699, 8698, 9696, 1881, 6889, 8888, 9886, 1961, 6969, 8968, 9966
        */

        // run time  O(5^n), exponential.
        // space O(n/2  + 5^(n/2)), n/2 call stack + 5^(n/2) 
        public IList<string> FindStrobogrammatic(int n)
        {
            return Find(n, n);
        }

        private IList<string> Find(int m, int n)
        {
            if (m == 0)
            {
                return new List<string> { "" };
            }

            if (m == 1)
            {
                return new List<string> { "0", "1", "8" };
            }

            var res = new List<string>();
            var cur = Find(m - 2, n);
            foreach (var val in cur)
            {
                if (m != n)
                {
                    res.Add($"0{val}0");
                }

                res.Add($"1{val}1");
                res.Add($"6{val}9");
                res.Add($"8{val}8");
                res.Add($"9{val}6");
            }
            return res;
        }

        public void Run()
        {
            var n = 10;
            var res = FindStrobogrammatic(n);
            // Console.WriteLine(string.Join(",", res));
            Console.WriteLine(res.Count);
        }
    }
}
