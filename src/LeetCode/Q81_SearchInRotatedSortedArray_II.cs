using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.

    (i.e., [0,0,1,2,2,5,6] might become [2,5,6,0,0,1,2]).

    You are given a target value to search. If found in the array return true, otherwise return false.

    Example 1:

    Input: nums = [2,5,6,0,0,1,2], target = 0
    Output: true
    Example 2:

    Input: nums = [2,5,6,0,0,1,2], target = 3
    Output: false
    Follow up:

    This is a follow up problem to Search in Rotated Sorted Array, where nums may contain duplicates.
    Would this affect the run-time complexity? How and why?

    */
    public class Q81_SearchInRotatedSortedArray_II : IProblem
    {
        public bool Search(int[] nums, int target)
        {
            if (nums == null || nums.Length == 0)
            {
                return false;
            }

            int left = 0, right = nums.Length - 1;
            while (left <= right)
            {
                var mid = left + (right - left) / 2;
                if (target == nums[mid])
                {
                    return true;
                }
                // left is sorted
                if (nums[mid] > nums[right])
                {
                    if (target < nums[mid] && target >= nums[left])
                    {
                        right = mid - 1;
                    }
                    else
                    {
                        left = mid + 1;
                    }
                }
                else if (nums[mid] < nums[right])
                { // right is sorted
                    if (target > nums[mid] && target <= nums[right])
                    {
                        left = mid + 1;
                    }
                    else
                    {
                        right = mid - 1;
                    }
                }
                else
                {
                    right--;
                }
            }
            return false;
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
