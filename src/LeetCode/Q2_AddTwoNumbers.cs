using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    /*
    You are given two non-empty linked lists representing two non-negative integers.
    The digits are stored in reverse order and each of their nodes contain a single digit.
    Add the two numbers and return it as a linked list.

    You may assume the two numbers do not contain any leading zero, except the number 0 itself.

    Example:

    Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
    Output: 7 -> 0 -> 8
    Explanation: 342 + 465 = 807.
    */
    public class Q2_AddTwoNumbers : IProblem
    {
        public ListNode AddTwoNumbers(ListNode l1, ListNode l2)
        {
            return AddTwoNumbers(l1, l2, 0);
        }

        private ListNode AddTwoNumbers(ListNode l1, ListNode l2, int co)
        {
            if (l1 == null && l2 == null && co == 0)
            {
                return null;
            }

            var v1 = l1 == null ? 0 : l1.val;
            var v2 = l2 == null ? 0 : l2.val;
            var sum = v1 + v2 + co;

            var node = new ListNode(sum % 10)
            {
                next = AddTwoNumbers(l1?.next, l2?.next, sum / 10)
            };
            return node;
        }

        public ListNode AddTwoNumbersIterative(ListNode l1, ListNode l2)
        {
            if (l1 == null || l2 == null)
            {
                return l1 ?? l2;
            }

            var p1 = l1;
            var p2 = l2;
            var co = 0;
            var dummy = new ListNode(-1);
            var head = dummy;
            while (p1 != null || p2 != null || co != 0)
            {
                var p1Val = p1?.val ?? 0;
                var p2Val = p2?.val ?? 0;
                var sum = p1Val + p2Val + co;
                var val = sum % 10;
                co = sum / 10;
                head.next = new ListNode(val);
                head = head.next;
                p1 = p1?.next;
                p2 = p2?.next;
            }
            return dummy.next;
        }

        public void Run()
        {
            var l1 = Lib.ToListNode(new[] { 2, 3, 1, 5, 6 });
            var l2 = Lib.ToListNode(new[] { 5, 8, 9, 2, 2, 9 });
            var node = AddTwoNumbers(l1, l2);
            Lib.PrintList(node);
        }
    }
}
