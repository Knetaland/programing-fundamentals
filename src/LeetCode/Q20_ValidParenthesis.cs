using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q20_ValidParenthesis : IProblem
    {
        public bool IsValid(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return true;
            }

            var stack = new Stack<char>();
            foreach (var c in s)
            {
                if (c == '{')
                {
                    stack.Push('}');
                }
                else if (c == '[')
                {
                    stack.Push(']');
                }
                else if (c == '(')
                {
                    stack.Push(')');
                }
                else if (stack.Count == 0 || stack.Pop() != c)
                {
                    return false;
                }
            }
            return stack.Count == 0;
        }
        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
