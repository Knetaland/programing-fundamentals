using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Implement a basic calculator to evaluate a simple expression string.

    The expression string may contain open ( and closing parentheses ), the plus + or minus sign -, non-negative integers and empty spaces .

    Example 1:

    Input: "1 + 1"
    Output: 2
    Example 2:

    Input: " 2-1 + 2 "
    Output: 3
    Example 3:

    Input: "(1+(4+5+2)-3)+(6+8)"
    Output: 23
    Note:
    You may assume that the given expression is always valid.
    Do not use the eval built-in library function.
    */
    public class Q224_BasicCalculator : IProblem
    {
        public int Calculate(string s) {
            var stack = new Stack<int>();
            int DEFAULT_SIGN = 1;
            var sign = DEFAULT_SIGN; // start with positive
            var res = 0;
            for (var i = 0; i < s.Length; i++) {
                var c = s[i];
                if (char.IsDigit(c)) {
                    var num = 0;
                    while(i < s.Length && char.IsDigit(s[i])) { // find the entire number
                        num = 10 * num + (s[i] - '0');
                        i++;
                    }
                    i--; // the while loop exits with invalid i, move one index back
                    res += sign * num;
                }
                else if (c == '+') sign = 1;
                else if (c == '-') sign = -1;
                else if (c == '(') {
                    stack.Push(res); // save the current res, reset res for calculating result inside ()
                    res = 0;    
                    stack.Push(sign); // stack, LIFO, record the operation
                    sign = DEFAULT_SIGN; // reset the sign to default
                } else if (c == ')') {
                    var preSign = stack.Pop();
                    var preRes = stack.Pop();
                    res = preRes + preSign * res;
                }
            }

            return res;
        }
        

        public int CalculateRecursive(string s) {
            var i = 0;
            return calc(s);

            int calc(string s) {
                var stack = new Stack<int>();
                var operation = '+'; // start with positive
                var num = 0;
                while (i< s.Length) {
                    var c = s[i];
                    i++;
                    if (char.IsDigit(c)) num = num * 10 + (c - '0');
                    if (c == '(') num = calc(s); // recurse, new stack for operation insdie ()
                    if (i == s.Length || c == '+' || c == '-' || c == ')' ) {
                        if (operation == '+') stack.Push(num);
                        else if (operation == '-') stack.Push(-num); // can only be + or - in this question
                        operation = c;
                        num = 0;
                    }
                    if (c == ')') break; 
                }
                return stack.Sum();
            }

        }

        public void Run()  {
            var s = "(1+(4+5+2)-3)+(6+8)";
            // var s ="2147483647";
            // var res = Calculate(s);
            var res2 = CalculateRecursive(s);
            // Console.WriteLine("res is " + res);
            Console.WriteLine("res2 is " + res2);
        }

    }
}
