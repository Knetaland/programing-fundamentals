using System;
using System.Collections;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q490_Maze : IProblem
    {
        /*
        490. The Maze
        There is a ball in a maze with empty spaces and walls. The ball can go through empty spaces by rolling up, down, left or right, but it won't stop rolling until hitting a wall. When the ball stops, it could choose the next direction.
        Given the ball's start position, the destination and the maze, determine whether the ball could stop at the destination.
        The maze is represented by a binary 2D array. 1 means the wall and 0 means the empty space. You may assume that the borders of the maze are all walls. The start and destination coordinates are represented by row and column indexes.
        Example 1
        Input 1: a maze represented by a 2D array

        0 0 1 0 0
        0 0 0 0 0
        0 0 0 1 0
        1 1 0 1 1
        0 0 0 0 0

        Input 2: start coordinate (rowStart, colStart) = (0, 4)
        Input 3: destination coordinate (rowDest, colDest) = (4, 4)

        Output: true
        Explanation: One possible way is : left -> down -> left -> down -> right -> down -> right.
        */

         int[][] dirs = [
            [1,0],
            [-1,0],
            [0,-1],
            [0,1]
         ];

        public void Run()
        {
            var maze = new[,] {
                {0, 0, 1, 0, 0},
                {0, 0, 0, 0, 1},
                {0, 0, 0, 1, 0},
                {1, 1, 0, 1, 1},
                {0, 0, 0, 0, 0},
            };
            var source = new[] { 0, 4 };
            var destination = new[] { 4, 4 };

            // var res = HasPath(maze, source, destination);
            // Console.WriteLine("has path " + res);

            var resBfs = HasPathBfs(maze, source, destination);
            Console.WriteLine("bfs path " + resBfs);
        }

        public bool HasPath(int[,] maze, int[] start, int[] destination)
        {
            var visited = new bool[maze.GetLength(0), maze.GetLength(1)];
            return Dfs(maze, start[0], start[1], destination[0], destination[1], visited);
        }

        public bool HasPathBfs(int[,] maze, int[] start, int[] destination)
        {
            return Bfs(maze, start, destination);
        }

        private bool Dfs(int[,] maze, int i, int j, int di, int dj, bool[,] visited)
        {
            if (visited[i, j])
            {
                return false;
            }

            if (i == di && j == dj)
            {
                return true;
            }

            var res = false;
            int m = maze.GetLength(0), n = maze.GetLength(1);
            visited[i, j] = true;

            foreach (var dir in dirs)
            {
                int x = i, y = j;
                while (x >= 0 && y >= 0 && x < m && y < n && maze[x, y] != 1)
                {
                    x += dir[0];
                    y += dir[1];
                }
                // need to go back one move, since prev calc makes x,y out of bound
                x -= dir[0];
                y -= dir[1];
                if (!visited[x, y])
                {
                    res |= Dfs(maze, x, y, di, dj, visited);
                }
            }
            return res;
        }


        private bool Bfs(int[,] maze, int[] start, int[] destination)
        {
            var queue = new Queue<int[]>();
            int m = maze.GetLength(0), n = maze.GetLength(1);
            var visited = new bool[m, n];
            queue.Enqueue(start);
            visited[start[0], start[1]] = true;
            while (queue.Count > 0)
            {
                var pos = queue.Dequeue();
                if (pos[0] == destination[0] && pos[1] == destination[1]) return true;

                foreach (var dir in dirs)
                {
                    int x = pos[0], y = pos[1];
                    while (x >= 0 && y >= 0 && x < m && y < n && maze[x, y] != 1)
                    {
                        x += dir[0];
                        y += dir[1];
                    }
                    x -= dir[0];
                    y -= dir[1];

                    if (!visited[x, y])
                    {
                        visited[x, y] = true;
                        queue.Enqueue([x, y]);
                    }
                }
            }
            return false;
        }
    }
}
