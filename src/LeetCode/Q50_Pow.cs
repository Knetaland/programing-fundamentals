using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q50_Pow : IProblem
    {
        public double Pow(double x, int n)
        {
            if (n < 0)
            {
                return PowRecursive(1 / x, -n);
            }

            return PowRecursive(x, n);
        }
        /* 
        2 ^ 5 = 4 * 4 * 2 = 32
         half = 2 ^ 2 = 2 * 2 = 4
            half = 2 ^ 1 = 1 * 1 * 2 => 2
                half = 2 ^ 0 => 1
        */

        // O(log(n))
        public double PowRecursive(double x, int n)
        {
            // Console.WriteLine($"recursively calling {x}^{n}");
            if (n == 0)
            {
                return 1;
            }

            var half = PowRecursive(x, n / 2);
            // Console.WriteLine($"n is {n}, half is {half}");
            if (n % 2 == 0)
            {
                return half * half;
            }

            return x * half * half;
        }

        public void Run()
        {
            double x = -3.0;
            int n = 11;
            var output = Pow(x, n);
            Console.WriteLine(output);
        }
    }
}
