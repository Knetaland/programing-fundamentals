using System;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    nums = [1, 2, 3]
    target = 4

    The possible combination ways are:
    (1, 1, 1, 1)
    (1, 1, 2)
    (1, 2, 1)
    (1, 3)
    (2, 1, 1)
    (2, 2)
    (3, 1)

    Note that different sequences are counted as different combinations.

    Therefore the output is 7.
     */
    public class Q377_CombinationSum_IV : IProblem
    {
        // NOT GOOD!
        public int CombinationSum4(int[] nums, int target)
        {
            if (target == 0)
            {
                return 1;
            }
            // if( target < 0) return 0;
            var i = 0;
            var res = 0;
            for (; i < nums.Length; i++)
            {
                if (target >= nums[i])
                {
                    res += CombinationSum4(nums, target - nums[i]);
                }
            }
            return res;
        }

        public int CombinationSum4DP(int[] nums, int target)
        {
            var dp = new int[target + 1];
            dp[0] = 1;
            for (var i = 1; i < dp.Length; i++)
            {
                foreach (var a in nums)
                {
                    if (i >= a)
                    {
                        dp[i] += dp[i - a];
                    }
                }
            }
            return dp[target];
        }

        public int CombinationSum4DPImprove(int[] nums, int target)
        {
            var dp = new int[target + 1];
            dp[0] = 1;
            Array.Sort(nums);
            for (var i = 1; i < dp.Length; i++)
            {
                foreach (var a in nums)
                {
                    if (a > i)
                    {
                        break;
                    }

                    dp[i] += dp[i - a];
                }
            }
            return dp[target];
        }
        public void Run()
        {
            var input = new[] { 3, 12, 4, 1, 23, 5, 6, 2, 10, 8 };
            var target = 32;
            // using (Lib.StartTimer())
            // {
            //     var output = CombinationSum4(input, target);
            //     Console.WriteLine("res is " + output);
            // }

            using (Lib.UseTimer())
            {
                var output = CombinationSum4DP(input, target);
                Console.WriteLine("DP res is " + output);
            }

            using (Lib.UseTimer())
            {
                var output = CombinationSum4DPImprove(input, target);
                Console.WriteLine("DP improve res is " + output);
            }
        }
    }
}
