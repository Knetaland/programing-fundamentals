﻿    using System;
using System.Linq;
using System.Reflection.Metadata;
using Fundamental.Core.Interfaces;

namespace LeetCode;

/*
https://leetcode.com/problems/number-of-closed-islands/description/


Given a 2D grid consists of 0s (land) and 1s (water).  An island is a maximal 4-directionally connected group of 0s and a closed island is an island totally (all left, top, right, bottom) surrounded by 1s.

Return the number of closed islands.

Example 1:

Input: grid = [[1,1,1,1,1,1,1,0],[1,0,0,0,0,1,1,0],[1,0,1,0,1,1,1,0],[1,0,0,0,0,1,0,1],[1,1,1,1,1,1,1,0]]
Output: 2
Explanation: 
Islands in gray are closed because they are completely surrounded by water (group of 1s).
Example 2:



Input: grid = [[0,0,1,0,0],[0,1,0,1,0],[0,1,1,1,0]]
Output: 1
Example 3:

Input: grid = [[1,1,1,1,1,1,1],
               [1,0,0,0,0,0,1],
               [1,0,1,1,1,0,1],
               [1,0,1,0,1,0,1],
               [1,0,1,1,1,0,1],
               [1,0,0,0,0,0,1],
               [1,1,1,1,1,1,1]]
Output: 2
 
*/

public class Q1254_Num_Closed_Islands : IProblem
{
    const int LAND = 0;
    const int WATER = 1;

    public int ClosedIslandExpandFromEdge(int[][] grid)
    {
        var res = 0;
        // start from the edges, sink the land that's connected to the edges
        for (var i = 0; i < grid.Length; i++)
        {
            sink(i, 0); // col 1
            sink(i, grid[0].Length - 1); // last col 
        }

        for (var j = 1; j < grid[0].Length; j++)
        {
            sink(0, j); // row 1
            sink(grid.Length - 1, j); // last row
        }

        for (var i = 0; i < grid.Length; i++)
        {
            for (var j = 0; j < grid[0].Length; j++)
            {
                if (grid[i][j] == LAND)
                {
                    sink(i, j);
                    res++;
                }
            }
        }
        return res;

        void sink(int row, int col)
        {
            if (row < 0 || row >= grid.Length || col < 0 || col >= grid[0].Length || grid[row][col] == WATER)
            {
                return;
            }
            grid[row][col] = WATER;
            sink(row + 1, col);
            sink(row - 1, col);
            sink(row, col + 1);
            sink(row, col - 1);
        }
    }




    public int ClosedIsland(int[][] grid)
    {
        var res = 0;
        for (var i = 0; i < grid.Length; i++)
        {
            for (var j = 0; j < grid[0].Length; j++)
            {
                if (grid[i][j] == LAND)
                {
                    if (dfs(grid, i, j))
                    {
                        Console.WriteLine($"land on [{i},{j}], expand...");
                        res++;
                    }
                }
            }
        }
        return res;

        bool dfs(int[][] g, int row, int col)
        {
            if (row < 0 || row >= g.Length || col < 0 || col >= g[0].Length)
                return false; // out of bound, meaing last recursion was at the boundary
            if (g[row][col] == WATER)
                return true; // stop exploring when it's water
            g[row][col] = WATER;
            return dfs(g, row + 1, col) &
            dfs(g, row - 1, col) &
            dfs(g, row, col + 1) &
            dfs(g, row, col - 1);
        }
    }

    public void Run()
    {
        int[][] grids = [
            [0,0,1,1,0,1,0,0,1,0],
            [1,1,0,1,1,0,1,1,1,0],
            [1,0,1,1,1,0,0,1,1,0],
            [0,1,1,0,0,0,0,1,0,1],
            [0,0,0,0,0,0,1,1,1,0],
            [0,1,0,1,0,1,0,1,1,1],
            [1,0,1,0,1,1,0,0,0,1],
            [1,1,1,1,1,1,0,0,0,0],
            [1,1,1,0,0,1,0,1,0,1],
            [1,1,1,0,1,1,0,1,1,0]
        ];
        var gridsCopy = grids.Select(innerArray =>  innerArray.ToArray()).ToArray();
        var res = ClosedIsland((int[][])grids.Clone());
        var res2 = ClosedIslandExpandFromEdge(gridsCopy);

        Console.WriteLine("res is " + res);
        Console.WriteLine("res2 is " + res2);
    }
}
