﻿using System;
using System.Collections.Generic;
using System.Text;
using Fundamental.Core.Interfaces;

namespace LeetCode;
/*
https://leetcode.ca/all/694.html

Given a non-empty 2D array grid of 0's and 1's, an island is a group of 1's (representing land) connected 4-directionally (horizontal or vertical.) You may assume all four edges of the grid are surrounded by water.

Count the number of distinct islands. An island is considered to be the same as another if and only if one island can be translated (and not rotated or reflected) to equal the other.

Example 1:

11000
11000
00011
00011
Given the above grid map, return 1.
Example 2:

11011
10000
00001
11011
Given the above grid map, return 3.

Notice that:
11
1
and
 1
11
are considered different island shapes, because we do not consider reflection / rotation.
Note: The length of each dimension in the given grid does not exceed 50.
*/

public class Q694_Num_Distinct_Islands : IProblem
{
    public int NumIslands(int[][] grid)
    {
        int[][] dirs = [
            [-1, 0], // up
            [1, 0], // down
            [0, -1], // left
            [0, 1], // right
        ];
        var set = new HashSet<string>();
        // to get the distinct islands, the travel path will be the same, which we can capture using string
        char[] dirPath = ['u', 'd', 'l', 'r'];
        for (var i = 0; i < grid.Length; i++)
        {
            for (var j = 0; j < grid[0].Length; j++)
            {
                if (grid[i][j] == 1)
                {
                    var sb = new StringBuilder();
                    dfs(grid, i, j, sb, '[');
                    Console.WriteLine("island Pattern is " + sb.ToString());
                    set.Add(sb.ToString());
                }
            }
        }
        return set.Count;

        
        void dfs(int[][] grid, int x, int y, StringBuilder sb, char path)
        {
            if (x < 0 || x >= grid.Length || y < 0 || y >= grid[0].Length || grid[x][y] == 0)
                return;
            grid[x][y] = 0;
            sb.Append(path);
            for (var i = 0; i < dirs.Length; i++)
            {
                dfs(grid, x + dirs[i][0], y + dirs[i][1], sb, dirPath[i]);
            }
            sb.Append(']');
        }
    }

    public void Run()
    {
        // int[][] grids = [
        //     [1,1,0,0,0],
        //     [1,1,0,0,0],
        //     [0,0,0,1,1],
        //     [0,0,0,1,1]
        // ];
        int[][] grids = [
[1,1,0,1,1],
[1,0,0,0,0],
[0,0,0,0,1],
[1,1,0,1,1],
        ];
        var res = NumIslands(grids);

        Console.WriteLine("res is " + res);
    }
}
