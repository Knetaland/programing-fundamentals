using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Node = Fundamental.Core.Models.NaryTreeNode;

namespace LeetCode.DFS
{
    /*
    Given the root of an n-ary tree, return the postorder traversal of its nodes' values.

    Nary-Tree input serialization is represented in their level order traversal. Each group of children is separated by the null value (See examples)

    

    Example 1:


    Input: root = [1,null,3,2,4,null,5,6]
    Output: [5,6,3,2,4,1]
    Example 2:


    Input: root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
    Output: [2,6,14,11,7,3,12,8,4,13,9,10,5,1]
    

    Constraints:

    The number of nodes in the tree is in the range [0, 104].
    0 <= Node.val <= 104
    The height of the n-ary tree is less than or equal to 1000

    */
    public class Q590_Nary_Tree_Postorder_Traverse : IProblem
    {
        public IList<int> Postorder(Node root) {
            var res = new List<int> ();
            recurse(root, res);
            return res;
            
            void recurse(Node n, IList<int> ans) {
                if (n == null) return ;
                foreach(var c in n.children) {
                    recurse(c, ans);
                }
                ans.Add(n.val);
            }
        }

        public IList<int> PostorderInterative(Node root) {
            var res = new List<int> ();
            if (root == null) return res;

            var stack = new Stack<Node>();
            var visited = new HashSet<Node>();
            
            stack.Push(root);

            while(stack.Count > 0) {
                var node = stack.Peek();
                if (node.children== null || node.children.Count == 0) {
                    res.Add(node.val);
                    visited.Add(node);
                    stack.Pop();
                } else {
                    for(var i = node.children.Count -1; i >=0; i--) {
                        if (visited.Contains(node.children[i])) { // if children has been processed, pop current node and move on
                            res.Add(node.val);
                            visited.Add(node);
                            stack.Pop();
                            break;
                        }
                        stack.Push(node.children[i]);
                    }
                }
            }
            return res;
        }

        // 3. in order traverse and reverse
        
        public void Run() => throw new System.NotImplementedException();
    }
}
