using System;

namespace LeetCode.DFS;

/**
    https://leetcode.com/problems/flood-fill/

    You are given an image represented by an m x n grid of integers image, where image[i][j] represents the pixel value of the image. You are also given three integers sr, sc, and color.
    Your task is to perform a flood fill on the image starting from the pixel image[sr][sc].

    To perform a flood fill:

    Begin with the starting pixel and change its color to color.
    Perform the same process for each pixel that is directly adjacent (pixels that share a side with the original pixel, either horizontally or vertically) and shares the same color as the starting pixel.
    Keep repeating this process by checking neighboring pixels of the updated pixels and modifying their color if it matches the original color of the starting pixel.
    The process stops when there are no more adjacent pixels of the original color to update.
    Return the modified image after performing the flood fill.

 
**/
public class Q733_Flood_Fill
{
    public int[][] FloodFill(int[][] image, int sr, int sc, int color) {
        var dirs = new int[][] {
            [0,1],
            [1,0],
            [0,-1],
            [-1,0],
        };
        var m =  image.Length;
        var n = image[0].Length;
        var oldColor = image[sr][sc];
        if (oldColor != color) {
            dfs(sr, sc);
        }
        void dfs(int r, int c) {
            if (r >= 0 && c >=0 && r < m && c < n && image[r][c] == oldColor) {
                image[r][c] = color;
                foreach(var dir in dirs) {
                    dfs(r+dir[0], c +dir[1]);
                }
            }
        }
        return image;
    }
}
