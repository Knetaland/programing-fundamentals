using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Node = Fundamental.Core.Models.NaryTreeNode;
namespace LeetCode.DFS
{
    public class Q589_Nary_Tree_Preorder_Traverse : IProblem
    {
        /*
        Given the root of an n-ary tree, return the preorder traversal of its nodes' values.

        Nary-Tree input serialization is represented in their level order traversal. Each group of children is separated by the null value (See examples)

        

        Example 1:



        Input: root = [1,null,3,2,4,null,5,6]
        Output: [1,3,5,6,2,4]
        Example 2:



        Input: root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
        Output: [1,2,3,6,7,11,14,4,8,12,5,9,13,10]
        

        Constraints:

        The number of nodes in the tree is in the range [0, 104].
        0 <= Node.val <= 104
        The height of the n-ary tree is less than or equal to 1000.
        */

        public IList<int> Preorder(Node root) {
            var res = new List<int> ();
            if (root == null) return res;
            // LIFO
            var stack = new Stack<Node>();
            stack.Push(root);
            while(stack.Count > 0) {
                var node = stack.Pop();
                res.Add(node.val);
                // push from right to left, so that left one will get pop first for pre-order
                for(var i = node.children.Count -1; i >=0; i--) {
                    stack.Push(node.children[i]);
                }
            }
            return res;
        }

        public IList<int> PreorderRecursive(Node root) {
            var res = new List<int>();
            recurse(root, res);
            return res;

            void recurse(Node n, IList<int> ans) {
                if (n == null) return ;
                ans.Add(n.val);
                foreach(var c in n.children) {
                    recurse(c, ans);
                }
            }
        }

        public void Run() => throw new System.NotImplementedException();
    }
}
