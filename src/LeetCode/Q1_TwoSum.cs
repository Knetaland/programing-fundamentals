using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

    You may assume that each input would have exactly one solution, and you may not use the same element twice.

    You can return the answer in any order.

    

    Example 1:

    Input: nums = [2,7,11,15], target = 9
    Output: [0,1]
    Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
    Example 2:

    Input: nums = [3,2,4], target = 6
    Output: [1,2]
    Example 3:

    Input: nums = [3,3], target = 6
    Output: [0,1]
    */
    public class Q1_TwoSum : IProblem
    {
        public void Run() => throw new NotImplementedException();

        public int[] TwoSum(int[] nums, int target)
        {
            if (nums == null || nums.Length == 0)
            {
                return null;
            }

            var map = new Dictionary<int, int>();
            for (var i = 0; i < nums.Length; i++)
            {
                if (map.ContainsKey(nums[i]))
                {
                    return new int[] { map[nums[i]], i };
                }

                map[target - nums[i]] = i;
            }

            return null;
        }

        // public void Run()
        // {
        //     var input = new int[] { 2, 3, 5, 1, 3, 5, 5, 6, 1, 3, 5, 4, 3, 8, 4, 12, 3, 4, 1, 23, 9, 2, 10 };
        //     var target = 2;
        //     Console.WriteLine($"{target} can be found in {string.Join(",", TwoSum(input, target))}");
        // }
    }
}
