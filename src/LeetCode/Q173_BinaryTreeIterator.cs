using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    public class Q173_BinaryTreeIterator : IProblem
    {
        public void Run()
        {
            throw new NotImplementedException();
        }
    }

    public class BSTIterator
    {
        private Stack<TreeNode> _stack;
        public BSTIterator(TreeNode root)
        {
            _stack = new Stack<TreeNode>();
            pushAllLeft(root);
        }
        
        /** @return whether we have a next smallest number */
        public bool HasNext()
        {
            return _stack.Count > 0;
        }

        public int Next()
        {
            var node = _stack.Pop();
            pushAllLeft(node.right);
            return node.val;
        }

        private void pushAllLeft(TreeNode root) {
            while (root != null) {
                _stack.Push(root);
                root = root.left;
            }
        }
    }
}
