using System;
using System.Collections.Generic;
using System.Text;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    public class Q102_TreeLevelTraversal : IProblem
    {
        public IList<IList<int>> LevelOrder(TreeNode root)
        {
            var res = new List<IList<int>>();
            if (root == null)
            {
                return res;
            }

            var queue = new Queue<TreeNode>();
            queue.Enqueue(root);
            while (queue.Count > 0)
            {
                var count = queue.Count;
                var list = new List<int>();
                while (count > 0)
                {
                    var node = queue.Dequeue();
                    list.Add(node.val);
                    if (node.left != null)
                    {
                        queue.Enqueue(node.left);
                    }

                    if (node.right != null)
                    {
                        queue.Enqueue(node.right);
                    }

                    count--;
                }
                res.Add(list);
            }
            return res;
        }

        public IList<IList<int>> LevelOrderRecursive(TreeNode root)
        {
            var res = new List<IList<int>>();
            Helper(root, 0, res);
            return res;
        }

        private void Helper(TreeNode root, int depth, IList<IList<int>> res)
        {
            if (root == null)
            {
                return;
            }
            while (res.Count <= depth)
            {
                res.Add(new List<int>());
            }
            res[depth].Add(root.val);
            Helper(root.left, depth + 1, res);
            Helper(root.right, depth + 1, res);
        }

        public void Run()
        {
            var tree = new TreeNode(3)
            {
                left = new TreeNode(9),
                right = new TreeNode(20)
                {
                    left = new TreeNode(15),
                    right = new TreeNode(7),
                }
            };

            using (Lib.UseTimer())
            {
                var res = LevelOrder(tree);
                var sb = new StringBuilder();
                for (var i = 0; i < res.Count; i++)
                {
                    sb.AppendLine(string.Join(",", res[i]));
                }
                Console.WriteLine($"level order is \n {sb}");
            }

            using (Lib.UseTimer())
            {
                var res = LevelOrderRecursive(tree);
                var sb = new StringBuilder();
                for (var i = 0; i < res.Count; i++)
                {
                    sb.AppendLine(string.Join(",", res[i]));
                }
                Console.WriteLine($"Recursive level order is \n {sb}");
            }
        }
    }
}
