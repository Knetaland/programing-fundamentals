using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{

    // https://www.youtube.com/watch?v=3ZDZ-N0EPV0
    public class Q44_WildcardMatching : IProblem
    {

        public bool IsMatch(string s, string p)
        {
            int sIndex = 0, pIndex = 0, starIndex = -1, savedSIndex = 0;
            while (sIndex < s.Length)
            {
                if (pIndex < p.Length && (p[pIndex] == s[sIndex] || p[pIndex] == '?'))
                {
                    pIndex++;
                    sIndex++;
                }
                else if (pIndex < p.Length && p[pIndex] == '*')
                {
                    starIndex = pIndex;
                    savedSIndex = sIndex;
                    pIndex++;
                }
                else if (starIndex != -1)
                {
                    pIndex = starIndex + 1;
                    savedSIndex++;
                    sIndex = savedSIndex;
                }
                else
                {
                    return false;
                }
            }
            while (pIndex < p.Length && p[pIndex] == '*')
            {
                pIndex++;
            }

            return pIndex == p.Length;
        }
        public void Run()
        {
            var s = "abcdefgg";
            var p = "a*d*g";
            Console.WriteLine($"{s} matches pattern {p}? {IsMatch(s, p)} ");
        }
    }
}
