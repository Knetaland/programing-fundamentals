using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q91_DecodeWays : IProblem
    {
        /*
        A message containing letters from A-Z is being encoded to numbers using the following mapping:

        'A' -> 1
        'B' -> 2
        ...
        'Z' -> 26
        Given an encoded message containing digits, determine the total number of ways to decode it.

        For example,
        Given encoded message "12", it could be decoded as "AB" (1 2) or "L" (12).

        The number of ways decoding "12" is 2.
         */

        /* 
        NOTE: if first char is '0', then 0 way to decode it, becase '09' returns 0 in OJ
         */
        public int Decode(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return 0;
            }

            var dp = new int[s.Length + 1];
            dp[0] = 1;
            dp[1] = s[0] == '0' ? 0 : 1;
            for (var i = 2; i < dp.Length; i++)
            {
                // check only 1 char
                var digit = s[i - 1] - '0';
                if (digit >= 1 && digit <= 9)
                {
                    dp[i] += dp[i - 1];
                }
                // checking 2 chars
                var digits = Convert.ToInt16(s.Substring(i - 2, 2));
                if (digits >= 10 && digits <= 26)
                {
                    dp[i] += dp[i - 2];
                }
            }
            return dp[s.Length];
        }
        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
