using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    /*
    Given a linked list, rotate the list to the right by k places, where k is non-negative.

    Example 1:

    Input: 1->2->3->4->5->NULL, k = 2
    Output: 4->5->1->2->3->NULL
    Explanation:
    rotate 1 steps to the right: 5->1->2->3->4->NULL
    rotate 2 steps to the right: 4->5->1->2->3->NULL
    Example 2:

    Input: 0->1->2->NULL, k = 4
    Output: 2->0->1->NULL
    Explanation:
    rotate 1 steps to the right: 2->0->1->NULL
    rotate 2 steps to the right: 1->2->0->NULL
    rotate 3 steps to the right: 0->1->2->NULL
    rotate 4 steps to the right: 2->0->1->NULL
    */
    public class Q61_RotateList : IProblem
    {
        public ListNode RotateRight(ListNode head, int k)
        {
            if (head == null)
            {
                return head;
            }

            var pointer = head;
            var length = 0;
            while (pointer != null)
            {
                pointer = pointer.next;
                length++;
            }
            var n = k % length;
            if (n == 0)
            {
                return head;
            }

            pointer = head;
            while (n > 0)
            {
                pointer = pointer.next;
                n--;
            }
            var p2 = head;
            while (pointer.next != null)
            {
                p2 = p2.next;
                pointer = pointer.next;
            }
            var newHead = p2.next;
            // found nth from last, it will be the new head
            // set prev to be end
            p2.next = null;
            pointer.next = head;
            return newHead;
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
