using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q34_FindFirstAndLastPositionOfElementInSortedArray : IProblem
    {
        public int[] SearchRange(int[] nums, int target)
        {
            var res = new[] { -1, -1 };
            if (nums == null)
            {
                return res;
            }

            res[0] = FindLeft(nums, target);
            res[1] = FindRight(nums, target);
            return res;
        }

        private int FindLeft(int[] nums, int target)
        {
            int index = -1, left = 0, right = nums.Length;
            while (left < right)
            {
                var mid = (left + right) / 2;
                if (target > nums[mid])
                {
                    left = mid + 1;
                }
                else
                {
                    right = mid;
                }

                if (nums[mid] == target)
                {
                    index = mid;
                }
            }
            return index;
        }

        private int FindRight(int[] nums, int target)
        {
            int index = -1, left = 0, right = nums.Length;
            while (left < right)
            {
                var mid = (left + right) / 2;
                if (target >= nums[mid])
                {
                    left = mid + 1;
                }
                else
                {
                    right = mid;
                }

                if (nums[mid] == target)
                {
                    index = mid;
                }
            }
            return index;
        }

        //------------- Method 2 ----------------//

        public int[] SearchRange2(int[] nums, int target)
        {
            var low = FindLowerBound(nums, target);
            if (low == nums.Length || nums[low] != target)
            {
                return new[] { -1, -1 };
            }
            // get lowerbound for item greater than target, then step left 1
            return new[] { low, FindLowerBound(nums, target + 1) - 1 };
        }

        private int FindLowerBound(int[] nums, int target)
        {
            int low = 0, high = nums.Length;
            while (low < high)
            {
                var mid = low + (high - low) /2 ;
                if (nums[mid] < target)
                {
                    low = mid + 1;
                }
                else
                {
                    high = mid;
                }
            }
            // low should be equal to high here
            return low;
        }


        public void Run()
        {

        }
    }
}
