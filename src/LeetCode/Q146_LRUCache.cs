using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Design and implement a data structure for Least Recently Used (LRU) cache. It should support the following operations: get and put.

    get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
    put(key, value) - Set or insert the value if the key is not already present. When the cache reached its capacity, it should invalidate the least recently used item before inserting a new item.

    The cache is initialized with a positive capacity.

    Follow up:
    Could you do both operations in O(1) time complexity?

    Example:

    LRUCache cache = new LRUCache(2)

    cache.put(1, 1);
    cache.put(2, 2);
    cache.get(1);       // returns 1
    cache.put(3, 3);    // evicts key 2
    cache.get(2);       // returns -1 (not found)
    cache.put(4, 4);    // evicts key 1
    cache.get(1);       // returns -1 (not found)
    cache.get(3);       // returns 3
    cache.get(4);       // returns 4
    */
    public class Q146_LRUCache : IProblem
    {
        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }

    public class LRUCache2 {

        class Node {
            public int key, value;
            public Node(int key, int value) {
                this.key = key;
                this.value = value;
            }
        }
        private readonly int _capacity;
        private LinkedList<Node> _list;
        private Dictionary<int, LinkedListNode<Node>> _map;

        public LRUCache2(int capacity) {
            _capacity = capacity;
            _list = new LinkedList<Node>();
            _map = new Dictionary<int, LinkedListNode<Node>>();
        }

        public int Get(int key)
        {
            if (!_map.ContainsKey(key)) {
                return -1;
            }
            var node = _map[key];
            _list.Remove(node);
            _list.AddFirst(node);
            return node.Value.value;
        }


        public void Put(int key, int value) {
            if (_map.ContainsKey(key)) {
                var node = _map[key];
                node.Value.value = value;
                _list.Remove(node);
                _list.AddFirst(node);
            } else {
                var count = _map.Count;
                if (count == _capacity) { // full, need to remove
                    var last = _list.Last;
                    _list.RemoveLast();
                    _map.Remove(last.Value.value); // remove from map by key
                }
                _map.Add(key, new LinkedListNode<Node>(new Node(key, value)));
                _list.AddFirst(_map[key]);
            }
        }
    }

    public class LRUCache
    {
        private readonly Dictionary<int, Node> _map;
        private readonly int _capacity;
        private Node _head, _tail;
        

        public LRUCache(int capacity)
        {
            _map = new Dictionary<int, Node>();
            _capacity = capacity;
            _head = new Node(-1, -1);
            _tail = new Node(-1, -1);
            _head.Next = _tail;
            _tail.Prev = _head;
        
        }

        public int Get(int key)
        {
            if (_map.TryGetValue(key, out var n)) {
                Remove(n);
                // move to front of queue
                MoveToHead(n);
                return n.Value;
            }
            return -1;
        }


        private void MoveToHead(Node n)
        {
            var headNext = _head.Next;
            _head.Next = n;
            n.Prev = _head;
            n.Next = headNext;
            headNext.Prev = n;
        }

        private void Remove(Node n)
        {
            n.Prev.Next = n.Next;
            n.Next.Prev = n.Prev;
        }

        public void Put(int key, int value)
        {
            if (_map.ContainsKey(key))
            {
                var n = _map[key];
                n.Value = value;
                Remove(n);
                MoveToHead(n);
            }
            else
            {
                if (_map.Count == _capacity)
                {   // already full
                    var toBeRemoved = _tail.Prev;
                    // remove tail
                    Remove(toBeRemoved);
                    _map.Remove(toBeRemoved.Key);
                }
                var n = new Node(key, value);
                MoveToHead(n);
                _map.Add(n.Key, n);
            }
        }

        class Node
        {
            public int Key, Value;
            public Node Prev, Next;
            public Node(int k, int v)
            {
                Key = k;
                Value = v;
            }
        }
    }
}
