using System;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    public class Q211_AddAndSearchWord : IProblem
    {
        public void Run()
        {
            var word = "abc";
            var dic = new WordDictionary();
            dic.AddWord(word);
            dic.AddWord("bcd");
            dic.AddWord("a..");
            Console.WriteLine("abc is found? " + dic.Search("abc"));
            Console.WriteLine("bcd is found? " + dic.Search("bcd"));
            Console.WriteLine("a.. is found? " + dic.Search("a.."));
            Console.WriteLine("abd is found? " + dic.Search("abd"));
        }
    }

    public class WordDictionary
    {
        private Trie root;
        /** Initialize your data structure here. */
        public WordDictionary()
        {
            root = new Trie();
        }

        /** Adds a word into the data structure. */
        public void AddWord(string word)
        {
            var cur = root;
            foreach (var c in word)
            {
                if (!cur.children.ContainsKey(c))
                {
                    var node = new Trie();
                    cur.children.Add(c, node);
                }
                cur = cur.children[c];
            }
            cur.endOfWord = true;
        }

        /** Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter. */
        public bool Search(string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                return false;
            }

            var node = root;

            return Search(node, word, 0);
        }
        private bool Search(Trie node, string word, int i)
        {
            if (node == null)
            {
                return false;
            }

            if (i == word.Length)
            {
                return node.endOfWord;
            }

            var c = word[i];
            if (c == '.')
            {
                foreach (var child in node.children)
                {
                    if (Search(child.Value, word, i + 1))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
                return Search(node.children.ContainsKey(c) ? node.children[c] : null, word, i + 1);
            }
        }
    }
}
