using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class MinComparer : IComparer<int>
    {
        public int Compare(int x, int y) => y - x;
    }
    public class Q295_FindMedianFromDataStream : IProblem
    {
        public void Run()
        {
            var _map = new SortedDictionary<int, int>(Comparer<int>.Create((a,b) => b -a));

            Test();
        }

        public void Test()
        {
            // var maxHeap = new PriorityQueue<int, int>();
            var minHeap = new PriorityQueue<int, int>(Comparer<int>.Create((x,y) => y.CompareTo(x)));

            // maxHeap.Enqueue(10, 10);
            // maxHeap.Enqueue(8, 8);
            // maxHeap.Enqueue(5, 5);

            // while (maxHeap.Count > 0)
            // {
            //     var item = maxHeap.Dequeue();
            // }

            // minHeap.Enqueue(10, 10);
            // minHeap.Enqueue(8, 8);
            // minHeap.Enqueue(5, 5);

            // while (minHeap.Count > 0)
            // {
            //     var item = minHeap.Dequeue();
            // }
        }
    }
}