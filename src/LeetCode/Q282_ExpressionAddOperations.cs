using System;
using System.Collections.Generic;
using System.Text;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q282_ExpressionAddOperations : IProblem
    {
        public IList<string> AddOperators(string num, int target)
        {
            var res = new List<string>();
            Dfs(num, target, 0, new StringBuilder(num.Length), 0L, 0L, res);
            return res;
        }

        public void Dfs(string num, int target, // input
            int pos, StringBuilder exp, long prev, long curr, // state
            IList<string> res) // output
        {
            if (pos == num.Length)
            {
                if (curr == target)
                {
                    res.Add(exp.ToString());
                }
                return;
            }

            long val = 0;
            var oldLength = exp.Length;

            for (var i = pos; i < num.Length; i++)
            {
                val = val * 10 + num[i] - '0';
                if (exp.Length > 0)
                {
                    exp.Append('+');
                }

                exp.Append(val);

                Dfs(num, target, i + 1, exp, val, curr + val, res);
                exp.Length = oldLength;  // act as remove the last elements added earlier

                if (exp.Length > 0)
                {
                    exp.Append('-');
                    exp.Append(val);

                    Dfs(num, target, i + 1, exp, -val, curr - val, res);
                    exp.Length = oldLength;

                    exp.Append('*');
                    exp.Append(val);
                    Dfs(num, target, i + 1, exp, prev * val, curr - prev + prev * val, res);
                    exp.Length = oldLength;
                }

                if (val == 0)
                {
                    break;
                }
            }

        }

        public void Run()
        {
            var num = "3456237490";
            var target = 9191;
            var res = AddOperators(num, target);
            Console.WriteLine(string.Join("   ", res));
        }
    }
}
