using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q412_FizzBuzz : IProblem
    {
        public List<string> fizzBuzz(int n)
        {
            var res = new List<string>(n);
            for (int i = 1, fizz = 1, buzz = 1; i <= n; i++, fizz++, buzz++)
            {
                if (fizz == 3 && buzz == 5)
                {
                    fizz = 0;
                    buzz = 0;
                    res.Add("FizzBuzz");
                }
                else if (fizz == 3)
                {
                    fizz = 0;
                    res.Add("Fizz");
                }
                else if (buzz == 5)
                {
                    buzz = 0;
                    res.Add("Buzz");
                }
                else
                {
                    res.Add(i.ToString());
                }
            }
            return res;
        }

        void IProblem.Run()
        {
            var s = fizzBuzz(1);
        }
    }
}
