using System;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q28_FindNeedleInHaystack : IProblem
    {
        public int StrStr(string haystack, string needle)
        {
            if (string.IsNullOrEmpty(needle))
            {
                return 0;
            }

            if (needle.Length > haystack.Length)
            {
                return -1;
            }

            for (var i = 0; i <= haystack.Length - needle.Length; i++)
            {
                for (var j = 0; j <= needle.Length; j++)
                {
                    if (j == needle.Length)
                    {
                        return i;
                    }

                    if (haystack[i + j] != needle[j])
                    {
                        break;
                    }
                }
            }
            return -1;
        }

        public int StrStrNew(string haystack, string needle)
        {
            if (string.IsNullOrEmpty(needle))
            {
                return 0;
            }
            if (needle.Length > haystack.Length)
            {
                return -1;
            }

            for (var i = 0; i <= haystack.Length - needle.Length; i++)
            {
                if (needle[0] == haystack[i])
                {
                    var j = 1;
                    for (; j < needle.Length; j++)
                    {
                        if (!(needle[j] == haystack[i + j]))
                        {
                            break;
                        }
                    }
                    if (j == needle.Length)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }
        public void Run()
        {
            string haystack = "a", needle = "a";


            using (Lib.UseTimer())
            {
                var ind = StrStrNew(haystack, needle);
                Console.WriteLine($"NEW index for needle is {ind}");
            }

            using (Lib.UseTimer())
            {
                var ind1 = StrStr(haystack, needle);
                Console.WriteLine($"index for needle is {ind1}");
            }
        }
    }
}
