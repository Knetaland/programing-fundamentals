using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q29_DivideTwoIntegers : IProblem
    {
        // O(logn)
        public int Divide(int dividend, int divisor)
        {
            // if (dividend == int.MinValue && divisor == -1) return int.MaxValue; // edge case
            long dvd = Math.Abs((long)dividend),
                 dvs = Math.Abs((long)divisor),
                 res = 0;
            while (dvd >= dvs)
            {
                long shift = 1, temp = dvs;
                while (dvd >= temp << 1)
                {
                    temp <<= 1; // same as * 2
                    shift <<= 1;
                }
                dvd -= temp;
                res += shift;
            }
            if (dividend < 0 ^ divisor < 0)
            {
                res = -res;
            }

            return res > int.MaxValue ? int.MaxValue : (int)res;
        }
        public void Run()
        {
            var dividend = 15;
            var divisor = 3;
            Console.WriteLine($"answer is {Divide(dividend, divisor)}");
        }
    }
}
