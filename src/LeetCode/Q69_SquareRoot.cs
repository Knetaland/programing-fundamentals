using System;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q69_SquareRoot : IProblem
    {
        public int Sqrt(int x)
        {
            if (x == 0)
            {
                return 0;
            }

            int start = 1, end = x, ans = 0;
            while (start <= end)
            {
                var mid = start + (end - start) / 2;
                if (mid <= x / mid)
                {
                    start = mid + 1;
                    ans = mid;
                }
                else
                {
                    end = mid - 1;
                }
            }
            return ans;
        }

        public int SqrtNewton(int x)
        {
            long r = x;
            while (r * r > x)
            {
                r = (r + x / r) / 2;
            }

            return (int)r;
        }


        public void Run()
        {
            var input = 38938193;
            using (Lib.UseTimer())
            {
                Console.WriteLine($"Binary Search: sqrt of {input} is {Sqrt(input)}");
            }
            using (Lib.UseTimer())
            {
                Console.WriteLine($"Newton's method: sqrt of {input} is {SqrtNewton(input)}");
            }
        }
    }
}
