using System;
using System.Collections;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q286_WallsAndGates : IProblem
    {
        /*
        You are given a m x n 2D grid initialized with these three possible values.

-1 - A wall or an obstacle.
0 - A gate.
INF - Infinity means an empty room. We use the value 231 - 1 = 2147483647 to represent INF as you may assume that the distance to a gate is less than 2147483647.
Fill each empty room with the distance to its nearest gate. If it is impossible to reach a gate, it should be filled with INF.

For example, given the 2D grid:

INF  -1  0  INF
INF INF INF  -1
INF  -1 INF  -1
  0  -1 INF INF
After running your function, the 2D grid should be:

  3  -1   0   1
  2   2   1  -1
  1  -1   2  -1
  0  -1   3   4
        */
        const int WALL = -1;
        const int GATE = 0;
        const int EMPTY = int.MaxValue;
        private int[][] DIRS = [
            [0,1],
            [0,-1],
            [1,0],
            [-1,0],
        ];

        public void WallsAndGates(int[,] rooms)
        {   
            var m = rooms.GetLength(0);
            var n = rooms.GetLength(1);

            for (var i = 0; i < m; i++)
            {
                for (var j = 0; j < n; j++)
                {
                    // if we see a gate, go from gate to fill the shortest distance.
                    if (rooms[i, j] == GATE)
                    {
                        DFS(rooms, i, j, 0);
                    }
                }
            }
        }

        void DFS(int[,] rooms, int i, int j, int distance)
        {
            var m = rooms.GetLength(0);
            var n = rooms.GetLength(1);

            if (i < 0 || j < 0 || i >= m || j >= n || rooms[i, j] == WALL || rooms[i, j] < distance)
            {
                return;
            }
            rooms[i, j] = distance;
            DFS(rooms, i + 1, j, distance + 1);
            DFS(rooms, i - 1, j, distance + 1);
            DFS(rooms, i, j + 1, distance + 1);
            DFS(rooms, i, j - 1, distance + 1);
        }

        public void WallsAndGatesbfs(int[,] rooms) {
            var m = rooms.GetLength(0);
            var n = rooms.GetLength(1);
            var queue= new Queue<(int, int)>();
            

            for(var i = 0; i < m; i++) {
                for(var j = 0; j < n; j++) {
                    if (rooms[i, j] == GATE) {
                        queue.Enqueue((i, j ));
                        Console.WriteLine($"gate found at position [{i},{j}]");
                        rooms[i, j] = 0; // reuse as distance
                    }
                    
                }
            }

            while(queue.Count > 0) {
                var pos = queue.Dequeue();
                foreach(var dir in DIRS) {
                    var x = pos.Item1 + dir[0];
                    var y = pos.Item2 + dir[1];
                     if (x >=0 && x< m && y >=0 && y < n && rooms[x, y] == EMPTY) {
                        queue.Enqueue((x,y));
                        rooms[x,y] = rooms[pos.Item1, pos.Item2] + 1;
                     }
                }
               
            }
        }

        public void Run() {
            var rooms = new int [,] {
                {EMPTY, WALL, GATE, EMPTY},
                {EMPTY, EMPTY, EMPTY, WALL},
                {EMPTY, WALL, EMPTY, WALL},
                {GATE, WALL, EMPTY, EMPTY}
            };
            WallsAndGatesbfs(rooms);

            foreach(var room in rooms) {
                room.Print();
            }
        }

    }
}
