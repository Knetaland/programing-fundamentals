using System;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{

    /*
    Reverse a linked list from position m to n. Do it in one-pass.

    Note: 1 ≤ m ≤ n ≤ length of list.

    Example:

    Input: 1->2->3->4->5->NULL, m = 2, n = 4
    Output: 1->4->3->2->5->NULL
    */
    public class Q92_ReverseLinkedList_II : IProblem
    {
        public ListNode Reverse(ListNode head, int m, int n)
        {
            if (head == null)
            {
                return null;
            }

            var dummy = new ListNode(-1)
            {
                next = head
            };
            var prev = dummy;
            var length = n - m;
            while (m - 1 > 0)
            {
                prev = prev.next;
                if (prev == null)
                {
                    return head;
                }

                m--;
            }
            var cur = prev.next;
            while (length > 0)
            {
                // store cur.next
                var next = cur.next;
                // get what's after next
                cur.next = next.next;
                next.next = prev.next;
                prev.next = next;
                length--;
            }
            return dummy.next;
        }

        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
