using System;
using System.Collections.Generic;

namespace LeetCode;

/*
https://leetcode.com/problems/implement-queue-using-stacks/description/

Implement a first in first out (FIFO) queue using only two stacks. The implemented queue should support all the functions of a normal queue (push, peek, pop, and empty).

Implement the MyQueue class:

void push(int x) Pushes element x to the back of the queue.
int pop() Removes the element from the front of the queue and returns it.
int peek() Returns the element at the front of the queue.
boolean empty() Returns true if the queue is empty, false otherwise.
Notes:

You must use only standard operations of a stack, which means only push to top, peek/pop from top, size, and is empty operations are valid.
Depending on your language, the stack may not be supported natively. You may simulate a stack using a list or deque (double-ended queue) as long as you use only a stack's standard operations.

*/
public class Q232_Implement_Queue_Using_Stacks
{
    public class MyQueue
    {
        private Stack<int> _left;
        private Stack<int> _right;
        public MyQueue()
        {
            _left = new Stack<int>();
            _right = new Stack<int>();
        }

        public void Push(int x)
        {
            _right.Push(x);
        }

        public int Pop()
        {
            Clean();
            return _left.Pop();
        }

        public int Peek()
        {
            Clean();
            return _left.Peek();
        }

        private void Clean() {
            if (_left.Count == 0)
            {
                while (_right.Count > 0)
                {
                    _left.Push(_right.Pop());
                }
            }
        }

        public bool Empty()
        {
            return _left.Count == 0 && _right.Count == 0;
        }
    }
}
