using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    // O(n) 
    // O(n) for the stack
    public class Q227_BasicCalculatorII : IProblem
    {
        /*
        Implement a basic calculator to evaluate a simple expression string.

        The expression string contains only non-negative integers, +, -, *, / operators and empty spaces . The integer division should truncate toward zero.

        Example 1:

        Input: "3+2*2"
        Output: 7
        Example 2:
        Input: " 3/2 "
        Output: 1
        Example 3:

        Input: " 3+5 / 2 "
        Output: 5
        */

        public int Calculate(string s) {
            var stack = new Stack<int>();
            var operation = '+'; // default
            var res = 0;
            var num = 0;
            for(var i = 0; i < s.Length; i++) {
                var c = s[i];
                Console.WriteLine($"Processing char {c}, num currently is {num}");
                if (char.IsDigit(c)) {
                    num = num * 10 + (c- '0');
                    Console.WriteLine($"{c} is a digit, calculate num = {num}");
                }
                if (i == s.Length - 1 || !char.IsDigit(c) && c != ' ') { // if last emelent, add it to stack as well
                    // when we reach here, we need to calculate the previous noted operation and push it to stack.
                    // e.g. 3*2+1, first * will push 3 to stack and set operation to *, and til reading +,
                    // it pops 3 out and use the prev operation * with current num 2 => 3*2 and push it back to stack, and move on, now
                    // the operation is set to +, until it reach another signal to do the + operation.
                    Console.WriteLine($"operation is {operation}, processing...");
                    if (operation == '+') { 
                        Console.WriteLine($"+: Adding num: {num} to stack");
                        stack.Push(num); 
                    }
                    else if (operation == '-') {
                        Console.WriteLine($"-: Adding negative num: {-num} to stack");
                        stack.Push(-num);
                    }
                    else if (operation == '*') {
                        var preNum = stack.Pop();
                        Console.WriteLine($"*: prenum is {preNum}, times * {num} to stack");
                        stack.Push(preNum * num);
                    } else if (operation == '/') {
                        var preNum = stack.Pop();
                        Console.WriteLine($"/: prenum is {preNum}, divided / {num} to stack");
                        stack.Push(preNum / num);
                    }
                    num = 0; // reset num for next operation
                    operation = c;
                    Console.WriteLine($"setting operation to {operation}, and reset num");
                }
            }
            while (stack.Count > 0) {
                res += stack.Pop();
            }
            return res;
        }

        // no stack, keep track of lastNum, +/- will apply to result, * or / will apply lastNum */ curNum
        public int CalculateNoStack(string s) {
            var lastNum = 0;
            var curNum = 0;
            var res = 0;
            var operation = '+';
            for(var i = 0; i < s.Length; i++) {

                var c = s[i];
                Console.WriteLine($"processing c {c} at index {i}");
                if (char.IsDigit(c)) {
                    curNum = curNum * 10  + (c - '0');
                }
                if (i == s.Length - 1 || (!char.IsDigit(c) && c != ' ')) {
                    if (operation == '+' || operation == '-') {
                        Console.WriteLine($"operation {operation}, res {res}, lastNum {lastNum}");
                        res += lastNum;
                        Console.WriteLine($"new res: {res}");
                        lastNum = operation == '+' ? curNum : -curNum;
                    } else if (operation == '*') {
                        Console.WriteLine($"*:  curNum: {curNum}, lastNum {lastNum}");
                        lastNum *= curNum;
                    } else if (operation == '/') {
                        Console.WriteLine($"/:  curNum: {curNum}, lastNum {lastNum}");
                        lastNum /= curNum;
                    }
                    operation = c;
                    Console.WriteLine($"operation set to {c}");
                    curNum = 0;
                }
            }
            res += lastNum;
            return res;
        }

        public void Run()
        {
            var input = "35+5/2-7*5";
            var res = CalculateNoStack(input);
            Console.WriteLine(res);
        }
    }
}
