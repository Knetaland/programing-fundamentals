using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as one sorted array.

    Note:
    You may assume that nums1 has enough space (size that is greater or equal to m + n) to hold additional elements from nums2. The number of elements initialized in nums1 and nums2 are m and n respectively.
     */
    public class Q88_MergeSortedArray : IProblem
    {
        public void Merge(int[] nums1, int m, int[] nums2, int n)
        {
            int right = m + n - 1, i = m - 1, j = n - 1;
            while (i >= 0 && j >= 0)
            {
                nums1[right--] = nums1[i] > nums2[j] ? nums1[i--] : nums2[j--];
            }
            // if j >=0 => still have element in j that's not merged into nums1
            // else everything is merged, including element in nums1 originally
            while (j >= 0)
            {
                nums1[right--] = nums2[j--];
            }
        }

        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
