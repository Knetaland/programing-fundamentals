using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    public class Q100_IsSameTree : IProblem
    {
        public bool IsSameTree(TreeNode p, TreeNode q)
        {
            // return Recursive(p, q);
            return Iterate(p, q);
        }

        private bool Recursive(TreeNode p, TreeNode q)
        {
            if (p == null && q == null)
            {
                return true;
            }

            if ((p == null || q == null) || p.val != q.val)
            {
                return false;
            }

            return IsSameTree(p.left, q.left) && IsSameTree(p.right, q.right);
        }

        private bool Iterate(TreeNode p, TreeNode q)
        {
            Queue<TreeNode> q1 = new Queue<TreeNode>(), q2 = new Queue<TreeNode>();
            q1.Enqueue(p);
            q2.Enqueue(q);
            while (q1.Count > 0 && q2.Count > 0)
            {
                var node1 = q1.Dequeue();
                var node2 = q2.Dequeue();
                if (node1 == null && node2 == null)
                {
                    continue;
                }
                if (node1 == null || node2 == null || node1.val != node2.val)
                {
                    return false;
                }
                q1.Enqueue(node1.left);
                q1.Enqueue(node1.right);
                q2.Enqueue(node1.left);
                q2.Enqueue(node1.right);
            }
            return q1.Count == 0 && q2.Count == 0;
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
