using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q27_RemoveElement : IProblem
    {
        public int RemoveElement(int[] nums, int val)
        {
            if (nums == null)
            {
                return 0;
            }

            var index = 0;
            for (var i = 0; i < nums.Length; i++)
            {
                if (nums[index] != val)
                {
                    nums[index] = nums[i];
                    index++;
                }
            }
            return index;
        }
        public void Run()
        {

        }
    }
}
