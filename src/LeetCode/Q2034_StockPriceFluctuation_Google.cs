using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q2034_StockPriceFluctuation_Google : IProblem
    {
        public void Run() {
            var test = new StockPrice();
            test.Add(1, 12);
            Console.WriteLine($"--------");
            Console.WriteLine($"max is {test.Maximum()}");
            Console.WriteLine($"min is {test.Minimum()}");
            test.Add(2, 24);
            Console.WriteLine($"--------");
            Console.WriteLine($"max is {test.Maximum()}");
            Console.WriteLine($"min is {test.Minimum()}");
            test.Add(3, 48);
            Console.WriteLine($"--------");
            Console.WriteLine($"max is {test.Maximum()}");
            Console.WriteLine($"min is {test.Minimum()}");
            test.Update(2, 10);
            Console.WriteLine($"--------");
            Console.WriteLine($"max is {test.Maximum()}");
            Console.WriteLine($"min is {test.Minimum()}");
            test.Add(3, 13);
            Console.WriteLine($"--------");
            Console.WriteLine($"max is {test.Maximum()}");
            Console.WriteLine($"min is {test.Minimum()}");
            test.Update(4, 100);
            Console.WriteLine($"--------");
            Console.WriteLine($"max is {test.Maximum()}");
            Console.WriteLine($"min is {test.Minimum()}");
            Console.WriteLine($"current is {test.Current()}");

            test.Remove(4);
            Console.WriteLine($"--------");
            Console.WriteLine($"max is {test.Maximum()}");
            Console.WriteLine($"min is {test.Minimum()}");
            Console.WriteLine($"current is {test.Current()}");

            test.Remove(2);
            Console.WriteLine($"--------");
            Console.WriteLine($"max is {test.Maximum()}");
            Console.WriteLine($"min is {test.Minimum()}");
            Console.WriteLine($"current is {test.Current()}");
        }
    }

    public class StockPrice {
        SortedDictionary<int, int> _map;
        SortedDictionary<int, HashSet<int>> _priceToTimestampMap; // use sorted set to get past TLE
        int _current = int.MinValue;

        public StockPrice() {
            _map = new SortedDictionary<int, int>();
            _priceToTimestampMap =  new SortedDictionary<int, HashSet<int>>();
        }
        
        public void Add(int timestamp, int price) {
            Update(timestamp, price);
        }

        public void Remove(int timestamp) {
            var price = _map[timestamp];
            // remove from map
            _map.Remove(timestamp);
            if (_current == timestamp) {
                _current = _map.Keys.Last();
            }

            _priceToTimestampMap[price].Remove(timestamp);
            if (_priceToTimestampMap[price].Count == 0) {
                _priceToTimestampMap.Remove(price);
            }
        }

        public void Update(int timestamp, int price) {
            _current = Math.Max(timestamp, _current);
            if (_map.ContainsKey(timestamp)) {
                var originalPrice = _map[timestamp];
                _priceToTimestampMap[originalPrice].Remove(timestamp);
                if ( _priceToTimestampMap[originalPrice].Count ==0) {
                    _priceToTimestampMap.Remove(originalPrice);
                }
            }
            _map[timestamp] = price;
            if (!_priceToTimestampMap.ContainsKey(price)) {
                _priceToTimestampMap.Add(price, new HashSet<int>());
            }
            _priceToTimestampMap[price].Add(timestamp);
        }
        
        public int Current() {
        return _map[_current];
        }
        
        public int Maximum() {
            return _priceToTimestampMap.Keys.Last();
        }
        
        public int Minimum() {
            return _priceToTimestampMap.Keys.First();
        }
    }
}