using System;
using System.Text;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q67_AddBinary : IProblem
    {
        public string AddBinary(string a, string b)
        {
            var sb = new StringBuilder();
            var i = a.Length - 1;
            var j = b.Length - 1;
            var co = 0;
            while (i >= 0 || j >= 0)
            {
                var aVal = i >= 0 ? a[i] - '0' : 0;
                var bVal = j >= 0 ? b[j] - '0' : 0;
                var sum = aVal + bVal + co;
                sb.Insert(0, sum % 2);
                co = sum / 2;
                i--;
                j--;
            }
            if (co > 0)
            {
                sb.Insert(0, '1');
            }

            return sb.ToString();
        }
        public void Run()
        {
            string a = "1111", b = "1111";
            var res = AddBinary(a, b);
            Console.WriteLine(res);
        }
    }
}
