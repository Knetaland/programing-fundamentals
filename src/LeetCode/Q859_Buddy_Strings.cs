using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q859_Buddy_Strings : IProblem
    {
        /*
        Given two strings s and goal, return true if you can swap two letters in s so the result is equal to goal, otherwise, return false.

        Swapping letters is defined as taking two indices i and j (0-indexed) such that i != j and swapping the characters at s[i] and s[j].

        For example, swapping at indices 0 and 2 in "abcd" results in "cbad".
        

        Example 1:

        Input: s = "ab", goal = "ba"
        Output: true
        Explanation: You can swap s[0] = 'a' and s[1] = 'b' to get "ba", which is equal to goal.
        Example 2:

        Input: s = "ab", goal = "ab"
        Output: false
        Explanation: The only letters you can swap are s[0] = 'a' and s[1] = 'b', which results in "ba" != goal.
        Example 3:

        Input: s = "aa", goal = "aa"
        Output: true
        Explanation: You can swap s[0] = 'a' and s[1] = 'a' to get "aa", which is equal to goal.
        

        Constraints:

        1 <= s.length, goal.length <= 2 * 104
        s and goal consist of lowercase letters.
        */
        public bool BuddyStrings(string s, string goal) {
            if(s.Length != goal.Length) return false;
            if (s == goal) { // same, check if there are dup letter
                var set = new HashSet<char>();
                foreach(var c in s) {
                    set.Add(c);
                }
                return set.Count < s.Length;
            } else {
                var diff = new List<int>();
                for(var i = 0; i < s.Length; i++) {
                    if (s[i] != goal[i]) {
                        diff.Add(i);
                    }
                }
                return diff.Count == 2 && s[diff[0]] == goal[diff[1]] && s[diff[1]] == goal[diff[0]];
            }
        }

        public IList<string> CheckPool(string goal, string[] pool) {
            var func = BuddyStrings;
            var res = new List<string>();
            foreach(var s in pool) {
                var check = func(s, goal);
                if (check) {
                    res.Add(s);
                }
            }
            return res;
        }

        public void Run() {
            var pool = new [] {"abc", "acb"};
            var goal = "abc";
            CheckPool(goal, pool);
        }
    }
}
