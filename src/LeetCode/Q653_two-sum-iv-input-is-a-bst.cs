/*
 * @lc app=leetcode id=653 lang=csharp
 *
 * [653] Two Sum IV - Input is a BST
 */

// @lc code=start

using System.Collections.Generic;
using Fundamental.Core.Models;
/**
* Definition for a binary tree node.
* public class TreeNode {
*     public int val;
*     public TreeNode left;
*     public TreeNode right;
*     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
*         this.val = val;
*         this.left = left;
*         this.right = right;
*     }
* }
     }
* }
*/
public class Q653_two_sum_iv_input_in_a_bst {
    public bool FindTarget(TreeNode root, int k) {
        var map  = new HashSet<int>();
        return traverse(root);

        bool traverse(TreeNode node) {
            if (node == null) return false;
            if (map.Contains(node.val)) {
                return true;
            }
            map.Add(k - node.val);
            return traverse(node.left) || traverse(node.right);
        }
    }
}
// @lc code=end

