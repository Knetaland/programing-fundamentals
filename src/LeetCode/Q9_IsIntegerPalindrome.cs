using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q9_IsIntegerPalindrome : IProblem
    {
        public bool IsPalindrome(int x)
        {
            if (x < 0 || (x != 0 && x % 10 == 0))
            {
                return false;
            }

            var rev = 0;
            while (x > rev)
            {
                rev = rev * 10 + x % 10;
                x /= 10;
            }
            return rev == x || rev / 10 == x;
        }

        public void Run()
        {
            var input = 9877789;
            Console.WriteLine($"{input} is {IsPalindrome(input)}");
        }
    }
}
