using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    public class Q94_InorderTraversal : IProblem
    {
        public IList<int> InorderTraversal(TreeNode root)
        {
            var res = new List<int>();
            if (root == null)
            {
                return res;
            }

            var stack = new Stack<TreeNode>();
            var cur = root;
            while (cur != null || stack.Count > 0)
            {
                while (cur != null)
                {
                    stack.Push(cur);
                    cur = cur.left;
                }
                var node = stack.Pop();
                res.Add(node.val);
                cur = node.right;
            }
            return res;
        }
        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
