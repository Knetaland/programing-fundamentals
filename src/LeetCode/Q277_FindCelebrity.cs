using System;
using Fundamental.Core.Interfaces;

/*
Suppose you are at a party with n people (labeled from 0 to n - 1) and among them,
there may exist one celebrity. The definition of a celebrity is that all the other n - 1
people know him/her but he/she does not know any of them.

Now you want to find out who the celebrity is or verify that there is not one. 
The only thing you are allowed to do is to ask questions like: "Hi, A. 
Do you know B?" to get information of whether A knows B. You need to find out the celebrity (or verify 
there is not one) by asking as few questions as possible (in the asymptotic sense).
You are given a helper function bool knows(a, b) which tells you whether A knows B. 
Implement a function int findCelebrity(n), your function should minimize the number of calls to knows.

Note: There will be exactly one celebrity if he/she is in the party. 
Return the celebrity's label if there is a celebrity in the party. If there is no celebrity, return -1.
 */
namespace LeetCode
{
    public class Q277_FindCelebrity : IProblem
    {
        public int Find(int n)
        {
            var inDegree = new int[n];
            var outDegree = new int[n];
            for (var i = 0; i < n; i++)
            {
                for (var j = 0; j < n; j++)
                {
                    if (knows(i, j))
                    {
                        outDegree[i]++;
                        inDegree[j]++;
                    }
                }
            }
            for (var i = 0; i < n; i++)
            {
                if (inDegree[i] == n - 1 && outDegree[i] == 0)
                {
                    return i;
                }
            }
            return -1;
        }

        public int FindLinear(int n)
        {
            int candidate = 0, p = n;
            while (candidate < p)
            {
                if (knows(candidate, p))
                {
                    candidate++; //  candidate knows p, so cur candidate is not celebrity, move next
                }
                else
                {
                    p--; // doesnt know p, p is not celebrity
                }
            }
            // equivalent to above
            // for(var i = 0; i < n;i++) {
            //     if (knows(candidate, i)) candidate = i;
            // }

            for (var i = 0; i < n; i++)
            {
                if (i != candidate && (!knows(i, candidate) || knows(candidate, i)))
                {
                    return -1;
                }
            }
            return candidate;
        }

        // place holder
        private bool knows(int a, int b)
        {
            return false;
        }
        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
