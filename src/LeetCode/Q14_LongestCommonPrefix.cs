using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q14_LongestCommonPrefix : IProblem
    {
        public string prefix(string[] strs)
        {
            if (strs == null || strs.Length == 0)
            {
                return string.Empty;
            }

            var i = 0;
            var lcp = strs[i];
            i++;
            for (; i < strs.Length; i++)
            {
                for (var j = 0; j < lcp.Length; j++)
                {
                    if (j >= strs[i].Length || lcp[j] != strs[i][j])
                    {
                        lcp = lcp.Substring(0, j);
                        break;
                    }
                }
            }

            return lcp;
        }


        public void Run()
        {

        }
    }
}
