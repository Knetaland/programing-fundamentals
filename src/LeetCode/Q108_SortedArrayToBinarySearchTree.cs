using System;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    public class Q108_SortedArrayToBinarySearchTree : IProblem
    {
        public TreeNode SortedArrayToBST(int[] nums)
        {
            if (nums == null || nums.Length == 0)
            {
                return null;
            }

            var tree = Lib.ToBinarySearchTree(nums, 0, nums.Length - 1);
            return tree;
        }
        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
