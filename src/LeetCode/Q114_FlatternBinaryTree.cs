using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    public class Q114_FlatternBinaryTree : IProblem
    {
        public void FlattenWithStack(TreeNode root)
        {
            if (root == null)
            {
                return;
            }

            var stack = new Stack<TreeNode>();
            stack.Push(root);
            while (stack.Count > 0)
            {
                var node = stack.Pop();
                if (node.right != null)
                {
                    stack.Push(node.right);
                }

                if (node.left != null)
                {
                    stack.Push(node.left);
                }

                if (stack.Count > 0)
                {
                    node.right = stack.Peek();
                }

                node.left = null;
            }
        }

        public void FlatternInPlace(TreeNode root)
        {
            var cur = root;
            while (cur != null)
            {
                if (cur.left != null)
                {  // if left exist, make left right most leave connect to root.right, then make left the new right
                    var left = cur.left;
                    var p = left;
                    while (p.right != null)
                    {
                        p = p.right;
                    }

                    p.right = cur.right;
                    cur.right = left;
                    cur.left = null;
                }
                cur = cur.right;
            }
        }

        public void FlattenRecurse(TreeNode root)
        {
            if (root == null)
            {
                return;
            }

            if (root.left != null)
            {
                FlattenRecurse(root.left);
            }

            if (root.right != null)
            {
                FlattenRecurse(root.right);
            }

            var temp = root.right;
            root.right = root.left;
            root.left = null;
            while (root.right != null)
            {
                root = root.right;
            }

            root.right = temp;
        }
        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
