using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    819. Most Common Word
    Given a paragraph and a list of banned words, return the most frequent word that is not in the list of banned words.  It is guaranteed there is at least one word that isn't banned, and that the answer is unique.

    Words in the list of banned words are given in lowercase, and free of punctuation.  Words in the paragraph are not case sensitive.  The answer is in lowercase.

    Example:

    Input: 
    paragraph = "Bob hit a ball, the hit BALL flew far after it was hit."
    banned = ["hit"]
    Output: "ball"
    Explanation: 
    "hit" occurs 3 times, but it is a banned word.
    "ball" occurs twice (and no other word does), so it is the most frequent non-banned word in the paragraph. 
    Note that words in the paragraph are not case sensitive,
    that punctuation is ignored (even if adjacent to words, such as "ball,"), 
    and that "hit" isn't the answer even though it occurs more because it is banned.

    Note:

    1 <= paragraph.length <= 1000.
    0 <= banned.length <= 100.
    1 <= banned[i].length <= 10.
    The answer is unique, and written in lowercase (even if its occurrences in paragraph may have uppercase symbols, and even if it is a proper noun.)
    paragraph only consists of letters, spaces, or the punctuation symbols !?',;.
    There are no hyphens or hyphenated words.
    Words only consist of letters, never apostrophes or other punctuation symbols.
    */
    public class Q819_MostCommonWord : IProblem
    {
        public string MostCommonWord(string paragraph, string[] banned)
        {
            var s = Regex.Replace(paragraph, "[^a-zA-Z0-9]", " ");
            s = s.ToLower();
            var words = s.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            var map = new Dictionary<string, int>();
            var bannedSet = new HashSet<string>();
            foreach (var w in banned)
            {
                bannedSet.Add(w);
            }
            var max = 0;
            var res = "";
            foreach (var word in words)
            {
                if (!bannedSet.Contains(word))
                {
                    if (!map.ContainsKey(word))
                    {
                        map.Add(word, 0);
                    }
                    map[word]++;
                    if (map[word] > max)
                    {
                        max = map[word];
                        res = word;
                    }
                }
            }
            return res;
        }

        public string MostCommondWordOptimize(string paragraph, string[] banned)
        {
            paragraph += ".";
            var bannedSet = new HashSet<string>();
            foreach (var s in banned)
            {
                bannedSet.Add(s);
            }
            var map = new Dictionary<string, int>();
            var word = new StringBuilder();
            var max = 0;
            var res = "";
            for (var i = 0; i < paragraph.Length; i++)
            {
                if (char.IsLetter(paragraph[i]))
                {
                    word.Append(paragraph[i]);
                }
                else
                {
                    if (word.Length > 0)
                    {
                        var w = word.ToString().ToLower();
                        if (!bannedSet.Contains(w))
                        {

                            if (!map.ContainsKey(w))
                            {
                                map.Add(w, 0);
                            }
                            map[w]++;
                            if (map[w] > max)
                            {
                                max = map[w];
                                res = w;
                            }
                        }
                    }
                    word = new StringBuilder();
                }
            }
            return res;
        }

        public void Run()
        {
            var banned = new[] { "hit" };

            // using(Lib.UseTimer()) {
            //     var res = MostCommonWord(s, banned);
            //     Console.WriteLine("Most commond word is " + res);
            // }
            using (Lib.UseTimer())
            {
                var input = "Bob";
                var b = new string[0];
                var res = MostCommondWordOptimize(input, b);
                Console.WriteLine("Most commond word is " + res);
            }
        }
    }
}
