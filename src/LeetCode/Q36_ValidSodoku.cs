using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
     * 
     * Determine if a 9x9 Sudoku board is valid. Only the filled cells need to be validated according to the following rules:

Each row must contain the digits 1-9 without repetition.
Each column must contain the digits 1-9 without repetition.
Each of the 9 3x3 sub-boxes of the grid must contain the digits 1-9 without repetition.

A partially filled sudoku which is valid.

The Sudoku board could be partially filled, where empty cells are filled with the character '.'.

Example 1:

Input:
[
  ["5","3",".",".","7",".",".",".","."],
  ["6",".",".","1","9","5",".",".","."],
  [".","9","8",".",".",".",".","6","."],
  ["8",".",".",".","6",".",".",".","3"],
  ["4",".",".","8",".","3",".",".","1"],
  ["7",".",".",".","2",".",".",".","6"],
  [".","6",".",".",".",".","2","8","."],
  [".",".",".","4","1","9",".",".","5"],
  [".",".",".",".","8",".",".","7","9"]
]
Output: true
Example 2:

Input:
[
  ["8","3",".",".","7",".",".",".","."],
  ["6",".",".","1","9","5",".",".","."],
  [".","9","8",".",".",".",".","6","."],
  ["8",".",".",".","6",".",".",".","3"],
  ["4",".",".","8",".","3",".",".","1"],
  ["7",".",".",".","2",".",".",".","6"],
  [".","6",".",".",".",".","2","8","."],
  [".",".",".","4","1","9",".",".","5"],
  [".",".",".",".","8",".",".","7","9"]
]
Output: false
Explanation: Same as Example 1, except with the 5 in the top left corner being 
    modified to 8. Since there are two 8's in the top left 3x3 sub-box, it is invalid.
Note:

A Sudoku board (partially filled) could be valid but is not necessarily solvable.
Only the filled cells need to be validated according to the mentioned rules.
The given board contain only digits 1-9 and the character '.'.
The given board size is always 9x9.
     */
    /*
    这道题让我们验证一个方阵是否为数独矩阵，判断标准是看各行各列是否有重复数字，以及每个小的3x3的小方阵里面是否有重复数字，如果都无重复，
    则当前矩阵是数独矩阵，但不代表待数独矩阵有解，只是单纯的判断当前未填完的矩阵是否是数独矩阵。那么根据数独矩阵的定义，我们在遍历每个数字的时候，
    就看看包含当前位置的行和列以及3x3小方阵中是否已经出现该数字，那么我们需要三个标志矩阵，分别记录各行，各列，各小方阵是否出现某个数字，其中行和
    列标志下标很好对应，就是小方阵的下标需要稍稍转换一下，*/
    public class Q36_ValidSodoku : IProblem
    {
        public bool IsValidSudoku(char[,] board)
        {
            int m = board.GetLength(0), n = board.GetLength(1);
            int[,] row = new int[m, n],
                   col = new int[m, n],
                   cell = new int[m, n];
            for (var i = 0; i < m; i++)
            {
                for (var j = 0; j < n; j++)
                {
                    // number is filled
                    if (board[i, j] >= '1' && board[i, j] <= '9')
                    {
                        var num = board[i, j] - '1';
                        if (row[i, num] > 0 || col[num, j] > 0 || cell[3 * (i / 3) + (j / 3), num] > 0)
                        {
                            return false;
                        }

                        row[i, num]++;
                        col[num, j]++;
                        cell[3 * (i / 3) + (j / 3), num]++;
                    }
                }
            }
            return true;
        }

        public bool IsValidSudoku2(char[][] board)
        {
            var EMPTY = '.';
            var size = board.Length;
            var boxSize = (int)Math.Sqrt(size);
            var rowHash = new HashSet<char>[size];
            var colHash = new HashSet<char>[size];
            var boxHash = new HashSet<char>[size];

            for (var i = 0; i < size; i++)
            {
                rowHash[i] = new HashSet<char>();
                colHash[i] = new HashSet<char>();
                boxHash[i] = new HashSet<char>();
            }

            for (var i = 0; i < size; i++)
            {
                for (var j = 0; j < size; j++)
                {
                    if (board[i][j] != EMPTY)
                    {
                        var val = board[i][j];
                        // row
                        if (rowHash[i].Contains(val))
                        {
                            return false;
                        }
                        rowHash[i].Add(val);

                        // col
                        if (colHash[j].Contains(val))
                        {
                            return false;
                        }
                        colHash[j].Add(val);

                        // box
                        var boxNum = boxSize * (i / boxSize) + (j / boxSize);
                        if (boxHash[boxNum].Contains(val))
                        {
                            return false;
                        }
                        boxHash[boxNum].Add(val);
                    }
                }
            }

            return true;
        }
        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
