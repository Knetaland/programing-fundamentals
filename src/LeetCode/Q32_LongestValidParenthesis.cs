using System;
using System.Collections.Generic;

namespace LeetCode
{
    /*
    Given a string containing just the characters '(' and ')', find the length of the longest valid (well-formed) parentheses substring.

    Example 1:

    Input: "(()"
    Output: 2
    Explanation: The longest valid parentheses substring is "()"
    Example 2:

    Input: ")()())"
    Output: 4
    Explanation: The longest valid parentheses substring is "()()"
    */
    public class Q32_LongestValidParenthesis
    {
        public int LongestValidParentheses(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return 0;
            }

            var stack = new Stack<int>();
            int leftMost = -1, max = 0;
            for (var i = 0; i < s.Length; i++)
            {
                if (s[i] == '(')
                {
                    stack.Push(i);
                }
                else if (s[i] == ')')
                {
                    if (stack.Count == 0)
                    {
                        leftMost = i;
                    }
                    else
                    {
                        stack.Pop();
                        var start = stack.Count == 0 ? leftMost : stack.Peek();
                        max = Math.Max(max, i - start);
                    }
                }
            }
            return max;
        }
    }
}
