using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    You have a total of n coins that you want to form in a staircase shape, where every k-th row must have exactly k coins.

    Given n, find the total number of full staircase rows that can be formed.

    n is a non-negative integer and fits within the range of a 32-bit signed integer.

    Example 1:

    n = 5

    The coins can form the following rows:
    ¤
    ¤ ¤
    ¤ ¤

    Because the 3rd row is incomplete, we return 2.
    Example 2:

    n = 8

    The coins can form the following rows:
    ¤
    ¤ ¤
    ¤ ¤ ¤
    ¤ ¤

    Because the 4th row is incomplete, we return 3.
     */
    public class Q441_ArrangeCoins : IProblem
    {
        public int ArrangeCoins(int n)
        {
            var i = 1;
            while (n >= i)
            {
                n -= i++;
            }

            return i - 1;
        }

        public int ArrangeCoinsBS(int n)
        {
            int start = 0, end = n;
            while (start <= end)
            {
                var mid = start + (end - start) / 2;
                if (mid * (mid + 1) / 2 <= n)
                {
                    start = mid + 1;
                }
                else
                {
                    end = mid - 1;
                }
            }
            return start - 1;
        }
        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
