using System;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q417_PacificAtlanticWaterFlow : IProblem
    {
        public IList<IList<int>> PacificAtlanticBFS(int[][] matrix)
        {
            var res = new List<IList<int>>();
            if (matrix == null || matrix.Length == 0)
            {
                return res;
            }

            var m = matrix.Length;
            var n = matrix[0].Length;
            var atlanticQueue = new Queue<Tuple<int, int>>();
            var pacificQueue = new Queue<Tuple<int, int>>();
            var dirs = new[] {
                new [] {0, -1},
                new [] {0, 1},
                new [] {1, 0},
                new [] {-1, 0},
            };

            var atlanticVisited = new bool[m, n];
            var pacificVisited = new bool[m, n];

            for (var x = 0; x < m; x++)
            {
                pacificQueue.Enqueue(new Tuple<int, int>(x, 0));
                atlanticQueue.Enqueue(new Tuple<int, int>(x, n - 1));
                pacificVisited[x, 0] = true;
                atlanticVisited[x, n - 1] = true;
            }

            for (var y = 0; y < n; y++)
            {
                pacificQueue.Enqueue(new Tuple<int, int>(0, y));
                atlanticQueue.Enqueue(new Tuple<int, int>(m - 1, y));
                pacificVisited[0, y] = true;
                atlanticVisited[m - 1, y] = true;
            }

            BFS(pacificQueue, pacificVisited);
            BFS(atlanticQueue, atlanticVisited);

            for (var i = 0; i < m; i++)
            {
                for (var j = 0; j < n; j++)
                {
                    if (pacificVisited[i, j] && atlanticVisited[i, j])
                    {
                        res.Add(new List<int> { i, j });
                    }
                }
            }
            return res;

            void BFS(Queue<Tuple<int, int>> queue, bool[,] visited)
            {
                while (queue.Count > 0)
                {
                    var node = queue.Dequeue();
                    foreach (var dir in dirs)
                    {
                        var x = node.Item1 + dir[0];
                        var y = node.Item2 + dir[1];
                        if (x < 0 || y < 0 || x >= m || y >= n || visited[x, y] || matrix[x][y] < matrix[node.Item1][node.Item2])
                        {
                            continue;
                        }
                        queue.Enqueue(new Tuple<int, int>(x, y));
                        visited[x, y] = true;
                    }
                }
            }
        }

        public IList<IList<int>> PacificAtlanticBruteForce(int[][] matrix)
        {
            var res = new List<IList<int>>();
            if (matrix == null || matrix.Length == 0)
            {
                return res;
            }

            var m = matrix.Length;
            var n = matrix[0].Length;

            for (var x = 0; x < m; x++)
            {
                for (var y = 0; y < n; y++)
                {
                    if (Helper(x, y, matrix[x][y]) == 3)
                    {
                        res.Add(new List<int> { x, y });
                    }
                }
            }
            return res;

            int Helper(int x, int y, int h)
            {
                if (x < 0 || y < 0)
                {
                    return 1; // flow to pacific
                }

                if (x >= m || y >= n)
                {
                    return 2; // flow to atlantic
                }

                if (matrix[x][y] > h)
                {
                    return 0; // taller, can't flow to that grid
                }

                h = matrix[x][y];
                matrix[x][y] = int.MaxValue; // marked as visited for now
                // Binary 10 | 01 = 11, which is 3, means cna flow to both oceans
                var valid = Helper(x - 1, y, h) |
                    Helper(x + 1, y, h) |
                    Helper(x, y - 1, h) |
                    Helper(x, y + 1, h);
                matrix[x][y] = h;  // set the visited back to original so other path can go through the grid again
                return valid;
            }
        }


        /*
        going from each point to ocean will get timeout in leetcode, this approach is going from ocean to land.
        1. find all nodes reachable from pacific
        2. find all nodes reachable from atlantic
        3. join them to see the nodes both reachable from pacific and atlantic
        */
        public IList<IList<int>> PacificAtlanticDFS(int[][] matrix)
        {
            var res = new List<IList<int>>();
            if (matrix == null || matrix.Length == 0)
            {
                return res;
            }

            var m = matrix.Length;
            var n = matrix[0].Length;
            var pacific = new bool[m, n];
            var atlantic = new bool[m, n];

            for (var x = 0; x < m; x++)
            {
                DFS(x, 0, 0, pacific, matrix);
                DFS(x, n - 1, 0, atlantic, matrix);
            }

            for (var y = 0; y < n; y++)
            {
                DFS(0, y, 0, pacific, matrix);
                DFS(m - 1, y, 0, atlantic, matrix);
            }

            for (var i = 0; i < m; i++)
            {
                for (var j = 0; j < n; j++)
                {
                    if (atlantic[i, j] && pacific[i, j])
                    {
                        res.Add(new List<int> { i, j });
                    }
                }
            }
            return res;
        }

        public void DFS(int x, int y, int h, bool[,] visited, int[][] matrix)
        {
            if (x < 0 || y < 0 || x >= matrix.Length || y >= matrix[0].Length || visited[x, y])
            {
                return;
            }
            if (matrix[x][y] < h)
            {
                return;
            }
            visited[x, y] = true;
            DFS(x - 1, y, matrix[x][y], visited, matrix);
            DFS(x + 1, y, matrix[x][y], visited, matrix);
            DFS(x, y - 1, matrix[x][y], visited, matrix);
            DFS(x, y + 1, matrix[x][y], visited, matrix);
        }
        public void Run()
        {
            var input = new int[][] {
                new int[] {1,2,2,3,5},
                new int[] {3,2,3,4,4},
                new int[] {2,4,5,3,1},
                new int[] {6,7,1,4,5},
                new int[] {5,1,1,2,4},
            };

            using (Lib.UseTimer())
            {
                var res = PacificAtlanticBruteForce(input);
                Lib.Print(res);
            }
            using (Lib.UseTimer())
            {
                var res = PacificAtlanticDFS(input);
                Lib.Print(res);
            }
            using (Lib.UseTimer())
            {
                var res = PacificAtlanticBFS(input);
                Lib.Print(res);
            }
        }
    }
}
