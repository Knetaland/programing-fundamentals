//http://www.1point3acres.com/bbs/thread-161904-1-1.html
using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q1002_TaskScheduler : IProblem
    {
        /*
        第二题是Task Schedule, 地里有面经。大致意思是每种task都有冷却时间，比如task1执行后，要经过interval时间后才能再次执行，求总共所需时间。
        Sample 1 
        tasks: 1, 1, 2, 1.  recovery interval: 2. 
        output: 7  (order is 1 _ _ 1 2 _ 1)
        . From 1point 3acres bbs
        Sample 2
        tasks: 1, 2, 3, 1, 2, 3.  recovery interval: 3
        output: 7  (order is 1 2 3 _ 1 2 3) */

        public int Schedule(int[] tasks, int cd)
        {
            if (tasks == null)
            {
                return 0;
            }

            if (cd == 0)
            {
                return tasks.Length;
            }

            int pos = 0, timer = 0;
            var map = new Dictionary<int, int>();
            for (; pos < tasks.Length; pos++)
            {
                var cur = tasks[pos];
                if (!map.ContainsKey(cur))
                {
                    map.Add(cur, timer + cd + 1);
                }
                else
                {
                    if (timer >= map[cur])
                    {
                        map[cur] = timer + cd + 1;
                    }
                    else
                    {
                        pos--;
                    }
                }
                timer++;
            }
            return timer;
        }
        public void Run()
        {
            var input = new[] { 1, 2, 3, 1, 2, 3 };
            Console.WriteLine("time to run task " + Schedule(input, 3));
        }
    }
}
