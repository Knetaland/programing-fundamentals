using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q58_LengthOfLastWord : IProblem
    {
        public int LengthOfLastWord(string s)
        {
            var end = s.Length - 1;
            while (end >= 0 && s[end] == ' ')
            {
                end--;
            }

            var count = 0;
            while (end >= 0 && s[end] != ' ')
            {
                count++;
                end--;
            }
            return count;
        }
        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
