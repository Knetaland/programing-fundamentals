using System;
using System.Collections.Generic;
using System.Text;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    public class Q257_BinaryTreePath : IProblem
    {

        public IList<string> BinaryTreePaths_V1(TreeNode root) {
            var res = new List<string>();
            if (root == null)
            {
                return res;
            }

            Backtrack(root, res, "");
            return res;
        }
        private void Backtrack(TreeNode root, List<string> res, string path)
        {
            if (root == null) return ;
            if (root.left == null && root.right == null)
            {
                res.Add(path + root.val);
                return ;
            }
            Backtrack(root.left, res, path + root.val +"->");
            Backtrack(root.right, res, path + root.val +"->");
        }


        public IList<string> BinaryTreePaths(TreeNode root)
        {
            var res = new List<string>();
            if (root == null)
            {
                return res;
            }

            Backtrack(root, res, new StringBuilder());
            return res;
        }

        private void Backtrack(TreeNode root, List<string> res, StringBuilder path)
        {
            if (root == null) return ;
            var oldLength = path.Length;
            path.Append(root.val);
            if (root.left == null && root.right == null)
            {
                res.Add(path.ToString());
            } else  {
                 path.Append("->");
                 Backtrack(root.left, res, path);
                Backtrack(root.right, res, path);
            }
            path.Length = oldLength;
        }

        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
