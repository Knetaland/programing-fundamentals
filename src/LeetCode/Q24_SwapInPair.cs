using System;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    public class Q24_SwapInPair : IProblem
    {
        /*
        Given a linked list, swap every two adjacent nodes and return its head.

        You may not modify the values in the list's nodes, only nodes itself may be changed.
        */
        public ListNode SwapPairs(ListNode head)
        {
            var dummy = new ListNode(0)
            {
                next = head
            };
            var cur = dummy;
            // do swap when there are at least two nodes
            while (cur.next != null && cur.next.next != null)
            {
                var first = cur.next;
                var second = first.next;
                first.next = second.next;
                second.next = first;
                cur.next = second;
                cur = first;
            }
            return dummy.next;
        }

        public ListNode SwapInPairsRecursive(ListNode head)
        {
            // base case
            if (head == null || head.next == null)
            {
                return head;
            }

            var first = head;
            var second = head.next;
            first.next = SwapInPairsRecursive(second.next);
            second.next = first;
            return second;
        }
        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
