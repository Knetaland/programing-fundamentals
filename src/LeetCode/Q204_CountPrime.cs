using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
     * ount the number of prime numbers less than a non-negative number, n.

Example:

Input: 10
Output: 4
Explanation: There are 4 prime numbers less than 10, they are 2, 3, 5, 7.
     */
    public class Q204_CountPrime : IProblem
    {
        public int CountPrimes(int n)
        {
            var isPrime = new bool[n];
            for (var i = 2; i < n; i++)
            {
                isPrime[i] = true;
            }

            for (var i = 2; i * i < n; i++)
            {
                if (!isPrime[i])
                {
                    continue;
                }

                for (var j = i * i; j < n; j += i)
                {
                    isPrime[i] = false;
                }
            }
            return isPrime.Count(x => x);
        }
        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
