using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q1109_FlightBookings : IProblem
    {
        public int[] CorpFlightBookings(int[][] bookings, int n) {
            var flights = new int[n];

            foreach(var booking in bookings) {
                var start = booking[0];
                var end = booking[1];
                var count = booking[2];

                flights[start-1] += count;
                if (end < n) {
                    flights[end] -= count;
                }
            }


            for(var i = 1; i < n; i++) {
                flights[i] = flights[i-1] + flights[i];
            }

            Console.WriteLine($"[{string.Join(",", flights)}]");

            return flights;
        }

        public void Run() {
            var bookings = new int[][] {
                new [] {1,2,10},
                new [] {2,3,20},
                new [] {2,5,25},
            };
            var n = 5;

            var re = CorpFlightBookings(bookings, n);
            Console.WriteLine($"[{string.Join(",", re)}]");
        }
    }
}