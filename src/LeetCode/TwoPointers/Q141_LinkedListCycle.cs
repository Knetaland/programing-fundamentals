using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode.TwoPointers
{
    public class Q141_LinkedListCycle : IProblem
    {
        public bool HasCycle(ListNode head)
        {
            if (head == null)
            {
                return false;
            }

            var p1 = head;
            var p2 = head.next;
            while (p1 != null && p2 != null & p2.next != null)
            {
                if (p1 == p2)
                {
                    return true;
                }

                p1 = p1.next;
                p2 = p2.next.next;
            }
            return false;
        }



        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
