using System.Collections.Generic;
using Fundamental.Core.Helpers;

namespace LeetCode
{
    public class Q345_ReverseVowelsOfAString
    {
        /*
        Write a function that takes a string as input and reverse only the vowels of a string.

        Example 1:

        Input: "hello"
        Output: "holle"
        Example 2:

        Input: "leetcode"
        Output: "leotcede"
        Note:
        The vowels does not include the letter "y".
        */

        public string ReverseVowels(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return s;
            }

            var vowelMap = new HashSet<char> {
                'a','e','i','o','u', 'A','E','I','O','U'
            };
            var arr = s.ToCharArray();
            int l = 0, r = arr.Length - 1;
            while (l < r)
            {
                if (!vowelMap.Contains(arr[l]))
                {
                    l++;
                }
                else if (!vowelMap.Contains(arr[r]))
                {
                    r--;
                }
                else
                {
                    Lib.Swap(arr, l, r);
                    l++;
                    r--;
                }
            }

            return new string(arr);
        }
    }
}
