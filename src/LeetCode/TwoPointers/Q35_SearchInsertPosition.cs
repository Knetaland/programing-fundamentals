using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q35_SearchInsertPosition : IProblem
    {
        public int SearchInsert(int[] nums, int target)
        {
            int left = 0, right = nums.Length;
            while (left < right)
            {
                var mid = (left + right) >> 1;
                if (target > nums[mid])
                {
                    left = mid + 1;
                }
                else
                {
                    right = mid;
                }
            }
            return left;
        }

        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
