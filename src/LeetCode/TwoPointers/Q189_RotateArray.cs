namespace LeetCode
{
    public class Q189_RotateArray
    {
        /*
        Given an array, rotate the array to the right by k steps, where k is non-negative.

 

        Example 1:

        Input: nums = [1,2,3,4,5,6,7], k = 3
        Output: [5,6,7,1,2,3,4]
        Explanation:
        rotate 1 steps to the right: [7,1,2,3,4,5,6]
        rotate 2 steps to the right: [6,7,1,2,3,4,5]
        rotate 3 steps to the right: [5,6,7,1,2,3,4]
        Example 2:

        Input: nums = [-1,-100,3,99], k = 2
        Output: [3,99,-1,-100]
        Explanation: 
        rotate 1 steps to the right: [99,-1,-100,3]
        rotate 2 steps to the right: [3,99,-1,-100]
        

        Constraints:

        1 <= nums.length <= 105
        -231 <= nums[i] <= 231 - 1
        0 <= k <= 105
        

        Follow up:

        Try to come up with as many solutions as you can. There are at least three different ways to solve this problem.
        Could you do it in-place with O(1) extra space?
        */

        public void Rotate(int[] nums, int k) {
            var steps = k % nums.Length;
            if (steps == 0) return ;
            var temp = new int[nums.Length];

            for(var i= 0; i <  steps; i++) {
                temp[i] = nums[nums.Length - steps + i];
            }
            var j = 0;
            for(var i = steps; i < nums.Length; i++,j++) {
                temp[i] = nums[j];
            }
            for (var i = 0; i < nums.Length; i++) {
                            nums[i] = temp[i];                
            }
        }

        // O(1) space
        public void RotateFollowUp(int[] nums, int k) {
            var steps = k % nums.Length;
            if (steps == 0) return;
            Reverse(0, nums.Length-1);
            Reverse(0, steps-1);
            Reverse(steps, nums.Length -1);

            void Reverse(int left, int right) {
                while(left < right) {
                    var temp = nums[left];
                    nums[left] = nums[right];
                    nums[right] = temp;
                    left++;
                    right--;
                }
            }
        }
    }
}
