using System;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;


/*
Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.

For example, given nums = [0, 1, 0, 3, 12], after calling your function, nums should be [1, 3, 12, 0, 0].

Note:
You must do this in-place without making a copy of the array.
Minimize the total number of operations.
*/
namespace LeetCode
{
    public class Q283_MoveZero : IProblem
    {
        public void MoveZeroes(int[] nums)
        {
            if (nums == null)
            {
                return;
            }

            var zeroIndex = 0;
            for (var i = 0; i < nums.Length; i++)
            {
                //  [1, 3, 0, 0, 12],
                if (nums[i] != 0)
                {
                    // reduce unnecessary swap, only swap when zeroIndex has value 0
                    if (nums[zeroIndex] == 0)
                    {
                        Lib.Swap(nums, i, zeroIndex);
                    }
                    zeroIndex++;
                }
            }
        }

        public void MoveZeroes2(int[] nums)
        {
            if (nums == null)
            {
                return;
            }

            var nonZeroIndex = 0;
            for (var i = 0; i < nums.Length; i++)
            {
                if (nums[i] != 0)
                {
                    nums[nonZeroIndex++] = nums[i];
                }
            }
            while (nonZeroIndex < nums.Length)
            {
                nums[nonZeroIndex++] = 0;
            }
        }

        public void Run()
        {
            var input = new[] { 1, 2, 4, 0, 0, 0, 43, 0, 13, 0, 32, 31, 0 };
            Console.WriteLine("input: " + string.Join(",", input));
            using (Lib.UseTimer())
            {
                MoveZeroes(input);
                Console.WriteLine("output: " + string.Join(",", input));
            }
            using (Lib.UseTimer())
            {
                MoveZeroes2(input);
                Console.WriteLine("output 2 : " + string.Join(",", input));
            }
        }
    }
}
