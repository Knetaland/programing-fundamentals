using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given a non-negative integer c, your task is to decide whether there're two integers a and b such that a2 + b2 = c.

    Example 1:

    Input: 5
    Output: True
    Explanation: 1 * 1 + 2 * 2 = 5
    

    Example 2:

    Input: 3
    Output: False
    */
    public class Q633_SumOfSquareNumbers : IProblem
    {
        public bool JudgeSquareSum(int c)
        {
            if (c < 0)
            {
                return false;
            }

            int l = 0, r = (int)Math.Sqrt(c);
            while (l <= r)
            {
                var sum = l * l + r * r;
                if (sum == c)
                {
                    return true;
                }

                if (sum < c)
                {
                    l++;
                }
                else
                {
                    r--;
                }
            }
            return false;
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
