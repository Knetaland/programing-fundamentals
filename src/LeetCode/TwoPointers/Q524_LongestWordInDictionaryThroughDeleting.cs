using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode.TwoPointers
{
    /*
    Given a string and a string dictionary, 
    find the longest string in the dictionary that can be formed by deleting some characters of the given 
    string. If there are more than one possible results, 
    return the longest word with the smallest lexicographical order. If there is no possible result, return the empty string.

    Example 1:

    Input:
    s = "abpcplea", d = ["ale","apple","monkey","plea"]

    Output: 
    "apple"
    Example 2:

    Input:
    s = "abpcplea", d = ["a","b","c"]

    Output: 
    "a"
    */
    public class Q524_LongestWordInDictionaryThroughDeleting : IProblem
    {
        public string FindLongestWord(string s, IList<string> d)
        {
            var max = 0;
            var maxWord = "";
            foreach (var word in d)
            {
                // since answer needs to be the smallest lexicographical order. 
                if (maxWord.Length > word.Length ||
                    // same length as max, if lexicographical order is larger, no point to check subsequence
                    (maxWord.Length == word.Length && maxWord.CompareTo(word) < 0))
                {
                    continue;
                }

                if (IsSubsequence(s, word))
                {
                    if (word.Length >= max)
                    {  // no need because of check above
                        max = word.Length;   // no need because of check above
                        maxWord = word;
                    }
                }
            }
            return maxWord;
        }

        public bool IsSubsequence(string s, string t)
        {
            if (t.Length > s.Length)
            {
                return false;
            }

            var j = 0;
            for (var i = 0; i < s.Length && j < t.Length; i++)
            {
                if (s[i] == t[j])
                {
                    j++;
                }
            }
            return j == t.Length;
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
