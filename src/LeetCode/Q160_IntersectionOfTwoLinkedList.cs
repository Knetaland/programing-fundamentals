using System;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    public class Q160_IntersectionOfTwoLinkedList : IProblem
    {
        /*
        Write a program to find the node at which the intersection of two singly linked lists begins.

        For example, the following two linked lists:

        */

        /*We can use two iterations to do that. In the first iteration, we will reset the pointer of 
        one linkedlist to the head of another linkedlist after it reaches the tail node. 
        In the second iteration, we will move two pointers until they points to the same node. 
        Our operations in first iteration will help us counteract the difference. 
        So if two linkedlist intersects, the meeting point in second iteration must be the intersection point.
        If the two linked lists have no intersection at all, then the meeting pointer in second iteration must be the tail node of both lists, which is null */

        public ListNode GetIntersectionNode(ListNode headA, ListNode headB)
        {
            if (headA == null || headB == null)
            {
                return null;
            }

            ListNode a = headA, b = headB;
            // when one list reaches the end, it will start from the other list. So when the other list ends,
            // first list should reach the position where match the pos of start of the shorter list

            while (a != b)
            {
                a = a == null ? headB : a.next;
                b = b == null ? headA : b.next;
            }
            return a;
        }


        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
