using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    public class Q101_SymmetricTree : IProblem
    {
        public bool IsSymmetric(TreeNode root)
        {
            if (root == null)
            {
                return true;
            }

            return Helper(root.left, root.right);
        }

        private bool Helper(TreeNode left, TreeNode right)
        {
            if (left == null && right == null)
            {
                return true;
            }

            if (left == null || right == null)
            {
                return false;
            }

            if (left.val != right.val)
            {
                return false;
            }

            return Helper(left.left, right.right) && Helper(left.right, right.left);
        }


        public bool IsSymmetricIterative(TreeNode root)
        {
            if (root == null)
            {
                return true;
            }

            var stack = new Stack<(TreeNode, TreeNode)>();
            stack.Push((root.left, root.right));
            while (stack.Count > 0)
            {
                var pair = stack.Pop();
                var left = pair.Item1;
                var right = pair.Item2;
                if (left == null && right == null)
                {
                    continue;
                }

                if (left == null || right == null)
                {
                    return false;
                }

                if (left.val == right.val)
                {
                    stack.Push((left.left, right.right));
                    stack.Push((left.right, right.left));
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
