using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    You are given coins of different denominations and a total amount of money amount. Write a function to compute the fewest number of coins that you need to make up that amount. If that amount of money cannot be made up by any combination of the coins, return -1.

    Example 1:
    coins = [1, 2, 5], amount = 11
    return 3 (11 = 5 + 5 + 1)

    Example 2:
    coins = [2], amount = 3
    return -1.

    Note:
    You may assume that you have an infinite number of each kind of coin.
     */
    public class Q322_CoinChange : IProblem
    {
        // res[i] = math.Min(res[i], 1+ res[i- coins[j]])
        // 1. not taking coin, res[i]
        // 2. taking the coin, 1 + res[i-coins[j]]
        public int CoinChange(int[] coins, int amount)
        {
            var res = new int[amount + 1];
            var cs = new int[amount + 1];
            for (var i = 1; i < res.Length; i++)
            {
                res[i] = int.MaxValue - 1;
            }
            for (var j = 0; j < coins.Length; j++)
            {
                for (var i = coins[j]; i < res.Length; i++)
                {
                    if (res[i - coins[j]] < res[i])
                    {
                        res[i] = 1 + res[i - coins[j]];
                        cs[i] = j;
                    }

                }
            }

            if (res[amount] == int.MaxValue - 1)
            {
                return -1;
            }

            var x = amount;
            Console.Write("coins are :");
            while (x > 0)
            {
                Console.Write(coins[cs[x]] + " ");
                x = x - coins[cs[x]];
            }
            return res[amount];
        }


        public int CoinChangeBFS(int[] coins, int amount) {
            var queue = new Queue<int>();
            var count = 0;
            var visited = new HashSet<int>();
            queue.Enqueue(amount);
            while(queue.Count > 0) {
                var n = queue.Count;
                for(var i = 0; i < n; i++){
                    var curAmount = queue.Dequeue();
                    visited.Add(curAmount);
                    if (curAmount == 0) return count;
                    foreach(var coin in coins) {
                        var temp = curAmount - coin;
                        if (temp >=0 && !visited.Contains(temp)) {
                            queue.Enqueue(temp);
                        }
                    }
                }
                count++;
            }
            return -1;
        }
        public void Run()
        {
            var coins = new[] { 1, 5, 10 };
            var amount = 33;
            // var x = CoinChange(coins, amount);
            var x = CoinChangeBFS(coins, amount);
            Console.WriteLine("Min coins required: " + x);
        }
    }
}
