using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given a m x n matrix, if an element is 0, set its entire row and column to 0. Do it in-place.
    */
    public class Q73_SetMatrixZeroes : IProblem
    {
        /*
        O(m*n)
        O(m+n)
        */
        public void SetZeroes(int[][] matrix)
        {
            int m = matrix.Length, n = matrix[0].Length;
            var rowFlags = new bool[m];
            var colFlags = new bool[n];

            for (var i = 0; i < m; i++)
            {
                for (var j = 0; j < n; j++)
                {
                    if (matrix[i][j] == 0)
                    {
                        rowFlags[i] = true;
                        colFlags[j] = true;
                    }
                }
            }

            for (var i = 0; i < m; i++)
            {
                for (var j = 0; j < n; j++)
                {
                    if (rowFlags[i] || colFlags[j])
                    {
                        matrix[i][j] = 0;
                    }
                }
            }
        }



        /*
        https://www.youtube.com/watch?v=T41rL0L3Pnw
         O(m*n): 2 (m * n)
         O(1): use two bools
        */
        public void SetZeroesInPlace(int[][] matrix)
        {
            int m = matrix.Length, n = matrix[0].Length;
            var firstRowZero = false;
            var firstColZero = false;

            for (var row = 0; row < m; row++)
            {
                for (var col = 0; col < n; col++)
                {
                    if (matrix[row][col] == 0)
                    {
                       // using the first row and first col to store the state, we can do this because
                       // we are going left -> right, top -> bottom, the 1st row and 1st col will be visited when 
                       // going through i,j, manipulating them wont cause discrepancy. 
                       matrix[0][col] = 0;
                       matrix[row][0] = 0;
                       if (row == 0) {
                        firstRowZero = true;
                       }
                       if (col == 0) {
                        firstColZero = true;
                       }
                       
                    }
                }
            }

            // from row1+ and col1+
            for (var row = 1; row < m; row++)
            {
                for (var col = 1; col < n; col++)
                {
                    if (matrix[row][0]==0 || matrix[0][col]==0)
                    {
                        matrix[row][col] = 0;
                    }
                }
            }

            if (firstRowZero) {
                for(var col = 0; col < n; col++) {
                    matrix[0][col] = 0;
                }
            }
            if (firstColZero) {
                for(var row = 0; row < m; row++) {
                    matrix[row][0] = 0;
                }
            }
            
        }
        public void Run()
        {
            int[][] matrix = [[0,1,2,0],[3,4,5,2],[1,3,1,5]];
            SetZeroesInPlace(matrix);
            matrix.Print();
        }
    }
}
