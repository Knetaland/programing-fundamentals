using System;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q8_Atoi : IProblem
    {
        public int Atoi(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return 0;
            }
            // Ignore leading white space
            var trim = str.Trim();
            
            int i = 0, sign = 1, res = 0;
            // Parse positive/negative number
            if (trim[i] == '+' || trim[i] == '-')
            {
                sign = trim[i] == '+' ? 1 : -1;
                i++;
            }
            for (; i < trim.Length; i++)
            {
                var digit = trim[i] - '0';
                // make sure char is a digit
                if (digit < 0 || digit > 9)
                {
                    break;
                }

                if (res > int.MaxValue / 10  || (int.MaxValue / 10 == res &&  digit > int.MaxValue % 10 ))
                {
                    return sign == 1 ? int.MaxValue : int.MinValue;
                }

                res = res * 10 + digit;
            }
            return res * sign;
        }

        public int AtoiNew(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return 0;
            }
            var res = 0;
            var i = 0;
            var sign = 1;
            while (i < str.Length && str[i] == ' ')
            {
                i++;
            }
            if (str[i] == '+' || str[i] == '-')
            {
                sign = str[i] == '+' ? 1 : -1;
                i++;
            }
            for (; i < str.Length; i++)
            {
                var digit = str[i] - '0';
                if (digit < 0 || digit > 9)
                {
                    break;
                }
                if (sign == 1)
                {
                    if (res > int.MaxValue / 10 || (res == int.MaxValue / 10 && digit > int.MaxValue % 10))
                    {
                        return int.MaxValue;
                    }
                }
                else
                {
                    if (res * -1 < int.MinValue / 10 || (res * -1 == int.MinValue / 10 && digit * -1 < int.MinValue % 10))
                    {
                        return int.MinValue;
                    }
                }

                res = res * 10 + digit;
            }
            return res * sign;
        }

        public void Run()
        {
            var str = "0-2";

            using (Lib.UseTimer())
            {
                var res = AtoiNew(str);
                Console.WriteLine($"{str} atoi NEW is {res}");
            }
            using (Lib.UseTimer())
            {
                var res = Atoi(str);
                Console.WriteLine($"{str} atoi is {res}");
            }

            using (Lib.UseTimer())
            {
                var res = Atoi(str);
                Console.WriteLine($"{str} atoi is {res}");
            }
            using (Lib.UseTimer())
            {
                var res = AtoiNew(str);
                Console.WriteLine($"{str} atoi NEW is {res}");
            }
        }
    }
}
