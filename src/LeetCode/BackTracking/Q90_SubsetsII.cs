using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q90_SubsetsII : IProblem
    {
        public IList<IList<int>> Subsets(int[] nums)
        {
            var res = new List<IList<int>>();
            if (nums == null)
            {
                return res;
            }

            Array.Sort(nums);
            Backtrack(res, nums, new List<int>(), 0);
            return res;
        }

    /*  
        [1,2,2]
       level: 
         start 2         
         temp  [1,]
        res: [[], [1], [1,2], [1,2,2], ]
           
    */
    

        private void Backtrack(List<IList<int>> res, int[] nums, List<int> list, int start)
        {
            res.Add(new List<int>(list));
            for (var i = start; i < nums.Length; i++)
            {
                if (i > start && nums[i] == nums[i - 1])
                {
                    continue;
                }

                list.Add(nums[i]);
                Backtrack(res, nums, list, i + 1);
                list.RemoveAt(list.Count - 1);
            }
        }


        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
