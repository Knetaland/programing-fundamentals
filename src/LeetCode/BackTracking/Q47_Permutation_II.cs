using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode.BackTracking
{
    public class Q47_Permutation_II : IProblem
    {
        public IList<IList<int>> Permute(int[] nums)
        {
            var res = new List<IList<int>>();
            if (nums == null)
            {
                return res;
            }

            var used = new bool[nums.Length];
            Array.Sort(nums);
            BackTrack(res, nums, new List<int>(), used);
            return res;
        }

        private void BackTrack(List<IList<int>> res, int[] nums, List<int> list, bool[] used)
        {
            if (list.Count == nums.Length)
            {
                res.Add(new List<int>(list));
                return;
            }
            for (var i = 0; i < nums.Length; i++)
            {
                if (used[i])
                {
                    continue;
                }
                // when a number has the same value with its previous, we can use this number only if his previous is used
                if (i > 0 && nums[i - 1] == nums[i] && !used[i - 1])
                {
                    continue;
                }

                list.Add(nums[i]);
                used[i] = true;
                BackTrack(res, nums, list, used);
                used[i] = false;
                list.RemoveAt(list.Count - 1);
            }
        }

        public void Run()
        {
            var input = new[] { 2, 1, 2, 3 };
            var output = Permute(input);
            foreach (var o in output)
            {
                Console.WriteLine(string.Join(",", o));
            }
        }
    }
}
