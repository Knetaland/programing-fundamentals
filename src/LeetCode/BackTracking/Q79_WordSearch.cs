using Fundamental.Core.Interfaces;

namespace LeetCode.BackTracking
{
    public class Q79_WordSearch : IProblem
    {
        /*Given a 2D board and a word, find if the word exists in the grid.

        The word can be constructed from letters of sequentially adjacent cell, where "adjacent" cells are those horizontally or vertically neighboring. The same letter cell may not be used more than once.

        For example,
        Given board =

        [
         ['A','B','C','E'],
         ['S','F','C','S'],
         ['A','D','E','E']
        ]
        word = "ABCCED", -> returns true,
        word = "SEE", -> returns true,
        word = "ABCB", -> returns false.*/
        public bool Exist(char[][] board, string word)
        {
            for (var i = 0; i < board.Length; i++)
            {
                for (var j = 0; j < board[0].Length; j++)
                {
                    if (board[i][j] == word[0] && Exist(board, i, j, word, 0))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private bool Exist(char[][] board, int i, int j, string word, int current)
        {
            if (current == word.Length)
            {
                return true;
            }

            if (i < 0 || j < 0 || i >= board.Length || j >= board[0].Length)
            {
                return false;
            }
            // if current char is not the same of current postion at word, or current char is already visited
            if (board[i][j] != word[current] || board[i][j] == '*')
            {
                return false;
            }

            var temp = board[i][j];
            board[i][j] = '*'; // marked as visited
            var exist = Exist(board, i, j - 1, word, current + 1) ||
                Exist(board, i, j + 1, word, current + 1) ||
                Exist(board, i - 1, j, word, current + 1) ||
                Exist(board, i + 1, j, word, current + 1);
            board[i][j] = temp;
            return exist;
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
