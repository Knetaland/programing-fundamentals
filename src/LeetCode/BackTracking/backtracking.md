# Backtracking

result = []

def backtrack(path, 选择列表)
  if 满足结束条件：
    result.add(path)
    return

  for 选择 in  选择列表：
    做选择
    backtrack(path, 选择列表)
    撤销选择

https://leetcode.com/problems/subsets/solutions/27281/A-general-approach-to-backtracking-questions-in-Java-(Subsets-Permutations-Combination-Sum-Palindrome-Partitioning)/
