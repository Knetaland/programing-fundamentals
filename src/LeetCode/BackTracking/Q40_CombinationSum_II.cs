using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q40_CombinationSum_II : IProblem
    {
        public IList<IList<int>> CombinationSum2(int[] candidates, int target)
        {
            var res = new List<IList<int>>();
            Array.Sort(candidates);
            Backtrack(res, new List<int>(), candidates, target, 0);
            return res;
        }

        private void Backtrack(List<IList<int>> res, List<int> curList, int[] nums, int remain, int pos)
        {
            if (remain < 0)
            {
                return;
            }

            if (remain == 0)
            {
                res.Add(new List<int>(curList));
                return;
            }
            for (var i = pos; i < nums.Length; i++)
            {
                if (i > pos && nums[i] == nums[i - 1])
                {
                    continue;
                }

                curList.Add(nums[i]);
                Backtrack(res, curList, nums, remain - nums[i], i + 1);
                curList.RemoveAt(curList.Count - 1);
            }
        }


        public void Run()
        {
            var input = new[] { 10, 1, 2, 7, 6, 1, 5 };
            var target = 8;
            var res = CombinationSum2(input, target);
            Console.WriteLine($"{string.Join(",", input)} for target {target} is [");
            foreach (var r in res)
            {
                Console.WriteLine(string.Join(",", r));
            }
            Console.WriteLine("]");
        }
    }
}
