﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode;


/*
https://leetcode.com/problems/word-break-ii/solutions/44167/my-concise-java-solution-based-on-memorized-dfs/

Given a string s and a dictionary of strings wordDict, add spaces in s to construct a sentence where each word is a valid dictionary word. Return all such possible sentences in any order.

Note that the same word in the dictionary may be reused multiple times in the segmentation.

Example 1:

Input: s = "catsanddog", wordDict = ["cat","cats","and","sand","dog"]
Output: ["cats and dog","cat sand dog"]
Example 2:

Input: s = "pineapplepenapple", wordDict = ["apple","pen","applepen","pine","pineapple"]
Output: ["pine apple pen apple","pineapple pen apple","pine applepen apple"]
Explanation: Note that you are allowed to reuse a dictionary word.
Example 3:

Input: s = "catsandog", wordDict = ["cats","dog","sand","and","cat"]
Output: []
 

Constraints:

1 <= s.length <= 20
1 <= wordDict.length <= 1000
1 <= wordDict[i].length <= 10
s and wordDict[i] consist of only lowercase English letters.
All the strings of wordDict are unique.
Input is generated in a way that the length of the answer doesn't exceed 105.
*/
public class Q140_WordBreak_II : IProblem
{
    public void Run() {
        var s = "catsandog";
        //  "pineapplepenapple";
        string[] wordDict =  ["cats","dog","sand","and","cat"];
        // ["apple","pen","applepen","pine","pineapple"];
        var res = WordBreak(s, wordDict);
        // var res = wordBreak2(s, new HashSet<string>(wordDict));
        res.Print();
    }

    // Simple backtracking
    public IList<string> WordBreak(string s, IList<string> wordDict) {
        var res = new List<string>();
        dfs(s, "");
        return res;
        void dfs(string rest, string phrases) {
            // Console.WriteLine($"processing {rest} with phrase {phrases}");
            if (rest.Length == 0) {
                res.Add(phrases.Trim());
                // Console.WriteLine($"Adding phrases {phrases} to result set");
                return ;
            }
            foreach (var word in wordDict) {
                // Console.WriteLine($"checking if word {word} is in string {rest}");
                if (rest.StartsWith(word)) {
                    dfs(rest.Substring(word.Length), phrases + " " + word);
                }
            }
        }
    }

    public IList<string> WordBreakWithMemo(string s, IList<string> wordDict) {
        var memo = new Dictionary<string, List<string>>();

        return dfs(s);

        IList<string> dfs(string rest) {
            if (memo.ContainsKey(rest)) {
                Console.WriteLine($"memo found rest [{rest}], returing [{string.Join(',', memo[rest])}]");
                return memo[rest];
            }
            var res = new List<string>();
            if (rest.Length == 0) {
                res.Add("");
                return res;
            }

            foreach(var word in wordDict) {
                if (rest.StartsWith(word)) {
                    Console.WriteLine($"word [{rest}] starts with {word}, backtracking...");
                    var sublist = dfs(rest.Substring(word.Length));
                    Console.WriteLine($"sublist is now [{string.Join(',', sublist)}]");
                    foreach(var sub in sublist) {
                        res.Add(word + (sub.Length == 0 ? "" : " ") +  sub);
                    }
                    Console.WriteLine($"res is now [{string.Join(',', res)}]");
                }
            }
            Console.WriteLine($"Adding to memo {rest}: [{string.Join(',', res)}]");
            memo.Add(rest, res);
            return res;
        }

    }


    Dictionary<string, List<string>> memo = new Dictionary<string, List<string>>();
    public List<string> wordBreak2(string s, ISet<string> wordDict) {
        List<string> res = new List<string>();
        if(s == null || s.Length == 0) {
            // Console.WriteLine($"BASE case: s is empty");
            return res;
        }
        if(memo.ContainsKey(s)) {
            // Console.WriteLine($"map contains {s}, returning {string.Join(',', memo[s])}");
            return memo[s];
        }
        if(wordDict.Contains(s)) {
            // Console.WriteLine($"dictionary contains {s}, adding it to res");
            res.Add(s);
        }
        for(int i = 1 ; i < s.Length ; i++) {
            string t = s.Substring(i);
            // Console.WriteLine($"t is {t} at index {i}");
            if(wordDict.Contains(t)) {
                // Console.WriteLine($"{t} is found in dictionary! running wordbreak on {s.Substring(0 , i)}");
                List<string> temp = wordBreak2(s.Substring(0 , i) , wordDict);
                if(temp.Count != 0) {
                    // Console.WriteLine($"temp is not empty, adding them to res {string.Join(',', temp)}");
                    for(int j = 0 ; j < temp.Count ; j++) {
                        res.Add(temp[j] + " " + t);
                    }
                    // Console.WriteLine($"res is now {string.Join(',', res)}");
                }
            }
        }
        memo.Add(s , res);
        // Console.WriteLine($"!!! adding to map {s}: [{string.Join(',', res)}]");
        return res;
    }
}
