using System;
using System.Collections.Generic;
using System.Text;
using Fundamental.Core.Interfaces;

namespace LeetCode.BackTracking
{
    // CHECK https://www.youtube.com/watch?v=lmypbtgdpuQ
    public class Q126_WordLadder_II : IProblem
    {
        public void Run()
        {
            string beginWord = "cet", endWord = "ism";
            var wordList = new[] { "kid", "tag", "pup", "ail", "tun", "woo", "erg", "luz", "brr", "gay", "sip", "kay", "per", "val", "mes", "ohs", "now", "boa", "cet", "pal", "bar", "die", "war", "hay", "eco", "pub", "lob", "rue", "fry", "lit", "rex", "jan", "cot", "bid", "ali", "pay", "col", "gum", "ger", "row", "won", "dan", "rum", "fad", "tut", "sag", "yip", "sui", "ark", "has", "zip", "fez", "own", "ump", "dis", "ads", "max", "jaw", "out", "btu", "ana", "gap", "cry", "led", "abe", "box", "ore", "pig", "fie", "toy", "fat", "cal", "lie", "noh", "sew", "ono", "tam", "flu", "mgm", "ply", "awe", "pry", "tit", "tie", "yet", "too", "tax", "jim", "san", "pan", "map", "ski", "ova", "wed", "non", "wac", "nut", "why", "bye", "lye", "oct", "old", "fin", "feb", "chi", "sap", "owl", "log", "tod", "dot", "bow", "fob", "for", "joe", "ivy", "fan", "age", "fax", "hip", "jib", "mel", "hus", "sob", "ifs", "tab", "ara", "dab", "jag", "jar", "arm", "lot", "tom", "sax", "tex", "yum", "pei", "wen", "wry", "ire", "irk", "far", "mew", "wit", "doe", "gas", "rte", "ian", "pot", "ask", "wag", "hag", "amy", "nag", "ron", "soy", "gin", "don", "tug", "fay", "vic", "boo", "nam", "ave", "buy", "sop", "but", "orb", "fen", "paw", "his", "sub", "bob", "yea", "oft", "inn", "rod", "yam", "pew", "web", "hod", "hun", "gyp", "wei", "wis", "rob", "gad", "pie", "mon", "dog", "bib", "rub", "ere", "dig", "era", "cat", "fox", "bee", "mod", "day", "apr", "vie", "nev", "jam", "pam", "new", "aye", "ani", "and", "ibm", "yap", "can", "pyx", "tar", "kin", "fog", "hum", "pip", "cup", "dye", "lyx", "jog", "nun", "par", "wan", "fey", "bus", "oak", "bad", "ats", "set", "qom", "vat", "eat", "pus", "rev", "axe", "ion", "six", "ila", "lao", "mom", "mas", "pro", "few", "opt", "poe", "art", "ash", "oar", "cap", "lop", "may", "shy", "rid", "bat", "sum", "rim", "fee", "bmw", "sky", "maj", "hue", "thy", "ava", "rap", "den", "fla", "auk", "cox", "ibo", "hey", "saw", "vim", "sec", "ltd", "you", "its", "tat", "dew", "eva", "tog", "ram", "let", "see", "zit", "maw", "nix", "ate", "gig", "rep", "owe", "ind", "hog", "eve", "sam", "zoo", "any", "dow", "cod", "bed", "vet", "ham", "sis", "hex", "via", "fir", "nod", "mao", "aug", "mum", "hoe", "bah", "hal", "keg", "hew", "zed", "tow", "gog", "ass", "dem", "who", "bet", "gos", "son", "ear", "spy", "kit", "boy", "due", "sen", "oaf", "mix", "hep", "fur", "ada", "bin", "nil", "mia", "ewe", "hit", "fix", "sad", "rib", "eye", "hop", "haw", "wax", "mid", "tad", "ken", "wad", "rye", "pap", "bog", "gut", "ito", "woe", "our", "ado", "sin", "mad", "ray", "hon", "roy", "dip", "hen", "iva", "lug", "asp", "hui", "yak", "bay", "poi", "yep", "bun", "try", "lad", "elm", "nat", "wyo", "gym", "dug", "toe", "dee", "wig", "sly", "rip", "geo", "cog", "pas", "zen", "odd", "nan", "lay", "pod", "fit", "hem", "joy", "bum", "rio", "yon", "dec", "leg", "put", "sue", "dim", "pet", "yaw", "nub", "bit", "bur", "sid", "sun", "oil", "red", "doc", "moe", "caw", "eel", "dix", "cub", "end", "gem", "off", "yew", "hug", "pop", "tub", "sgt", "lid", "pun", "ton", "sol", "din", "yup", "jab", "pea", "bug", "gag", "mil", "jig", "hub", "low", "did", "tin", "get", "gte", "sox", "lei", "mig", "fig", "lon", "use", "ban", "flo", "nov", "jut", "bag", "mir", "sty", "lap", "two", "ins", "con", "ant", "net", "tux", "ode", "stu", "mug", "cad", "nap", "gun", "fop", "tot", "sow", "sal", "sic", "ted", "wot", "del", "imp", "cob", "way", "ann", "tan", "mci", "job", "wet", "ism", "err", "him", "all", "pad", "hah", "hie", "aim", "ike", "jed", "ego", "mac", "baa", "min", "com", "ill", "was", "cab", "ago", "ina", "big", "ilk", "gal", "tap", "duh", "ola", "ran", "lab", "top", "gob", "hot", "ora", "tia", "kip", "han", "met", "hut", "she", "sac", "fed", "goo", "tee", "ell", "not", "act", "gil", "rut", "ala", "ape", "rig", "cid", "god", "duo", "lin", "aid", "gel", "awl", "lag", "elf", "liz", "ref", "aha", "fib", "oho", "tho", "her", "nor", "ace", "adz", "fun", "ned", "coo", "win", "tao", "coy", "van", "man", "pit", "guy", "foe", "hid", "mai", "sup", "jay", "hob", "mow", "jot", "are", "pol", "arc", "lax", "aft", "alb", "len", "air", "pug", "pox", "vow", "got", "meg", "zoe", "amp", "ale", "bud", "gee", "pin", "dun", "pat", "ten", "mob" };
            var map = BuildMap(beginWord, wordList);
            var length = BFS(beginWord, endWord, map);
            Console.WriteLine("shortest length is " + length);
            var res = FindLadders(beginWord, endWord, wordList, length);
            //  var res = FindLaddersCopy(beginWord, endWord, wordList); 
            foreach (var s in res)
            {
                Console.WriteLine($"{string.Join("-> ", s)}");
            }
        }

        public int BFS(string beginWord, string endWord, Dictionary<string, List<string>> map)
        {
            var queue = new Queue<string>();
            var visited = new HashSet<string>
            {
                beginWord
            };
            queue.Enqueue(beginWord);
            var pathLength = 1;
            while (queue.Count > 0)
            {
                pathLength++;
                var size = queue.Count;
                while (size > 0)
                {
                    var word = queue.Dequeue();
                    foreach (var transform in map[word])
                    {
                        if (transform == endWord)
                        {
                            return pathLength;
                        }
                        if (!visited.Contains(transform))
                        {
                            queue.Enqueue(transform);
                            visited.Add(transform);
                        }
                    }
                    size--;
                }
            }
            return 0;
        }

        public IList<IList<string>> FindLadders(string beginWord, string endWord, IList<string> wordList, int shortest)
        {
            var map = BuildMap(beginWord, wordList);
            Console.WriteLine("map built");
            var res = new List<IList<string>>();
            var visited = new HashSet<string>
            {
                beginWord
            };
            Backtrack(res, new List<string> { beginWord }, endWord, visited, map, shortest);
            return res;
        }

        public void Backtrack(IList<IList<string>> res, IList<string> curList, string endWord, HashSet<string> visited, Dictionary<string, List<string>> map, int shortest)
        {
            if (curList.Count > shortest)
            {
                return;
            }
            var lastWord = curList[curList.Count - 1];

            if (lastWord == endWord)
            {

                if (res.Count == 0)
                {
                    res.Add(new List<string>(curList));
                }
                else if (curList.Count == res[0].Count)
                {
                    res.Add(new List<string>(curList));
                }
                else if (curList.Count < res[0].Count)
                {
                    res.Clear();
                    res.Add(new List<string>(curList));
                }
                return;
            }
            if (res.Count == 0 || curList.Count < res[0].Count)
            {
                // Console.WriteLine($"try and current list is {string.Join("-> ", curList)}");
                if (map.ContainsKey(lastWord))
                {
                    foreach (var nextWord in map[lastWord])
                    {
                        if (!visited.Contains(nextWord))
                        {
                            visited.Add(nextWord);
                            curList.Add(nextWord);
                            Backtrack(res, curList, endWord, visited, map, shortest);
                            curList.RemoveAt(curList.Count - 1);
                            visited.Remove(nextWord);
                        }
                    }
                }
            }
        }

        Dictionary<string, List<string>> BuildMap(string beginWord, IList<string> wordList)
        {
            var map = new Dictionary<string, List<string>>();
            var wordSet = new HashSet<string>
            {
                beginWord
            };
            foreach (var w in wordList)
            {
                wordSet.Add(w);
            }
            // build the graph
            foreach (var w in wordSet)
            {
                var n = w.Length;
                for (var i = 0; i < n; i++)
                {
                    var sb = new StringBuilder(w);
                    for (var t = 'a'; t <= 'z'; t++)
                    {
                        if (sb[i] != t)
                        {
                            sb[i] = t;
                            var newWord = sb.ToString();
                            if (wordSet.Contains(newWord))
                            {
                                if (!map.ContainsKey(w))
                                {
                                    map.Add(w, new List<string>());
                                }
                                map[w].Add(newWord);
                            }
                        }
                    }
                }
            }
            return map;
        }

        // public IList<IList<string>> FindLaddersCopy(string beginWord, string endWord, IList<string> wordList) {
        //     var res = new List<IList<string>>();
        //     var dict = new HashSet<string>{wordList[0], wordList[wordList.Count-1]};
        //     var p = new List<string>{beginWord};
        //     var paths = new Queue<List<string>>();
        //     paths.Enqueue(p);
        //     int level = 1, minLevel = int.MaxValue;
        //     var words  =new HashSet<string>();

        //     while(paths.Count > 0) {
        //         var t = paths.Dequeue();
        //         if (t.Count >  level) {
        //             foreach(var w in words) {
        //                 dict.Remove(w);
        //             }
        //             words.Clear();
        //             level = t.Count;
        //             if (level > minLevel) {
        //                 break;
        //             }
        //         }

        //         string last = t[t.Count-1];
        //         for(var i = 0; i < last.Length; i++) {
        //             var newLast = new StringBuilder(last);
        //             for(var ch = 'a' ; ch <='z'; ch++) {
        //                 newLast[i] = ch;
        //                 if (!dict.Contains(newLast.ToString())) {
        //                     continue;
        //                 }
        //                 words.Add(newLast.ToString());
        //                 var nextPath = t;
        //                 nextPath.Add(newLast.ToString());
        //                 if (newLast.ToString() == endWord) {
        //                     res.Add(nextPath);
        //                      minLevel = level;                             
        //                 } else {
        //                     paths.Enqueue(nextPath);
        //                 }
        //             }
        //         }
        //     }
        //     return res;
        // }
    }


}
