using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode.BackTracking
{
    /*
    Find all possible combinations of k numbers that add up to a number n, given that only numbers from 1 to 9 can be used and each combination should be a unique set of numbers.

    Note:

    All numbers will be positive integers.
    The solution set must not contain duplicate combinations.
    Example 1:

    Input: k = 3, n = 7
    Output: [[1,2,4]]
    Example 2:

    Input: k = 3, n = 9
    Output: [[1,2,6], [1,3,5], [2,3,4]]
    */
    public class Q216_CombinationSum_III : IProblem
    {
        public IList<IList<int>> CombinationSum3(int k, int n)
        {
            var res = new List<IList<int>>();
            BackTrack(res, new List<int>(), k, n, 1);
            return res;
        }

        private void BackTrack(IList<IList<int>> res, IList<int> curList, int k, int remain, int pos)
        {
            if (remain < 0)
            {
                return;
            }

            if (curList.Count == k && remain == 0)
            {
                res.Add(new List<int>(curList));
                return;
            }
            for (var i = pos; i <= 9; i++)
            {
                curList.Add(i);
                BackTrack(res, curList, k, remain - i, i + 1);
                curList.RemoveAt(curList.Count - 1);
            }
        }

        public void Run()
        {
            int k = 3, n = 9;
            var res = CombinationSum3(k, n);
            Lib.Print(res);
        }
    }
}
