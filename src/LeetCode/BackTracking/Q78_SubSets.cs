using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

/*

Given a set of distinct integers, nums, return all possible subsets (the power set).

Note: The solution set must not contain duplicate subsets.

Example:

Input: nums = [1,2,3]
Output:
[
  [3],
  [1],
  [2],
  [1,2,3],
  [1,3],
  [2,3],
  [1,2],
  []
]
/*

    /*
    Time complexity: O(2^n)
    Space complexity: O(n)
    */
namespace LeetCode
{
    public class Q78_SubSets : IProblem
    {
        public IList<IList<int>> Subsets(int[] nums)
        {
            var res = new List<IList<int>>();
            if (nums == null)
            {
                return res;
            }

            Backtrack(res, nums, new List<int>(), 0);
            return res;
        }

        private void Backtrack(List<IList<int>> res, int[] nums, List<int> list, int start)
        {
            res.Add(new List<int>(list));
            for (var i = start; i < nums.Length; i++)
            {
                if (i > start && nums[i] == nums[i - 1])  // shouldn't matter in this question, as the numbers are distinct
                {
                    continue;
                }

                list.Add(nums[i]);
                Backtrack(res, nums, list, i + 1);
                list.RemoveAt(list.Count - 1);
            }
        }

        public void Run()
        {
            var input = new[] { 1, 2, 2 };
            var output = Subsets(input);
            Lib.Print(output);
        }
    }
}
