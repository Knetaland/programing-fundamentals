using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q17_LetterCombinationsOfAPhoneNumber : IProblem
    {
        public IList<string> LetterCombinations(string digits)
        {
            var map = new[] { "", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz" };
            var res = new List<string>();
            if (string.IsNullOrEmpty(digits))
            {
                return res;
            }

            res.Add("");
            var index = 0;
            for (var i = 0; i < digits.Length; i++)
            {
                var digit = digits[i] - '0';
                if (digit >= 0 || digit <= 9)
                {
                    var currentLevel = res.Where(s => s.Length == index).ToList();
                    foreach (var word in currentLevel)
                    {
                        foreach (var d in map[digit])
                        {
                            res.Add(word + d);
                        }
                    }
                    index++;
                }
            }
            res = res.Where(r => r.Length == index).ToList();

            return res;
        }

        public IList<string> LetterCombinations2(string digits)
        {
            var map = new[] { "", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz" };
            var res = new List<string>();
            if (string.IsNullOrEmpty(digits))
            {
                return res;
            }

            Backtrack(res, digits, map, new StringBuilder(), 0);

            return res;
        }

        private void Backtrack(List<string> res, string digits, string[] map, StringBuilder path, int pos)
        {
            if (path.Length == digits.Length)
            {
                res.Add(path.ToString());
                return;
            }
            var letters = map[digits[pos] - '0'];
            foreach (var l in letters)
            {
                path.Append(l);
                Backtrack(res, digits, map, path, pos + 1);
                path.Remove(path.Length - 1, 1);
            } 
        }

        public void Run()
        {
            var digits = "23";
            var list = LetterCombinations(digits);
            Console.WriteLine($"{digits} to letters {string.Join(",", list)}");
        }
    }
}
