using System;
using System.Collections.Generic;
using System.Text;
using Fundamental.Core.Interfaces;

namespace LeetCode.BackTracking
{
    /**
     * 
     * Given two integers n and k, return all possible combinations of k numbers out of 1 … n.

        Example:

        Input: n = 4, k = 2
        Output:
        [
          [2,4],
          [3,4],
          [2,3],
          [1,2],
          [1,3],
          [1,4],
        ]

        Solution: DFS
        Time complexity: O(C(n, k))
        Space complexity: O(k)
     */
    public class Q77_Combinations : IProblem
    {
        public IList<IList<int>> Combine(int n, int k)
        {
            var res = new List<IList<int>>();
            BackTrack(res, new List<int>(), n, k, 1);
            return res;
        }

        public IList<IList<char>> Combine(string n, int k)
        {
            var res = new List<IList<char>>();
            BackTrack2(res, new List<char>(), n, k, 0);
            return res;
        }

        private void BackTrack2(IList<IList<char>> res, IList<char> curList, string n, int k, int pos)
        {
            if (curList.Count == k)
            {
                res.Add(new List<char>(curList));
                return;
            }

            for (var i = pos; i < n.Length; i++)
            {
                curList.Add(n[i]);
                BackTrack2(res, curList, n, k, i + 1);
                curList.RemoveAt(curList.Count - 1);
            }
        }

        private void BackTrack(IList<IList<int>> res, IList<int> curList, int n, int k, int pos)
        {
            if (curList.Count == k)
            {
                res.Add(new List<int>(curList));
                return;
            }

            for (var i = pos; i <= n; i++)
            {
                curList.Add(i);
                BackTrack(res, curList, n, k, i + 1);
                curList.RemoveAt(curList.Count - 1);
            }
        }
        public void Run()
        {
            // int n = 4, k = 2;
            // var output = Combine(n, k);
            // foreach(var o in output)
            // {
            //     Console.WriteLine($"{string.Join(",", o)}");
            // }

            var teams = "ABCDEF";
            var size = 2;
            var matchs = Combine(teams, size);
            foreach (var o in matchs)
            {
                Console.WriteLine($"{string.Join(",", o)}");
            }
        }
    }
}
