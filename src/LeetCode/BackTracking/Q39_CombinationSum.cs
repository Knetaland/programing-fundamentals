using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    // https://leetcode.com/problems/combination-sum/discuss/16502/A-general-approach-to-backtracking-questions-in-Java-(Subsets-Permutations-Combination-Sum-Palindrome-Partitioning)
    /*
    Given a set of candidate numbers (candidates) (without duplicates) and a target number (target), find all unique combinations in candidates where the candidate numbers sums to target.

    The same repeated number may be chosen from candidates unlimited number of times.

    Note:

    All numbers (including target) will be positive integers.
    The solution set must not contain duplicate combinations.
    Example 1:

    Input: candidates = [2,3,6,7], target = 7,
    A solution set is:
    [
    [7],
    [2,2,3]
    ]
    Example 2:

    Input: candidates = [2,3,5], target = 8,
    A solution set is:
    [
    [2,2,2,2],
    [2,3,3],
    [3,5]
    ]
    

    Constraints:

    1 <= candidates.length <= 30
    1 <= candidates[i] <= 200
    Each element of candidate is unique.
    1 <= target <= 500
    */
    public class Q39_CombinationSum : IProblem
    {
        public IList<IList<int>> CombinationSum(int[] candidates, int target)
        {
            var res = new List<IList<int>>();
            Array.Sort(candidates);
            Backtrack(res, new List<int>(), candidates, target, 0);
            return res;
        }

        private void Backtrack(List<IList<int>> res, List<int> curList, int[] nums, int remain, int pos)
        {
            if (remain < 0)
            {
                return;
            }

            if (remain == 0)
            {
                res.Add(new List<int>(curList));
                return;
            }
            for (var i = pos; i < nums.Length; i++)
            {
                curList.Add(nums[i]);
                Backtrack(res, curList, nums, remain - nums[i], i);
                curList.RemoveAt(curList.Count - 1);
            }
        }

        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
