using System;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q46_Permutation : IProblem
    {
        /*
        Given a collection of distinct integers, return all possible permutations.

        Example:

        Input: [1,2,3]
        Output:
        [
        [1,2,3],
        [1,3,2],
        [2,1,3],
        [2,3,1],
        [3,1,2],
        [3,2,1]
        ]
        */

        public IList<IList<int>> Permute(int[] nums)
        {
            var res = new List<IList<int>>();
            if (nums == null)
            {
                return res;
            }

            BackTrack(res, nums, new List<int>(), 0);
            return res;
        }

        /*
        Time complexity: O(n!)
        Space complexity: O(n)
        
        */
        private void BackTrack(IList<IList<int>> res, int[] nums, IList<int> list, int level)
        {
            Console.WriteLine("LV " + level);
            if (list.Count == nums.Length)
            {
                res.Add(new List<int>(list));
                Lib.Print(res);
                return;
            }
            for (var i = 0; i < nums.Length; i++)
            {
                if (list.Contains(nums[i]))
                {
                    // Console.WriteLine("we at LV " + level + $"item at {i} already processed");
                    continue;
                }
                // Console.WriteLine("we at LV " + level + $"with i = {i} element: {nums[i]}");
                list.Add(nums[i]);
                // Console.WriteLine($"after adding element {nums[i]}, list is" + string.Join(",", list));
                BackTrack(res, nums, list, level + 1);
                list.RemoveAt(list.Count - 1);
                // Console.WriteLine($"after removing element {nums[i]}, list is" + string.Join(",", list));
            }
        }

        public void Run()
        {
            var input = new[] { 3, 2, 1 };
            var output = Permute(input);
            foreach (var o in output)
            {
                //Console.WriteLine(string.Join(",", o));
            }
        }
    }
}
