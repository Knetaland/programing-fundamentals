using System;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;


/*Given an array with n objects colored red, white or blue, sort them so that objects of the same color are adjacent, with the colors in the order red, white and blue.

Here, we will use the integers 0, 1, and 2 to represent the color red, white, and blue respectively. */
namespace LeetCode
{
    public class Q75_SortColors : IProblem
    {
        public void SortColors(int[] nums)
        {
            int redIndex = 0, blueIndex = nums.Length - 1, i = 0;
            while (i <= blueIndex)
            {
                if (nums[i] == 0)
                {
                    Lib.Swap(nums, i++, redIndex++);
                }
                else if (nums[i] == 2)
                {
                    Lib.Swap(nums, i, blueIndex--);
                }
                else
                {
                    i++;
                }
            }
        }
        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
