using System;
using System.Text;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q43_MultiplyStrings : IProblem
    {
        // https://discuss.leetcode.com/topic/30508/easiest-java-solution-with-graph-explanation
        // https://www.youtube.com/watch?v=Z_xGMYUSEJ8
        //`num1[i] * num2[j]` will be placed at indices `[i + j`, `i + j + 1]`
        public string Multiply(string num1, string num2)
        {
            int m = num1.Length, n = num2.Length;
            var pos = new int[m + n];
            for (var i = m - 1; i >= 0; i--)
            {
                for (var j = n - 1; j >= 0; j--)
                {
                    var mul = (num1[i] - '0') * (num2[j] - '0');
                    int posRight = i + j + 1, posLeft = posRight - 1;
                    mul += pos[posRight];
                    pos[posLeft] += mul / 10;
                    pos[posRight] = mul % 10;
                }
            }
            var sb = new StringBuilder();

            for (var i = 0; i < pos.Length; i++)
            {
                if (pos[i] == 0 && sb.Length == 0)
                {
                    continue;
                }

                sb.Append(pos[i]);
            }
            return sb.Length == 0 ? "0" : sb.ToString();
        }
        public void Run()
        {
            var x = 897978908;
            var y = 2321;
            Console.WriteLine($"result of {x} * {y}:");
            Console.WriteLine($"Expected: {x * y}");
            Console.WriteLine($"Actual: {Multiply(x.ToString(), y.ToString())}");
        }
    }
}
