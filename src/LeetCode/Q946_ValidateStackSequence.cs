using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q946_ValidateStackSequence : IProblem
    {
        public void Run() {
            var pushed = new [] {1,2,3,4,5};
            var popped = new [] {4,5,3,2,1};

            var res = ValidateStackSequences(pushed, popped);

            Console.WriteLine($"res is {res}");
        }

        public bool ValidateStackSequences(int[] pushed, int[] popped) {
            var stack = new Stack<int>();
            var pushedIndex = 0;
            for(var i = 0; i < popped.Length; i++) {
                if (stack.Count == 0 || stack.Peek() != popped[i]) {
                    while (pushedIndex < pushed.Length && pushed[pushedIndex]!= popped[i]) {
                        stack.Push(pushed[pushedIndex]);
                        pushedIndex++;
                    }
                    if (pushedIndex < pushed.Length) stack.Push(pushed[pushedIndex++]);
                }
            
                var top = stack.Pop();
                if (top != popped[i]) return false;
            }
            return true;
        }
    }
}