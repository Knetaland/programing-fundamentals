using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    There are two sorted arrays nums1 and nums2 of size m and n respectively.

    Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).

    You may assume nums1 and nums2 cannot be both empty.

    Example 1:

    nums1 = [1, 3]
    nums2 = [2]

    The median is 2.0
    Example 2:

    nums1 = [1, 2]
    nums2 = [3, 4]

    The median is (2 + 3)/2 = 2.5
    */
    public class Q4_Median : IProblem
    {
        // count as you do the merge
        public double Median(int[] nums1, int[] nums2)
        {
            int m = nums1.Length, n = nums2.Length;
            var mid = (m + n + 1) / 2;
            if ((m + n) % 2 == 0)
            {
                mid++;
            }

            var res = new int[mid];
            int i = 0, i1 = 0, i2 = 0;
            while (i < mid)
            {
                if (i1 < m && i2 < n)
                {
                    res[i++] = nums1[i1] < nums2[i2] ? nums1[i1++] : nums2[i2++];
                }
                else if (i1 < m)
                {
                    res[i++] = nums1[i1++];
                }
                else
                {
                    res[i++] = nums2[i2++];
                }
            }
            // if even length, get two and div
            if ((m + n) % 2 == 0)
            {
                return (res[mid - 1] + res[mid - 2]) / 2.0;
            }

            return res[mid - 1];
        }


        public void Run()
        {
            var nums1 = new[] { 1, 2 };
            var nums2 = new[] { 3, 4 };
            Console.WriteLine(Median(nums1, nums2));
        }
    }
}
