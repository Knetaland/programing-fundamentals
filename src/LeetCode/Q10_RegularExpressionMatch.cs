using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    // https://www.youtube.com/watch?v=l3hda49XcDE&t=368s

    /*
    Given an input string (s) and a pattern (p), implement regular expression matching with support for '.' and '*'.

    '.' Matches any single character.
    '*' Matches zero or more of the preceding element.
    The matching should cover the entire input string (not partial).

    Note:

    s could be empty and contains only lowercase letters a-z.
    p could be empty and contains only lowercase letters a-z, and characters like . or *.
    Example 1:

    Input:
    s = "aa"
    p = "a"
    Output: false
    Explanation: "a" does not match the entire string "aa".
    Example 2:

    Input:
    s = "aa"
    p = "a*"
    Output: true
    Explanation: '*' means zero or more of the preceding element, 'a'. Therefore, by repeating 'a' once, it becomes "aa".
    Example 3:

    Input:
    s = "ab"
    p = ".*"
    Output: true
    Explanation: ".*" means "zero or more (*) of any character (.)".
    Example 4:

    Input:
    s = "aab"
    p = "c*a*b"
    Output: true
    Explanation: c can be repeated 0 times, a can be repeated 1 time. Therefore, it matches "aab".
    Example 5:

    Input:
    s = "mississippi"
    p = "mis*is*p*."
    Output: false
    */
    public class Q10_RegularExpressionMatch : IProblem
    {
        // https://www.youtube.com/watch?v=EdWzV-9lQMw
        public bool IsMatch(string s, string p)
        {
            if (p.Length == 0)
            {
                return s.Length == 0;
            }

            var firstCharMatched = s.Length > 0 && (s[0] == p[0] || p[0] == '.');
            if (p.Length > 1 && p[1] == '*')
            {
                return IsMatch(s, p.Substring(2)) || (firstCharMatched && IsMatch(s.Substring(1), p));
            }
            else
            {
                return firstCharMatched && IsMatch(s.Substring(1), p.Substring(1));
            }
        }

        public bool IsMatchDp(string s, string p)
        {
            var map = new bool[s.Length + 1, p.Length + 1];
            map[0, 0] = true;
            for (var i = 1; i <= p.Length; i++)
            {
                if (i > 1 && p[i - 1] == '*')
                {
                    map[0, i] = map[0, i - 2];
                }
            }
            for (var i = 1; i <= s.Length; i++)
            {
                for (var j = 1; j <= p.Length; j++)
                {
                    if (s[i - 1] == p[j - 1] || p[j - 1] == '.')
                    {
                        map[i, j] = map[i - 1, j - 1];
                    }
                    else if (p[j - 1] == '*')
                    {
                        map[i, j] = map[i, j - 2];
                        if (!map[i, j])
                        {
                            if (p[j - 2] == '.' || s[i - 1] == p[j - 2])
                            {
                                map[i, j] = map[i - 1, j];
                            }
                        }
                    }
                    else
                    {
                        map[i, j] = false;
                    }
                }
            }
            return map[s.Length, p.Length];
        }
        public void Run()
        {

        }
    }
}
