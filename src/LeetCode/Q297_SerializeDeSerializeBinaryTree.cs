using System;
using System.Collections.Generic;
using System.Text;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    public class Q297_SerializeDeSerializeBinaryTree : IProblem
    {
        public string Serialize(TreeNode root)
        {
            var sb = new StringBuilder();
            Serialize(root, sb);
            return sb.ToString();
        }



        // preorder

        private void Serialize(TreeNode root, StringBuilder sb)
        {
            if (root == null)
            {
                sb.Append("#,");
                return;
            }
            sb.Append($"{root.val},");
            Serialize(root.left, sb);
            Serialize(root.right, sb);
        }

        private string SerializeOld(TreeNode root) {
            if (root == null) return "#";
            return root.val + ","+SerializeOld(root.left) + "," + SerializeOld(root.right);
        }

        public string Serialize2(TreeNode root) {
            // if (root == null) return "";
            var res = new List<string>();
            serialize(root);
            return string.Join(",", res);

            void serialize(TreeNode node) {
                if (node == null) return ;
                res.Add($"{node.val}");
                serialize(node.left);
                serialize(node.right);
            }

        }

        public TreeNode Deserialize(string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                return null;
            }

            var node = Deserialize(new Queue<string>(data.Split(',')));
            return node;
        }

        public TreeNode Deserialize(Queue<string> queue)
        {
            if (queue.Count == 0)
            {
                return null;
            }

            var n = queue.Dequeue();
            if (n == "#")
            {
                return null;
            }

            var node = new TreeNode(Convert.ToInt32(n))
            {
                left = Deserialize(queue),
                right = Deserialize(queue)
            };
            return node;
        }
        public void Run()
        {
            var input = new int?[] { 1, 2, 3, null, null, 4, 5 };
            var tree = Lib.ToBinaryTree(input, 0);
            var s = Serialize(tree);
            Console.WriteLine(s);
            var eah = Deserialize(s);
            var sol = new Q102_TreeLevelTraversal();
            var res = sol.LevelOrder(eah);
             var sb = new StringBuilder();
            for (var i = 0; i < res.Count; i++)
            {
                sb.AppendLine(string.Join(",", res[i]));
            }
            Console.WriteLine($"Recursive level order is \n {sb}");
        }
    }
}
