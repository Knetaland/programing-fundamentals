using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /** 
    * Follow up: find most frequent task schedule time; 
    * Find the task that appears for the most time 
    * Use a map to count the number of the times the task appears  then get the maximum count 
    * the result is decided by the maximum count and the number of tasks with maximum count 
    */
    public class Q1003_TaskScheduler_II : IProblem
    {
        public int Schedule(int[] tasks, int cd)
        {
            if (tasks == null || tasks.Length == 0)
            {
                return 0;
            }

            if (cd == 0)
            {
                return tasks.Length;
            }

            var maxFreq = 0;
            var maxCount = 0;
            // store the frequency
            var map = new Dictionary<int, int>();
            // get the most frequent task, doing other task while cd 
            foreach (var task in tasks)
            {
                if (!map.ContainsKey(task))
                {
                    map.Add(task, 0);
                }
                map[task]++;
                if (maxFreq < map[task])
                {
                    maxFreq++;
                    maxCount = 1; // need one additional round for this job
                }
                else if (maxFreq == map[task])
                {
                    Console.WriteLine("max count is " + maxCount);
                    maxCount++;
                }
            }
            Console.WriteLine("finanl max count is " + maxCount);
            return (cd + 1) * (maxFreq - 1) + maxCount;
        }
        public void Run()
        {
            var tasks = new[] { 1, 1, 2, 2, 3, 3 };
            Console.WriteLine("time to run: " + Schedule(tasks, 2));
        }
    }
}
