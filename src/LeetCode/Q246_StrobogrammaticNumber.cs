using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q246_StrobogrammaticNumber : IProblem
    {
        /*
        A strobogrammatic number is a number that looks the same when rotated 180 degrees (looked at upside down).

        Write a function to determine if a number is strobogrammatic. The number is represented as a string.

        Example 1:

        Input:  "69"
        Output: true
        Example 2:

        Input:  "88"
        Output: true
        Example 3:

        Input:  "962"
        Output: false
        */

        public bool IsStrobogrammatic(string num)
        {
            if (string.IsNullOrEmpty(num))
            {
                return true;
            }

            var map = new Dictionary<char, char>()
            {
                ['0'] = '0',
                ['1'] = '1',
                ['6'] = '9',
                ['8'] = '8',
                ['9'] = '6',
            };
            int l = 0, r = num.Length - 1;
            while (l <= r)
            {  // in the case of something like "2"
                // 0 1 8, 6 9
                if (!map.ContainsKey(num[l]) || map[num[l]] != num[r])
                {
                    return false;
                }

                // if (num[l] == num[r]) {
                //     if (num[l] != '1' && num[l]!='0' && num[l] != '8') {
                //         return false;
                //     }
                // }
                // else {
                //     if (!(num[l] == '6' && num[r] == '9') ||  (num[l] == '9' || num[r] == '6')){
                //         return false;
                //     }
                // }
                l++;
                r--;
            }
            return true;
        }

        public void Run()
        {
            var num = "61019";
            var res = IsStrobogrammatic(num);
            Console.WriteLine("res is " + res);
        }
    }
}
