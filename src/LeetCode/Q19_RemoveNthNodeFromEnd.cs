using System;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    public class Q19_RemoveNthNodeFromEnd : IProblem
    {
        public ListNode RemoveNthFromEnd(ListNode head, int n)
        {
            if (head == null)
            {
                return null;
            }

            ListNode fast = head, slow = head, prev = head;
            while (n > 0 && fast != null)
            {
                fast = fast.next;
                n--;
            }
            if (fast == null)
            {
                return head.next;
            }

            while (fast != null)
            {
                fast = fast.next;
                prev = slow;
                slow = slow.next;
            }
            prev.next = slow.next;
            return head;
        }

        public void Run()
        {
            var head = Lib.ToListNode(new[] { 1, 2, 4, 5 });
            var pos = 2;
            Console.Write("Input: ");
            Lib.PrintList(head);
            var after = RemoveNthFromEnd(head, pos);
            Console.WriteLine($" After removing {pos}th position from the end: ");
            Lib.PrintList(after);
        }
    }
}
