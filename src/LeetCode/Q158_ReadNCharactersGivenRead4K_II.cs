using System;

namespace LeetCode
{
    /*
    The API: int read4(char *buf) reads 4 characters at a time from a file.

    The return value is the actual number of characters read. For example, it returns 3 if there is only 3 characters left in the file.

    By using the read4 API, implement the function int read(char *buf, int n) that reads n characters from the file.

    Note:
    The read function may be called multiple times.
    */
    public class Q158_ReadNCharactersGivenRead4_II
    {
        int pointer = 0; // index of temp buffer
        int len = 0;  // read4(temp)
        char[] temp = new char[4];
        public int Read(char[] buff, int n)
        {
            var total = 0;
            while (total < n)
            {
                if (pointer == 0)  // previous remainder is done, go ahead and fetch more data
                {
                    len = Read4(temp);
                }
                if (len == 0)
                {
                    break;
                }

                while (total < n && pointer < len)
                {
                    buff[total++] = temp[pointer++];
                }
                if (pointer == len)
                {
                    pointer = 0; // means all 4 char are processed, reset pointer so it will fetch 
                }
            }
            return total;
        }

        int Read4(char[] buff)
        {
            return new Random().Next(4);
        }
    }


    public class Q157_ReadNCharactersGivenRead4
    {
        public int Read(char[] buff, int n)
        {
            var temp = new char[4];
            var total = 0;
            while (total < n)
            {
                var count = Read4(temp);
                count = Math.Min(count, n - total);
                for (var i = 0; i < count; i++)
                {
                    buff[total++] = temp[i];
                }
                if (count < 4)
                {
                    break;
                }
            }
            return total;
        }
        int Read4(char[] buff)
        {
            return new Random().Next(4);
        }
    }
}
