using System;
using Fundamental.Core.DataStructure;
using Fundamental.Core.Helpers;

namespace LeetCode
{
    public class Q973_KClosestPiontsToOrigin
    {
        // O(N), space: O(1)
        public int[][] KClosest(int[][] points, int K)
        {
            int left = 0, right = points.Length - 1;
            while (left <= right)
            {
                var pivot = Partition(points, left, right);
                if (pivot == K - 1)
                {
                    break;
                }
                if (pivot < K)
                {
                    left = pivot + 1;
                }
                else
                {
                    right = pivot - 1;
                }
            }
            var res = new int[K][];
            Array.Copy(points, res, K);
            return res;
        }

        // O(nlog(K)), Space(O(K))
        public int[][] KClosestWithMaxHeap(int[][] points, int K)
        {
            var n = points.Length;
            // max heap, remove max when reaches capacity. the remaining will be smallest K items
            var heap = new Heap<int[]>((a, b) => (b[0] * b[0] + b[1] * b[1]) - (a[0] * a[0] + a[1] * a[1]));
            foreach (var point in points)
            {
                heap.Add(point);
                if (heap.Size > K)
                {
                    heap.Poll();
                }
            }
            var res = new int[K][];
            var index = 0;
            while (heap.Size > 0)
            {
                res[index++] = heap.Poll();
            }
            return res;
        }

        public int Partition(int[][] points, int left, int right)
        {
            var pivot = points[left];
            int l = left + 1, r = right;
            while (l <= r)
            {
                while (l <= r && Compare(points[l], pivot) <= 0)
                {
                    l++;
                }

                while (l <= r && Compare(points[r], pivot) >= 0)
                {
                    r--;
                }

                if (l < r)
                {
                    Lib.Swap(points, l, r);
                }
            }
            Lib.Swap(points, left, r);
            return r;
        }

        int Compare(int[] a, int[] b) => (a[0] * a[0] + a[1] * a[1]) - (b[0] * b[0] + b[1] * b[1]);
    }
}
