using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class ThreeSumClosest : IProblem
    {
        public int Closest(int[] nums, int target)
        {
            if (nums == null || nums.Length < 3)
            {
                return int.MaxValue;
            }

            Array.Sort(nums);
            var res = nums[0] + nums[1] + nums[2];
            for (var i = 0; i < nums.Length - 1; i++)
            {
                int left = i + 1, right = nums.Length - 1;
                while (left < right)
                {
                    var sum = nums[i] + nums[left] + nums[right];
                    if (sum > target)
                    {
                        right--;
                    }
                    else
                    {
                        left++;
                    }

                    if (Math.Abs(sum - target) < Math.Abs(res - target))
                    {
                        res = sum;
                    }
                }
            }
            return res;
        }
        public void Run()
        {
        }
    }
}
