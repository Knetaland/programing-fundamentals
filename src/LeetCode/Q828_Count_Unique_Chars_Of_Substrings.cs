using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Interfaces;

namespace LeetCode;

public class Q828_Count_Unique_Chars_Of_Substrings : IProblem
{
    public void Run()  {
        var res = UniqueLetterString("ABA");
        Console.WriteLine(res);
    }

    public int UniqueLetterString(string s) {
        var res = new HashSet<string>();
        for(var i = 0; i < s.Length; i++) {
            for(var j = i+1; j <= s.Length; j++) {
                res.Add(s.Substring(i, j - i));
            }
        }

        var total =res.Select(r => Count(r)).Sum();

        foreach(var r in res) {
            var count = Count(r);
            Console.WriteLine($"string {r} has count of {count}");

        }
        return total;
    }

    public int Count(string s) {
        var count = s.Distinct().Count();
        return count;
    }
}
