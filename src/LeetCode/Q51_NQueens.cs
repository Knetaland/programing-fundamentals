using System;
using System.Collections.Generic;
using System.Text;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    The n-queens puzzle is the problem of placing n queens on an n×n chessboard
    such that no two queens attack each other.

    Given an integer n, return all distinct solutions to the n-queens puzzle.

    Each solution contains a distinct board configuration of the n-queens' placement, where 'Q' and '.' both indicate a queen and an empty space respectively.

    There exist two distinct solutions to the 4-queens puzzle:

    [
     [".Q..",  // Solution 1
      "...Q",
      "Q...",
      "..Q."],

     ["..Q.",  // Solution 2
      "Q...",
      "...Q",
      ".Q.."]
    ]
    */
    public class Q51_NQueens : IProblem
    {
        /// <summary>
        /// 经典解法为回溯递归，一层一层的向下扫描，需要用到一个pos数组，其中pos[i]表示第i行皇后的位置，初始化为-1，
        /// 然后从第0开始递归，每一行都一次遍历各列，判断如果在该位置放置皇后会不会有冲突，
        /// 以此类推，当到最后一行的皇后放好后，一种解法就生成了，将其存入结果res中，然后再还会继续完成搜索所有的情况
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public IList<IList<string>> SolveNQueens(int n)
        {
            var res = new List<IList<string>>();
            // used to store position of queen on each row
            var pos = new int[n];
            // init position for each row to be -1
            for (var i = 0; i < n; i++)
            {
                pos[i] = -1;
            }
            BackTrack(res, 0, pos);
            return res;
        }

        private void BackTrack(List<IList<string>> res, int row, int[] pos)
        {
            var n = pos.Length;
            // means row already pass max row
            if (row == n)
            {
                var board = InitBoard(n);
                var placement = new List<string>();
                for (var i = 0; i < n; i++)
                {
                    board[i][pos[i]] = 'Q';
                    placement.Add(board[i].ToString());
                }
                res.Add(placement);
                return;
            }

            for (var col = 0; col < n; col++)
            {
                if (IsValid(pos, row, col))
                {
                    pos[row] = col;
                    BackTrack(res, row + 1, pos);
                    pos[row] = -1;
                }
            }
        }

        private bool IsValid(int[] pos, int row, int col)
        {
            for (var i = 0; i < row; i++)
            {
                // at the same col or  on the same diagnal  dif(row) == dif(col)
                //  00, 11, [01,10], [01,23]
                if (col == pos[i] || Math.Abs(row - i) == Math.Abs(col - pos[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public List<StringBuilder> InitBoard(int n)
        {
            var res = new List<StringBuilder>();
            for (var i = 0; i < n; i++)
            {
                var row = new StringBuilder(new string('.', n));
                res.Add(row);
            }
            return res;
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
