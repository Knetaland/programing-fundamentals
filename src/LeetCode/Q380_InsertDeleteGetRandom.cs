using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    380. Insert Delete GetRandom O(1)
Medium

Design a data structure that supports all following operations in average O(1) time.

insert(val): Inserts an item val to the set if not already present.
remove(val): Removes an item val from the set if present.
getRandom: Returns a random element from current set of elements. Each element must have the same probability of being returned.
Example:

// Init an empty set.
RandomizedSet randomSet = new RandomizedSet();

// Inserts 1 to the set. Returns true as 1 was inserted successfully.
randomSet.insert(1);

// Returns false as 2 does not exist in the set.
randomSet.remove(2);

// Inserts 2 to the set, returns true. Set now contains [1,2].
randomSet.insert(2);

// getRandom should return either 1 or 2 randomly.
randomSet.getRandom();

// Removes 1 from the set, returns true. Set now contains [2].
randomSet.remove(1);

// 2 was already in the set, so return false.
randomSet.insert(2);

// Since 2 is the only number in the set, getRandom always return 2.
randomSet.getRandom();
    */
    public class Q380_InsertDeleteGetRandom : IProblem
    {
        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }

    public class RandomizedSet
    {
        private readonly Dictionary<int, int> _map;
        private readonly List<int> _list;
        private readonly Random _r;

        /** Initialize your data structure here. */
        public RandomizedSet()
        {
            _map = new Dictionary<int, int>();
            _list = new List<int>();
            _r = new Random();
        }

        /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
        public bool Insert(int val)
        {
            if (_map.ContainsKey(val))
            {
                return false;
            }
            _map.Add(val, _list.Count);
            _list.Add(val);
            return true;
        }

        /** Removes a value from the set. Returns true if the set contained the specified element. */
        public bool Remove(int val)
        {
            if (!_map.ContainsKey(val))
            {
                return false;
            }
            var pos = _map[val];
            _map.Remove(val);
            var lastPos = _list.Count - 1; // swap position with last element if item to be remove is not the last one
            if (pos != lastPos)
            {
                _list[pos] = _list[lastPos];
                _map[_list[lastPos]] = pos;
            }
            _list.RemoveAt(lastPos);
            return true;
        }


        /** Get a random element from the set. */
        public int GetRandom()
        {
            return _list[_r.Next(_list.Count)];
        }
    }
}
