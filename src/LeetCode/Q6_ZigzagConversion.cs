using System;
using System.Text;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q6_ZigzagConversion : IProblem
    {
        public string Convert(string s, int numRows)
        {
            if (numRows <= 1)
            {
                return s;
            }

            var lists = new StringBuilder[numRows];
            for (var i = 0; i < numRows; i++)
            {
                lists[i] = new StringBuilder();
            }

            int direction = 1, row = 0;
            for (var i = 0; i < s.Length; i++)
            {
                if (row == 0)
                {
                    direction = 1;
                }

                if (row == numRows - 1)
                {
                    direction = -1;
                }

                lists[row].Append(s[i]);
                row += direction;
            }
            var sb = new StringBuilder();
            foreach (var l in lists)
            {
                sb.Append(l);
            }
            return sb.ToString();
        }

        public void Run()
        {
            var s = "PAYPALISHIRING";
            var numRows = 3;
            Console.WriteLine($"{s} becomes {Convert(s, numRows)} with {numRows} rows.");
        }
    }
}
