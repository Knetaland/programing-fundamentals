﻿using System;
using Fundamental.Core;
using Fundamental.Core.Interfaces;

namespace LeetCode;

/*
https://leetcode.com/problems/number-of-provinces/description/

There are n cities. Some of them are connected, while some are not. If city a is connected directly with city b, and city b is connected directly with city c, then city a is connected indirectly with city c.

A province is a group of directly or indirectly connected cities and no other cities outside of the group.

You are given an n x n matrix isConnected where isConnected[i][j] = 1 if the ith city and the jth city are directly connected, and isConnected[i][j] = 0 otherwise.

Return the total number of provinces.

Input: isConnected = [[1,1,0],[1,1,0],[0,0,1]]
Output: 2

Input: isConnected = [[1,0,0],[0,1,0],[0,0,1]]
Output: 3
*/

public class Q547_Num_Of_Provinces : IProblem
{
    public int FindCircleNum(int[][] isConnected) {
        var m = isConnected[0].Length;
        var dsu = new DSU(m);
        for(var i = 0; i < m; i++) {
            for(var j = 0; j < m; j++) {
                if (isConnected[i][j] == 1) {
                    dsu.union(i, j);
                }
            }
        }
        var res = 0;
        for(var i = 0; i < m; i++) {
            if (dsu.find(i) == i) res++;
        }
        return res;
    }

    public int FindCircleNumDfs(int[][] isConnected) {
        var visited = new bool[isConnected.Length];
        var count = 0;
        for(var person = 0; person < isConnected.Length; person++) {
            if (!visited[person]) {
                count++; // one cycle, even if only by self
                dfs(isConnected, visited, person); // check cur person's connection
            }
        }
        return count;
    }

    public void dfs(int[][] isConnected, bool[] visited, int person) {
        for(var other = 0; other < isConnected.Length; other++) { // check if cur person is connected with others
            if (isConnected[person][other] == 1 && !visited[other]) {
                visited[other] = true;
                dfs(isConnected, visited, other); // dfs on other's connection  
            }
        }
    }

    public void Run() {
        int[][] isConnected = [[1,0,0],[0,1,0],[0,0,1]];
        var res = FindCircleNum(isConnected);

        Console.WriteLine("res is " + res);
    }
}
