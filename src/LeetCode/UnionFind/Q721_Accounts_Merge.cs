using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Fundamental.Core.Interfaces;

namespace LeetCode.UnionFind;

/*
    https://leetcode.com/problems/accounts-merge/
    Given a list of accounts where each element accounts[i] is a list of strings, where the first element accounts[i][0] is a name, and the rest of the elements are emails representing emails of the account.

    Now, we would like to merge these accounts. Two accounts definitely belong to the same person if there is some common email to both accounts. Note that even if two accounts have the same name, they may belong to different people as people could have the same name. A person can have any number of accounts initially, but all of their accounts definitely have the same name.

    After merging the accounts, return the accounts in the following format: the first element of each account is the name, and the rest of the elements are emails in sorted order. The accounts themselves can be returned in any order.

    

    Example 1:

    Input: accounts = [["John","johnsmith@mail.com","john_newyork@mail.com"],["John","johnsmith@mail.com","john00@mail.com"],["Mary","mary@mail.com"],["John","johnnybravo@mail.com"]]
    Output: [["John","john00@mail.com","john_newyork@mail.com","johnsmith@mail.com"],["Mary","mary@mail.com"],["John","johnnybravo@mail.com"]]
    Explanation:
    The first and second John's are the same person as they have the common email "johnsmith@mail.com".
    The third John and Mary are different people as none of their email addresses are used by other accounts.
    We could return these lists in any order, for example the answer [['Mary', 'mary@mail.com'], ['John', 'johnnybravo@mail.com'], 
    ['John', 'john00@mail.com', 'john_newyork@mail.com', 'johnsmith@mail.com']] would still be accepted.
    Example 2:

    Input: accounts = [["Gabe","Gabe0@m.co","Gabe3@m.co","Gabe1@m.co"],["Kevin","Kevin3@m.co","Kevin5@m.co","Kevin0@m.co"],["Ethan","Ethan5@m.co","Ethan4@m.co","Ethan0@m.co"],["Hanzo","Hanzo3@m.co","Hanzo1@m.co","Hanzo0@m.co"],["Fern","Fern5@m.co","Fern1@m.co","Fern0@m.co"]]
    Output: [["Ethan","Ethan0@m.co","Ethan4@m.co","Ethan5@m.co"],["Gabe","Gabe0@m.co","Gabe1@m.co","Gabe3@m.co"],["Hanzo","Hanzo0@m.co","Hanzo1@m.co","Hanzo3@m.co"],["Kevin","Kevin0@m.co","Kevin3@m.co","Kevin5@m.co"],["Fern","Fern0@m.co","Fern1@m.co","Fern5@m.co"]]
    

    Constraints:

    1 <= accounts.length <= 1000
    2 <= accounts[i].length <= 10
    1 <= accounts[i][j].length <= 30
    accounts[i][0] consists of English letters.
    accounts[i][j] (for j > 0) is a valid email.
*/
public class Q721_Accounts_Merge : IProblem
{
    public IList<IList<string>> AccountsMerge(IList<IList<string>> accounts) {

        /*
        1. build email -> id[] map: 
        2. loop through each account, in each email in the account, dfs on the mapped ids, maintain a email list along the way.
          - skip id that has been processed
        */
        var emailToIdMap = new Dictionary<string, List<int>>(); // list index is the ID
        var res = new List<IList<string>>();
        // construct email -> id[] map
        // O(N) N is the num of emails, space O(N)
        for(var i = 0; i < accounts.Count; i++) {
             for(var k = 1; k < accounts[i].Count; k++) { // emails starts from index 1
                var email = accounts[i][k];
                if (!emailToIdMap.ContainsKey(email)) {
                    emailToIdMap.Add(email, new List<int>());
                }
                emailToIdMap[email].Add(i);
             }
        }

        // space O(M) M is the num of accounts
        var processed = new bool[accounts.Count];

        for(var i = 0; i < accounts.Count; i++) {
            if (processed[i]) continue;
            var emailSet = new HashSet<string>();
            merge(i, emailSet);
            var list = new List<string>
            (
                // accounts[i][0] // add name
                emailSet
            );
            list.Sort();
            list.Insert(0, accounts[i][0]);
            res.Add(list);
        }
        return res;

        //DFS
        void merge(int emailId, HashSet<string> emailSet) {
            if (processed[emailId]) return ;
            processed[emailId] = true;
            for(var i = 1; i < accounts[emailId].Count; i++) {
                var email = accounts[emailId][i];
                if (emailSet.Contains(email)) continue;
                emailSet.Add(accounts[emailId][i]);
                // find the ids associated with the email
                foreach(var id in emailToIdMap[email]) {
                    if (processed[id]) continue; 
                    merge(id, emailSet); // 
                }
            }
        }
    }
    
    public void Run() => throw new NotImplementedException();
}
