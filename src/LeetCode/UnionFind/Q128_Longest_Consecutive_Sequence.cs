﻿using System;
using System.Collections.Generic;
using Fundamental.Core;
using Fundamental.Core.Interfaces;

namespace LeetCode;

/*
https://leetcode.com/problems/longest-consecutive-sequence/description/

Given an unsorted array of integers nums, return the length of the longest consecutive elements sequence.

You must write an algorithm that runs in O(n) time.

 

Example 1:

Input: nums = [100,4,200,1,3,2]
Output: 4
Explanation: The longest consecutive elements sequence is [1, 2, 3, 4]. Therefore its length is 4.
Example 2:

Input: nums = [0,3,7,2,5,8,4,6,0,1]
Output: 9

*/
public class Q128_Longest_Consecutive_Sequence : IProblem 
{
     public int LongestConsecutive(int[] nums) {
        if (nums == null || nums.Length == 0) return 0;
        var set = new HashSet<int>(nums);
        var max = 1;
        foreach(var num in nums) {
            var count = 1;
            if (set.Contains(num)) {
                var smaller = num - 1;
                while(set.Contains(smaller)) {
                    count++;
                    set.Remove(smaller);
                    smaller--;
                }
                var larger = num + 1;
                while(set.Contains(larger)) {
                    count++;
                    set.Remove(larger);
                    larger++;
                }
            }
            max = Math.Max(max, count);
        }
        return max;
    }

    public int LongestConsecutiveUnionFind(int[] nums) {
        var dsu = new DsuWithSize(nums.Length);
        var map = new Dictionary<int, int>();
        for(var i = 0; i < nums.Length; i++) {
            if (map.ContainsKey(nums[i])) continue;
            map[nums[i]] = i;
            if (map.ContainsKey(nums[i] + 1)) dsu.union(map[nums[i]+1], i);
            if (map.ContainsKey(nums[i] - 1)) dsu.union(map[nums[i]-1], i);
        }
        return dsu.max();
    }

    public void Run() {
        int[]  nums = [0,3,7,2,5,8,4,6,0,1];
        var res = LongestConsecutiveUnionFind(nums);

        Console.WriteLine("res is " + res);
    }
}
