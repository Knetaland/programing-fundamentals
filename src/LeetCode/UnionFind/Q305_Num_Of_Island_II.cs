﻿using System;
using System.Collections.Generic;
using Fundamental.Core;
using Fundamental.Core.Interfaces;

namespace LeetCode;

/*
https://leetcode.ca/all/305.html

A 2d grid map of m rows and n columns is initially filled with water. We may perform an addLand operation which turns the water at position (row, col) into a land. Given a list of positions to operate, count the number of islands after each addLand operation. An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically. You may assume all four edges of the grid are all surrounded by water.

Example:

Input: m = 3, n = 3, positions = [[0,0], [0,1], [1,2], [2,1]]
Output: [1,1,2,3]
Explanation:

Initially, the 2d grid grid is filled with water. (Assume 0 represents water and 1 represents land).

0 0 0
0 0 0
0 0 0
Operation #1: addLand(0, 0) turns the water at grid[0][0] into a land.

1 0 0
0 0 0   Number of islands = 1
0 0 0
Operation #2: addLand(0, 1) turns the water at grid[0][1] into a land.

1 1 0
0 0 0   Number of islands = 1
0 0 0
Operation #3: addLand(1, 2) turns the water at grid[1][2] into a land.

1 1 0
0 0 1   Number of islands = 2
0 0 0
Operation #4: addLand(2, 1) turns the water at grid[2][1] into a land.

1 1 0
0 0 1   Number of islands = 3
0 1 0
*/
public class Q305_Num_Of_Island_II : IProblem
{
    int[][] dirs = [
        [0,1], [0,-1], [1,0], [-1,0]
    ];
    public IList<int> numOfIslands(int m, int n, int[][] positions) {
        var res = new List<int>();
        var count = 0;
        var island = new bool[m,n];
        var dsu = new DSU(m * n);
        foreach(var pos in positions) {
            if (island[pos[0], pos[1]]) { // the land has been filled
                res.Add(count);
                continue;
            }
            
            island[pos[0],pos[1]] = true;
            count++; // fill the land and increment island count

            // do union find around the land
            foreach(var dir in dirs) {
                var x = pos[0] + dir[0];
                var y = pos[1] + dir[1];
                if (x <0 || x >= m || y <0 || y >= n || !island[x,y]) {
                    continue;
                }
                var posRoot = dsu.find(pos[0]* n + pos[1]); // use x * col (# of items in row)
                var curRoot = dsu.find(x * n + y);
                if (posRoot != curRoot) {
                    dsu.union(posRoot, curRoot);
                    count--; // island connected, decrement count
                }
            }
            res.Add(count);
        }

        return res;
    }

    public void Run() {
        int m = 3, n = 3;
        int[][] positions =  [[0,0], [0,1], [1,2], [2,1]];
        var res = numOfIslands(m, n, positions);
        Console.WriteLine(string.Join(",", res));
    }
}
