using System;
using System.Collections;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q200_NumberOfIslands : IProblem
    {
         int[][] DIRS = [
                [0, 1],
                [0, -1],
                [1, 0],
                [-1, 0]
            ];
        public int NumIslands(char[,] grid)
        {
            if (grid == null)
            {
                return 0;
            }

            var count = 0;
            for (var i = 0; i < grid.GetLength(0); i++)
            {
                for (var j = 0; j < grid.GetLength(1); j++)
                {
                    if (grid[i, j] == '1')
                    {
                        Sink(grid, i, j);
                        count++;
                    }
                }
            }
            return count;
        }

        public int NumIslandsWithVisitedMap(char[,] grid)
        {
            if (grid == null)
            {
                return 0;
            }

            var count = 0;
            var visited = new bool[grid.GetLength(0), grid.GetLength(1)];
            for (var i = 0; i < grid.GetLength(0); i++)
            {
                for (var j = 0; j < grid.GetLength(1); j++)
                {
                    if (!visited[i, j] && grid[i, j] == '1')
                    {
                        Explore(grid, visited, i, j);
                        count++;
                    }
                }
            }
            return count;
        }

        private void Explore(char[,] grid, bool[,] visited, int i, int j)
        {
            if (i < 0 || j < 0 || i >= grid.GetLength(0) || j >= grid.GetLength(1) || visited[i, j] || grid[i, j] != '1')
            {
                return;
            }

            visited[i, j] = true;
            Explore(grid, visited, i - 1, j);
            Explore(grid, visited, i + 1, j);
            Explore(grid, visited, i, j - 1);
            Explore(grid, visited, i, j + 1);
        }

        private void Sink(char[,] grid, int i, int j)
        {
            if (i < 0 || i >= grid.GetLength(0) || j < 0 || j >= grid.GetLength(1) || grid[i, j] != '1')
            {
                return;
            }

            grid[i, j] = '0'; // sink it
            Sink(grid, i - 1, j);
            Sink(grid, i + 1, j);
            Sink(grid, i, j - 1);
            Sink(grid, i, j + 1);
        }


        
        public int NumIslandsBfs(char[,] grid) {
            var count = 0;
           
            for(var i = 0; i < grid.GetLength(0); i++) {
                for(var j = 0; j < grid.GetLength(1); j++) {
                    if (grid[i,j] == '1') {
                        count++;
                        bfs(grid, i, j);

                    }
                }
            }
            return count;

            void bfs(char[,] g, int row, int col) {
                var queue = new Queue<int[]>();
                queue.Enqueue([row, col]);
                while(queue.Count > 0) {
                    var point  = queue.Dequeue();
                    var x = point[0];
                    var y = point[1];
                    if (x <0 || x >= g.GetLength(0) || y <0 || y >= g.GetLength(1) || g[x,y] == '0') continue;
                    g[x,y] = '0'; // sink
                    foreach(var dir in DIRS) {
                        queue.Enqueue([x + dir[0], y + dir[1]]);
                    }
                }
            }
        }

        public int NumIslandsDfsWithStack(char[,] grid) {
            var count = 0;
           
            for(var i = 0; i < grid.GetLength(0); i++) {
                for(var j = 0; j < grid.GetLength(1); j++) {
                    if (grid[i,j] == '1') {
                        count++;
                        dfs(grid, i, j);

                    }
                }
            }
            return count;

            void dfs(char[,] g, int row, int col) {
                var stack = new Stack<int[]>();
                stack.Push([row, col]);
                while(stack.Count > 0) {
                    var point  = stack.Pop();
                    var x = point[0];
                    var y = point[1];
                    if (x <0 || x >= g.GetLength(0) || y <0 || y >= g.GetLength(1) || g[x,y] == '0') continue;
                    g[x,y] = '0'; // sink
                    foreach(var dir in DIRS) {
                        stack.Push([x + dir[0], y + dir[1]]);
                    }
                }
            }
        }

        public void Run()
        {
            var input = new char[,] {
                {'1','1','1','1','0'},
                {'1','1','0','1','0'},
                {'1','1','0','0','0'},
                {'0','0','0','1','0'},
            };
            Console.WriteLine("num of island using visited map: " + NumIslandsWithVisitedMap(input));
            Console.WriteLine("num of island dfs: " + NumIslands((char[,])input.Clone()));
            Console.WriteLine("num of island bfs: " + NumIslandsBfs((char[,])input.Clone()));
            Console.WriteLine("num of island dfs stack: " + NumIslandsDfsWithStack((char[,])input.Clone()));
        }
    }
}
