using System;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    public class Q21_MergeTwoSortedList : IProblem
    {
        public ListNode MergeTwoLists(ListNode l1, ListNode l2)
        {
            var dummy = new ListNode(0);
            var pointer = dummy;
            while (l1 != null && l2 != null)
            {
                if (l1.val < l2.val)
                {
                    pointer.next = l1;
                    l1 = l1.next;
                }
                else
                {
                    pointer.next = l2;
                    l2 = l2.next;
                }
                pointer = pointer.next;
            }
            pointer.next = l1 ?? l2;
            return dummy.next;
        }
        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
