﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Interfaces;

namespace LeetCode;

/*
https://leetcode.com/problems/number-of-black-blocks/description/ 
You are given two integers m and n representing the dimensions of a 0-indexed m x n grid.

You are also given a 0-indexed 2D integer matrix coordinates, where coordinates[i] = [x, y] indicates that the cell with coordinates [x, y] is colored black. All cells in the grid that do not appear in coordinates are white.

A block is defined as a 2 x 2 submatrix of the grid. More formally, a block with cell [x, y] as its top-left corner where 0 <= x < m - 1 and 0 <= y < n - 1 contains the coordinates [x, y], [x + 1, y], [x, y + 1], and [x + 1, y + 1].

Return a 0-indexed integer array arr of size 5 such that arr[i] is the number of blocks that contains exactly i black cells.
*/
public class Q2768_NumOfBlocks : IProblem
{
    public long [] findNumOfBlocks(int m, int n, int[][] coordinates) {
        var dirs = new int[][] {
            new int [] { -1, -1}, // top left
            new int [] { -1, 0}, // top
            new int [] { 0, -1}, // left
            new int [] { 0, 0}, // current
        };
        //  go through each black cell
        var map = new Dictionary<(int,int), int>();
        var blockMap = new Dictionary<(int, int), List<(int,int)>>();
        foreach(var cor in coordinates) {
            var blocks = new List<(int, int)>();
            //  to form a square, the current cell will be in one of the 4 squares that top left start at { topleft, top, left, cur }.
            foreach(var dir in dirs) {
                var x = cor[0] + dir [0];
                var y = cor[1] + dir [1];
                // find the blocks that contains the current black cell
                if (x >=0 && x < m-1 && y >=0 && y < n-1) {
                    Console.WriteLine($"Found black cell at [{x}, {y}]");
                    var key =  (x,y);
                    // increment the count of black cells in current block
                    map[key] = map.GetValueOrDefault(key, 0) + 1;
                    blocks.Add((x,y));
                }
            }
            blockMap.Add((cor[0], cor[1]), blocks);
        }

        /**
        {
            (0,0): 1
            (0,1): 1
        }
        **/
        // group by count, find all the squares 
        var blocksByCount = map.GroupBy(m => m.Value).ToDictionary(g => g.Key, g => g.Count());

        var res = new long[ 5];
        for(var i =1; i < res.Length; i++) {
            if (blocksByCount.ContainsKey(i)) {
                res[i] = blocksByCount[i];
            }
        }
        var squaresFound = blocksByCount.Values.Sum();
        var blocksFound = blockMap.Values.Count();
        Console.WriteLine($"squaresFound {squaresFound} and blocksFound {blocksFound}is the same: {squaresFound == blocksFound}");
        var maxPossibleSquareCount = (long)(m-1) * (n-1);
        res [0] = maxPossibleSquareCount - squaresFound; // res.Sum();
        return res;
    }
    
    public void Run() {
        int m = 3, n =3;
        var cors = new int [][] {
            new int[] {0,0},
            new int[] {1,1},
            new int[] {0,2}
        };
        var res = findNumOfBlocks(m, n, cors) ;

        Console.WriteLine($"Answer is {String.Join(",", res)}" );
    }
}
