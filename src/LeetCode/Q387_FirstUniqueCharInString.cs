using System;
using System.Collections.Generic;
using System.Text;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
     * 
     * Given a string, find the first non-repeating character in it and return it's index. If it doesn't exist, return -1.

Examples:

s = "leetcode"
return 0.

s = "loveleetcode",
return 2.
Note: You may assume the string contain only lowercase letters.
     */
    public class Q387_FirstUniqueCharInString : IProblem
    {
        public int FirstUniqChar(string s)
        {
            var map = new Dictionary<char, int>();
            foreach (var c in s)
            {
                if (!map.ContainsKey(c))
                {
                    map.Add(c, 0);
                }
                map[c]++;
            }
            for (var i = 0; i < s.Length; i++)
            {
                if (map[s[i]] == 1)
                {
                    return i;
                }
            }
            return -1;
        }
        public void Run()
        {
            var s = "loveleetcode";
            var index = FirstUniqChar(s);
            Console.WriteLine($"first unique index is {index}");
        }
    }
}
