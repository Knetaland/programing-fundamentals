using System;
using System.Collections.Generic;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given an unsorted integer array, find the smallest missing positive integer.

    Example 1:

    Input: [1,2,0]
    Output: 3
    Example 2:

    Input: [3,4,-1,1]
    Output: 2
    Example 3:

    Input: [7,8,9,11,12]
    Output: 1
    */
    public class Q41_FirstMissingPositive : IProblem
    {
        public int FirstMissingPositive(int[] nums)
        {
            if (nums == null || nums.Length == 0)
            {
                return 1;
            }
            // set the max, currently the first element
            var max = nums[0];
            var dic = new Dictionary<int, bool>();
            foreach (var num in nums)
            {
                if (num > 0) // only care about positive nums
                {
                    if (!dic.ContainsKey(num))
                    {
                        dic.Add(num, true);
                        max = Math.Max(max, num);
                    }
                }
            }
            // now loop from 1 to max, see which num is missing
            for (var i = 0; i <= max; i++)
            {
                if (!dic.ContainsKey(i))
                {
                    return i;
                }
            }

            return max + 1;
        }

        public int FirstMissingPositive_opt(int[] nums)
        {
            var i = 0;
            while (i < nums.Length)
            {
                // do not swap if
                // 1. nums[i] is in right position
                // 2. nums[i] < 1
                // 3. nums[i] > nums.Length
                if (i == nums[i] - 1 || nums[i] < 1 || nums[i] > nums.Length)
                {
                    i++;
                }
                else if (nums[nums[i] - 1] != nums[i])
                {
                    Lib.Swap(nums, i, nums[i] - 1);
                }
                else
                {
                    i++;
                }
            }
            i = 0;
            for (; i < nums.Length; i++)
            {
                if (nums[i] != i + 1)
                {
                    return i + 1;
                }
            }
            return nums.Length + 1;
        }

        public void Run()
        {
            throw new System.NotImplementedException();
        }
    }
}
