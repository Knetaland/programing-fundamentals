using System;
using Fundamental.Core.Interfaces;
/*
Given an input string, reverse the string word by word.

For example,
Given s = "the sky is blue",
return "blue is sky the".
 */
namespace LeetCode
{
    public class Q151_ReverseWordsInAString : IProblem
    {
        public string ReverseWords(string s)
        {
            if (string.IsNullOrWhiteSpace(s))
            {
                return string.Empty;
            }

            var sSplit = s.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            int start = 0, end = sSplit.Length - 1;
            while (start < end)
            {
                var temp = sSplit[start];
                sSplit[start++] = sSplit[end];
                sSplit[end--] = temp;
            }
            return string.Join(" ", sSplit);
        }
        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
