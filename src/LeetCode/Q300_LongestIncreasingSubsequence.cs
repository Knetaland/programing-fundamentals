using System;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q300_LongestIncreasingSubsequence : IProblem
    {
        public int LengthOfLIS(int[] nums)
        {
            if (nums == null || nums.Length == 0)
            {
                return 0;
            }
            // even with 1 char, longest is 1
            var max = 1;
            var dp = new int[nums.Length];
            // init with LIS to 1 
            for (var i = 0; i < dp.Length; i++)
            {
                dp[i] = 1;
            }
            for (var i = 1; i < nums.Length; i++)
            {
                for (var j = 0; j < i; j++)
                {
                    if (nums[j] < nums[i])
                    {
                        dp[i] = Math.Max(dp[i], dp[j] + 1);
                        max = Math.Max(dp[i], max);
                    }
                }
            }
            return max;
        }

        // http://www.geeksforgeeks.org/longest-monotonically-increasing-subsequence-size-n-log-n/
        /*
        Our strategy determined by the following conditions,

        1. If A[i] is smallest among all end 
        candidates of active lists, we will start 
        new active list of length 1.
        2. If A[i] is largest among all end candidates of 
        active lists, we will clone the largest active 
        list, and extend it by A[i].

        3. If A[i] is in between, we will find a list with 
        largest end element that is smaller than A[i]. 
        Clone and extend this list by A[i]. We will discard all
        other lists of same length as that of this modified list.
        */
        public int LISWithBinarySearch(int[] nums)
        {
            if (nums == null || nums.Length == 0)
            {
                return 0;
            }

            var tailTable = new int[nums.Length];
            var len = 1;
            tailTable[0] = nums[0];
            for (var i = 1; i < nums.Length; i++)
            {
                if (nums[i] < tailTable[0])
                {
                    tailTable[0] = nums[i];
                }
                else if (nums[i] > tailTable[len - 1])
                {
                    tailTable[len++] = nums[i];
                }
                // in middle, replace the ceiling
                else
                {
                    tailTable[FindCeiling(tailTable, 0, len - 1, nums[i])] = nums[i];
                }
            }
            return len;
        }

        private int FindCeiling(int[] nums, int left, int right, int target)
        {
            while (left < right)
            {
                var mid = (left + right) / 2;
                if (nums[mid] >= target)
                {
                    right = mid;
                }
                else
                {
                    left = mid + 1;
                }
            }
            return right;
        }

        public void Run()
        {
            var nums = new[] { 4, 10, 4, 3, 8, 9 };
            var lis = LISWithBinarySearch(nums);
            Console.WriteLine("LIS is " + lis);
        }
    }
}
