using System;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    public class Q206_ReverseLinkedList : IProblem
    {
        public ListNode Reverse(ListNode head)
        {
            if (head == null)
            {
                return null;
            }

            ListNode prev = null;
            while (head != null)
            {
                var next = head.next;
                head.next = prev;
                prev = head;
                head = next;
            }
            return prev;
        }

        public ListNode Reverse2(ListNode head)
        {
            return ReverseRecursive(head, null);
        }

        public void PrintReverse(ListNode head)
        {
            if (head == null)
            {
                return;
            }

            PrintReverse(head.next);
            Console.Write($"{head.val} ");
        }

        private ListNode ReverseRecursive(ListNode head, ListNode prev)
        {
            if (head == null)
            {
                return prev;
            }

            var next = head.next;
            head.next = prev;
            return ReverseRecursive(next, head);
        }

        public void Run()
        {
            var x = new int[] { 1, 2, 3, 4, 5 };
            var node = Lib.ToListNode(x);
            Lib.PrintList(node);
            var reverse = Reverse2(node);
            Console.WriteLine("Reversed:");
            Lib.PrintList(reverse);
            Console.WriteLine("Printing reverse of the reverse");
            PrintReverse(reverse);
            Console.WriteLine("");
            Console.WriteLine("reverse again iterative");
            var reverseIterative = Reverse(reverse);
            Lib.PrintList(reverseIterative);
        }
    }
}
