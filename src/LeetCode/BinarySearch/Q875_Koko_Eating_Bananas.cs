using System;
using Fundamental.Core.Interfaces;
using System.Linq;

namespace LeetCode.BinarySearch
{
    public class Q875_Koko_Eating_Bananas : IProblem
    {
        /*
        Koko loves to eat bananas. There are n piles of bananas, the ith pile has piles[i] bananas. The guards have gone and will come back in h hours.

        Koko can decide her bananas-per-hour eating speed of k. Each hour, she chooses some pile of bananas and eats k bananas from that pile. If the pile has less than k bananas, she eats all of them instead and will not eat any more bananas during this hour.

        Koko likes to eat slowly but still wants to finish eating all the bananas before the guards return.

        Return the minimum integer k such that she can eat all the bananas within h hours.

        

        Example 1:

        Input: piles = [3,6,7,11], h = 8
        Output: 4
        Example 2:

        Input: piles = [30,11,23,4,20], h = 5
        Output: 30
        Example 3:

        Input: piles = [30,11,23,4,20], h = 6
        Output: 23
        

        Constraints:

        1 <= piles.length <= 104
        piles.length <= h <= 109
        1 <= piles[i] <= 109
        */
        public int MinEatingSpeed(int[] piles, int h) {
            var speed  = 1;
            while(true) {
                var turns = 0;
                foreach(var bananas in piles) {
                    turns += (int)Math.Ceiling((decimal)bananas/speed);
                    // Console.WriteLine($"turn {turns}");
                    if (turns > h) break;
                }
                if (turns <= h) {
                    return speed;
                } else {
                    speed++;
                }
            }
        }

        public int MinEatingSpeed_Binary_Search(int[] piles, int h) {
            var max = piles.Max();
            var low = 1;
            var high = max;
            while(low < high) {
                var speed = (low + high) /2;
                var turns = 0;
                foreach(var pile in piles) {
                    turns += (int)Math.Ceiling((decimal)pile/speed);
                    if (turns > h) break;
                }
                if (turns <= h) {
                    high = speed;
                } else {
                    low = speed+1;
                }
            }
            return high;
        }

        public void Run() {
            var piles = new [] {3,6,7,11};
            var h = 8;
            var speed = MinEatingSpeed(piles, h);
            Console.WriteLine($"Min speed is {speed}");
            var speedBianry = MinEatingSpeed_Binary_Search(piles, h);
            Console.WriteLine($"Min speed Binary Search is {speedBianry}");
        }
    }
}
