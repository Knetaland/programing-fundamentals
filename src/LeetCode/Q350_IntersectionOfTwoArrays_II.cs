using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given two arrays, write a function to compute their intersection.

    Example 1:

    Input: nums1 = [1,2,2,1], nums2 = [2,2]
    Output: [2,2]
    Example 2:

    Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
    Output: [4,9]
    Note:

    Each element in the result should appear as many times as it shows in both arrays.
    The result can be in any order.
    Follow up:

    What if the given array is already sorted? How would you optimize your algorithm?
    What if nums1's size is small compared to nums2's size? Which algorithm is better?
    What if elements of nums2 are stored on disk, and the memory is limited such that you cannot load all elements into the memory at once?
    */
    public class Q350_IntersectionOfTwoArrays_II : IProblem
    {
        public int[] Intersect(int[] nums1, int[] nums2)
        {
            var map = new Dictionary<int, int>();
            var res = new List<int>();
            foreach (var num in nums1)
            {
                if (!map.ContainsKey(num))
                {
                    map.Add(num, 0);
                }
                map[num]++;
            }
            foreach (var num in nums2)
            {
                if (map.ContainsKey(num))
                {
                    res.Add(num);
                    map[num]--;
                    if (map[num] == 0) map.Remove(num);
                }
            }
            return res.ToArray();
        }

        // follow up, if array is already sorted. Can use 2 pointers. if nums1[i] == nums2[j], increment both i and j,
        // otherwise, increment the one with smaller value. 
        public int[] IntersectForSorted(int[] nums1, int[] nums2)
        {
            var res = new List<int>();
            Array.Sort(nums1);
            Array.Sort(nums2);
            int i = 0, j = 0;
            while (i < nums1.Length && j < nums2.Length)
            {
                if (nums1[i] == nums2[j])
                {
                    res.Add(nums1[i]);
                    i++;
                    j++;
                }
                else if (nums1[i] < nums2[j])
                {
                    i++;
                }
                else
                {
                    j++;
                }
            }
            return res.ToArray();
        }

        // follow up, if one list is much shorter than the other one. Use binary search
        // we want nums2 length to be much shorter 
        public int[] IntersectUsingBS(int[] nums1, int[] nums2)
        {
            if (nums2.Length > nums1.Length)
            {
                return IntersectUsingBS(nums2, nums1);
            }

            var res = new List<int>();
            Array.Sort(nums1);
            Array.Sort(nums2);
            var lowerBound = 0;
            for (var i = 0; i < nums2.Length; i++)
            {
                var index = FindLower(nums1, lowerBound, nums2[i]);
                if (index < nums1.Length && nums1[index] == nums2[i])
                {
                    res.Add(nums2[i]);
                    lowerBound = index + 1; // look from found index forward to avoid dup, and improve efficiency
                }
            }
            return res.ToArray();
        }

        public void Run()
        {
            // var nums1 = new int [(int)1e8];
            // var nums2 = new [] {(int)1e8-2, (int)1e8-1};
            var nums1 = new[] { 4, 7, 9, 7, 6, 7 };
            var nums2 = new[] { 5, 0, 0, 6, 1, 6, 2, 2, 4 };

            // int[] test = [-1, 1, 2, 2, 2, 2, 2, 2, 2];
            // var lower = test.FindLastIndexLessOrEqualsToTarget(0);
            // Console.WriteLine($"higher bound is {test[lower]}");
            // for(var i = 0; i < nums1.Length; i++) {
            //     nums1[i] = i+1;
            // }
            using (Lib.UseTimer())
            {
                var res = IntersectUsingBS(nums1, nums2);
                Console.WriteLine("res is " + $"{string.Join(",", res)}");
            }
            // using(Lib.UseTimer()) {
            //     var res = IntersectForSorted(nums1, nums2);
            //     Console.WriteLine("res is " + $"{string.Join(",", res)}");
            // }
            // var nums = new [] {0,1,1,1,1,2};

            // var t = 1;
            // var res = FindLowerBound(nums,t);
            // Console.WriteLine("lowerbound is " +nums[res] + " at index " + res);      
            // var res2 = FindUpperBound(nums,t);
            // Console.WriteLine("upperbound is " +nums[res2] + " at index " + res2);      
        }

        private int FindLower(int[] nums, int lo, int target)
        {
            int l = lo, r = nums.Length;
            while (l < r)
            {
                var mid = l + (r - l) / 2;
                if (nums[mid] < target)
                {
                    l = mid + 1;
                }
                else
                {
                    r = mid;
                }
            }
            Console.WriteLine($"l is same as r: {l == r}");
            return r;
        }


    }
}
