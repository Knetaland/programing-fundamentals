using System;
using System.Collections.Generic;
using System.Text;
using Fundamental.Core.Interfaces;

namespace LeetCode.DP
{
    /*Given two integer arrays A and B, return the maximum length of an subarray that appears in both arrays.

    Example 1:

    Input:
    A: [1,2,3,2,1]
    B: [3,2,1,4,7]
    Output: 3
    Explanation: 
    The repeated subarray with maximum length is [3, 2, 1].
 

    Note:

    1 <= len(A), len(B) <= 1000
    0 <= A[i], B[i] < 100
     */
    public class Q718_MaximumLengthOfRepeatedSubArray : IProblem
    {
        public int FindLength(int[] A, int[] B)
        {
            var m = A.Length;
            var n = B.Length;
            var dp = new int[m, n];
            var max = 0;
            for (var i = 0; i < m; i++)
            {
                for (var j = 0; j < n; j++)
                {
                    if (A[i] == B[j])
                    {
                        dp[i, j] = dp[i - 1, j - 1] + 1;
                        max = Math.Max(dp[i, j], max);
                    }
                }
            }
            return max;
        }

        public List<string> FindLongestSubArray(string[] s, string[] t)
        {
            var res = new List<string>();
            if (s.Length < t.Length)
            {
                var temp = s;
                s = t;
                t = temp;
            }
            var longest = int.MinValue;
            var index = -1;
            for (var i = 0; i < s.Length; i++)
            {
                for (var j = 0; j < t.Length; j++)
                {
                    var curMax = 0;
                    while (i + j < s.Length && j < t.Length && t[j] == s[i + j])
                    {
                        curMax++;
                        j++;
                    }
                    if (curMax > longest)
                    {
                        longest = curMax;
                        index = j - 1;
                    }
                }
            }
            while (longest > 0)
            {
                res.Add(t[index]);
                index--;
                longest--;
            }
            res.Reverse();
            return res;
        }
        public void Run()
        {
            string[] user0 = { "/start", "/pink", "/register", "/orange", "/red", "a" };
            string[] user1 = { "/start", "/green", "/blue", "/pink", "/register", "/orange", "/one/two" };
            string[] user2 = { "a", "/one", "/two" };
            string[] user3 = { "/red", "/orange", "/yellow", "/green", "/blue", "/purple", "/white", "/amber", "/HotRodPink", "/BritishRacingGreen" };
            string[] user4 = { "/red", "/orange", "/amber", "/random", "/green", "/blue", "/purple", "/white", "/lavender", "/HotRodPink", "/BritishRacingGreen" };
            string[] user5 = { "a" };
            var res = FindLongestSubArray(user3, user4);
            Console.WriteLine(string.Join(",", res));
        }
    }
}
