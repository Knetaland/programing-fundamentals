using System;
using Fundamental.Core.Interfaces;

namespace LeetCode.DP
{
    public class Q221_MaximalSquare : IProblem
    {
        public int MaximalSquare(char[][] matrix)
        {
            int rows = matrix.Length, cols = rows > 0 ? matrix[0].Length : 0;
            var maxsqlen = 0;
            for (var i = 0; i < rows; i++)
            {
                for (var j = 0; j < cols; j++)
                {
                    if (matrix[i][j] == '1')
                    {
                        Console.WriteLine($"!!!!!found 1 at index ({i}, {j})");
                        var sqlen = 1;
                        var flag = true;
                        while (sqlen + i < rows && sqlen + j < cols && flag)
                        {
                            for (var k = j; k <= sqlen + j; k++)
                            {
                                Console.WriteLine($"Checking index ({i + sqlen}, {k})");
                                if (matrix[i + sqlen][k] == '0')
                                {
                                    Console.WriteLine($"detect 0 at index ({i + sqlen}, {k})... break");
                                    flag = false;
                                    break;
                                }
                            }
                            if (flag)
                            {
                                for (var k = i; k <= sqlen + i; k++)
                                {
                                    Console.WriteLine($"Loop 2 Checking index ({k}, {j + sqlen})");
                                    if (matrix[k][j + sqlen] == '0')
                                    {
                                        Console.WriteLine($"Loop 2 detect 0 at ({k}, {j + sqlen})...break");
                                        flag = false;
                                        break;
                                    }
                                }
                                if (flag)
                                {
                                    sqlen++;
                                    Console.WriteLine($"length increased to {sqlen})");
                                }
                            }
                        }
                        if (maxsqlen < sqlen)
                        {
                            Console.WriteLine($"NEW NEW NEW max square detected with length {sqlen}");
                            maxsqlen = sqlen;
                        }
                    }
                }
            }
            return maxsqlen * maxsqlen;
        }

        public int MaximalSquareDP(char[][] matrix)
        {
            var m = matrix.Length;
            if (m == 0)
            {
                return 0;
            }

            var n = matrix[0].Length;
            var dp = new int[m, n];
            var max = int.MinValue;
            for (var i = 0; i < m; i++)
            {
                dp[i, 0] = matrix[i][0] - '0';
                if (dp[i, 0] > max)
                {
                    max = dp[i, 0];
                }
            }
            for (var j = 0; j < n; j++)
            {
                dp[0, j] = matrix[0][j] - '0';
                if (dp[0, j] > max)
                {
                    max = dp[0, j];
                }
            }

            for (var i = 1; i < m; i++)
            {
                for (var j = 1; j < n; j++)
                {
                    if (matrix[i][j] == '1')
                    {
                        dp[i, j] = Math.Min(dp[i - 1, j - 1], Math.Min(dp[i, j - 1], dp[i - 1, j])) + 1;
                        if (dp[i, j] > max)
                        {
                            max = dp[i, j];
                        }
                    }
                }
            }
            return max == int.MinValue ? 0 : max * max;
        }

        public void Run()
        {
            var input = new[] {
                new [] {'1','0','1','0','0'},
                new [] {'1','0','1','1','1'},
                new [] {'1','1','1','1','1'},
                new [] {'1','0','0','1','0'},
            };
            var res = MaximalSquareDP(input);
            Console.WriteLine("res is " + res);
        }
    }
}
