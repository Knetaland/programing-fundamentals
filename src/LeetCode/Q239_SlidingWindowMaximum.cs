using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given an array nums, there is a sliding window of size k which is moving from the very left of the array to the very right. You can only see the k numbers in the window. Each time the sliding window moves right by one position. Return the max sliding window.

    Example:

    Input: nums = [1,3,-1,-3,5,3,6,7], and k = 3
    Output: [3,3,5,5,6,7] 
    Explanation: 

    Window position                Max
    ---------------               -----
    [1  3  -1] -3  5  3  6  7       3
    1 [3  -1  -3] 5  3  6  7       3
    1  3 [-1  -3  5] 3  6  7       5
    1  3  -1 [-3  5  3] 6  7       5
    1  3  -1  -3 [5  3  6] 7       6
    1  3  -1  -3  5 [3  6  7]      7
    Note: 
    You may assume k is always valid, 1 ≤ k ≤ input array's size for non-empty array.

    Follow up:
    Could you solve it in linear time?
    */
    public class Q239_SlidingWindowMaximum : IProblem
    {
        public int[] Slide(int[] nums, int k)
        {
            if (nums == null || nums.Length == 0)
            {
                return new int[0];
            }

            var res = new int[nums.Length - k + 1];
            var list = new LinkedList<int>();
            for (var i = 0; i < nums.Length; i++)
            {
                // if (list.Count > 0 && list.First.Value == i - k)
                // {
                //     list.RemoveFirst();
                // }

                // while (list.Count > 0 && nums[list.Last.Value] < nums[i])
                // {
                //     list.RemoveLast();
                // }

                // list.AddLast(i);
                // if (i + 1 >= k) // e.g. index at 2, but windows includes 3 numbers
                // {
                //     res[i + 1 - k] = nums[list.First.Value];
                // }
                var startWindowIndex = i - k + 1;
                while(list.Count > 0 && i - list.First.Value >= k) list.RemoveFirst(); // make sure size is k-1
                while(list.Count > 0 && nums[i] >= nums[list.Last.Value]) list.RemoveLast();
                list.AddLast(i);
                if (startWindowIndex >=0) {
                    res[startWindowIndex] = nums[list.First.Value];
                }
            }
            return res;
        }

        public void Run()
        {
            var nums = new[] { 1,3,-1,-3,5,3,6,7 };
            var k = 3;
            var s = Slide(nums, k);
            Console.WriteLine(string.Join(",", s));
        }
    }
}
