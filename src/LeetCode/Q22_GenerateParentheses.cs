using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    /*
    Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.

    For example, given n = 3, a solution set is:

    [
    "((()))",
    "(()())",
    "(())()",
    "()(())",
    "()()()"
    ]
    */
    public class Q22_GenerateParentheses : IProblem
    {
        public IList<string> GenerateParenthesis(int n)
        {
            var list = new List<string>();
            Generate("", list, n, n);
            return list;
        }

        // can insert '(' when left > 0
        // can insert ')' when left < right (count remained )
        /* left and right represents the remaining number of ( and ) that need to be added. 
            When left > right, there are more ")" placed than "(". Such cases are wrong and the method stops. 
            */
        private void Generate(string cur, List<string> list, int left, int right)
        {
            if (left > right)
            {
                return;
            }

            if (left == 0 && right == 0)
            {
                list.Add(cur);
                return;
            }
            if (left > 0)
            {
                Generate(cur + "(", list, left - 1, right);
            }

            if (right > 0)
            {
                Generate(cur + ")", list, left, right - 1);
            }
        }

        public void Run()
        {
            var n = 3;
            Console.WriteLine(string.Join(",", GenerateParenthesis(n)));
        }
    }
}
