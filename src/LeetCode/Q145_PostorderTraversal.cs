using System;
using System.Collections.Generic;
using Fundamental.Core.Interfaces;
using Fundamental.Core.Models;

namespace LeetCode
{
    public class Q145_PostorderTraversal : IProblem
    {
        public IList<int> PostorderTraversal(TreeNode root)
        {
            var res = new List<int>();
            if (root == null)
            {
                return res;
            }

            var resStack = new Stack<TreeNode>();
            var tempStack = new Stack<TreeNode>();
            tempStack.Push(root);
            while (tempStack.Count > 0)
            {
                var node = tempStack.Pop();
                resStack.Push(node);
                if (node.left != null)
                {
                    tempStack.Push(node.left);
                }

                if (node.right != null)
                {
                    tempStack.Push(node.right);
                }
            }

            while (resStack.Count > 0)
            {
                res.Add(resStack.Pop().val);
            }
            return res;
        }

        public IList<int> PostorderTraversalRecursive(TreeNode root)
        {
            var res = new List<int>();
            Recurse(root, res);
            return res;
        }

        private void Recurse(TreeNode root, IList<int> list)
        {
            if (root == null)
            {
                return;
            }

            Recurse(root.left, list);
            Recurse(root.right, list);
            list.Add(root.val);
        }

        // Method 2
        /*后续的顺序是左-右-根，所以当一个节点值被取出来时，它的左右子节点要么不存在，要么已经被访问过了。具体思路可参见神网友 Yu's Coding Garden 的博客 */
        /*
            while the stack is not empty, do:
            if 
                the top node is a leaf node (no left&right children), pop it.
                or if the top node has been visited, pop it. (here we use a sign head to show the latest popped node, if the top node's child = the latest popped node, either left or right, it must has been visited.)
            else
                b. push the right child of the top node (if exists)
                c. push the left child of the top node (if exists)
        */
        public IList<int> PostorderTraversal2(TreeNode root)
        {
            var res = new List<int>();
            if (root == null)
            {
                return res;
            }

            var s = new Stack<TreeNode>();
            s.Push(root);
            var head = root;
            while (s.Count > 0)
            {
                var top = s.Peek();
                if ((top.left == null && top.right == null) || top.left == head || top.right == head)
                {
                    var pop = s.Pop();
                    res.Add(pop.val);
                    head = pop;
                }
                else
                {
                    if (top.right != null)
                    {
                        s.Push(top.right);
                    }

                    if (top.left != null)
                    {
                        s.Push(top.left);
                    }
                }
            }
            return res;
        }
        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
