using System;
using System.Collections.Generic;
using System.Linq;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q381_InsertDeleteGetRandom_DuplicatesAllowed : IProblem
    {
        public void Run()
        {
            throw new NotImplementedException();
        }
    }
    public class RandomizedCollection
    {
        private readonly Dictionary<int, HashSet<int>> _map;
        private readonly List<int> _list;
        private readonly Random _r;
        /** Initialize your data structure here. */
        public RandomizedCollection()
        {
            _map = new Dictionary<int, HashSet<int>>();
            _list = new List<int>();
            _r = new Random();
        }

        /** Inserts a value to the collection. Returns true if the collection did not already contain the specified element. */
        public bool Insert(int val)
        {
            if (!_map.ContainsKey(val))
            {
                _map.Add(val, new HashSet<int>());
            }
            _map[val].Add(_list.Count);
            _list.Add(val);
            return _map[val].Count == 1;  // if 1, means newly inserted, previously empty
        }

        /** Removes a value from the collection. Returns true if the collection contained the specified element. */
        public bool Remove(int val)
        {
            if (!_map.ContainsKey(val) || _map[val].Count == 0)
            {
                return false;
            }

            var pos = _map[val].First();
            var lastPos = _list.Count - 1;
            _map[val].Remove(pos);
            if (pos != lastPos)
            {
                _list[pos] = _list[lastPos];
                _map[_list[lastPos]].Remove(lastPos);
                _map[_list[lastPos]].Add(pos);
            }
            _list.RemoveAt(lastPos);
            return true;
        }

        /** Get a random element from the collection. */
        public int GetRandom()
        {
            return _list[_r.Next(_list.Count)];
        }
    }
}
