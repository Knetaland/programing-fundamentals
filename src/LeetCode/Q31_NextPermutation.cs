using System;
using Fundamental.Core.Helpers;
using Fundamental.Core.Interfaces;

namespace LeetCode
{
    public class Q31_NextPermutation : IProblem
    {
        // check out http://bangbingsyb.blogspot.com/2014/11/leetcode-next-permutation.html
        public void NextPermutation(int[] nums)
        {
            if (nums == null || nums.Length < 2)
            {
                return;
            }

            var i = nums.Length - 1;
            // going from right to left, find the first num where value drops, means that's where we are going to choose the next number bigger than it
            while (i - 1 >= 0 && nums[i - 1] >= nums[i])
            {
                i--;
            }

            var target = i - 1;
            if (target < 0)
            {
                // Lib.Reverse(nums, 0, nums.Length - 1);
                Array.Reverse(nums);
                return;
            }
            // from right again, find first num > than target value, so it's the next closest bigger number
            for (var j = nums.Length - 1; j > target; j--)
            {
                if (nums[j] > nums[target])
                {
                    Lib.Swap(nums, j, target);
                    break;
                }
            }
            var index = target + 1;
            Array.Reverse(nums, index, nums.Length - index);
        }

        public void Run()
        {
            var nums = new[] { 1, 2, 3 };
            Console.WriteLine($"{string.Join(",", nums)} next permutation is ");
            NextPermutation(nums);
            Console.WriteLine($"{string.Join(",", nums)}");
        }
    }
}
