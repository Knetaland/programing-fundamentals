import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) throws Exception {
        List<Problem> problems = List.of(
            // new Q505_Mazz_II(),
            new Q480_Sliding_Window_Median()
        );
          
        // problems.add(new Q352_SummaryRange());

        for(var p : problems) {
            p.run();
        }
    }
}
