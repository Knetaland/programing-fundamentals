import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

public class Q218_Skyline implements Problem {
    public List<List<Integer>> getSkyline(int[][] buildings) {
        var heights = new ArrayList<int[]>();
        List<List<Integer>> res = new ArrayList<>();

        for (var b : buildings) {
            heights.add(new int[] { b[0], -b[2] }); // negative so when sorting, it will be first. so - is for up, + for
            heights.add(new int[] { b[1], b[2] });  // down
            
        }
        Collections.sort(heights, (a, b) -> a[0] == b[0] ? a[1] - b[1] : a[0] - b[0]);

        PriorityQueue<Integer> queue = new PriorityQueue<>((a, b) -> b - a);

        var preMax = 0;
        queue.offer(0);

        for (var height : heights) {
            if (height[1] < 0) { // flying
                queue.offer(-height[1]);
            } else {
                queue.remove(height[1]); // house ending, remove
            }

            var curMax = queue.peek();
            if (curMax != preMax) {
                res.add(List.of(height[0], curMax));
                preMax = curMax;
            }
        }
        return res;
    }

    @Override
    public void run() {
        

    }
}
