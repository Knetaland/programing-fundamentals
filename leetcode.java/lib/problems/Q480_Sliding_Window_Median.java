import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.TreeSet;

public class Q480_Sliding_Window_Median implements Problem{

    @Override
    public void run() {
        int[] nums =  {1,3,-1,-3,5,3,6,7};
        int k = 3;
        var res = medianSlidingWindow2(nums, k);
        for(var num : res) {
            System.out.println(num);    
        }
    }

    public double[] medianSlidingWindow(int[] nums, int k) {
        var res = new double[nums.length - k + 1];
        var solution = new MedianRetriever(k);
        for(var i = 0; i < k; i++) {
            solution.add(nums[i]);
        }
        res[0] = solution.getMedian();
        for(var i = k; i < nums.length; i++) {
            solution.remove(nums[i-k]);
            solution.add(nums[i]);
            res[i-k+1] = solution.getMedian();
        }
        return res;
    }

    // TC: O(Nlogk), SC: O(k)
    public double[] medianSlidingWindow2(int[] nums, int k) {
        double[] res = new double[nums.length - k + 1];
        int index = 0;
        Comparator<Integer> comparator = (a, b) -> (nums[a] != nums[b] ? Integer.compare(nums[a], nums[b]) : a - b);
        TreeSet<Integer> lo = new TreeSet<>(comparator.reversed()); // max on top
        TreeSet<Integer> hi = new TreeSet<>(comparator); // min on top
            
        for (int i = 0; i < nums.length; i++) {
                lo.add(i);
                hi.add(lo.pollFirst());
                // balance
                if(hi.size()>lo.size()) {
                    lo.add(hi.pollFirst());
                }
                // window size met, calculate median
                if (lo.size() + hi.size() == k) {
                    var median = lo.size()==hi.size()? ((double) nums[lo.first()] + nums[hi.first()])/2.0 : nums[lo.first()];
                    res[index]= median;
                    // after getting the median, remove left
                    if (!lo.remove(index)) {
                        hi.remove(index);
                    }
                    index++;
                }
            }
            return res;
    }


    public class MedianRetriever {
        private final int k;
        private final PriorityQueue<Integer> _small;
        private final PriorityQueue<Integer> _large;
        private HashMap<Integer, Integer> _delay;
        private int _smallSize, _largeSize;
    
        public MedianRetriever(int k) {
            this.k = k;
            _large = new PriorityQueue<Integer>((a,b) -> a.compareTo(b));
            _small = new PriorityQueue<Integer>((a,b) -> b.compareTo(a));
            _delay = new HashMap<Integer, Integer>();
        }

        public double getMedian() {
            if (k % 2 == 0) return ((double)_small.peek() + _large.peek()) / 2;
            return _small.peek();
        }

        public void add(int num) {
            if (_small.isEmpty() || num <= _small.peek()) {
                _small.add(num);
                _smallSize++;
            } else {
                _large.add(num);
                _largeSize++;
            }
            balance();
        }

        public void remove(int num) {
            _delay.put(num, _delay.getOrDefault(num,0) + 1);
            if (num <= _small.peek()) {
                _smallSize--;
                prune(_small);
            } else {
                _largeSize--;
                prune(_large);
            }
            balance();
        }

        private void prune(PriorityQueue<Integer> queue) {
            while(!queue.isEmpty() && _delay.containsKey(queue.peek())) {
                var num = queue.peek();
                _delay.put(num, _delay.get(num) -1);
                if (_delay.get(num) == 0) {
                    _delay.remove(num);
                }
                queue.poll();
            }
        }

        private void balance() {
            if (_smallSize > _largeSize + 1) {
                 _large.add(_small.poll());
                 _largeSize++;
                 _smallSize--;
            } else if (_largeSize > _smallSize) {
                _small.add(_large.poll());
                _largeSize--;
                _smallSize++;
            }
        }
    }

}

