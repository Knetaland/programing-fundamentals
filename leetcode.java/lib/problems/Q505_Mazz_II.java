import java.util.Arrays;
import java.util.PriorityQueue;

/*
    There is a ball in a maze with empty spaces and walls. 
    The ball can go through empty spaces by rolling up, down, left or right, but it won't stop rolling until hitting a wall.
    When the ball stops, it could choose the next direction.

    Given the ball's start position, the destination and the maze, find the shortest distance for 
    the ball to stop at the destination. The distance is defined by the number of empty spaces traveled by the ball
    from the start position (excluded) to the destination (included). If the ball cannot stop at the destination, return -1.

    The maze is represented by a binary 2D array. 1 means the wall and 0 means the empty space. 
    You may assume that the borders of the maze are all walls. The start and destination coordinates are represented by row and 
    column indexes.

    // ask to find find the shortest distance for the ball to stop at the destination
    */
public class Q505_Mazz_II implements Problem {

    public int shortestDistance(int[][] maze, int[] start, int[] destination) {
        var dirs = new int[][] {
            {0, 1},
            {0, -1},
            {1, 0},
            {-1, 0}
        };
        var m = maze.length;
        var n = maze[0].length;
        var distance = new int[m][n];
        // default to max distance
        for(var row : distance) {
            Arrays.fill(row, Integer.MAX_VALUE);
        }

        // [x, y, dist] -> 2nd index is the distance, shortest first
        var pq = new PriorityQueue<int[]>((a,b) -> a[2] - b[2]);

        pq.offer(new int[] { start[0], start[1], 0 });
        distance [start[0]][start[1]] = 0;
        while(!pq.isEmpty()) {
            var curPosition = pq.poll();
            for(var dir : dirs) {
                var currentDist = curPosition[2];
                var x = curPosition[0];
                var y = curPosition[1];
                while(x >=0 && y >=0 && x < m && y < n  && maze[x][y] != 1) { // check if position is valid
                    x += dir[0];
                    y += dir[1];
                    currentDist++;
                }
                // since exited from invalid, get prev
                currentDist--;
                x -= dir[0];
                y -= dir[1];

                if (currentDist < distance[x][y]) {
                    System.out.println("shorter distance " + currentDist + " detected at [" + x + "," + y +"]");
                    distance[x][y] = currentDist;
                    pq.add(new int[]{x,y,currentDist});
                }
            }
        }

        var finalDistance = distance[destination[0]][destination[1]];
        return finalDistance == Integer.MAX_VALUE ? -1 : finalDistance;
    }

    @Override
    public void run() {
        int[][] maze = {
            {0, 0, 1, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 1, 0},
            {1, 1, 0, 1, 1},
            {0, 0, 0, 0, 0},
        };
        int[] source =  { 0, 4 };
        int[] destination = { 4, 4 };

        var distance = shortestDistance(maze, source, destination);
        System.out.println("min ste is " + distance);
    }

}
