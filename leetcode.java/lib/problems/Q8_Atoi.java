public class Q8_Atoi implements Problem {
    public int Atoi(String str){
        if (str == null || str.isBlank()) return 0;
        int res = 0;
        int i = 0;
        str = str.trim();
        int sign = 1;
        var c = str.charAt(i);
        if (c == '+' || c == '-') {
            sign = c == '+' ? 1 : -1;
            i++;
        } 
        for(; i<str.length(); i++ ) {
            c = str.charAt(i);
            var digit = c - '0';
            if (digit < 0 || digit > 9) break;
            if (res >  Integer.MAX_VALUE /10 || (res == Integer.MAX_VALUE && digit > Integer.MAX_VALUE %10 ) ) {
                return sign == 1 ? Integer.MAX_VALUE : Integer.MIN_VALUE;
            }
            res = res * 10 + digit;
        }

        return res;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        
    }
}
