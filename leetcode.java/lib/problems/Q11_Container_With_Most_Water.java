public class Q11_Container_With_Most_Water implements Problem{

    public int maxArea(int[] height) {
        if (height.length < 2) return 0;
        int left = 0, right = height.length -1;
        int max = 0;
        while(left < right) {
            var h = Math.min(height[left], height[right]);
            max = Math.max(max, h * (right - left));
            if (height[left] < height[right]) {
                left++;
            } else {
                right--;
            }
        }
        return max;
    }


    @Override
    public void run() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'run'");
    }

}
