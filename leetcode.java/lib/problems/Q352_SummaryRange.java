import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;

public class Q352_SummaryRange implements Problem {
    TreeSet<int[]> set = new TreeSet<>((a, b) -> a[0] == b[0] ? a[1]-b[1] : a[0]-b[0]);
    TreeMap<Integer, Integer> map = new TreeMap();

    // O(log N)
    public void addNum(int val) {
        var interval = new int[] { val, val };
        if (set.contains(interval)) return ;
        var lower = set.lower(interval);
        var higher = set.higher(interval);
        if (higher != null && higher[0] == val) return;
        // the interval merges the two 
        if (lower != null && lower[1] + 1 == val && higher != null && val + 1 == higher[0]) {
            lower[1] = higher[1];
            set.remove(higher);
        } else if (lower != null && lower[1] + 1 >= val) { // if end of lower + 1 is > val, e.g. [1, 4], 2
            lower[1] = Math.max(lower[1], val);
        } else if (higher != null && val == higher[0]-1) {  // if start of higher is val + 1
            higher[0] = val;
        } else {
            set.add(interval);
        }
    }

    public void addNum2(int val) {
        if (map.containsKey(val)) return ;
        var low = map.lowerKey(val);
        var high = map.higherKey(val);
        // falls in between 2, merge
        if (low != null && map.get(low) + 1 == val && high != null && high-1 == val) {
            map.put(low, map.get(high));
            map.remove(high);
        } else if (low != null && map.get(low) + 1 >= val) { // this will only set when val is exactly end +1, when val is already within current interval, it will use current end. 
            map.put(low, Math.max(map.get(low), val));
        } else if (high !=null && map.get(high) -1 == val) {
            map.put(val, map.get(high));
            map.remove(high);
        } else {
            map.put(val, val);
        }
    }

    // using treeset
    public int[][] getIntervals() {
        var res = new ArrayList<int[]>();
        for(var interval : set) {
            res.add(interval);
        }
        return res.toArray(new int[res.size()][]);
    }


    // using treemap
    public int[][] getIntervals2() {
        List<int[]> res = new ArrayList();
        for(var v : map.keySet()) {
            res.add(new int[] {v, map.get(v)});
        }
        return res.toArray(new int[res.size()][]);
    }


    @Override
    public void run() {
        this.addNum2(1);
        this.addNum2(3);
        this.addNum2(7);
        this.addNum2(2);
        this.addNum2(6);
        var intervals = this.getIntervals2();

        for(var v : intervals) {
            System.out.println(String.format("[%s, %s]", v[0], v[1]));
        }
        
    }
}
