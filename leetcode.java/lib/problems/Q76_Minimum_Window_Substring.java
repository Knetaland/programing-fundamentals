import java.util.HashMap;

public class Q76_Minimum_Window_Substring {
    public String minWindow(String s, String t) {
        HashMap<Character, Integer> map = new HashMap<>();
        int needed = 0;
        int index  =0;
        int minLeft = 0;
        int min = Integer.MAX_VALUE;

        for(var c : t.toCharArray()) {
            map.put(c, map.getOrDefault(c, 0) +1);
            needed++;
        }

        for(var i = 0; i < s.length(); i++) {
            var c = s.charAt(i);
            if (map.containsKey(c)) {
                map.put(c, map.get(c) - 1);
                if (map.get(c)>=0) needed--;
            }
            while (needed == 0) {
                var length = i - index + 1;
                if (length < min) {
                    min = length;
                    minLeft = index;
                }
                var left = s.charAt(index);
                if (map.containsKey(left)) {
                    map.put(left, map.get(left) + 1);
                    if (map.get(left) > 0) needed++;
                }
                index++;
            }
        }

        return min == Integer.MAX_VALUE ? "" : s.substring(minLeft, minLeft + min);
    }

}
