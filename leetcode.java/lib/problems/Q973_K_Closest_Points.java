import java.util.PriorityQueue;

public class Q973_K_Closest_Points {
    public int[][] kClosest(int[][] points, int k) {
        var pq = new PriorityQueue<int[]>((a,b) -> b[0]*b[0]+b[1]*b[1] - (a[0]*a[0]+a[1]*a[1]));
        for (int[] point : points) {
            pq.offer(point);
            if (pq.size() > k) {
                pq.poll();
            }
        }
        var res = new int[pq.size()][];
        var i = 0;
        while(!pq.isEmpty()) {
            res[i]=pq.poll();
            i++;
        }
        return res;
    }
}
