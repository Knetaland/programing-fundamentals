public class Q125_Valid_Palindrome {
    public boolean isPalindrome(String s) {
        if (s == null || s.isEmpty()) return true;
        s = s.trim().toLowerCase();
        int left = 0, right = s.length() -1;
        while(left < right) {
            while (left < right && !Character.isLetterOrDigit(s.charAt(left))) left++;
            while (right > left && !Character.isLetterOrDigit(s.charAt(right))) right--;
            if (s.charAt(left) != s.charAt(right)) return false;
            left++;
            right--;
        }
        return true;
    }
}
