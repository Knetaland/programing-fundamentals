import java.util.PriorityQueue;

/**
 * https://leetcode.com/problems/kth-largest-element-in-an-array/description/
 * 
 */

public class Q215_Kth_Largest_Element implements Problem {

    // O(nLgk)
    // O(k)
    public int findKthLargest(int[] nums, int k) {
        var pq = new PriorityQueue<Integer>();
        for (var num : nums) {
            pq.offer(num);
            if(pq.size() > k) pq.poll();
        }
        return pq.peek();
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'run'");
    }
    
}
