import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class Playground {
    private final int[][] DIRS = {
        {0, 1},
        {0, -1},
        {1, 0},
        {-1, 0}
    };

    
    public void test() {
        LinkedList<Integer> queue = new LinkedList<Integer>();
        queue.getFirst();
        queue.remove()
        queue.offer(1);
        queue.poll();
        queue.peek();
        queue.size();
        queue.add(1);
        queue.addFirst(2);

        var sb = new StringBuilder();
        sb.append('1');
        sb.toString();

        Stack<Integer> stack = new Stack<>();
        stack.push(1);
        stack.pop();
        stack.isEmpty();
        stack.peek();
        Integer[] s = stack.toArray(new Integer[0]);
        var sdfd = s.length;
        Arrays.sort(s);
        var a = s[0].intValue();

        var str = "r3rw,qrwewq";
        var strChunks = str.split(",");
        List<Integer> test = new ArrayList<>();

        test.stream().filter(c -> c > 0);

        Math.min(1, 3);
        Math.abs(1- 2);

        Character.isDigit('1');
        Character.isLetterOrDigit('9');
    }

}
