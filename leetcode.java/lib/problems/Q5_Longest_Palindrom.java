public class Q5_Longest_Palindrom {
    public String longestPalindrome(String s) {
        if (s == null  || s.length() < 2) return s;

        int leftMost = 0, rightMost = 0, i = 0, n = s.length();
        while( i < n) {
            var start = i;
            while (i+1 < n && s.charAt(i+1) == s.charAt(start)) i++;
            var end = i;
            // expand
            while (start >=0 && end < n && s.charAt(start) == s.charAt(end)) {
                start--;
                end++;
            }
            if (end - start > rightMost - leftMost) {
                rightMost = end;
                leftMost = start;
            }
            i++;
        }
        return s.substring(leftMost, rightMost + 1); // end index is exclusive, need to move to next
    }
}
