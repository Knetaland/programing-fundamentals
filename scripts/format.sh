#!/bin/sh

#
# modified from sample: https://prettier.io/docs/en/precommit.html
#
# install dotnet-format: dotnet tool install -g dotnet-format
# copy to .git/hooks/pre-commit and make executable
#

# --staged, --cached
FILES=$(git diff --name-only --diff-filter=ACM "*.cs" | sed 's| |\\ |g')
[ -z "$FILES" ] && exit 0

# Format all updated .cs files, they will be pass to dotnet format as a space seperated list
echo "$FILES" | cat | xargs dotnet format -v d --include

# Add back the modified files to staging
# echo "$FILES" | xargs git add

exit 0